package com.igtb.ingestion.payment.instruction.es.models;

import com.igtb.ingestion.payment.instruction.models.AddressType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ContactAdressES.
 */
@ToString
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
public class ContactAdressES {
	
	private AddressType addressType;
	
	private String  department;
	
	private String subdepartment;
	
	/** The address 1. */
	private String address1;
	
	/** The address 2. */
	private String address2;
	
	/** The address 3. */
	private String address3;
	
	/** The postal code. */
	private String postalCode;
	
	/** The city. */
	private CityES city;
	
	/** The state. */
	private StateES state;
	
	/** The country. */
	private CountryES country;
}
