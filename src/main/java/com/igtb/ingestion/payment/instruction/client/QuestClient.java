package com.igtb.ingestion.payment.instruction.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.config.AppPropsProvider;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.QuestException;
import com.igtb.ingestion.payment.instruction.models.quest.QuestRequest;
import com.igtb.ingestion.payment.instruction.models.quest.QuestResponse;

/**
 * The Class QuestClient.
 */
@Component
public class QuestClient {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(QuestClient.class);
	
	/** The gkp feign client. */
	@Autowired
	private DigitalFeignClient digitalFeignClient;
	
	/** The digital http rest client. */
	@Autowired
	private DigitalHttpRestClient digitalHttpRestClient;
	
	/** The props. */
	@Autowired
	private AppPropsProvider props;

	/**
	 * Gets the equivalent amounts.
	 *
	 * @param payload the payload
	 * @return the equivalent amounts
	 * @throws QuestException the quest exception
	 */
	public QuestResponse getEquivalentAmounts(final QuestRequest payload) throws QuestException {
		
		LOGGER.info("getEquivalentAmounts from Quest started with requestBody [{}]", payload);
		
		try {
			
			ResponseEntity<QuestResponse> questRespEntity = null;
			QuestResponse questResp = null;
			HttpStatus statusCode = null;
			final HttpHeaders headers = new HttpHeaders();
			headers.set(props.getQuestHeaderKey(), props.getQuestHeaderValue());
			
			if(PaymentIngestionConstant.STUB.equalsIgnoreCase(props.getQuestRouterName())) {
				// get data from stub
				LOGGER.debug("building stub equivalent amounts");
				//questRespEntity = getEqAmountsStubbed(payload);
				questRespEntity = digitalHttpRestClient.execute(props.getQuestUri(), new ObjectMapper().writeValueAsString(payload), headers, HttpMethod.POST);
			}
			else {
				// Use Feign client
				LOGGER.debug("feign call started");
				questRespEntity = digitalFeignClient.getDetailsFromQuest(payload, headers);
				LOGGER.debug("feign call completed");
			}
			
			if(questRespEntity!=null) {
				statusCode = questRespEntity.getStatusCode();
				questResp = questRespEntity.getBody();
			}
			if(HttpStatus.OK.equals(statusCode)) {
				LOGGER.debug("Received Quest resposne {}",questResp);
				if (questResp != null && questResp.getData() != null) {
					questResp.setResCode(PaymentIngestionConstant.OK_CODE_200);
				}
				if (questResp != null && questResp.getData() == null) {
					questResp.setResCode(PaymentIngestionConstant.ERR_CODE_400_BAD_REQ);
					LOGGER.warn("Quest resonse: user details not found for request [{}]",payload);
				}
				LOGGER.info("Quest Response: {}",questResp);
				return questResp;
			}
			else {
				LOGGER.error("{} - {} ResponseEntity:[{}]", PaymentIngestionConstant.SYS_UNSUP_FUNC, PaymentIngestionConstant.QUEST_UNEXPECTED_EXP, questRespEntity);
				throw new QuestException(PaymentIngestionConstant.QUEST_UNEXPECTED_EXP);
			}

		} catch (Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_091 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_091_Desc);
			// Unexpected exception
			LOGGER.error("{} - {} ErrorMsg:{}, StackTrace:", PaymentIngestionConstant.SYS_UNSUP_FUNC, PaymentIngestionConstant.QUEST_UNEXPECTED_EXP, ex.getMessage(), ex);
			throw new QuestException(ex.getMessage());
		}
		
	}

	/**
	 * Gets the eq amounts stubbed.
	 *
	 * @param payload the payload
	 * @return the eq amounts stubbed
	 */
	/*private ResponseEntity<QuestResponse> getEqAmountsStubbed(final QuestRequest payload) {
		final QuestResponse questResponse = new QuestResponse();
		final List<AmountVariable> stubTargetCcyAmt = ((QuestVariableEqAmount)payload.getVariables()).getAmounts();
		stubTargetCcyAmt.stream().forEach(amt -> amt.setCurrencyCode(props.getTargetBaseCcy()));
		final QuestEqAmountResponse questEqAmountResponse = new QuestEqAmountResponse(); 
		questEqAmountResponse.setGetEquivalentAmounts(stubTargetCcyAmt);
		questResponse.setData(questEqAmountResponse);
		return new ResponseEntity<QuestResponse>(questResponse, HttpStatus.OK);
	}*/

}
