package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UserInfo.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class UserInfo {

	/** The user name. */
	private String userName;

	/** The domain name. */
	private String domainName;

	/** The role. */
	private String role;

	/** The name. */
	private String name;

	/** The image id. */
	private String imageId;
	
	/** The email. */
	private List<String> email;

	/** The phone. */
	private List<String> phone;

}
