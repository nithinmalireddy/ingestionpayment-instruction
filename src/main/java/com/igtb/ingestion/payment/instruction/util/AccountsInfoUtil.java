package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.AcctCyyES;
import com.igtb.ingestion.payment.instruction.es.models.AddressES;
import com.igtb.ingestion.payment.instruction.es.models.AgentES;
import com.igtb.ingestion.payment.instruction.es.models.BankES;
import com.igtb.ingestion.payment.instruction.es.models.BankProxyES;
import com.igtb.ingestion.payment.instruction.es.models.CityES;
import com.igtb.ingestion.payment.instruction.es.models.CommInfoES;
import com.igtb.ingestion.payment.instruction.es.models.ContactAdressES;
import com.igtb.ingestion.payment.instruction.es.models.CountryES;
import com.igtb.ingestion.payment.instruction.es.models.CreditorES;
import com.igtb.ingestion.payment.instruction.es.models.DebtorES;
import com.igtb.ingestion.payment.instruction.es.models.EntityES;
import com.igtb.ingestion.payment.instruction.es.models.OrganisationES;
import com.igtb.ingestion.payment.instruction.es.models.ParentBankES;
import com.igtb.ingestion.payment.instruction.es.models.StateES;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.models.Account;
import com.igtb.ingestion.payment.instruction.models.Agent;
import com.igtb.ingestion.payment.instruction.models.City;
import com.igtb.ingestion.payment.instruction.models.Creditor;
import com.igtb.ingestion.payment.instruction.models.Debtor;
import com.igtb.ingestion.payment.instruction.models.PostalAddress;
import com.igtb.ingestion.payment.instruction.models.State;

import lombok.AllArgsConstructor;

/**
 * Account Info Utility.
 * 
 */
@Component
@AllArgsConstructor
public class AccountsInfoUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountsInfoUtil.class);

	/** Es Fetch Data Utility. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The payment elastic properties. */
	private PaymentElasticProperties paymentElasticProperties;

	@Autowired
	private EventValidationConfig eventValidationConfig;

	/** The Constant NAME. */
	private static final String NAME = "name";

	private final ObjectMapper mapper;
	
	/**
	 * Gets the account details.
	 *
	 * @param account
	 *            the account
	 * @param typeOfAccount
	 *            the type of account
	 * @param acctID
	 *            the acct ID
	 * @param option
	 *            the option
	 * @param piType
	 *            the pi type
	 * @param schemeName
	 *            the scheme name
	 * @param bankProxyES
	 *            the bank proxy ES
	 * @return the account details
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public AccountES getAccountDetails(Account account, final String typeOfAccount, String acctID,
			final String option,  String piType, String schemeName, BankProxyES bankProxyES,
			final String serviceKey) throws IOException, DataNotFoundException {
		final AccountES accountES = new AccountES();
		LOGGER.info("getAccountDetails :: schemeName  {}" , schemeName);
		LOGGER.info("getAccountDetails :: acctID {}" , acctID);
		LOGGER.info("getAccountDetails :: serviceKey  {}" , serviceKey);
		
//		check creditoraccount alias present in contacts account or not if present set contactaccount details else set adhoch details
		if(serviceKey.contains(PaymentIngestionConstant.FULFILLRTP) && typeOfAccount.equals(PaymentIngestionConstant.CREDITOR_ACCOUNT)) {
			final JsonNode contactJsonNode = paymentIngestionQueryUtil.fetchExistingContactAccountsFromES(Optional.ofNullable(account).map(Account::getName).orElse(null));
			if (contactJsonNode != null && !contactJsonNode.equals("")) {
				acctID=contactJsonNode.get(PaymentIngestionConstant._ID).asText();
				schemeName=PaymentIngestionConstant.GUID;
			}else {
				schemeName=PaymentIngestionConstant.ADHOC;
			}
		}
		// if schemeName is GUID
		if (PaymentIngestionConstant.GUID.equalsIgnoreCase(schemeName)) {
			accountES.setId(acctID);
			// 1. Gets the accounts Json from ES.
			final JsonNode accountJsonNode = paymentIngestionQueryUtil.getAccountsDtlsFromES(acctID, typeOfAccount,option, piType, serviceKey);
			
			// 2. Set basic account details.
			if (accountJsonNode != null && accountJsonNode.isContainerNode()) {
				accountES.setAccountNo(Optional.ofNullable(CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.ACC_NO)).map(Object::toString).orElse(null));
				accountES.setName(Optional.ofNullable(CommonUtil.getValueFromJsonNode(accountJsonNode, "", NAME)).map(Object::toString).orElse(null));
				accountES.setShortAccountNo(Optional.ofNullable(CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.SHRT_ACC_NO)).map(Object::toString).orElse(null));
				accountES.setStatus(Optional.ofNullable(CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.STATUS)).map(Object::toString).orElse(null));
				accountES.setAlias(Optional.ofNullable(account).map(Account::getAlias).map(Object::toString).orElse(null));
				accountES.setAliasHandleType(Optional.ofNullable(account).map(Account::getAccountType).map(Object::toString).map(Object::toString).orElse(null));
				accountES.setLegalName(Optional.ofNullable(account).map(Account::getLegalName).map(Object::toString).orElse(null));
				String accountCreationDate=Optional.ofNullable(account).map(Account::getAccountCreationDate).map(Object::toString).orElse(null);
				if(isValidFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT3,accountCreationDate,Locale.ENGLISH)) {
					accountES.setAccountCreationDate(accountCreationDate);					
				}else {
					Date currentDate = new Date();
			        DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT3);
			        accountES.setAccountCreationDate(df.format(currentDate));
				}
				final JsonNode tagsJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.TAGS);
				final List<String> tagsList = new ArrayList<>();
				if (tagsJson != null && tagsJson.isArray()) {
					final Iterator<JsonNode> iter = Optional.ofNullable(tagsJson).map(JsonNode::elements).orElse(Collections.emptyIterator());
					tagsJson.forEach((JsonNode node) -> tagsList.add(Optional.ofNullable(iter.next()).map(JsonNode::asText).orElse(null)));
				}
				accountES.setTags(tagsList);

				// 3. Set Bank details.
				final BankES bank = new BankES();
				final JsonNode bankJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.BANK);
				bank.setId(Optional.ofNullable(bankJson).map(obj -> obj.get(PaymentIngestionConstant.ID)).map(JsonNode::asText).orElse(null));
				bank.setKey(Optional.ofNullable(bankJson).map(obj -> obj.get(PaymentIngestionConstant.KEY)).map(JsonNode::asText).orElse(null));
				bank.setName(Optional.ofNullable(bankJson).map(obj -> obj.get(NAME)).map(JsonNode::asText).orElse(null));
				accountES.setBank(bank);

				// 4. Set entity details.
				final EntityES entity = new EntityES();
				final JsonNode entityJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.ENTITY);
				entity.setId(Optional.ofNullable(entityJson).map(obj -> obj.get(PaymentIngestionConstant.ID)).map(JsonNode::asText).orElse(null));
				entity.setName(Optional.ofNullable(entityJson).map(obj -> obj.get(NAME)).map(JsonNode::asText).orElse(null));
				accountES.setEntity(entity);

				// 5. Set country details.
				final CountryES country = new CountryES();
				final JsonNode countryJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, "",PaymentIngestionConstant.COUNTRY);

				final String iso2CharCode = Optional.ofNullable(countryJson).map(tmp -> tmp.get(PaymentIngestionConstant.CODE)).map(JsonNode::asText).orElse(null);
				String iso3CharCode = null;
				if (StringUtils.isNotBlank(iso2CharCode)) {
					final JsonNode sourceCountryNode = paymentIngestionQueryUtil.jestGetESBuilder(iso2CharCode,paymentElasticProperties.getRefDataIndexName(),paymentElasticProperties.getCountriesType());
					iso3CharCode = Optional.ofNullable(sourceCountryNode).map(node -> node.get(PaymentIngestionConstant.ISO_3CHAR_CODE)).map(JsonNode::asText).orElse(null);
				}
				country.setIso2CharCode(iso2CharCode);
				country.setIso3CharCode(iso3CharCode);
				country.setName(Optional.ofNullable(countryJson).map(obj -> obj.get(NAME)).map(JsonNode::asText).orElse(null));
				country.setName(Optional.ofNullable(countryJson).map(obj -> obj.get(NAME)).map(JsonNode::asText).orElse(null));
				accountES.setCountry(country);

				// 6. Set currency details.
				final AcctCyyES acctCcy = new AcctCyyES();
				final JsonNode accCcyJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, "", PaymentIngestionConstant.ACCT_CCY);
				acctCcy.setCode(Optional.ofNullable(accCcyJson).map(obj -> obj.get(PaymentIngestionConstant.CODE)).map(JsonNode::asText).orElse(null));
				acctCcy.setName(Optional.ofNullable(accCcyJson).map(obj -> obj.get(NAME)).map(JsonNode::asText).orElse(null));
				accountES.setAcctCcy(acctCcy);

				if (accountJsonNode != null && accountJsonNode.hasNonNull(PaymentIngestionConstant.BANK) && accountJsonNode.get(PaymentIngestionConstant.BANK).hasNonNull(PaymentIngestionConstant.BANK_PROXY)) {
					final JsonNode bankProxyJson = (JsonNode) CommonUtil.getValueFromJsonNode(accountJsonNode, PaymentIngestionConstant.BANK,PaymentIngestionConstant.BANK_PROXY);
					if (bankProxyJson != null) {
						final ObjectMapper objectMapper = new ObjectMapper();
						objectMapper.readValue(objectMapper.writeValueAsString(bankProxyJson),BankProxyES.class);
					}
				}

			} else {
				throw new DataNotFoundException("Account details for the given account guid >>>>  [" + acctID + "] not found in Account ES.");
			}
			accountES.setSchemeName(PaymentIngestionConstant.GUID);
			// else if not GUID, means ADHOC/or ANYID
		} else if(schemeName.equals(PaymentIngestionConstant.ADHOC)){
			LOGGER.info("In else ADHOC getAccountDetails accountId:{}" , acctID);
			//ADHOC changes-CIBCCBX-6528
			LOGGER.info("In else ADHOC getAccountDetails schemeNameCode:{} " , Optional.ofNullable(account).map(Account::getSchemeNameCode).orElse(null));
			if(null == acctID)
				accountES.setId("");
			else
			    accountES.setId(Optional.ofNullable(account).map(Account::getIdentification).orElse(null));
			accountES.setSchemeNameCode(Optional.ofNullable(account).map(Account::getSchemeNameCode).orElse(null));
			accountES.setContactName(Optional.ofNullable(account).map(Account::getContactName).orElse(null));
			accountES.setIsAddedContactBookFlag(Optional.ofNullable(account).map(Account::getIsAddedConactBookFlag).orElse(null));
			
			// accountNo -> acctID
			accountES.setAccountNo(Optional.ofNullable(account).map(Account::getAccountNo).orElse(null));
			
			// accountES.name -> name
			accountES.setName(Optional.ofNullable(account).map(Account::getName).orElse(null));

			// accountNumType -> accNumType
			accountES.setAccNumType(Optional.ofNullable(account).map(Account::getAccountType).orElse(null));

			// alias-> alias
			accountES.setAlias(Optional.ofNullable(account).map(Account::getAlias).orElse(null));
			
			// aliasHandleType-> aliasHandleType
			accountES.setAliasHandleType(Optional.ofNullable(account).map(Account::getAliasHandleType).orElse(null));
			
			
		} else {
			LOGGER.info("In else getAccountDetails accountId:{}" , acctID);
			// accountNo -> acctID
			
			accountES.setAccountNo(acctID);

			// accountES.name -> name
			accountES.setName(Optional.ofNullable(account).map(Account::getName).orElse(null));

			// accountNumType -> accNumType
			accountES.setAccNumType(Optional.ofNullable(account).map(Account::getAccountType).orElse(null));
		}
		return accountES;
	}
	/**
	 * Method to fetch the creditor details from ES/or Action Payload(depending on
	 * whether ADHOC/or not)
	 *
	 * @param creditor
	 *            the creditor
	 * @param creditorId
	 *            the creditor id
	 * @param option
	 *            the option
	 * @param schemeName
	 *            the scheme name
	 * @param piType
	 *            the pi type
	 * @return the creditor details
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public CreditorES getCreditorDetails(final Creditor creditor, final String creditorId, final String option,
			final String schemeName, final String piType) throws DataNotFoundException {
		final CreditorES creditorES = new CreditorES();
		if ("GUID".equalsIgnoreCase(schemeName)) {
			LOGGER.info("IN GUID");
			creditorES.setId(creditorId);
			enrichedOtherCreditorInformation(creditor, creditorES);
		} else {
			LOGGER.info("ELSE of GUID");
			// ADHOC created when doing bene txn(meaning when option is PAYMNT i.e DFT,
			// CBFT, or A2ADC)
			if (Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT))
					.isPresent() || Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT_RTP))
					.isPresent() 
					|| (piType.equals("file") && !eventValidationConfig.getCashAccountSupportedProduct()
							.contains(Optional.ofNullable(option).map(op -> op.toLowerCase()).orElse("")))) {
				enrichedOtherCreditorInformation(creditor, creditorES);
			} else {
				// else if TRANSFER then GUID is mandatory
				throw new DataNotFoundException("Mandatory creditor GUID is not found. Hence sending to DLQ.");
			}
		}
		creditorES.setSchemeName(schemeName);
		return creditorES;
	}

	/**
	 * Method to fetch the creditor details from ES/or Action Payload(depending on
	 * whether ADHOC/or not)
	 *
	 * @param creditor
	 *            the creditor
	 * @param creditorId
	 *            the creditor id
	 * @param option
	 *            the option
	 * @param schemeName
	 *            the scheme name
	 * @param piType
	 *            the pi type
	 * @return the creditor details
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public DebtorES getDebtorDetails(final Debtor debtor, final String debtorId, final String option,
			final String schemeName, final String piType) throws DataNotFoundException {
		LOGGER.info("start getDebtorDetails() for debtorId:::::::{} " , debtorId);
		final DebtorES debtorES = new DebtorES();
		if (PaymentIngestionConstant.GUID.equalsIgnoreCase(schemeName)) {
			LOGGER.info("IN GUID");
			debtorES.setId(debtorId);
			debtorES.setAnyBIC(debtor.getAnyBIC());
			enrichedOtherDebtorInformation(debtor, debtorES);
		} else {
			LOGGER.info("ELSE of GUID");
			// ADHOC created when doing bene txn(meaning when option is PAYMNT i.e DFT,
			// CBFT, or A2ADC)
			if (Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT)).isPresent()
				|| Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT_RTP)).isPresent()
				|| (piType.equals(PaymentIngestionConstant.FILE)
			// &&
			// !eventValidationConfig.getCashAccountSupportedProduct().contains(Optional.ofNullable(option).map(op
			// -> op.toLowerCase()).orElse(""))
			)) {
				enrichedOtherDebtorInformation(debtor, debtorES);
			} else {
				// else if TRANSFER then GUID is mandatory
				throw new DataNotFoundException("Mandatory creditor GUID is not found. Hence sending to DLQ.");
			}
		}
		debtorES.setSchemeName(schemeName);
		return debtorES;
	}

	/**
	 * Enrich creditor communication information.
	 * 
	 * @param creditor
	 *            creditor action request
	 * @param creditorES
	 *            creditor inquiry request
	 */
	private void enrichedOtherCreditorInformation(final Creditor creditor, final CreditorES creditorES) {
		creditorES.setName(Optional.ofNullable(creditor).map(Creditor::getName).orElse(null));
		creditorES.setDesignation(Optional.ofNullable(creditor).map(Creditor::getDesignation).orElse(null));
		ContactAdressES contactAddress = new ContactAdressES();
		LOGGER.info("Start enrichedOtherCreditorInformation :: creditor.postalAddress");
		
		// Mudasir
		contactAddress.setAddress1(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getAddress1).orElse(null));
		contactAddress.setAddress2(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getAddress2).orElse(null));
		contactAddress.setAddress3(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getAddress3).orElse(null));

		// City
		CityES cityES = new CityES();
		cityES.setCode(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getCity).map(City::getCode).orElse(null));
		cityES.setName(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getCity).map(City::getName).orElse(null));
		contactAddress.setCity(cityES);

		StateES stateES = new StateES();
		stateES.setCode(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getState).map(State::getCode).orElse(null));
		stateES.setName(Optional.ofNullable(creditor.postalAddress).map(PostalAddress::getState).map(State::getName).orElse(null));
		contactAddress.setState(stateES);

		// set contact address
		CountryES country = new CountryES();
		country.setIso2CharCode(Optional.ofNullable(creditor).map(Creditor::getCountryOfResidence).orElse(null));
		contactAddress.setCountry(country);
		creditorES.setContactAddress(contactAddress);

		// set communication info
		creditorES.setCommunicationInfo(getCommunicationInfo(Optional.ofNullable(creditor).map(Creditor::getMobNb).orElse(null),Optional.ofNullable(creditor).map(Creditor::getPhneNb).orElse(null),Optional.ofNullable(creditor).map(Creditor::getEmailAdr).orElse(null),Optional.ofNullable(creditor).map(Creditor::getFaxNo).orElse(null)));
	}

	/**
	 * Enrich creditor communication information.
	 * 
	 * @param creditor
	 *            creditor action request
	 * @param creditorES
	 *            creditor inquiry request
	 */
	private void enrichedOtherDebtorInformation(final Debtor debtor, final DebtorES debtorES) {
		debtorES.setName(Optional.ofNullable(debtor).map(Debtor::getName).orElse(null));
		debtorES.setDesignation(Optional.ofNullable(debtor).map(Debtor::getDesignation).orElse(null));
		ContactAdressES contactAddress = new ContactAdressES();
		LOGGER.info("enrichedOtherDebtorInformation :: debtor.postalAddress");
		
		// PostalAddress-Mudasir
		contactAddress.setAddress1(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getAddress1).orElse(null));
		contactAddress.setAddress2(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getAddress2).orElse(null));
		contactAddress.setAddress3(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getAddress3).orElse(null));

		// City
		CityES cityES = new CityES();
		cityES.setCode(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getCity).map(City::getCode).orElse(null));
		cityES.setName(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getCity).map(City::getName).orElse(null));
		contactAddress.setCity(cityES);

		StateES stateES = new StateES();
		stateES.setCode(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getState).map(State::getCode).orElse(null));
		stateES.setName(Optional.ofNullable(debtor.postalAddress).map(PostalAddress::getState).map(State::getName).orElse(null));
		contactAddress.setState(stateES);

		// set contact address
		CountryES country = new CountryES();
		country.setIso2CharCode(Optional.ofNullable(debtor).map(Debtor::getCountryOfResidence).orElse(null));
		contactAddress.setCountry(country);
		debtorES.setContactAddress(contactAddress);

		// set communication info
		debtorES.setCommunicationInfo(getCommunicationInfo(Optional.ofNullable(debtor).map(Debtor::getMobNb).orElse(null),Optional.ofNullable(debtor).map(Debtor::getPhneNb).orElse(null),Optional.ofNullable(debtor).map(Debtor::getEmailAdr).orElse(null),null));
	}

	/**
	 * Fetch communication information on the basis of the mobile numbers and emails
	 * send by UI in action payload.(when wither sendAdviceFlag is set or it is an
	 * ADHOC payment)
	 * 
	 * @param mobileNumbers
	 *            mobile num string with a delimiter separator
	 * @param phones
	 *            phones string with a delimiter sepratator
	 * @param emails
	 *            email string with a delimiter separator
	 * @param sendAdviceFlag
	 *            send advice flag set(Y or N)
	 * @param isAdhoc
	 *            if ADHOC or not
	 * @param fax
	 *            fax detail
	 * @return {@link CommInfoES} communication information in case of send advices
	 */
	private CommInfoES getCommunicationInfo(final String mobileNumbers, final String phones, final String emails,
			final String fax) {
		CommInfoES commInfoES = new CommInfoES();
		commInfoES.setMobile(Optional.ofNullable(mobileNumbers).map(mobs -> mobs.split(PaymentIngestionConstant.COMMA))
				.filter(arr -> arr.length > 0).map(arr -> Arrays.asList(arr)).orElse(null));
		commInfoES.setPhone(Optional.ofNullable(phones).map(phs -> phs.split(PaymentIngestionConstant.COMMA)).filter(arr -> arr.length > 0)
				.map(arr -> Arrays.asList(arr)).orElse(null));
		commInfoES.setEmail(Optional.ofNullable(emails).map(mails -> mails.split(PaymentIngestionConstant.COMMA)).filter(arr -> arr.length > 0)
				.map(arr -> Arrays.asList(arr)).orElse(null));
		commInfoES.setFax(fax);
		return commInfoES;
	}

	/**
	 * Common method to populate creditorAgent/debtorAgent details.(in case of both
	 * ADHOC/and non ADHOC)
	 *
	 * @param agent
	 *            the agent
	 * @param agentType
	 *            the agent type
	 * @param bankProxyES
	 *            the bank proxy ES
	 * @return the agent details
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AgentES getAgentDetails(final Agent agent, final String agentType, final BankProxyES bankProxyES)
			throws IOException {
		AgentES agentES = new AgentES();
		// _id from action
		String agentId = Optional.ofNullable(agent).map(Agent::getBankAddnlInfo1).orElse(null);
		// BIC from action
		String agentBICId = Optional.ofNullable(agent).map(Agent::getIdentification).orElse(null);
		//bicfi
		String agentBICFI = Optional.ofNullable(agent).map(Agent::getBicfi).orElse(null);
		// id
		agentES.setId(agentId);
		agentES.setBicfi(agentBICFI);
		final String schemeName = Optional.ofNullable(agent).map(Agent::getSchemeName).orElse(null);

		// Fetch Bank Info from ES
		JsonNode bankJson = paymentIngestionQueryUtil.getBankInformationFromES(agentId, schemeName);

		// bic
		agentES.setBic(Optional.ofNullable(agentBICId).orElse(Optional
				.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "", PaymentIngestionConstant.BIC)).map(Object::toString).orElse(null)));

		JsonNode bankAddressFromES = null;
		if (bankJson != null && bankJson.hasNonNull(PaymentIngestionConstant.ADDRESS)) {
			bankAddressFromES = (JsonNode) CommonUtil.getValueFromJsonNode(bankJson, "",PaymentIngestionConstant.ADDRESS);
		}

		// address
		AddressES address = new AddressES();
		address.setAddress1(Optional.ofNullable(agent).map(Agent::getAddress1).orElse(Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankAddressFromES, "", PaymentIngestionConstant.ADDRESS1)).map(Object::toString).orElse(null)));
		address.setAddress2(Optional.ofNullable(agent).map(Agent::getAddress2).map(Object::toString).orElse(Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankAddressFromES, "", PaymentIngestionConstant.ADDRESS2)).map(Object::toString).orElse(null)));
		address.setAddress3(Optional.ofNullable(agent).map(Agent::getAddress3).map(Object::toString).orElse(Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankAddressFromES, "", PaymentIngestionConstant.ADDRESS3)).map(Object::toString).orElse(null)));
		
		CityES city = new CityES();
		city.setCode(Optional.ofNullable(agent).map(Agent::getCity).map(Object::toString).orElse(Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankAddressFromES, PaymentIngestionConstant.CITY, PaymentIngestionConstant.CODE)).map(Object::toString).orElse(null)));
		address.setCity(city);

		CountryES country = new CountryES();
		country.setIso3CharCode(Optional.ofNullable(agent).map(Agent::getCountry).map(Object::toString).orElse(Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankAddressFromES, PaymentIngestionConstant.COUNTRY, PaymentIngestionConstant.CODE)).map(Object::toString).orElse(null)));
		address.setCountry(country);
		agentES.setAddress(address);

		// if schemeName is not present
		if (StringUtils.isBlank(schemeName)) {
			if (StringUtils.isNotBlank(agentBICId)) {
				agentES.setSchemeName(PaymentIngestionConstant.BIC.toUpperCase());
			} // else if only bank code and branch code comes, then ingestion will enrich
				// schemeNm as: "BANK_BRANCH_CD"
			else if (StringUtils.isNotBlank(Optional.ofNullable(agent).map(Agent::getBankCode).orElse(null))&& StringUtils.isNotBlank(Optional.ofNullable(agent).map(Agent::getBranchCode).orElse(null))) {
				agentES.setSchemeName(PaymentIngestionConstant.BANK_BRANCH_CD);
			} // else if only bank name and branch name comes, then ingestion will enrich
				// schemeNm as: "BANK_BRANCH_NM"
			else if (StringUtils.isNotBlank(Optional.ofNullable(agent).map(Agent::getBankName).orElse(null))
					&& StringUtils.isNotBlank(Optional.ofNullable(agent).map(Agent::getBranchName).orElse(null))) {
				agentES.setSchemeName(PaymentIngestionConstant.BANK_BRANCH_NM);
			}
		} else {
			// else set set as is
			agentES.setSchemeName(schemeName);
		}

		// branch code
		agentES.setKey(Optional.ofNullable(Optional.ofNullable(agent).map(Agent::getBranchCode).orElse(null))
				.orElse((Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "", "key")).map(Object::toString)
						.orElse(null))));
		// branch name
		agentES.setName(Optional.ofNullable(Optional.ofNullable(agent).map(Agent::getBranchName).orElse(null))
				.orElse((Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "", "name"))
						.map(Object::toString).orElse(null))));

		OrganisationES org = new OrganisationES();
		// bank code
		org.setId(Optional.ofNullable(Optional.ofNullable(agent).map(Agent::getBankCode).orElse(null))
				.orElse((Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "org", "id"))
						.map(Object::toString).orElse(null))));
		// bank key
		org.setKey((Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "org", "key")).map(Object::toString)
				.orElse(null)));
		// bank name
		org.setName(Optional.ofNullable(Optional.ofNullable(agent).map(Agent::getBankName).orElse(null))
				.orElse((Optional.ofNullable(CommonUtil.getValueFromJsonNode(bankJson, "org", "name"))
						.map(Object::toString).orElse(null))));
		agentES.setOrg(org);

		agentES.setType(agent.getType());
		if (StringUtils.isNotBlank(agent.getIsHostBank())) {
			agentES.setIsHostBank(Boolean.valueOf(agent.getIsHostBank()));
		}
		agentES.setParentBank(new ParentBankES(agent.getBankCode()));

		if (agentType.equals("creditorAgent")) {
			agentES.setClearingCode(agentES.getBic());
			agentES.setClearingCodeType(agent.getBankIdType());
			agentES.setBankProxyES(bankProxyES);
		}
		LOGGER.info("Returning bank data json >> {}",
				agentES != null ? mapper.convertValue(agentES, JsonNode.class) : null);

		return agentES;
	}

	/**
	 * Gets the creditor file PI.
	 *
	 * @param creditorAccID
	 *            the creditor acc ID
	 * @param paymentRail
	 *            the payment rail
	 * @return the creditor file PI
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public CreditorES getCreditorFilePI(final String creditorAccID, final String paymentRail) throws IOException {
		if (eventValidationConfig.getCashAccountSupportedProduct()
				.contains(Optional.ofNullable(paymentRail).map(op -> op.toLowerCase()).orElse(""))) {
			return getCreditorFromCashAccounts(creditorAccID);
		} else {
			return getCreditorFromContactAccounts(creditorAccID);
		}
	}

	/**
	 * Gets the creditor from contact accounts.
	 *
	 * @param accountGUID
	 *            the account GUID
	 * @return the creditor from contact accounts
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private CreditorES getCreditorFromContactAccounts(final String accountGUID) throws IOException {
		LOGGER.debug("getCreditorFromContactAccounts for accountGUID: {}", accountGUID);
		final CreditorES creditorES = new CreditorES();
		// 1. Get the contact account using guid
		final JsonNode contactAccNode = paymentIngestionQueryUtil.jestGetESBuilder(accountGUID,
				paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getContactAccountsType());
		// 2. if contact account found in ES org DB
		if (contactAccNode != null) {
			final String contactId = Optional
					.ofNullable(CommonUtil.getValueFromJsonNode(contactAccNode, "contact", "id")).map(Object::toString)
					.map(String::trim).orElse(null);

			final JsonNode contactNode = paymentIngestionQueryUtil.jestGetESBuilder(contactId,
					paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getContactsType());
			if (contactNode != null) {
				creditorES.setName(Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactNode, "", "name"))
						.map(Object::toString).orElse(null));

				creditorES.setDesignation(
						Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactNode, "", "designation"))
								.map(Object::toString).orElse(null));
				ContactAdressES contactAddress = new ContactAdressES();
				contactAddress.setAddress1(Optional
						.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
								PaymentIngestionConstant.CONTACT_ADDRESS, PaymentIngestionConstant.ADDRESS1))
						.map(Object::toString).orElse(null));
				contactAddress.setAddress1(Optional
						.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
								PaymentIngestionConstant.CONTACT_ADDRESS, PaymentIngestionConstant.ADDRESS2))
						.map(Object::toString).orElse(null));
				contactAddress.setAddress1(Optional
						.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
								PaymentIngestionConstant.CONTACT_ADDRESS, PaymentIngestionConstant.ADDRESS3))
						.map(Object::toString).orElse(null));
				// set contact address
				final CountryES country = new CountryES();
				final JsonNode contactAddressNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode, "",
						PaymentIngestionConstant.CONTACT_ADDRESS);
				country.setIso2CharCode(Optional.ofNullable(
						CommonUtil.getValueFromJsonNode(contactAddressNode, PaymentIngestionConstant.COUNTRY, "code"))
						.map(Object::toString).orElse(null));
				contactAddress.setCountry(country);
				creditorES.setContactAddress(contactAddress);

				// set communication info
				final JsonNode communicationInfoNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode, "",
						"communicationInfo");
				final ObjectMapper objMapper = new ObjectMapper();
				final CommInfoES commInfoES = objMapper.readValue(objMapper.writeValueAsString(communicationInfoNode),
						CommInfoES.class);
				creditorES.setCommunicationInfo(commInfoES);
			}
		}
		LOGGER.debug("getCreditorFromContactAccounts returns creditorES: {}", creditorES);
		return creditorES;
	}

	private CreditorES getCreditorFromCashAccounts(final String accountGUID) throws IOException {
		LOGGER.debug("getCreditorFromCashAccounts for the account guid >>> [{}].............", accountGUID);
		final CreditorES creditorES = new CreditorES();
		final String querystring1 = "_id";
		final QueryBuilder query = QueryBuilders.termQuery(querystring1, accountGUID);
		final JsonNode hitsNode = paymentIngestionQueryUtil.jestSearchESBuilder(query.toString(),
				paymentElasticProperties.getAcctIndexName(), paymentElasticProperties.getAccountType());
		final JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0))
				.map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		if (sourceNode != null) {
			final String contactId = Optional.ofNullable(CommonUtil.getValueFromJsonNode(sourceNode, "entity", "id"))
					.map(Object::toString).map(String::trim).orElse(null);

			final JsonNode contactNode = paymentIngestionQueryUtil.jestGetESBuilder(contactId,
					paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getCorporates());

			if (contactNode != null) {
				creditorES.setName(Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactNode, "", "name"))
						.map(Object::toString).orElse(null));

				creditorES.setDesignation(null);
				ContactAdressES contactAddress = new ContactAdressES();
				contactAddress
						.setAddress1(Optional
								.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
										PaymentIngestionConstant.ADDRESS, PaymentIngestionConstant.ADDRESS1))
								.map(Object::toString).orElse(null));
				contactAddress
						.setAddress2(Optional
								.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
										PaymentIngestionConstant.ADDRESS, PaymentIngestionConstant.ADDRESS2))
								.map(Object::toString).orElse(null));
				contactAddress
						.setAddress3(Optional
								.ofNullable(CommonUtil.getValueFromJsonNode(contactNode,
										PaymentIngestionConstant.ADDRESS, PaymentIngestionConstant.ADDRESS3))
								.map(Object::toString).orElse(null));
				// set contact address
				final CountryES country = new CountryES();
				final JsonNode contactAddressNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode, "",
						PaymentIngestionConstant.ADDRESS);
				country.setIso2CharCode(Optional.ofNullable(
						CommonUtil.getValueFromJsonNode(contactAddressNode, PaymentIngestionConstant.COUNTRY, "code"))
						.map(Object::toString).orElse(null));
				contactAddress.setCountry(country);
				creditorES.setContactAddress(contactAddress);

				// set communication info
				final JsonNode communicationInfoNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode, "",
						"communicationInfo");
				final ObjectMapper objMapper = new ObjectMapper();
				final CommInfoES commInfoES = objMapper.readValue(objMapper.writeValueAsString(communicationInfoNode),
						CommInfoES.class);
				creditorES.setCommunicationInfo(commInfoES);
			}
		}
		LOGGER.debug("getCreditorFromCashAccounts return creditorES >>> {}", creditorES);
		return creditorES;
	}
	boolean isValidFormat(String format,String value,Locale locale) {
		try {
			DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, Locale.ENGLISH);
			
			LocalDateTime ldt = LocalDateTime.parse(value, fomatter);
			String result = ldt.format(fomatter);
			return result.equals(value);
			
		}catch(Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_080 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_080_Desc);
			return false;
		}
	}
}
