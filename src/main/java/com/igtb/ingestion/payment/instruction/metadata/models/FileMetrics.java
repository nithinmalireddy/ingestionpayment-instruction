package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class FileMetrics.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileMetrics {

    /** The total amount. */
    private Amount totalAmount;
    
    /** The valid records. */
    private Record validRecords;
    
    /** The invalid records. */
    private Record invalidRecords;
    
    /** The status. */
    private String status;
}
