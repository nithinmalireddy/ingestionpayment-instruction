
package com.igtb.ingestion.payment.instruction.models.irtp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "eventId",
    "linkedEditedChnlReqIds",
    "outcomeCategory",
    "status",
    "sourceIdentity"
})
public class RequestInfo {

    @JsonProperty("eventId")
    private String eventId;
    @JsonProperty("linkedEditedChnlReqIds")
    private Object linkedEditedChnlReqIds;
    @JsonProperty("outcomeCategory")
    private String outcomeCategory;
    @JsonProperty("status")
    private String status;
    @JsonProperty("sourceIdentity")
    private String sourceIdentity;

    @JsonProperty("eventId")
    public String getEventId() {
        return eventId;
    }

    @JsonProperty("eventId")
    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @JsonProperty("linkedEditedChnlReqIds")
    public Object getLinkedEditedChnlReqIds() {
        return linkedEditedChnlReqIds;
    }

    @JsonProperty("linkedEditedChnlReqIds")
    public void setLinkedEditedChnlReqIds(Object linkedEditedChnlReqIds) {
        this.linkedEditedChnlReqIds = linkedEditedChnlReqIds;
    }

    @JsonProperty("outcomeCategory")
    public String getOutcomeCategory() {
        return outcomeCategory;
    }

    @JsonProperty("outcomeCategory")
    public void setOutcomeCategory(String outcomeCategory) {
        this.outcomeCategory = outcomeCategory;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("sourceIdentity")
    public String getSourceIdentity() {
        return sourceIdentity;
    }

    @JsonProperty("sourceIdentity")
    public void setSourceIdentity(String sourceIdentity) {
        this.sourceIdentity = sourceIdentity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("eventId", eventId).append("linkedEditedChnlReqIds", linkedEditedChnlReqIds).append("outcomeCategory", outcomeCategory).append("status", status).append("sourceIdentity", sourceIdentity).toString();
    }

}
