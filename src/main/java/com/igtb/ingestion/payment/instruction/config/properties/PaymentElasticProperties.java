package com.igtb.ingestion.payment.instruction.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

/**
 * ElasticSearch Property file. During boot startup, binding takes place with
 * the respective external properties.
 * 
 */
@Configuration
@Getter
public class PaymentElasticProperties { // NOPMD TooManyFields

	/** The org index. */
	@Value("${elasticsearch.index.org}")
	private String orgIndex;

	/** The user type. */
	@Value("${elasticsearch.type.users}")
	private String userType;

	/** The application name. */
	// Application Level properties
	@Value("${application.name}")
	private String applicationName;

	/** The version. */
	@Value("${info.build.version}")
	private String version;
	
	/** The pymt index. */
	@Value("${elasticsearch.index.payments}")
	private String pymtIndex;
	
	/** The acct index name. */
	@Value("${elasticsearch.index.acct}")
	private String acctIndexName;
	
	/** The acct index name. */
	@Value("${elasticsearch.index.refdata}")
	private String refDataIndexName;
	
	/** The pymt type. */
	@Value("${elasticsearch.type.payinstr}")
	private String pymtType;

	/** The account type. */
	@Value("${elasticsearch.type.accounts}")
	private String accountType;
	
	/** The contacts type. */
	@Value("${elasticsearch.type.contacts}")
	private String contactsType;
	
	/** The contact accounts type. */
	@Value("${elasticsearch.type.contactaccounts}")
	private String contactAccountsType;
	
	/** The supporting documents type. */
	@Value("${elasticsearch.type.supportingDocuments}")
	private String supportingDocumentsType;
	
	/** The payment tax info type. */
	@Value("${elasticsearch.type.paymentTaxInfo}")
	private String paymentTaxInfoType;
	
	/** The payment types. */
	@Value("${elasticsearch.type.paymentTypes}")
	private String paymentTypes;
	
	/** The payment rails type. */
	@Value("${elasticsearch.type.paymentRails}")
	private String paymentRailsType;
	
	/** The payment reasons type. */
	@Value("${elasticsearch.type.paymentReasons}")
	private String paymentReasonsType;
	
	/** The payment instruction sets. */
	@Value("${elasticsearch.type.paymentInstructionSets}")
	private String paymentInstructionSets;
	
	/** The p I transition history. */
	@Value("${elasticsearch.type.pITransitionHistory}")
	private String pITransitionHistory;

	@Value("${elasticsearch.type.pIContactLastActivities}")
	private String pIContactLastActivities;
	
	/** The countries type. */
	@Value("${elasticsearch.type.countries}")
	private String countriesType;
	
	/** The org banks type. */
	@Value("${elasticsearch.type.banks}")
	private String bankType;
	
	/** The org domains type. */
	@Value("${elasticsearch.type.domains}")
	private String domainsType;
	
	/** The corporates. */
	@Value("${elasticsearch.type.corporates}")
	private String corporates;
	
	//added by vinay

	@Value("${elasticsearch.type.paynotificn}")
	private String paynotificnType;


	@Value("${elasticsearch.type.paynotificnEvntCofig}")
	private String paynotificnEvntCofigType;

	@Value("${elasticsearch.type.paydlqnotificn}")
	private String paydlqnotificnType;

}
