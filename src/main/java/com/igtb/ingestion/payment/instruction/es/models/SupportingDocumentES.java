package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Supporting document DAO Class.
 * 
 */
@ToString
@Getter
@Setter
public class SupportingDocumentES {

	/** The Supporting Doc Payment Transaction Id. */
	private String paymentTransactionId;
	
	/** The Supporting Doc Payment Instruction Id. */
	private String paymentInstructionId;
	
	/** The Supporting Doc Payment Instructions Doc Status. */
	private String paymentDocStatus;
	
	/** The Supporting Doc Document Id. */
	private String documentId;
	
	/** The Supporting Doc Document Type. */
	private String documentType;
	
	/** The Supporting Doc Document Name. */
	private String documentName;
	
	/** The Supporting Doc Document Format Type. */
	private String documentFormatType;
	
	/** The Supporting Document Remark. */
	private String documentRemark;
	
	/** The Supporting Doc Upload date time. */
	private String uploadDateTime;
	
	/** The Supporting Doc Size. */
	private Long size;

}
