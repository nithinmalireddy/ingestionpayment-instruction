package com.igtb.ingestion.payment.instruction.config;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;

/**
 * The Class PaymentJestConfig.
 */
@Configuration
public class PaymentJestConfig {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentJestConfig.class);

	/** To access externalized configuration. */
	@Resource
	private Environment env;

	/** To access externalized configuration. */
	//@Autowired
	//private PaymentElasticProperties paymentElasticProperties;

	/**
	 * Jest client.
	 *
	 * @return the jest client
	 */
	@Bean
	public JestClient jestClient() {
		final String protocol = env.getProperty(PaymentIngestionConstant.CFG_KEY_ES_DB_PROTOCOL);
		final String host = env.getProperty(PaymentIngestionConstant.CFG_KEY_ES_DB_HOST);
		final String port = env.getProperty(PaymentIngestionConstant.CFG_KEY_ES_DB_PORT);
		final String userName = env.getProperty(PaymentIngestionConstant.CFG_KEY_ES_DB_USER);
		final String pwd = env.getProperty(PaymentIngestionConstant.CFG_KEY_ES_DB_PASSWORD);
		
		final String portString = port == null || port.isEmpty() ? "" : ":" + port;
		final String url = protocol + "://" + host + portString;
		
        final JestClientFactory clientFactory = new JestClientFactory();
		
		return getConnection(userName, pwd, url, clientFactory); 
				//getElasticConnection(url);

		/*
		 * Setting up Payment instruction es mapping as accurate as how inquiry has configured.
		 */
		/*createIndexWithESMappings(jestClient, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPymtType(), PaymentIngestionConstant.ES_PYMT_INSTRUCTION_MAPPING_FILE_NAME);

		createIndexWithESMappings(jestClient, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentInstructionSets(), 
				PaymentIngestionConstant.ES_PYMT_INSTRUCTION_SETS_MAPPING_FILE_NAME);		
		/*
		 * Setting up payment instruction TransactionHistories es mapping as accurate as how inquiry has
		 * configured.
		 */
		/*createIndexWithESMappings(jestClient, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPITransitionHistory(),
				PaymentIngestionConstant.ES_PYMT_TRANS_HISTORY_MAPPING_FILE_NAME);*/
		
		//return jestClient;
	}

	private JestClient getConnection(final String userName, final String pwd, final String url,
			final JestClientFactory clientFactory) {
		clientFactory.setHttpClientConfig(new HttpClientConfig.Builder(url)
				.defaultCredentials(userName, pwd)
				.multiThreaded(true)
				.build());
		LOGGER.info("ElasticSearch url-------->>{}", url);
		return clientFactory.getObject();
	}

	/**
	 * Gets the elastic connection.
	 *
	 * @param url the url
	 * @return the elastic connection
	 */
	/*private JestClient getElasticConnection(final String url) {
		final JestClientFactory factory = new JestClientFactory();
		LOGGER.info("ElasticSearch url-------->>{}", url);
		factory.setHttpClientConfig(new HttpClientConfig.Builder(url).multiThreaded(true).build());
		return factory.getObject();
	}*/

	/**
	 * Inits the index.
	 *
	 * @param jestClient the jest client
	 * @param esIndexName the es index name
	 * @param esTypeName the es type name
	 * @param mappingResourceFileName the mapping resource file name
	 */
	/*private void createIndexWithESMappings(final JestClient jestClient, final String esIndexName,
			final String esTypeName, final String mappingResourceFileName) {
		try {
			if (jestClient != null) {
				try {
					// Create index (if doesn't already exist)
					// Load index setting from file
					final String settings = JSONHandler.getObjectMapper()
							.readTree(PaymentJestConfig.class.getClassLoader()
									.getResourceAsStream(PaymentIngestionConstant.ES_INDEX_SETTING_FILE_NAME))
							.toString();
					final Map<String, String> settingsAsMap = Settings.builder().loadFromSource(settings).build()
							.getAsMap();
					JestResult jestResult = jestClient
							.execute(new CreateIndex.Builder(esIndexName).settings(settingsAsMap).build());

					LOGGER.info("ES Document Index initialized");
					// Load index mapping from file
					final String mappings = JSONHandler.getObjectMapper().readTree(
							PaymentJestConfig.class.getClassLoader().getResourceAsStream(mappingResourceFileName))
							.toString();
					// Update index mapping
					final PutMapping putMapping = new PutMapping.Builder(esIndexName, esTypeName, mappings).build();
					
					 * LOGGER.info("Updated index mapping: {}",
					 * PaymentIngestionConstant.ES_PAYMENT_TRANS_HISTORY_MAPPING_FILE_NAME);
					 
					jestResult = jestClient.execute(putMapping);
					if (jestResult.isSucceeded()) {
						LOGGER.info("ES Mapping of Index -->> {} & Type -->> {} has been updated", esIndexName,
								esTypeName);
					} else {
						LOGGER.error("Failed updating ES Document Index mapping : {}", jestResult.getErrorMessage());
						throw new IllegalStateException(
								"Failed updating ES Document Index mapping :" + jestResult.getErrorMessage());
					}
				} catch (Exception e) {
					LOGGER.error("Exception during ES Index creation {}", e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception getting ES Client {}", e.getMessage(), e);
			throw new IllegalStateException(e);
		}
	}
*/	
}
