package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class SetElementInfoES.
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SetElementInfoES {

	/** The elements. */
	private List<String> elements;
	
	/** The rejected. */
	private List<String> rejected;
	
	/** The skipped. */
	private List<String> skipped;
	
}
