package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
/**
 * The Class WhtDetailES.
 */
public class WhtDetailES { //NOPMD TooManyFields
	
	/** The serial number. */
	private String serialNumber;
	
	/** The book number. */
	private String bookNumber;
	
	/** The sequence number. */
	private String sequenceNumber;
	
	/** The form type. */
	private String formType;
	
	/** The deduction type. */
	private String deductionType;
	
	/** The deduction type description. */
	private String deductionTypeDescription;
	
	/** The alternative payer details. */
	private String alternativePayerDetails;
	
	/** The update corporate details. */
	private String updateCorporateDetails;
	
	/** The tax withholder name. */
	private String taxWithholderName;
	
	/** The tax withholder branch code. */
	private String taxWithholderBranchCode;
	
	/** The tax withholder tax ID. */
	private String taxWithholderTaxID;
	
	/** The tax withholder address. */
	private String taxWithholderAddress;
	
	/** The alternative payer name. */
	private String alternativePayerName;
	
	/** The alternative payer branch code. */
	private String alternativePayerBranchCode; //NOPMD longVariable
	
	/** The alternative payer tax ID. */
	private String alternativePayerTaxID;
	
	/** The alternative payer address. */
	private String alternativePayerAddress;
	
	/** The taxable party name. */
	private String taxablePartyName;
	
	/** The taxable party tax ID. */
	private String taxablePartyTaxID;
	
	/** The taxable party address. */
	private String taxablePartyAddress;
	
	/** The delivery method. */
	private String deliveryMethod;
	
	/** The delivery location. */
	private String deliveryLocation;

}
