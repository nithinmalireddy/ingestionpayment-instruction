package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class BankProxyES.
 */
@Getter
@Setter
public class BankProxyES {

	/** The bene. */
	private BeneES bene;
	
	/** The correspondent. */
	private CorrespondentES correspondent;
	
}
