/*
 * 
 */
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * PaymentsInstructionsES Class.
 * 
 */
@ToString

/**
 * Gets the reason.
 *
 * @return the reason
 */
@Getter

/**
 * Sets the reason.
 *
 * @param reason
 *            the new reason
 */
@Setter
// @JsonInclude(JsonInclude.Include.ALWAYS)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentsInstructionsES { // NOPMD TooManyFields
	// New Fields added for ParentPayload - Starts

	@JsonProperty("msgId")
	private String msgId = null;

	@JsonProperty("fileNbOfTxs")
	private Integer fileNbOfTxs = null;

	@JsonProperty("fileCtrlSum")
	private Double fileCtrlSum = null;

	@JsonProperty("notificationChannel")
	private NotificationChannelES notificationChannel;
	
	@JsonProperty("fileAuthstnPrtry")
	private String fileAuthstnPrtry = null;

	@JsonProperty("pmtInfId")
	private String pmtInfId = null;

	@JsonProperty("batchNbOfTxs")
	private String batchNbOfTxs = null;

	@JsonProperty("batchCtrlSum")
	private Double batchCtrlSum = null;

	@JsonProperty("businessProductCode")
	private String businessProductCode = null;

	@JsonProperty("instructedAgentId")
	private String instructedAgentId = null;

	@JsonProperty("instructingAgentId")
	private String instructingAgentId = null;

	@JsonProperty("supplementaryData")
	private SupplementaryDataES supplementaryData = null;

	@JsonProperty("cancellationDetails")
	private CancellationDetailsES cancellationDetails = null;

	@JsonProperty("incomingRTP")
	private IncomingRTPES incomingRTP = null;

	@JsonProperty("originalGroupInformationAndStatus")
	private OriginalGroupInformationAndStatusES originalGroupInformationAndStatus = null;

	@JsonProperty("interacTransactionAmountLimit")
	private AmountInteractES interacTransactionAmountLimit = null;

	@JsonProperty("expiryDate")
	private String expiryDate = null;

	@JsonProperty("limitUtilizeDateTime")
	private String limitUtilizeDateTime = null;

	@JsonProperty("limitUtilizeFlag")
	private String limitUtilizeFlag = null;

	@JsonProperty("paymentCondition")
	private PaymentConditionES paymentCondition = null;

	@JsonProperty("initiatingParty")
	private InitiatingPartyES initiatingParty = null;
	@JsonProperty("communicationInfo")
	private CommunicationInfoES communicationInfo = null;

	@JsonProperty("faeIndicator")
	private String faeIndicator = null;

	/* New BCC Action field added to capture BCC action for FAE */
	@JsonProperty("bccAction")
	private String bccAction = null;

	// New Fields added for ParentPayload - Ends

	/** The Payment Instructions acceptTnCFlag. */
	private Boolean acceptTnCFlag;

	/** The Payment Instructions BankCommissionAmount. */
	private AmountES bankCommissionAmount;

	/** The Payment Instructions BatchId. */
	private String batchId;

	/** The Payment Instructions CategoryPurpose. */
	private String categoryPurpose;

	/** The Payment Instructions ChargeAmount. */
	private AmountES chargeAmount;

	/** The Payment Instructions ChargeBearer. */
	private String chargeBearer;

	/** The Payment Instructions ChargesAccount. */
	@JsonProperty("chargeAccount")
	private ChargesAccountES chargesAccount;
	/** The Payment Instructions ChargesAccount. */
//	@JsonProperty("chargeAccount")
//	private ChargeAccountES chargeAccount;

	/** The Payment Instructions Creditor. */
	// @NotNull
	private CreditorES creditor;

	/** The Payment Instructions CreditorAccount. */
	// @NotNull
	private AccountES creditorAccount;

	/** The Payment Instructions CreditorAgent. */
	// @NotNull
	private AgentES creditorAgent;

	private String bankRemark;

	/** The Payment Instructions Debtor. */
	// commented by vinay
	// @NotNull
	private String debtor;

	/** The Payment Instructions Debtor. */
	// commented by vinay
	// @NotNull
	private DebtorES debtorDetails;

	/** The Payment Instructions DebtorAccount. */
	// commented by vinay
	// @NotNull
	// @Valid
	private AccountES debtorAccount;

	/** The Payment Instructions DebtorAgent. */
	// @NotNull
	// @Valid
	private AgentES debtorAgent;

	/**
	 * The Payment Instructions other remaining information(consists of schemeName
	 * and acc num Types).
	 */
	// commonted by vinay
	// @NotNull
	// @Valid
	private RemainingInfoES otherRemainingInfo;

	/** The Payment Instructions EndToEndIdentification. */
	private String endToEndIdentification;

	/** The Payment Instructions EquivalentAmount. */
	private AmountES equivalentAmount;

	/** The Payment Instructions FileId. */
	private String fileId;

	/** The Payment Instructions Fxdetails. */
	private List<FxRateES> fxDetails;

	/** The Payment Instructions InstructedAmount. */
	// @NotNull
	// @Valid
	private AmountES instructedAmount;

	/** The Payment Instructions InstructionId. */
	private String instructionId;

	/** The Payment Instructions Option. */
	// @NotNull
	private String option;

	/** The Payment Instructions PaymentMethod. */
	private String paymentMethod;

	/** The Payment Instructions PaymentRail. */
	// @NotNull
	// @Valid
	private PaymentRailES paymentRailObj;

	/** The Payment Instructions PaymentType. */
	// @NotNull
	// @Valid
	private PaymentTypeES paymentTypesObj;

	/** The Payment Instructions ReleaseDate. */
	private String releaseDate;



	private String processingDate;

	/** The Payment Instructions RemittanceInformation_1. */
	@JsonProperty(value = "remittanceInformation_1")
	private String remittanceInformation1;

	/** The Payment Instructions RemittanceInformation_2. */
	@JsonProperty(value = "remittanceInformation_2")
	private String remittanceInformation2;

	/** The Payment Instructions RemittanceInformation_3. */
	@JsonProperty(value = "remittanceInformation_3")
	private String remittanceInformation3;

	/** The Payment Instructions RemittanceInformation_4. */
	@JsonProperty(value = "remittanceInformation_4")
	private String remittanceInformation4;

	/** The Payment Instructions RequestedExecutionDate. */
	// @NotNull
	private String requestedExecutionDate;

	/** The Payment Instructions StandingInstructionId. */
	private String standingInstructionId;

	/** The Payment Instructions UI Status. */
	@JsonProperty(value = "status")
	// @NotNull
	private String cbxUIStatus;

	/** The Payment Instructions TransactionId. */
	// @NotNull
	private String transactionId;

	/** The Payment Instructions UserDateTime. */
	@JsonProperty(value = "userDateTime")
	private String userDateTime;

	/** The Payment Instructions IntermediaryAgent1_BIC. */
	@JsonProperty(value = "intermediaryAgent1_BIC")
	private String intermediaryAgent1BIC;

	/** The IntermediaryAgent. */
	@JsonProperty("intermediaryAgent")
	private IntermediaryAgentES intermediaryAgentES;

	/** The Payment Instructions Remark. */
	private String remark;

	/** The Payment Instructions PaymentReason. */
	// @NotNull
	// @Valid
	private PaymentReasonES paymentReason;

	/** The Payment Instructions Debit Time Flag. */
	private Boolean debitTimeFlag;

	/** The Payment Instructions Debit Time. */
	private String debitTime;

	/** The wht dtls. */
	@JsonProperty(value = "whtDetails")
	private WhtDetailES whtDtls;

	/** The Payment Instructions WHT Date type Indicator. */
	private String dateTypeIndicator;

	/** The Payment Instructions StateInformation object. */
	@JsonProperty(value = "stateInfo")
	private StateInfo stateInfo;

	/** The Payment Instructions approver User Id. */
	private String approverUserId;

	/** The Payment Instructions makerUserDomainId. */
	@JsonProperty(value = "makerDomainName")
	private String makerUserDomainId;

	/** The Payment Instructions ApproverDomainId. */
	@JsonProperty(value = "approverDomainName")
	private String approverDomainId;

	/** The Payment Instructions MakerUserId. */
	private String makerUserId;

	/** The set info. */
	private SetInfo setInfo;

	/** The Set element info. */
	private SetElementInfo setElementInfo;

	/** The instructed amount base ccy. */
	private AmountES instructedAmountBaseCcy;

	/** The send advice flag. */
	private Boolean sendAdviceFlag;

	/** The txn currency. */
	private TxnCurrency txnCurrency;

	/** The additional info. */
	private Map<String, String> additionalInfo;

	/** The txn level addinl info. */
	private Map<String, String> txnLevelAddinlInfo;

	/** The multi set addnl info. */
	private List<Map<String, String>> multiSetAddnlInfo;

	// for active record
	/** The Payment Instructions Debit Date. */
	private String debitDate;

	/** The Payment Instructions Debit Reference. */
	private String debitReference;

	/** The cbx support status. */
	private String cbxSupportStatus;

	/** The cbx support status channel seq id. */
	private String cbxSupportStatusChannelSeqId;

	/** The parent create request channel seq id. */
	private String parentCreateRequestChannelSeqId;

	/** The h/u/c reject flag added for identifying when h/u/c in IPSH fails. */
	private Boolean hucRejectFlag;

	/**
	 * The reject reason field to store backend reject txns initiated at backend.
	 */
	private String reason;

	@JsonProperty("creationDateTime")
	private String creationDateTime = null;

	@JsonProperty("serviceLvlCode")
	private String serviceLvlCode = null;

	@JsonProperty("localInstrumentCode")
	private String localInstrumentCode = null;

	@JsonProperty("notificationLanguage")
	private String notificationLanguage = null;

	/** The creditor AccPayment Type. */
	@JsonProperty("creditorAccPaymentType")
	private String creditorAccPaymentType;

	/** The creditor AccAlias RefNo. */
	@JsonProperty("creditorAccAliasRefNo")
	private String creditorAccAliasRefNo;

	/** The creditor agent. */
	@JsonProperty("benefIdentifiers")
	private BenefIdentifiers benefIdentifiers;
	@JsonProperty("auditId")
	private String auditIdES;
	@JsonProperty("clearingSystemReference")
	private String clearingSystemReferenceES;
	@JsonProperty("typeOfAmount")
	private String typeOfAmountES;
	@JsonProperty("isCpFlagOn")
	private Boolean isCPFlagOnES;
	@JsonProperty("custId")
	private String custIdES;
	@JsonProperty("custName")
	private String custNameES;
	@JsonProperty("puId")
	private String puIdES;
	@JsonProperty("recommendCost")
	private RecommendationCostES recommendationCostES;
	@JsonProperty("cumulativeStatus")
	private String cumulativeStatusES;
	@JsonProperty("uiDisplayStatus")
	private String uiDisplayStatusES;
	@JsonProperty("txnWorkItemId")
	private String txnWorkItemIdES;
	
	@JsonProperty("fetchQueryStatus")
	private String fetchQueryStatus;

	// active suppdocs and whtdocs

	/** Supporting documents. */
	// @JsonIgnore
	private List<SupportingDocumentES> suppDocsAttachments;

	/** WHT documents. */
	// @JsonIgnore
	private List<WithholdingTaxES> whtDocsAttachments;
	@JsonProperty("isduplicateTxn")
	private String isDuplicateTxn;

	@JsonProperty("CADEquivalentAmount")
	private Double cadEquivalentAmount;
		
	@JsonProperty("groupChannelSeqId")
	private String groupChannelSeqId;
	@JsonProperty("paymentCenterStatus")
	private String paymentCenterStatus;
	/**
	 * Gets the supp docs attachments.
	 *
	 * @return the supp docs attachments
	 */
	@JsonIgnore
	public List<SupportingDocumentES> getSuppDocsAttachments() {
		return suppDocsAttachments;
	}

	/**
	 * Sets the supp docs attachments.
	 *
	 * @param suppDocsAttachments
	 *            the new supp docs attachments
	 */
	@JsonProperty
	public void setSuppDocsAttachments(List<SupportingDocumentES> suppDocsAttachments) {
		this.suppDocsAttachments = suppDocsAttachments;
	}

	/**
	 * Gets the wht docs attachments.
	 *
	 * @return the wht docs attachments
	 */
	@JsonIgnore
	public List<WithholdingTaxES> getWhtDocsAttachments() {
		return whtDocsAttachments;
	}

	/**
	 * Sets the wht docs attachments.
	 *
	 * @param whtDocsAttachments
	 *            the new wht docs attachments
	 */
	@JsonProperty
	public void setWhtDocsAttachments(List<WithholdingTaxES> whtDocsAttachments) {
		this.whtDocsAttachments = whtDocsAttachments;
	}

	/**
	 * unique id needed by send money as CC2 requires it. Assignment ID field in
	 * case of cancel money.
	 * 
	 * @return msgId
	 **/
	// @ApiModelProperty(required = true, value = "unique id needed by send money as
	// CC2 requires it. Assignment ID field in case of cancel money.")
	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public Integer getFileNbOfTxs() {
		return fileNbOfTxs;
	}

	public void setFileNbOfTxs(Integer fileNbOfTxs) {
		this.fileNbOfTxs = fileNbOfTxs;
	}

	public Double getFileCtrlSum() {
		return fileCtrlSum;
	}

	public void setFileCtrlSum(Double fileCtrlSum) {
		this.fileCtrlSum = fileCtrlSum;
	}

	public String getFileAuthstnPrtry() {
		return fileAuthstnPrtry;
	}

	public void setFileAuthstnPrtry(String fileAuthstnPrtry) {
		this.fileAuthstnPrtry = fileAuthstnPrtry;
	}

	@JsonProperty(value = "pmtInfId")
	public String getPmtInfId() {
		return pmtInfId;
	}

	@JsonProperty(value = "pmtInfId")
	public void setPmtInfId(String pmtInfId) {
		this.pmtInfId = pmtInfId;
	}

	@JsonProperty(value = "batchNbOfTxs")
	public String getBatchNbOfTxs() {
		return batchNbOfTxs;
	}

	@JsonProperty(value = "batchNbOfTxs")
	public void setBatchNbOfTxs(String batchNbOfTxs) {
		this.batchNbOfTxs = batchNbOfTxs;
	}

	@JsonProperty(value = "batchCtrlSum")
	public Double getBatchCtrlSum() {
		return batchCtrlSum;
	}

	@JsonProperty(value = "batchCtrlSum")
	public void setBatchCtrlSum(Double batchCtrlSum) {
		this.batchCtrlSum = batchCtrlSum;
	}

	@JsonProperty("instructedAgentId")
	public String getInstructedAgentId() {
		return instructedAgentId;
	}

	@JsonProperty("instructedAgentId")
	public void setInstructedAgentId(String instructedAgentId) {
		this.instructedAgentId = instructedAgentId;
	}

	@JsonProperty("instructingAgentId")
	public String getInstructingAgentId() {
		return instructingAgentId;
	}

	@JsonProperty("instructingAgentId")
	public void setInstructingAgentId(String instructingAgentId) {
		this.instructingAgentId = instructingAgentId;
	}

	@JsonProperty("supplementaryData")
	public SupplementaryDataES getSupplementaryData() {
		return supplementaryData;
	}

	@JsonProperty("supplementaryData")
	public void setSupplementaryData(SupplementaryDataES supplementaryData) {
		this.supplementaryData = supplementaryData;
	}

	@JsonProperty("cancellationDetails")
	public CancellationDetailsES getCancellationDetails() {
		return cancellationDetails;
	}

	@JsonProperty("cancellationDetails")
	public void setCancellationDetails(CancellationDetailsES cancellationDetails) {
		this.cancellationDetails = cancellationDetails;
	}

	@JsonProperty("incomingRTP")
	public IncomingRTPES getIncomingRTP() {
		return incomingRTP;
	}

	@JsonProperty("incomingRTP")
	public void setIncomingRTP(IncomingRTPES incomingRTP) {
		this.incomingRTP = incomingRTP;
	}

	@JsonProperty("originalGroupInformationAndStatus")
	public OriginalGroupInformationAndStatusES getOriginalGroupInformationAndStatus() {
		return originalGroupInformationAndStatus;
	}

	@JsonProperty("originalGroupInformationAndStatus")
	public void setOriginalGroupInformationAndStatus(
			OriginalGroupInformationAndStatusES originalGroupInformationAndStatus) {
		this.originalGroupInformationAndStatus = originalGroupInformationAndStatus;
	}

	@JsonProperty("interacTransactionAmountLimit")
	public AmountInteractES getInteracTransactionAmountLimit() {
		return interacTransactionAmountLimit;
	}

	@JsonProperty("interacTransactionAmountLimit")
	public void setInteracTransactionAmountLimit(AmountInteractES interacTransactionAmountLimit) {
		this.interacTransactionAmountLimit = interacTransactionAmountLimit;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public PaymentConditionES getPaymentCondition() {
		return paymentCondition;
	}

	public void setPaymentCondition(PaymentConditionES paymentCondition) {
		this.paymentCondition = paymentCondition;
	}

	public InitiatingPartyES getInitiatingParty() {
		return initiatingParty;
	}

	public void setInitiatingParty(InitiatingPartyES initiatingParty) {
		this.initiatingParty = initiatingParty;
	}

	public String getFaeIndicator() {
		return faeIndicator;
	}

	public void setFaeIndicator(String faeIndicator) {
		this.faeIndicator = faeIndicator;
	}
	public String getIsDuplicatTxn() {
		return isDuplicateTxn;
	}
	public void setIsDuplicatTxn(String duplicateTxn) {
		this.isDuplicateTxn = duplicateTxn;
	}
}
