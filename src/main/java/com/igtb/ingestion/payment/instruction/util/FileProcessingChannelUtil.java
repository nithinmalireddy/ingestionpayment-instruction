package com.igtb.ingestion.payment.instruction.util; //NOPMD excessiveImports

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.igtb.api.ingestion.commons.file.decisionmatrix.FileTransitionData;
import com.igtb.api.ingestion.commons.file.models.FileStateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.RejectionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.api.ingestion.commons.util.CommonsIngestionConstants;
import com.igtb.event.IgtbEventMessage;
import com.igtb.ingestion.payment.instruction.client.QuestClient;
import com.igtb.ingestion.payment.instruction.config.AppPropsProvider;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.Metrics;
import com.igtb.ingestion.payment.instruction.es.models.MetricsObj;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.TxnCurrency;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.QuestException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataPayload;
import com.igtb.ingestion.payment.instruction.models.BackendPayload;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Entity;
import com.igtb.ingestion.payment.instruction.models.FileAvailablePayload;
import com.igtb.ingestion.payment.instruction.models.MetadataUpdateEventPayload;
import com.igtb.ingestion.payment.instruction.models.TransactionFileRelation;
import com.igtb.ingestion.payment.instruction.models.TxnData;
import com.igtb.ingestion.payment.instruction.models.WkflwConductorTriggers;
import com.igtb.ingestion.payment.instruction.models.WkflwContext;
import com.igtb.ingestion.payment.instruction.models.WorkflowInitiatorEvent;
import com.igtb.ingestion.payment.instruction.models.quest.AmountVariable;
import com.igtb.ingestion.payment.instruction.models.quest.QuestRequest;
import com.igtb.ingestion.payment.instruction.models.quest.QuestResponse;
import com.igtb.ingestion.payment.instruction.models.quest.QuestVariableEqAmount;

import io.searchbox.action.BulkableAction;
import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import lombok.AllArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * The Class FileProcessingChannelUtil.
 */
@Component

/**
 * Instantiates a new file processing channel util.
 *
 * @param paymentElasticProperties
 *            the payment elastic properties
 * @param paymentIngestionQueryUtil
 *            the payment ingestion query util
 * @param fileProcessingHelper
 *            the file processing helper
 * @param paymentRabbitProperties
 *            the payment rabbit properties
 * @param joseClientUtil
 *            the jose client util
 * @param producerTemplate
 *            the producer template
 * @param objectMapper
 *            the object mapper
 * @param props
 *            the props
 * @param questClient
 *            the quest client
 */
@AllArgsConstructor
public class FileProcessingChannelUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessingChannelUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The payment ingestion mapper. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The file processing helper. */
	private final FileProcessingHelper fileProcessingHelper;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;

	/** The jose client util. */
	private JoseClientUtil joseClientUtil;

	/** The producer template. */
	private final ProducerTemplate producerTemplate;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/** The props. */
	private final AppPropsProvider props;

	/** The quest client. */
	private final QuestClient questClient;

	/**
	 * Gets the existing P iset.
	 *
	 * @param id
	 *            the id
	 * @param status
	 *            the status
	 * @param outcomeCategory
	 *            the outcome category
	 * @return the existing P iset
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public PaymentInstructionSetsES getExistingPIset(final String id, final String status, final String outcomeCategory)
			throws MessageProcessingException {

		try {
			final String searchVariable;
			if ((Arrays.asList("file_avilable", "metadata_available", "metadata_unavailable", "draft", "val_success",
					"val_timeout", "val_failure").contains(status))
					|| ("trashed".equalsIgnoreCase(status)
							&& outcomeCategory.equalsIgnoreCase("channel-file-state-updated"))) {
				searchVariable = "recordSetId";
			} else {
				searchVariable = "stateInfo.channelSeqId";
			}

			final JestResult jestResult = paymentIngestionQueryUtil.fetchExistingDocument(searchVariable, id,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());

			PaymentInstructionSetsES paymentInstructionSetsES = null;
			if (StringUtils.isNotBlank(jestResult.getSourceAsString())) {
				paymentInstructionSetsES = objectMapper.readValue(jestResult.getSourceAsString(),
						new TypeReference<PaymentInstructionSetsES>() {
						});
			}
			return paymentInstructionSetsES;
		} catch (IOException e) {

			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_013 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_013_Desc);
			LOGGER.error("No Existing Record found for the requested fileId/channelseqid: {}, exception is: {}", id, e);
			throw new MessageProcessingException("Exception while fetching records");
		}

	}

	/**
	 * First insert.
	 *
	 * @param event
	 *            the event
	 * @param context
	 *            the context
	 * @param payload
	 *            the payload
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws RequeueException
	 *             the requeue exception
	 */
	public void firstInsert(final JsonNode event, final Context context, final FileAvailablePayload payload)
			throws IOException, DLQException, MessageProcessingException, RequeueException {
		LOGGER.debug("processing file available for fileId: {}", payload.getFileId());

		// building payment instruction sets
		final PaymentInstructionSetsES paymentInstructionSetsES = fileProcessingHelper.buildPymtInstrSets(payload,
				context);

		// building stateInfo
		final JsonNode stateInfoJson = objectMapper.convertValue(new StateInfo(), JsonNode.class);
		fileProcessingHelper.buildFileStateInfo(payload.getEntity(), event, context, stateInfoJson,
				PaymentIngestionConstant.FLA, paymentInstructionSetsES, "");
		paymentInstructionSetsES.setStatus(paymentInstructionSetsES.getStateInfo().getTransitionInfo()
				.getAdditionalInfo().getDesc().toUpperCase(Locale.ENGLISH));

		// insert payment instruction sets
		final JestResult jestResult = paymentIngestionQueryUtil.indexBuilder(paymentInstructionSetsES,
				payload.getFileId(), paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentInstructionSets());

		LOGGER.debug("Insert paymentInstructionSetsES for id: {}, isSucceeded: {}",
				paymentInstructionSetsES.getRecordSetId(), jestResult.isSucceeded());
	}

	/**
	 * Process metadata event.
	 *
	 * @param event
	 *            the event
	 * @param context
	 *            the context
	 * @param payload
	 *            the payload
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void processMetadataAvailableEvent(final JsonNode event, final Context context,
			final MetadataPayload payload, final PaymentInstructionSetsES paymentInstructionSetsES)
			throws IOException, RequeueException, MessageProcessingException, DataNotFoundException {

		LOGGER.debug("processing metadata event for fileId: {} and metadataFileId: {}", payload.getFileId(),
				payload.getMetadataFileId());

		// building payment instruction sets
		final Map<String, Object> pIAndPISetMap = fileProcessingHelper.buildPISetAndPIByMetadataFile(payload, context,
				paymentInstructionSetsES);

		// building stateInfo
		final FileStateInfo fileStateInfo = new FileStateInfo();
		fileStateInfo.setStateInfo(paymentInstructionSetsES.getStateInfo());
		fileStateInfo.setHasSets(paymentInstructionSetsES.getHasSets());
		fileStateInfo.setPartialApproval(paymentInstructionSetsES.getPartialApproval());
		final JsonNode stateInfoJson = objectMapper.convertValue(fileStateInfo, JsonNode.class);
		fileProcessingHelper.buildFileStateInfo(paymentInstructionSetsES.getCorporateEntity().getId(), event, context,
				stateInfoJson, PaymentIngestionConstant.FLA, paymentInstructionSetsES, payload.getStatus());
		paymentInstructionSetsES.setStatus(paymentInstructionSetsES.getStateInfo().getTransitionInfo()
				.getAdditionalInfo().getDesc().toUpperCase(Locale.ENGLISH));

		// check and build multi-currency support for existing PI set(file) and
		// inserting new PI set(bulk) and inserted records as PI

		// get equivalent amount from quest query
		Map<String, AmountVariable> hmTargetCcyAmounts = null;

		// updating existing PI set(file) and inserting new PI set(bulk) and inserted
		// records as PI
		updateAndInsertRecords(paymentInstructionSetsES, pIAndPISetMap, hmTargetCcyAmounts);

		// build and send event for workflow initiate
		if (!payload.getStatus().equalsIgnoreCase("NONE_VALID")) {
			buildAndSendWorkFlowEvent(paymentInstructionSetsES, (String) pIAndPISetMap.get("fileServiceKey"));
		}
		LOGGER.info("Metadata available event handling done for fileId: {}", payload.getFileId());
	}

	/**
	 * Gets the eq amount by quest query.
	 *
	 * @param hmAmountVariable
	 *            the hm amount variable
	 * @return the eq amount by quest query
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private Map<String, AmountVariable> getEqAmountByQuestQuery(final Map<String, AmountVariable> hmAmountVariable)
			throws IOException {
		final QuestVariableEqAmount questVariableEqAmount = new QuestVariableEqAmount();
		questVariableEqAmount.setTargetCcy(props.getTargetBaseCcy());
		questVariableEqAmount.setAmounts(new ArrayList<>(hmAmountVariable.values()));

		final QuestRequest questRequest = new QuestRequest();
		questRequest.setVariables(questVariableEqAmount);
		questRequest.setQuery(props.getGetEqAmountQuery());

		try {
			final QuestResponse questResponse = questClient.getEquivalentAmounts(questRequest);

			@SuppressWarnings({ "rawtypes" })
			final List<AmountVariable> targetCcyAmounts = objectMapper.readValue(
					objectMapper.writeValueAsString(((HashMap) questResponse.getData()).get("getEquivalentAmounts")),
					TypeFactory.defaultInstance().constructCollectionType(List.class, AmountVariable.class));
			final Map<String, AmountVariable> hmTargetCcyAmounts = targetCcyAmounts.stream()
					.collect(Collectors.toMap(AmountVariable::getRef, amt -> amt));
			LOGGER.debug("Requested amount for conversion: {}, response received: {}", hmAmountVariable.size(),
					targetCcyAmounts.size());

			return hmTargetCcyAmounts;

		} catch (QuestException e) {

			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_014 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_014_Desc);
			LOGGER.warn("QuestException occurred, so keeping amount as it is.");
		}

		return null;
	}

	/**
	 * Modify eq amount based on base currency.
	 *
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param pIAndPISetMap
	 *            the i and PI set map
	 * @return the map
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JSONException
	 *             the JSON exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@SuppressWarnings("unchecked")
	private Map<String, AmountVariable> modifyEqAmountBasedOnBaseCurrency(
			PaymentInstructionSetsES paymentInstructionSetsES, Map<String, Object> pIAndPISetMap) throws IOException {

		final Map<String, AmountVariable> hmAmountVariable = new HashMap<>();

		buildAmountVariableForPiSet(paymentInstructionSetsES.getRecordSetId(), paymentInstructionSetsES.getMetrics(),
				hmAmountVariable);

		// paymentInstructionSetsES.get
		if (pIAndPISetMap != null) {
			final List<PaymentInstructionSetsES> pISetsES = (List<PaymentInstructionSetsES>) pIAndPISetMap
					.get("pISets");

			for (final PaymentInstructionSetsES piSetBulk : pISetsES) {
				buildAmountVariableForPiSet(piSetBulk.getRecordSetId(), piSetBulk.getMetrics(), hmAmountVariable);
			}

			final List<PaymentsInstructionsES> payInstructions = (List<PaymentsInstructionsES>) pIAndPISetMap
					.get("payInstr");

			for (final PaymentsInstructionsES paymentsInstructionsES : payInstructions) {
				buildAmountVariableForPI(
						paymentsInstructionsES.getFileId() + "." + paymentsInstructionsES.getTransactionId(),
						paymentsInstructionsES.getInstructedAmount(), hmAmountVariable);
			}
		}
		LOGGER.debug("hmAmountVariable size after all PI and PiSet scanning: {}", hmAmountVariable.size());
		return hmAmountVariable;
	}

	/**
	 * Builds the amount variable for PI.
	 *
	 * @param refId
	 *            the ref id
	 * @param currentEqAmount
	 *            the current eq amount
	 * @param hmAmountVariable
	 *            the hm amount variable
	 */
	private void buildAmountVariableForPI(final String refId, final AmountES currentEqAmount,
			Map<String, AmountVariable> hmAmountVariable) {

		if (isDifferentCurrency(currentEqAmount.getCurrencyCode(), currentEqAmount.getValue())) {
			hmAmountVariable.put(refId,
					new AmountVariable(currentEqAmount.getValue(), currentEqAmount.getCurrencyCode(), refId));
		}
	}

	/**
	 * Builds the amount variable for pi set.
	 *
	 * @param refId
	 *            the ref id
	 * @param metrics
	 *            the metrics
	 * @param hmAmountVariable
	 *            the hm amount variable
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JSONException
	 *             the JSON exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void buildAmountVariableForPiSet(final String refId, final Metrics metrics,
			final Map<String, AmountVariable> hmAmountVariable) throws IOException {

		final JSONObject metricsObj = new JSONObject(metrics);
		for (final Object key : metricsObj.keySet()) {
			final MetricsObj amountObj = objectMapper.readValue(metricsObj.get((String) key).toString(),
					MetricsObj.class);

			if (isDifferentCurrency(amountObj.getCcyCode(), amountObj.getAmount())) {
				hmAmountVariable.put(refId + (String) key,
						new AmountVariable(amountObj.getAmount(), amountObj.getCcyCode(), refId + (String) key));
			}
		}

		LOGGER.debug("hmAmountVariable at PiSet is: {} for refId: {}", hmAmountVariable.size(), refId);
	}

	/**
	 * Checks if is different currency.
	 *
	 * @param currentCurrency
	 *            the current currency
	 * @param amount
	 *            the amount
	 * @return true, if is different currency
	 */
	private boolean isDifferentCurrency(final String currentCurrency, final Double amount) {
		return !(props.getTargetBaseCcy().equalsIgnoreCase(currentCurrency) && amount > 0);
	}

	/**
	 * Process metadata un available event.
	 *
	 * @param event
	 *            the event
	 * @param context
	 *            the context
	 * @param payload
	 *            the payload
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	private void processMetadataUnAvailableEvent(final JsonNode event, final Context context,
			final MetadataPayload payload, final PaymentInstructionSetsES paymentInstructionSetsES)
			throws IOException, MessageProcessingException {
		LOGGER.debug("processing metadata unavailable event for fileId: {} and metadataFileId: {}", payload.getFileId(),
				payload.getMetadataFileId());

		// building payment instruction sets
		fileProcessingHelper.buildPISetByMetadataFile(payload, paymentInstructionSetsES);
		final Map<String, Object> additionalInfo = paymentInstructionSetsES.getAdditionalInfo();
		additionalInfo.put("fileStatus", payload.getStatus());
		paymentInstructionSetsES.setAdditionalInfo(additionalInfo);

		// building stateInfo
		final FileStateInfo fileStateInfo = new FileStateInfo();
		fileStateInfo.setStateInfo(paymentInstructionSetsES.getStateInfo());
		fileStateInfo.setHasSets(paymentInstructionSetsES.getHasSets());
		fileStateInfo.setPartialApproval(paymentInstructionSetsES.getPartialApproval());
		final JsonNode stateInfoJson = objectMapper.convertValue(fileStateInfo, JsonNode.class);
		fileProcessingHelper.buildFileStateInfo(paymentInstructionSetsES.getCorporateEntity().getId(), event, context,
				stateInfoJson, PaymentIngestionConstant.FLA, paymentInstructionSetsES, "");
		paymentInstructionSetsES.setStatus(paymentInstructionSetsES.getStateInfo().getTransitionInfo()
				.getAdditionalInfo().getDesc().toUpperCase(Locale.ENGLISH));

		// updating existing PI set(file) and inserting new PI set(bulk) and inserted
		// records as PI
		updateAndInsertRecords(paymentInstructionSetsES, null, null);

		LOGGER.info("Metadata unavailable event handling done for fileId: {}", payload.getFileId());
	}

	/**
	 * Update and insert records.
	 *
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param pIAndPISetMap
	 *            the i and PI set map
	 * @param hmTargetCcyAmounts
	 *            the hm target ccy amounts
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException
	 *             the json processing exception
	 */
	@SuppressWarnings("unchecked")
	private void updateAndInsertRecords(final PaymentInstructionSetsES paymentInstructionSetsES,
			final Map<String, Object> pIAndPISetMap, final Map<String, AmountVariable> hmTargetCcyAmounts)
			throws IOException {

		if (hmTargetCcyAmounts != null && !hmTargetCcyAmounts.isEmpty()) {
			checkForTargetCcyAmountPiSet(paymentInstructionSetsES, hmTargetCcyAmounts);
		}

		// set InstructedAmountBaseCcy from metrics -> total after conversion
		paymentInstructionSetsES
				.setInstructedAmountBaseCcy(new AmountES(paymentInstructionSetsES.getMetrics().getTotal().getAmount(),
						paymentInstructionSetsES.getMetrics().getTotal().getCcyCode()));

		// update existing document
		JestResult jestResult = paymentIngestionQueryUtil.updateExistingDocument(paymentInstructionSetsES,
				paymentInstructionSetsES.getRecordSetId(), paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentInstructionSets(), null);

		LOGGER.debug("Update paymentInstructionSetsES for id: {}, isSucceeded: {}",
				paymentInstructionSetsES.getRecordSetId(), jestResult.isSucceeded());

		if (pIAndPISetMap != null) {
			final List<PaymentInstructionSetsES> pISetsES = (List<PaymentInstructionSetsES>) pIAndPISetMap
					.get("pISets");
			final List<PaymentsInstructionsES> payInstructions = (List<PaymentsInstructionsES>) pIAndPISetMap
					.get("payInstr");

			// insert PI set bulk
			final List<Index> pISetList = new ArrayList<Index>();
			for (final PaymentInstructionSetsES pISetES : pISetsES) {
				if (hmTargetCcyAmounts != null && !hmTargetCcyAmounts.isEmpty()) {
					checkForTargetCcyAmountPiSet(pISetES, hmTargetCcyAmounts);
				}
				pISetES.setInstructedAmountBaseCcy(new AmountES(pISetES.getMetrics().getTotal().getAmount(),
						pISetES.getMetrics().getTotal().getCcyCode()));
				pISetES.setTxnCurrency(new TxnCurrency(pISetES.getMetrics().getTotal().getCcyCode(),
						pISetES.getMetrics().getTotal().getCcyCode()));

				pISetList.add(new Index.Builder(objectMapper.writeValueAsString(pISetES)).id(pISetES.getRecordSetId())
						.build()); // NOPMD instantiating

			}
			if (!pISetList.isEmpty()) {
				jestResult = paymentIngestionQueryUtil.insertBulkRecord(paymentElasticProperties.getPymtIndex(),
						paymentElasticProperties.getPaymentInstructionSets(), pISetList);
				LOGGER.debug("Insert paymentInstructionSetsES bulks for id: {}, isSucceeded: {}",
						paymentInstructionSetsES.getRecordSetId(), jestResult.isSucceeded());
			}

			// insert payment instruction build by metadata records
			final List<Index> payInstrList = new ArrayList<Index>();
			for (final PaymentsInstructionsES pI : payInstructions) {
				String refId = pI.getFileId() + "." + pI.getTransactionId();

				// Update equivalent amount with targetCcy conversion
				if (hmTargetCcyAmounts != null && !hmTargetCcyAmounts.isEmpty()
						&& hmTargetCcyAmounts.get(refId) != null) {
					AmountVariable amtObj = hmTargetCcyAmounts.get(refId);
					pI.getInstructedAmount().setValue(amtObj.getValue());
					pI.getInstructedAmount().setCurrencyCode(amtObj.getCurrencyCode());
				}

				pI.setInstructedAmountBaseCcy(pI.getInstructedAmount());
				pI.setTxnCurrency(new TxnCurrency(pI.getInstructedAmount().getCurrencyCode(),
						pI.getInstructedAmount().getCurrencyCode()));

				payInstrList.add(new Index.Builder(objectMapper.writeValueAsString(pI)).id(refId).build()); // NOPMD
																											// Avoid
																											// instantiating

			}
			if (!payInstrList.isEmpty()) {
				jestResult = paymentIngestionQueryUtil.insertBulkRecord(paymentElasticProperties.getPymtIndex(),
						paymentElasticProperties.getPymtType(), payInstrList);
				LOGGER.debug("Insert paymentsInstructionsES bulks for id: {}, isSucceeded: {}",
						paymentInstructionSetsES.getRecordSetId(), jestResult.isSucceeded());
			}
		}
	}

	/**
	 * Check for target ccy amount pi set.
	 *
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param hmTargetCcyAmounts
	 *            the hm target ccy amounts
	 */
	private void checkForTargetCcyAmountPiSet(final PaymentInstructionSetsES paymentInstructionSetsES,
			Map<String, AmountVariable> hmTargetCcyAmounts) {
		final Metrics metrics = paymentInstructionSetsES.getMetrics();
		final JSONObject metricsObj = new JSONObject(metrics);
		for (final Object key : metricsObj.keySet()) {
			final String keyStr = (String) key;

			if (hmTargetCcyAmounts.get(paymentInstructionSetsES.getRecordSetId() + keyStr) != null) {
				AmountVariable amtObj = hmTargetCcyAmounts
						.get(paymentInstructionSetsES.getRecordSetId() + (String) key);
				if (keyStr.equals("total") && paymentInstructionSetsES.getMetrics().getTotal() != null) {
					paymentInstructionSetsES.getMetrics().getTotal().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getTotal().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("min") && paymentInstructionSetsES.getMetrics().getMin() != null) {
					paymentInstructionSetsES.getMetrics().getMin().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getMin().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("max") && paymentInstructionSetsES.getMetrics().getMax() != null) {
					paymentInstructionSetsES.getMetrics().getMax().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getMax().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("valid") && paymentInstructionSetsES.getMetrics().getValid() != null) {
					paymentInstructionSetsES.getMetrics().getValid().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getValid().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("invalid") && paymentInstructionSetsES.getMetrics().getInvalid() != null) {
					paymentInstructionSetsES.getMetrics().getInvalid().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getInvalid().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("approved") && paymentInstructionSetsES.getMetrics().getApproved() != null) {
					paymentInstructionSetsES.getMetrics().getApproved().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getApproved().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals(PaymentIngestionConstant.REJECTED)
						&& paymentInstructionSetsES.getMetrics().getRejected() != null) {
					paymentInstructionSetsES.getMetrics().getRejected().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getRejected().setCcyCode(amtObj.getCurrencyCode());
				} else if (keyStr.equals("unacted") && paymentInstructionSetsES.getMetrics().getUnacted() != null) {
					paymentInstructionSetsES.getMetrics().getUnacted().setAmount(amtObj.getValue());
					paymentInstructionSetsES.getMetrics().getUnacted().setCcyCode(amtObj.getCurrencyCode());
				}
			}

		}
	}

	/**
	 * Builds the and send work flow event.
	 *
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param fileServiceKey
	 *            the file service key
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	private void buildAndSendWorkFlowEvent(final PaymentInstructionSetsES paymentInstructionSetsES,
			final String fileServiceKey) throws MessageProcessingException {
		LOGGER.info("Building workflow initiator event for fileId: {}", paymentInstructionSetsES.getRecordSetId());

		final UserInfo userInfo = Optional.ofNullable(paymentInstructionSetsES.getStateInfo())
				.map(stateInfo -> stateInfo.getInitiated()).map(initiate -> initiate.getUserInfo()).orElse(null);
		if (userInfo == null) {
			throw new MessageProcessingException("UserInfo not found for initiate");
		}

		final WorkflowInitiatorEvent sendWkflwEvent = new WorkflowInitiatorEvent();
		sendWkflwEvent.setChannelId("cbx");
		sendWkflwEvent.setChannelSeqId(null);
		sendWkflwEvent.setDomainSeqId(null);
		sendWkflwEvent.setPayloadType("file_initiate");

		final WkflwContext context = new WkflwContext();
		context.setAction("initiate");
		context.setFunction("upload");
		context.setChannelSeqId("");
		context.setDomainSeqId("");
		context.setDomainId(userInfo.getDomainName());
		context.setOrgId(paymentInstructionSetsES.getAdditionalInfo().get(PaymentIngestionConstant.UPLOAD_USER_ORG_ID)
				.toString());
		context.setUserId(userInfo.getUserName());
		context.setServiceKey(fileServiceKey);

		context.setRequestType(paymentInstructionSetsES.getStateInfo().getTransitionInfo().getRequestType());
		context.setOtherRltdChSeqId(null);
		context.setRelatedChnlRequests(null);

		final WkflwConductorTriggers trigger = new WkflwConductorTriggers();
		trigger.setContext(context);

		final JSONObject obj = new JSONObject();
		obj.put("fileId", paymentInstructionSetsES.getRecordSetId());
		trigger.setPayload(obj);

		final List<WkflwConductorTriggers> conductorTriggers = new ArrayList<>();
		conductorTriggers.add(trigger);
		sendWkflwEvent.setConductorTriggers(conductorTriggers);

		final String workflowInitiatorRmqRoute = customUrlBuilder(
				paymentRabbitProperties.getSender().get("workflowInitiator"));
		LOGGER.debug("WokflowEvent created is {}", sendWkflwEvent);

		final String signedEvent = joseClientUtil.wkflwInitJwtSignedEvent(new JSONObject(sendWkflwEvent).toString());
		producerTemplate.sendBody(workflowInitiatorRmqRoute, signedEvent);

		LOGGER.info("file_initiate event sent to workflow initiator for fileId: {}",
				paymentInstructionSetsES.getRecordSetId());

	}

	/**
	 * Custom url builder.
	 *
	 * @param rmqConfig
	 *            the rmq config
	 * @return the string
	 */
	private String customUrlBuilder(final Map<String, String> rmqConfig) {
		return paymentRabbitProperties.getName() + "://" + paymentRabbitProperties.getHost() + ":"
				+ paymentRabbitProperties.getPort() + "/" + rmqConfig.get("exchange") + "?exchangeType="
				+ paymentRabbitProperties.getExchangeType() + "&username=" + paymentRabbitProperties.getUser()
				+ "&password=" + paymentRabbitProperties.getPassword() + "&autoDelete=false" + "&autoAck=false"
				+ "&automaticRecoveryEnabled=false" + "&routingKey=" + rmqConfig.get("routingKey") + "&queue="
				+ rmqConfig.get("queue") + "&vhost=" + paymentRabbitProperties.getVhost() + "&concurrentConsumers=3"
				+ "&prefetchEnabled=true" + "&prefetchCount=" + paymentRabbitProperties.getPrefatchCount()
				+ "&deadLetterExchange=" + rmqConfig.get("deadLetterExchange") + "&deadLetterRoutingKey="
				+ rmqConfig.get("deadLetterRoutingKey") + "&deadLetterQueue=" + rmqConfig.get("deadLetterQueue")
				+ "&deadLetterExchangeType=" + paymentRabbitProperties.getExchangeType();
	}

	/**
	 * File update.
	 *
	 * @param event
	 *            the event
	 * @param context
	 *            the context
	 * @param metadataPayload
	 *            the metadata payload
	 * @param existingPiSetES
	 *            the existing pi set ES
	 * @param paymentWF
	 *            the payment WF
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void fileUpdate(final JsonNode event, final Context context, final MetadataPayload metadataPayload,
			final PaymentInstructionSetsES existingPiSetES, final PaymentWF paymentWF)
			throws IOException, RequeueException, MessageProcessingException, DataNotFoundException {

		// handling METADATA_UNAVAILABLE status
		if (metadataPayload != null && context.getEventType().getStatus()
				.equalsIgnoreCase(CommonsIngestionConstants.METADATA_UNAVAILABLE)) {
			processMetadataUnAvailableEvent(event, context, metadataPayload, existingPiSetES);
		} else {
			// building fileStateInfo
			final FileStateInfo fileStateInfo = new FileStateInfo();
			fileStateInfo.setStateInfo(existingPiSetES.getStateInfo());
			fileStateInfo.setHasSets(existingPiSetES.getHasSets());
			fileStateInfo.setPartialApproval(existingPiSetES.getPartialApproval());
			final JsonNode stateInfoJson = objectMapper.convertValue(fileStateInfo, JsonNode.class);
			final String entity = Optional.ofNullable(paymentWF).map(PaymentWF::getEntity).map(Entity::getId)
					.orElse(Optional.ofNullable(existingPiSetES).map(PaymentInstructionSetsES::getCorporateEntity)
							.map(corpEntity -> corpEntity.getId()).orElse(""));
			fileProcessingHelper.buildFileStateInfo(entity, event, context, stateInfoJson, PaymentIngestionConstant.FLA,
					existingPiSetES, "");
			existingPiSetES.setStatus(existingPiSetES.getStateInfo().getTransitionInfo().getAdditionalInfo().getDesc()
					.toUpperCase(Locale.ENGLISH));

			paymentIngestionQueryUtil.updateExistingDocument(existingPiSetES, existingPiSetES.getRecordSetId(),
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets(),
					null);
			LOGGER.info("fileUpdate done for key: {}", existingPiSetES.getRecordSetId());
		}

		// trashed event will publish event for fileStore to soft delete a file
		if (context.getEventType().getStatus().equalsIgnoreCase(CommonsIngestionConstants.TRASHED)) {
			buildAndSentFileStoreUpdateEvent(context, existingPiSetES.getRecordSetId());
		}
	}

	/**
	 * Builds the and sent file store update event.
	 *
	 * @param context
	 *            the context
	 * @param fileId
	 *            the file id
	 * @throws JsonProcessingException
	 *             the json processing exception
	 */
	private void buildAndSentFileStoreUpdateEvent(final Context context, final String fileId)
			throws JsonProcessingException {
		final TransactionFileRelation transactionFileRelation = new TransactionFileRelation();
		transactionFileRelation.setDelink(new HashSet<String>(Arrays.asList(fileId)));

		final IgtbEventMessage igtbEventMessage = new IgtbEventMessage(context.getEventType().getServiceKey(),
				context.getChannelSeqId(), context.getDomainSeqId(), "FILESTORE_UPDATE",
				new MetadataUpdateEventPayload(context.getChannelSeqId(), context.getDomainSeqId(),
						transactionFileRelation),
				context.getEventSource().getRegion(), context.getEventSource().getCountry(), "1.0",
				"ingestion-paymentinstruction", context.getRequester().getId(), context.getRequester().getDomainId());

		final String fileStoreUpdateRmqRoute = customUrlBuilder(
				paymentRabbitProperties.getSender().get("fileStoreMetadataUpdate"));
		LOGGER.debug("fileStoreUpdateRmqRoute rabbitMq connection url: {} ", fileStoreUpdateRmqRoute);

		final String signedEvent = joseClientUtil.jwtSignedEvent(objectMapper.writeValueAsString(igtbEventMessage));
		LOGGER.debug("FILESTORE_UPDATE signed event as: {}", signedEvent);
		producerTemplate.sendBody(fileStoreUpdateRmqRoute, signedEvent);

		LOGGER.info("FILESTORE_UPDATE [Trashed handling] event sent to file store for fileId: {}", fileId);

	}

	/**
	 * Requeue message.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 * @param requeueMsg
	 *            the requeue msg
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void requeueMessage(final Context context, final FileTransitionData stateTransitionData,
			final String requeueMsg) throws RequeueException, InterruptedException, DLQException {

		final String contextEventTime = Optional.ofNullable(context.getEventTime()).orElse(null);
		if (contextEventTime == null) {
			LOGGER.error(
					"EventTime is missing in the request for the channelSeqId ---> {}, hence pushing the event to DLQ",
					context.getChannelSeqId());
			throw new DLQException("Inbound request eventTime is missing");
		}
		// formatting the eventtime to localdatetime type
		final LocalDateTime formattedEventTime = getFormattedDate(context.getEventTime());
		final LocalDateTime currentDateTime = LocalDateTime.now();
		final Duration d1 = Duration.between(formattedEventTime, currentDateTime);
		final Duration d2 = Duration.ofMinutes(paymentRabbitProperties.getRmqRequeueExpTime());
		if (d1.compareTo(d2) > 0) {
			LOGGER.error("Requeue Time expired for the channelSeqId ---> {}, hence pushing the event to DLQ",
					context.getChannelSeqId());
			throw new DLQException("Requeue Time expired");

		} else {
			Thread.sleep(paymentRabbitProperties.getRetrySleepTime());
			LOGGER.info(
					"Requeue reason ---->>>>> From State: {} From OutCome: {} >>>>>>>>><<<<<<<<<< To State: {} To Outcome: {} ",
					stateTransitionData.getFromState(), stateTransitionData.getFromOutcomeCategory(),
					stateTransitionData.getToState(), stateTransitionData.getToOutcomeCategory());
			throw new RequeueException("---->>>> " + requeueMsg);
		}

	}

	/**
	 * Gets the formatted date.
	 *
	 * @param eventTime
	 *            the event time
	 * @return the formatted date
	 */
	private LocalDateTime getFormattedDate(final String eventTime) {
		if (eventTime == null) {
			return null;
		}
		try {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			return LocalDateTime.parse(eventTime, formatter);
		} catch (DateTimeParseException e) {

			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_015 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_015_Desc);
			LOGGER.error("----->>>>>> Invalid EventTime format <<<<<-----");
			return null;
		}

	}

	/**
	 * File instr update.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @param backendPayload
	 *            the backend payload
	 * @param existingPiSetES
	 *            the existing pi set ES
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void fileInstrUpdate(final JsonNode message, final Context context, final BackendPayload backendPayload,
			final PaymentInstructionSetsES existingPiSetES)
			throws IOException, RequeueException, MessageProcessingException, DataNotFoundException {

		@SuppressWarnings("rawtypes")
		List<BulkableAction> payIstrList = new ArrayList<>();

		if (context.getEventType().getStatus().equalsIgnoreCase(CommonsIngestionConstants.BACKEND_REJECTED)) {
			final Map<String, String> matchStrings = new HashMap<>();
			matchStrings.put("fileId", backendPayload.getFileId());
			matchStrings.put("stateInfo.docStatus", "transition");
			final String[] fetchStrings = { "transactionId" };
			JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings,
					fetchStrings, paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTypes());

			final List<String> txnList = jestResult.getSourceAsStringList();
			for (final String txnId : txnList) {

				// build stateInfo and bulk action for update
				payIstrList.add(paymentIngestionQueryUtil.getUpdateBuilder(backendPayload.getFileId() + "." + txnId,
						buildStateInfoPI(PaymentIngestionConstant.REJECTED, backendPayload.getRejectReason(),
								context)));
			}
		} else if (context.getEventType().getStatus().equalsIgnoreCase(CommonsIngestionConstants.BACKEND_PROCESSED)) {
			for (final TxnData txnData : backendPayload.getTxnData()) {
				final String status = txnData.getStatus().replace("processed", "posted");
				payIstrList.add(paymentIngestionQueryUtil.getUpdateBuilder(
						backendPayload.getFileId() + "." + txnData.getInternalTxnId(),
						buildStateInfoPI(status, txnData.getRejectReason(), context)));
			}
		}

		// updating ES
		if (!payIstrList.isEmpty()) {
			paymentIngestionQueryUtil.bulkActionExecutor(payIstrList, paymentElasticProperties.getPymtIndex(),
					paymentElasticProperties.getPymtType());
		}

		// File level stateInfo update
		fileUpdate(message, context, null, existingPiSetES, null);

		LOGGER.debug("fileInstrUpdate completed for fileId: {}", backendPayload.getFileId());
	}

	/**
	 * Builds the state info PI.
	 *
	 * @param status
	 *            the status
	 * @param rejectReason
	 *            the reject reason
	 * @param context
	 *            the context
	 * @return the state info
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private ObjectNode buildStateInfoPI(final String status, final String rejectReason, final Context context)
			throws IOException {
		// build state info for PI
		final StateInfo stateInfo = new StateInfo();

		final TransitionInfo transitionInfo = new TransitionInfo();
		if (status.equalsIgnoreCase(PaymentIngestionConstant.REJECTED)) {
			final RejectionInfo rejectionInfo = new RejectionInfo();
			rejectionInfo.setSource("system");
			rejectionInfo.setMessage(rejectReason);
			transitionInfo.setRejectionInfo(rejectionInfo);
			stateInfo.setDocStatus("uncorrelated");
		} else {
			stateInfo.setDocStatus("correlated");
		}
		transitionInfo.setState(status);
		transitionInfo.setStatus("completed");
		transitionInfo.setRequestType(context.getEventType().getRequestType());
		transitionInfo.setOutcomeCategory(context.getEventType().getOutcomeCategory());
		transitionInfo.setLastActivityTs(context.getEventTime());
		stateInfo.setTransitionInfo(transitionInfo);

		return (ObjectNode) objectMapper.createObjectNode().set("stateInfo",
				objectMapper.readValue(objectMapper.writeValueAsString(stateInfo), JsonNode.class));
	}
}
