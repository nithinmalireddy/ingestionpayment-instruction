package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class PaymentsNotification.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@Setter
public class PaymentsNotification {
	
	
	/** The event. */
	@JsonProperty("event")
	private String event = null;

	/** The notf_id. */
	@JsonProperty("notf_id")
	private String notificationId = null;
	

	@JsonProperty("notf_type")
	private String notfType = null;

	/** The notf_category. */
	@JsonProperty("notf_category")
	private String ntfcnCategory = null;


	@JsonProperty("notf_name")
	private String notfName = null;

	/** The creditor account. */
	@JsonProperty("name")
	private String name;


	@JsonProperty("name_type")
	private String nameType;


	@JsonProperty("debtorName")
	private String debtorName;

	/** The debtor account. */
	@JsonProperty("creditorName")
	private String creditorName;


	@JsonProperty("instAmtcurrency")
	private String instAmtcurrency;

	/** The instructed amount. */
	@JsonProperty("instAmtamount")
	private double instAmtamount;

	@JsonProperty("puid")
	private String participantUserId = null;

	@JsonProperty("interac_id")
	private String interacId = null;

	@JsonProperty("memo")
	private String message = null;

	@JsonProperty("priority")
	private int priority;

	@JsonProperty("creation_date")
	private String creationDate = null;
	
	@JsonProperty("reference_number")
	private String referenceNumber = null;
	
	
	@JsonProperty("participant_id")
	private String participantId = null;

	@JsonProperty("status")
	private String status = null;

	@JsonProperty("decline_reason")
	private String declinereason = null;

	@JsonProperty("account_alias_handle")
	private String accountAliasHandle = null;

	@JsonProperty("account_alias_reference")
	private String accountAliasReference = null;
}
