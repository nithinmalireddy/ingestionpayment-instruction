package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
@Getter
@Setter
public class CreditorNotificationAccountES {
	
	@JsonProperty("id")
	private String idES = null;
	
	@JsonProperty("name")
	private String nameES = null;
	
	@JsonProperty("accountNo")
	private String accountNoES = null;
	
	
}
