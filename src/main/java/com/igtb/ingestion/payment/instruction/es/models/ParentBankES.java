package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ParentBankES.
 *
 */
@ToString
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class ParentBankES {

	/** The code. */
	private String code;
}
