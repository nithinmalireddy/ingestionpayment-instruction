
package com.igtb.ingestion.payment.instruction.es.models; 

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "email",
    "phone"
})
public class NotificationChannelES {

    @JsonProperty("email")
    private EmailES email;
    @JsonProperty("phone")
    private PhoneES phone;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("email")
    public EmailES getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(EmailES email) {
        this.email = email;
    }

    @JsonProperty("phone")
    public PhoneES getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(PhoneES phone) {
        this.phone = phone;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
