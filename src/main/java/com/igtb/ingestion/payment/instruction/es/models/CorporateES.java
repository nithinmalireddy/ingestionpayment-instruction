package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CorporateES.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CorporateES {

	/** The id. */
	private String id;
	
	/** The key. */
	private String key;
	
}
