
package com.igtb.ingestion.payment.instruction.models;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class FileAvailablePayload.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileAvailablePayload {

    /** The service key. */
	@NotEmpty
    public String serviceKey;
    
    /** The entity. */
    private String entity;
    
    /** The file id. */
    @NotEmpty
    public String fileId;
    
    /** The file name. */
    private String fileName;
    
    /** The file type. */
    private String fileType;
    
    /** The file hash. */
    private String fileHash;
    
    /** The file hash fn. */
    private String fileHashFn;
    
    /** The file uri. */
    private String fileUri;
    
    /** The file size. */
    private Long fileSize;
    
    /** The file category. */
    private String fileCategory;
    
    /** The template. */
    private Template template;
    
    /** The bank. */
    private Bank bank;
    
    /** The upload date. */
    private String uploadDate;
    
    /** The upload user. */
    private UploadUser uploadUser;

    /** The additional properties. */
    private Map<String, Object> additionalProperties = new HashMap();
    
    @SuppressWarnings("unchecked")
	@JsonProperty("payload")
    private void getAdditionalProperties(final Map<String, Object> payload) { //NOPMD unusedPrivateMethod
    	this.additionalProperties = (Map<String, Object>) ((Map<String, Object>)payload.get("file")).get("tag");
    }
}
