
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class ChargeBearer.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the charge types.
 *
 * @return the charge types
 */
@Getter

/**
 * Sets the charge types.
 *
 * @param chargeTypes the new charge types
 */
@Setter
public class ChargeBearer {

    /** The charge types. */
    @JsonProperty("chargeTypes")
    public String chargeTypes;

}
