package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.WithholdingTaxES;
import com.igtb.ingestion.payment.instruction.models.TaxDetail;

import io.searchbox.core.Index;

/**
 * Withholding documents Utility.
 * 
 */
@Component
public class WitholdingTaxInfoUtil {
	
	/** WithHoldingTxList. */
	private List<WithholdingTaxES> withHoldingTxList;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(WitholdingTaxInfoUtil.class);
	
	/**
	 * Common method to persist i.e populate withholding tax list and index them in ES.
	 *
	 * @param taxDetails the tax details
	 * @param stateInfo the state info
	 * @return the WHT docs index list
	 * @throws IOException exception
	 */
	public List<Index> getWHTDocsIndexList(final List<TaxDetail> taxDetails, final StateInfo stateInfo) throws IOException {
		LOGGER.debug(">.............Started WitholdingTaxInfoUtil :: persistWHTDocsinES.............<");
		populateWHTDocsList(taxDetails, stateInfo); // populate the wht and create list of all attachments
		if(withHoldingTxList != null && withHoldingTxList.size() > 0) {
			return getWHTDocsIndexList(); // index all WHT docs in ES
		} else {
			LOGGER.warn("Returning from WitholdingTaxInfoUtil :: populateWHTDocsList empty");
			return null;
		}
	}

	/** 
	 * Create a withholding taxinfo documents list to be inserted in the elastic search.
	 * @param stateInfo 
	 * @param taxDetails 
	 * 
	 * @throws IOException exception
	 * @throws JsonProcessingException exception
	 */
	private void populateWHTDocsList(final List<TaxDetail> taxDetails, final StateInfo stateInfo) {
		LOGGER.info("Started WitholdingTaxInfoUtil :: populateWHTDocsList");

		withHoldingTxList = new ArrayList<>();
		if(taxDetails == null) {
			LOGGER.debug("No populateWHTDocsList found");
			return;
		}
		for (final TaxDetail taxDetail : taxDetails) {

			final WithholdingTaxES withHoldingTx = new WithholdingTaxES();
			withHoldingTx.setWhtIncomeTypeCode(taxDetail.getWhtIncomeTypeCode());
			withHoldingTx.setIncomeTypeDescription(taxDetail.getIncomeTypeDescription());
			withHoldingTx.setWhtDeductionDate(taxDetail.getWhtDeductionDate());
			withHoldingTx.setTaxRate(Float.parseFloat(taxDetail.getTaxRate()));

			// amount to be taxed
			final AmountES amtToBeTax = new AmountES();
			amtToBeTax.setValue(Double.parseDouble(taxDetail.getAmounttobeTaxed()));
			amtToBeTax.setCurrencyCode(taxDetail.getTaxableCurrency());
			withHoldingTx.setAmounttobeTaxed(amtToBeTax);

			// tax amount
			final AmountES taxAmount = new AmountES();
			taxAmount.setValue(Double.parseDouble(taxDetail.getTaxAmount()));
			taxAmount.setCurrencyCode(taxDetail.getTaxableCurrency());
			withHoldingTx.setTaxAmount(taxAmount);

			withHoldingTx.setParentInstructionId(stateInfo.getChannelSeqId());
			withHoldingTx.setParentTransactionId(stateInfo.getDomainSeqId());
			withHoldingTx.setParentDocStatus(stateInfo.getDocStatus());
			withHoldingTxList.add(withHoldingTx);
		}
	}
	
	/**
	 * Helper method to bulk index the withholding taxinfo documents.
	 *
	 * @return the WHT docs index list
	 */
	private List<Index> getWHTDocsIndexList() {
		LOGGER.info("Started WitholdingTaxInfoUtil :: addWHTDocsinES");
		final List<Index> listWHTIndex = new ArrayList<>();
		if(withHoldingTxList != null) {
			LOGGER.info("List of withholding documents to be inserted are -> {}",  withHoldingTxList);
			for(final WithholdingTaxES whtDoc : withHoldingTxList) {
				listWHTIndex.add(new Index.Builder(whtDoc).build());
			}
			
			return listWHTIndex;
			
		}
		return null;
	}

	
}
