
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "desc",
    "actionDesc",
    "lastActivityTs",
    "txnAmount",
    "requestedExecutionDate"
})
public class ActivityInfoES {

    @JsonProperty("desc")
    private String desc;
    @JsonProperty("actionDesc")
    private String actionDesc;
    @JsonProperty("lastActivityTs")
    private String lastActivityTs;
    @JsonProperty("txnAmount")
    private TxnAmountES txnAmount;
    @JsonProperty("requestedExecutionDate")
    private String requestedExecutionDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonProperty("actionDesc")
    public String getActionDesc() {
        return actionDesc;
    }

    @JsonProperty("actionDesc")
    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }

    @JsonProperty("lastActivityTs")
    public String getLastActivityTs() {
        return lastActivityTs;
    }

    @JsonProperty("lastActivityTs")
    public void setLastActivityTs(String lastActivityTs) {
        this.lastActivityTs = lastActivityTs;
    }

    @JsonProperty("txnAmount")
    public TxnAmountES getTxnAmount() {
        return txnAmount;
    }

    @JsonProperty("txnAmount")
    public void setTxnAmount(TxnAmountES txnAmount) {
        this.txnAmount = txnAmount;
    }

    @JsonProperty("requestedExecutionDate")
    public String getRequestedExecutionDate() {
        return requestedExecutionDate;
    }

    @JsonProperty("requestedExecutionDate")
    public void setRequestedExecutionDate(String requestedExecutionDate) {
        this.requestedExecutionDate = requestedExecutionDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
