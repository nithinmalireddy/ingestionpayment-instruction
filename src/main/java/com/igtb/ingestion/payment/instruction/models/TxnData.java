package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class TxnData.
 */
@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TxnData {

	/** The internal txn id. */
	private String internalTxnId;
	
	/** The status. */
	private String status;
	
	/** The reject reason. */
	private String rejectReason;

}
