package com.igtb.ingestion.payment.instruction.es.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.ToString;

@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)

/**
 * The Class PaymentRailES.
 */
public class PaymentRailES {
	
	/** The payment reason code. */
//	@NotNull
	private String code;
	
	/** The payment reason description. */
	private String description;

	/**
	 * Gets the code. 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * @param code the code to set
	 */
	public void setCode(final String code) {
		this.code = code;
	}

	/**
	 * Gets the description. 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * @param description the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

}
