package com.igtb.ingestion.payment.instruction.models.quest;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class QuestVariableEqAmount.
 */
@Getter
@Setter
@ToString
public class QuestVariableEqAmount {

	/** The target ccy. */
	public String targetCcy;
	
	/** The amounts. */
	public List<AmountVariable> amounts = null;

}