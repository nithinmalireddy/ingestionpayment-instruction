package com.igtb.ingestion.payment.instruction.commons.decision.cibc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class StateTransitionData
{
  private String fromOutcomeCategory;
  private String fromState;
  private String fromEventTime;
  private String toOutcomeCategory;
  private String toState;
  private String toEventTime;

  public String getFromEventTime()
  {
    return this.fromEventTime;
  }

  public void setFromEventTime(String fromEventTime) {
    this.fromEventTime = fromEventTime;
  }

  public String getToEventTime() {
    return this.toEventTime;
  }

  public void setToEventTime(String toEventTime) {
    this.toEventTime = toEventTime;
  }

  public String getFromState() {
    return this.fromState;
  }

  public void setFromState(String fromState) {
    this.fromState = fromState;
  }

  public String getToState() {
    return this.toState;
  }

  public void setToState(String toState) {
    this.toState = toState;
  }

  public String getFromOutcomeCategory() {
    return this.fromOutcomeCategory;
  }

  public void setFromOutcomeCategory(String fromOutcomeCategory) {
    this.fromOutcomeCategory = fromOutcomeCategory;
  }

  public String getToOutcomeCategory() {
    return this.toOutcomeCategory;
  }

  public void setToOutcomeCategory(String toOutcomeCategory) {
    this.toOutcomeCategory = toOutcomeCategory;
  }

  public String toString()
  {
    return "StateTransitionData [fromOutcomeCategory=" + this.fromOutcomeCategory + ", fromState=" + this.fromState + ", fromEventTime=" + this.fromEventTime + ", toOutcomeCategory=" + this.toOutcomeCategory + ", toState=" + this.toState + ", toEventTime=" + this.toEventTime + "]";
  }
}