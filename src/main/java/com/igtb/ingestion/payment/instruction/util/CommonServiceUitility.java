package com.igtb.ingestion.payment.instruction.util;

import java.util.UUID;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentIngestionProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import lombok.extern.slf4j.Slf4j;

/**
 * Logic to generate unique request id(for IIL),get current timestamp and update
 * header parameter
 */
@Slf4j
@Component
public class CommonServiceUitility {
	
	/** The payment ingestion properties. */
	@Autowired
	private PaymentIngestionProperties paymentIngestionProperties;
	
	@Autowired
	private JestClient jest;
	Logger logger = LoggerFactory.getLogger(CommonServiceUitility.class);

	public HttpHeaders setHeaderForActionEndPoint(String jwtToken) {
		HttpHeaders header = new HttpHeaders();
		String requestId = UUID.randomUUID().toString();
		// System.out.println("requestId::"+requestId);
		header.set("X-Correlation-Id", requestId);
		header.set("x-et-request-id", requestId);
		header.set("Authorization", jwtToken);
		header.setContentType(MediaType.APPLICATION_JSON);
		return header;
	}

	public HttpHeaders setHeaderForNewJwt(String dummyJWT) {
		HttpHeaders header = new HttpHeaders();
		header.set("Authorization", dummyJWT);
		header.setContentType(MediaType.APPLICATION_JSON);
		return header;
	}

	public JsonObject getUserInfo(String channelSeqId) {
		int countOfRecords = 0;
		JsonObject userInfo = null;
		final SearchSourceBuilder searchSrcBuilder = new SearchSourceBuilder();

		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("_id", channelSeqId));
		searchSrcBuilder.query(query);

		final Search search = new Search.Builder(searchSrcBuilder.toString())
				.addIndex(paymentIngestionProperties.getIndex()).addType(paymentIngestionProperties.getType()).build();

		try {
			final JestResult result = jest.execute(search);

			if (result.isSucceeded()) {
				countOfRecords = result.getJsonObject().get(paymentIngestionProperties.getEsHits()).getAsJsonObject()
						.get(paymentIngestionProperties.getEsHitCount()).getAsInt();
				logger.info("{} record(s) found", countOfRecords);

				if (countOfRecords > 0) {
					JsonElement record = null;

					JsonElement elements = result.getJsonObject().get(paymentIngestionProperties.getEsHits())
							.getAsJsonObject().get(paymentIngestionProperties.getEsHits());
					logger.info("elements ::{}", elements);
					record = elements.getAsJsonArray().get(0);
					logger.info("record ::{}", record);
					userInfo = record.getAsJsonObject().get(paymentIngestionProperties.getEsSource()).getAsJsonObject()
							.get("stateInfo").getAsJsonObject().get("initiated").getAsJsonObject().get("userInfo")
							.getAsJsonObject();
					userInfo.add("organisationId", userInfo.get("domainName"));
					// JsonObject
					// email=userInfo.get("email").getAsJsonArray().get(0).getAsJsonObject();
					// logger.info("email ::{}" ,userInfo.get("email").getAsJsonArray());
					// userInfo.remove("email");
					userInfo.add("emailId", userInfo.get("email").getAsJsonArray().get(0));
					userInfo.add("domainId", userInfo.get("domainName"));

					// userInfo=record.getAsJsonObject().get(interfaceConstants.getEsSource()).getAsJsonObject().get(interfaceConstants.getDebtorAccount()).getAsJsonObject().get(interfaceConstants.getAccountNumber()).getAsString();
					logger.info("userInfo ::{}", userInfo.toString());

				}

			}
		} catch (ElasticsearchException e) {
			logger.error("Query = {}", query);
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_007 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_007_Desc);
			logger.error("Exception in elasticsearch {}", e.getMessage());
		} catch (Exception e) {
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_008 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_008_Desc);
			logger.error("Exception found {}", e.getMessage());
		}
		return userInfo;
	}

}
