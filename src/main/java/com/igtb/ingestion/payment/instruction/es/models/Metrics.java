
package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Metrics.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Metrics {

    /** The total. */
    private MetricsObj total;
    
    /** The valid. */
    private MetricsObj valid;
    
    /** The invalid. */
    private MetricsObj invalid;
    
    /** The approved. */
    private MetricsObj approved;
    
    /** The rejected. */
    private MetricsObj rejected;
    
    /** The unacted. */
    private MetricsObj unacted;
    
    /** The min. */
    private MetricsObj min;
    
    /** The max. */
    private MetricsObj max;

}
