
package com.igtb.ingestion.payment.instruction.models.irtp;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.api.ingestion.commons.transhistory.models.RejectionInfo;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "channelSeqId",
    "domainSeqId",
    "requestInfo",
    "key",
    "desc",
    "rejectionInfo",
    "ts",
    "userInfo",
    "entityInfo"
})
public class PymntInstrTransHistory {

    @JsonProperty("channelSeqId")
    private String channelSeqId;
    @JsonProperty("domainSeqId")
    private String domainSeqId;
    @JsonProperty("requestInfo")
    private RequestInfo requestInfo;
    @JsonProperty("key")
    private String key;
    @JsonProperty("desc")
    private String desc;
    @JsonProperty("rejectionInfo")
    private RejectionInfo rejectionInfo;
    @JsonProperty("ts")
    private String ts;
    @JsonProperty("userInfo")
    private List<UserInfo> userInfo = null;
    @JsonProperty("entityInfo")
    private Object entityInfo;

    @JsonProperty("channelSeqId")
    public String getChannelSeqId() {
        return channelSeqId;
    }

    @JsonProperty("channelSeqId")
    public void setChannelSeqId(String channelSeqId) {
        this.channelSeqId = channelSeqId;
    }

    @JsonProperty("domainSeqId")
    public String getDomainSeqId() {
        return domainSeqId;
    }

    @JsonProperty("domainSeqId")
    public void setDomainSeqId(String domainSeqId) {
        this.domainSeqId = domainSeqId;
    }

    @JsonProperty("requestInfo")
    public RequestInfo getRequestInfo() {
        return requestInfo;
    }

    @JsonProperty("requestInfo")
    public void setRequestInfo(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonProperty("rejectionInfo")
    public RejectionInfo getRejectionInfo() {
        return rejectionInfo;
    }

    @JsonProperty("rejectionInfo")
    public void setRejectionInfo(RejectionInfo rejectionInfo) {
        this.rejectionInfo = rejectionInfo;
    }

    @JsonProperty("ts")
    public String getTs() {
        return ts;
    }

    @JsonProperty("ts")
    public void setTs(String ts) {
        this.ts = ts;
    }

    @JsonProperty("userInfo")
    public List<UserInfo> getUserInfo() {
        return userInfo;
    }

    @JsonProperty("userInfo")
    public void setUserInfo(List<UserInfo> userInfo) {
        this.userInfo = userInfo;
    }

    @JsonProperty("entityInfo")
    public Object getEntityInfo() {
        return entityInfo;
    }

    @JsonProperty("entityInfo")
    public void setEntityInfo(Object entityInfo) {
        this.entityInfo = entityInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("channelSeqId", channelSeqId).append("domainSeqId", domainSeqId).append("requestInfo", requestInfo).append("key", key).append("desc", desc).append("rejectionInfo", rejectionInfo).append("ts", ts).append("userInfo", userInfo).append("entityInfo", entityInfo).toString();
    }

}
