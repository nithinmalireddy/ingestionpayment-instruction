package com.igtb.ingestion.payment.instruction.consumer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.apache.camel.ProducerTemplate;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;
import com.igtb.ingestion.payment.instruction.service.PaymentIngestionServiceImpl;
import com.igtb.ingestion.payment.instruction.util.EventValidationUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionRedisUtil;
import com.igtb.ingestion.payment.instruction.models.Context;

import lombok.AllArgsConstructor;

/**
 * Consumer class which listens to the kafka topic.
 */
@Component
@DependsOn("paymentKafkaDLQProducer")
@AllArgsConstructor
public class PaymentKafkaConsumer implements AcknowledgingMessageListener<String, String> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentKafkaConsumer.class);
	

	/** concurrency consistency. */
	private final CountDownLatch latch = new CountDownLatch(1);

	/** Kafka producer to send to Dead Letter Queue. */
	private PaymentKafkaDLQProducer paymentKafkaDLQProducer;

	/** The payment ingestion service impl. */
	private PaymentIngestionServiceImpl serviceImpl;

	/** ObjectMapper to convert string to desired type. */
	private ObjectMapper objectMapper = new ObjectMapper();

	/** The producer template. */
	private ProducerTemplate producerTemplate;

	/** The event validation config. */
	private final EventValidationConfig eventValidationConfig;

	/** The event validation util. */
	private final EventValidationUtil eventValidationUtil;
	
	@Autowired
	private PaymentKafkaProperties kafkaProperties;

	/**
	 * Kafka Message receiver. 1. Read Kafka Message 2. Validate the message if
	 * malformed message, write into Kafka DLQ and commit the message If valid,
	 * continue to ES. 3. Send to Elasticsearch if connection not established or ES
	 * client returns isSucceeded false, raise issue and fail the server. Do not
	 * commit the message
	 *
	 * @param data
	 *            the data
	 * @param acknowledgment
	 *            the acknowledgment
	 */
	@SuppressWarnings("unused")
	@Override
	public void onMessage(final ConsumerRecord<String, String> data, final Acknowledgment acknowledgment) {
		Optional.ofNullable(data).ifPresent(record -> {
			//boolean isBackendUpdatedFAE = false;
			try{
				final JsonNode jsonNode = objectMapper.readTree(record.value());
				LOGGER.info("Backend event: {}", jsonNode);
				LOGGER.debug("Validating backend event eventId: {}", jsonNode.get(PaymentIngestionConstant.ID));
				JsonNode contextJson = jsonNode.get(PaymentIngestionConstant.CONTEXT);
				//Updating eventTime to format "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
				String requiredEventTimeStr=null;
				String value = contextJson.get(PaymentIngestionConstant.EVENTTIME).asText();
				if (!isValidFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",value, Locale.ENGLISH))
				{
					DateTimeFormatter requiredFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
					DateTimeFormatter currFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"); //.ISO_LOCAL_DATE_TIME;//'2011-12-03T10:15:30'
					String currEventTimeStr = value;
					LocalDateTime currEventTimeParsed =  LocalDateTime.parse(currEventTimeStr, currFormatter);
					requiredEventTimeStr = currEventTimeParsed.format(requiredFormatter);
					((ObjectNode) ((ObjectNode) jsonNode).get(PaymentIngestionConstant.CONTEXT)).put(PaymentIngestionConstant.EVENTTIME,requiredEventTimeStr);
					LOGGER.info("JSONNODE AFTER EVENT TIME CONVERSION::: {}", jsonNode);
				}
				Context context;
				if (contextJson == null) {
					throw new NullPointerException();
				} else {
					context = objectMapper.readValue(contextJson.toString(), Context.class);
					LOGGER.info("Context updated with miliseconds::{}",context);
				}
				final String eventStatus = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.STATUS)).map(status -> status.textValue()).orElse(null);
				final String requestType = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.REQUEST_TYPE)).map(reqType -> reqType.textValue()).orElse(null);
				final Map<String, ArrayList<String>> singlePIConfig = eventValidationConfig.getValidation().get(PaymentIngestionConstant.PAYMENT_INSTRUCTION_EVENTS);

				//start not required for CIBC as current 
				//final Map<String, ArrayList<String>> fileEventsConfig = eventValidationConfig.getValidation().get(PaymentIngestionConstant.FILE_EVENTS);
				//final Map<String, ArrayList<String>> bulkEventsConfig = eventValidationConfig.getValidation().get(PaymentIngestionConstant.BULK_EVENTS);

				//end 
				LOGGER.info("Value:::::{}",context.getEventType().getOutcomeCategory().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_STATE_UPDATED));
					if (eventValidationUtil.isValidEvent(jsonNode,singlePIConfig.get(PaymentIngestionConstant.SUPPORTED_REQUESTD_TYPE),singlePIConfig.get(PaymentIngestionConstant.SUPPORTED_SERVICE_KEY))) 
				{
					String eventType = "";
					if (singlePIConfig.get(PaymentIngestionConstant.SUPPORTED_REQUESTD_TYPE).contains(requestType)) {
						eventType = PaymentIngestionConstant.SINGLE;
						
					}





					//Start CIBC transition event for transition data(updating record on channelSeqID)
					if (PaymentIngestionConstant.BACKEND_ACK_SUCC.equalsIgnoreCase(eventStatus)|| PaymentIngestionConstant.BACKEND_REJECTED.equalsIgnoreCase(eventStatus)|| PaymentIngestionConstant.BACKEND_PROCESSED.equalsIgnoreCase(eventStatus)) 
					{
						if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
							Thread.sleep(10000);
						}
						acknowledgment.acknowledge();
						latch.countDown();
						((ObjectNode) ((ObjectNode) jsonNode).get(PaymentIngestionConstant.CONTEXT)).put(PaymentIngestionConstant.EVENTTIME,ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ISO_INSTANT));
						LOGGER.info("<<<<< Unwind call stopped from ingestion api as the same will be handled from Workflow and new limits module >>>>>");
						//FAE Unwind call for sendmoney/sendwires/fulfillRtp
						if (PaymentIngestionConstant.BACKEND_REJECTED.equalsIgnoreCase(eventStatus) && (!context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP))) {
							//serviceImpl.processUnwindCallOnFaeHold(context);
							LOGGER.info("Backend request: {}"+jsonNode+" on kafka message on PaymentKafkaPshcConsumer has been received.. Sending message on Limits topic:: {}", kafkaProperties.getKafkaLimitsTopic());
							
							if(jsonNode.has(PaymentIngestionConstant.CONTEXT) && jsonNode.get(PaymentIngestionConstant.CONTEXT) != null) {
								JsonNode contextJsonNode = jsonNode.get(PaymentIngestionConstant.CONTEXT);
								Context contextForLimits = objectMapper.readValue(contextJsonNode.toString(), Context.class);
								if(contextForLimits.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_REJECTED)) {
									LOGGER.info("Request status is "+contextForLimits.getEventType().getStatus()+" hence proceeding to limits...");
									paymentKafkaDLQProducer.sendMessageToLimits(kafkaProperties.getKafkaLimitsTopic(), record.value());
								} else {
									LOGGER.info("Request status is "+contextForLimits.getEventType().getStatus()+" hence can not proceed to limits...");
								}
							}
						}
						// publishing transition events from kafka to payment instruction specific
						// rabbitmq endpoint
						// so that it will be consumed by the rabbit listener and the same
						// stateingestion logic will be applied on it.
						
						/*if (PaymentIngestionConstant.BACKEND_ACK_SUCC.equalsIgnoreCase(eventStatus)) {
							serviceImpl.updateTransitionRecord(context);
						}*/
						
						if (eventType.equals(PaymentIngestionConstant.SINGLE)) {
							LOGGER.info("Sending kafka event to direct:stateeventsfromkafka");
							producerTemplate.requestBody("direct:stateeventsfromkafka", jsonNode.toString());
							LOGGER.info("kafka event to direct:stateeventsfromkafka has Processed");
						} else if (eventType.equals(PaymentIngestionConstant.FILE)) {
							LOGGER.debug("Sending kafka event to direct:filestateeventsfromkafka");
							producerTemplate.requestBody("direct:filestateeventsfromkafka", jsonNode.toString());
						} else if (eventType.equals(PaymentIngestionConstant.BULK)) {
							LOGGER.debug("Sending kafka event to direct:bulkstateeventsfromkafka");
							producerTemplate.requestBody("direct:bulkstateeventsfromkafka", jsonNode.toString());
						}
						if(PaymentIngestionConstant.BACKEND_ACK_SUCC.equalsIgnoreCase(eventStatus)) {
							PaymentIngestionRedisUtil paymentIngestionRedisUtil = new PaymentIngestionRedisUtil();
							paymentIngestionRedisUtil.pushBackendCreateDataToRedis(context.getChannelSeqId()+eventStatus,"Y");
							JsonNode lStoredJsonNode = isAnyNotificationRcvdBeforeAck(context,PaymentIngestionConstant.BACKEND_CREATED) ;
							if(lStoredJsonNode!=null) {
								LOGGER.debug("Message identified for request id :"+context.getChannelSeqId());
								serviceImpl.processKafkaMessage(lStoredJsonNode, PaymentIngestionConstant.SINGLE);
								paymentIngestionRedisUtil.removeKeyFromRedis(context.getChannelSeqId()+PaymentIngestionConstant.BACKEND_CREATED);
							}
						}
					} else if (PaymentIngestionConstant.BACKEND_UPDATED_FAE.equalsIgnoreCase(eventStatus) && (!context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP))) {
						//isBackendUpdatedFAE = true;
						LOGGER.info("Going to update transition doc for FAE flag for ::::{}",context.getEventType().getServiceKey());
						serviceImpl.processKafkaMessageFAE(jsonNode);
						acknowledgment.acknowledge();
						latch.countDown();
						LOGGER.info("Backend request FAE update is processed for::::{}",context.getEventType().getServiceKey());
					} else {
						LOGGER.info("Going to process the kafka message for serviceKey:::::: {} status:::::::::::{}",context.getEventType().getServiceKey(),context.getEventType().getStatus());
						serviceImpl.processKafkaMessage(jsonNode, eventType);
						acknowledgment.acknowledge();
						latch.countDown();
						LOGGER.info("Backend request on kafka message for :::::: {} :::::::::::{}",context.getEventType().getServiceKey(),context.getEventType().getStatus());
					}
				}
				 else {
					LOGGER.info("Backend event for Single PI, PI File/Bulk, Invalid Service Key or request type or Source Identity, hence ignoring the event id: {}",jsonNode.get(PaymentIngestionConstant.ID));
				}

			} catch (IOException | RequeueException | InterruptedException | DLQException | MessageProcessingException e) {
				//if (isBackendUpdatedFAE) {
					LOGGER.error("Error while updating response from PSHC {} {}",
							PaymentIngestionErrorCodeConstants.ERR_FAE_BCC_RESP_UPDATE,PaymentIngestionErrorCodeConstants.ERR_FAE_BCC_RESP_UPDATE_DESC);
				//}
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_077 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_077_Desc);
				LOGGER.error("Exception at PaymentKafkaConsumer as:::: {}", e);
				processedDataToDlq(data, e.getMessage(), acknowledgment);
			}catch(Exception ex) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_078 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_078_Desc);
				LOGGER.error("Tech Exception at PaymentKafkaConsumer as:::: {}", ex);
				processedDataToDlq(data, ex.getMessage(), acknowledgment);
			}
		});
	}

	/**
	 * Processed data to dlq.
	 *
	 * @param data
	 *            the data
	 * @param msg
	 *            the msg
	 * @param acknowledgment
	 *            the acknowledgment
	 */
	private void processedDataToDlq(final ConsumerRecord<String, String> data, final String msg,
			final Acknowledgment acknowledgment) {
		paymentKafkaDLQProducer.sendToDLQ(data,msg);
		acknowledgment.acknowledge();
		latch.countDown();
		LOGGER.error("Invalid Payload. Hence the payload is directed to DLQ. Error Message ::{}", msg);
	}
	public JsonNode isAnyNotificationRcvdBeforeAck(Context context, String eventStatus) {
		String lChannelSeqId = null;
		PaymentIngestionRedisUtil paymentIngestionRedisUtil = null;
		JsonNode requestJSonMsg = null;
		boolean isMsgPresent = false;
		try {
			lChannelSeqId = context.getChannelSeqId();
			LOGGER.debug("inside isAnyNotificationRcvdBeforeAck::::"+lChannelSeqId+" eventStatus:"+eventStatus);
			paymentIngestionRedisUtil = new PaymentIngestionRedisUtil();
			requestJSonMsg = paymentIngestionRedisUtil.pullBackendCreateDataToRedis(lChannelSeqId+eventStatus);
			if(requestJSonMsg!=null)isMsgPresent = true;
		}catch(Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_079 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_079_Desc);
			LOGGER.error("Exception while processing isAnyNotificationRcvdBeforeAck:::",ex);
		}
		return requestJSonMsg;
	}
	public boolean isValidFormat(String format, String value, Locale locale) {
	    LocalDateTime ldt = null;
	    DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);
	    try {
	        ldt = LocalDateTime.parse(value, fomatter);
	        String result = ldt.format(fomatter);
	        return result.equals(value);
	    } catch (DateTimeParseException e) {
	        try {
	        	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_080 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_080_Desc);
	            LocalDate ld = LocalDate.parse(value, fomatter);
	            String result = ld.format(fomatter);
	            return result.equals(value);
	        } catch (DateTimeParseException exp) {
	            try {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_081 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	    					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_081_Desc);
	                LocalTime lt = LocalTime.parse(value, fomatter);
	                String result = lt.format(fomatter);
	                return result.equals(value);
	            } catch (DateTimeParseException e2) {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_082 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	    					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_082_Desc);
	            }
	        }
	    }
	    return false;
	}
}
