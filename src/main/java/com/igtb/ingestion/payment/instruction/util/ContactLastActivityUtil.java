package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.transhistory.models.TransactionHistoryInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.ActivityInfoES;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.ContactLastActivityES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.StateDocInfoES;
import com.igtb.ingestion.payment.instruction.es.models.TxnAmountES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.models.Account;
import com.igtb.ingestion.payment.instruction.models.InstructedAmount;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;
import com.igtb.ingestion.payment.instruction.models.Context;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Get;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ContactLastActivityUtil {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionEnrichmentUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The payment ingestion mapper. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The object mapper. */
	private final ObjectMapper objectMapper;
	
	@Autowired
	private JestClient jestClient;

	public void pushContactLastActivityToES(final Context context, final PaymentsInstructionsES paymentsInstructionsES) {
		LOGGER.info("In pushContactLastActivityToES ");
		//LOGGER.info("context {} ",context);
		//LOGGER.info("paymentsInstructionsES {} ",paymentsInstructionsES);
		try {
			
			String contactAccountID="";
			ContactLastActivityES contactLastActivityES = new ContactLastActivityES();
			if(paymentsInstructionsES.getPaymentTypesObj().getCode().equals(PaymentIngestionConstant.SEND_RTP)) {
				contactAccountID=(Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getDebtorAccount).map(AccountES::getId).orElse(null));				
			}else {
				//SendMoney,IRTP
				contactAccountID=(Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getId).orElse(null));
			}
			final JsonNode contactAccNode = jestGetESBuilder(contactAccountID, paymentElasticProperties.getOrgIndex(),paymentElasticProperties.getContactAccountsType());
			final String contactId = Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactAccNode, PaymentIngestionConstant.CONTACT, PaymentIngestionConstant.ID)).map(Object::toString).map(String::trim).orElse(null);
            contactLastActivityES.setContactId(contactId); 
			contactLastActivityES.setCreated((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getCreationDateTime).orElse(null)));
			contactLastActivityES.setLastUpdated((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getCreationDateTime).orElse(null)));
			StateDocInfoES stateDocInfoES=new StateDocInfoES();
			stateDocInfoES.setDocStatus(PaymentIngestionConstant.DOC_STATUS_TRANSITION);//TODO
			stateDocInfoES.setDomainSeqId((Optional.ofNullable(context).map(Context::getDomainSeqId).orElse(null)));
			stateDocInfoES.setChannelSeqId((Optional.ofNullable(context).map(Context::getChannelSeqId).orElse(null)));
			//stateDocInfoES.setType(PaymentIngestionConstant.TYPE_PAYMENT);
			stateDocInfoES.setType(paymentsInstructionsES.getPaymentTypesObj().getCode());
			contactLastActivityES.setStateDocInfo(stateDocInfoES);
			ActivityInfoES activityInfoES = new ActivityInfoES();
			LOGGER.info("EventType status Draft or not:::::{}",context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.DRAFT_STATE));
			if(context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.DRAFT_STATE))
				activityInfoES.setDesc("Draft created");
			else
				activityInfoES.setDesc("Payment initiated");
			activityInfoES.setActionDesc((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getCbxUIStatus).orElse(null)));
			activityInfoES.setLastActivityTs((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getStateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getLastActivityTs).orElse(null)));
			activityInfoES.setRequestedExecutionDate((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getRequestedExecutionDate).orElse(null)));
			TxnAmountES txnAmountES =new TxnAmountES();
			txnAmountES.setValue((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getInstructedAmount).map(AmountES::getValue).orElse(null)));
			txnAmountES.setCcy((Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getInstructedAmount).map(AmountES::getCurrencyCode).orElse(null)));
			activityInfoES.setTxnAmount(txnAmountES);
			contactLastActivityES.setActivityInfo(activityInfoES);
			
			final JestResult insertedJestResult = paymentIngestionQueryUtil.indexBuilder(contactLastActivityES, contactAccountID,
					paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getPIContactLastActivities());
			final String insertedDocId = Optional.ofNullable(insertedJestResult.getJsonObject())
					.map(jsonObject -> jsonObject.get(PaymentIngestionConstant._ID)).map(id -> id.toString()).orElse(null);
			LOGGER.info("Pushed ContactLastActivity to ES with id :::: {}, ContactId:::: {} ",insertedDocId, contactLastActivityES.getContactId());
		}catch(Exception ex) {
			LOGGER.error("Exception thrown while inserting data into Contact Last Activity schema {} ", ex);
		}
	}
	public JsonNode jestGetESBuilder(final String id, final String indexName, final String typeName) {
		Get getBuilder = new Get.Builder(indexName, id).type(typeName).build();
		JsonNode rootNode = null;
		try {
			final DocumentResult docResult = jestClient.execute(getBuilder);
			if (!docResult.isSucceeded() && docResult.getErrorMessage() != null&& docResult.getErrorMessage().length() > 0) {
				LOGGER.error("Exception occured while getting the documents with errorMessages : [{}]",docResult.getErrorMessage());
			}
			final String jsonResult = Optional.ofNullable(docResult).map(result -> result.getJsonString()).orElse(null);
			LOGGER.debug("Current state search result::::  {}", jsonResult);
			rootNode = null;
			if (jsonResult != null)
				rootNode = objectMapper.readTree(jsonResult);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_047 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_047_Desc);
			LOGGER.error("Exception occured in ElasticUtil GetBuilder due to reason :::: [{}]",e.getMessage());
		}
		return Optional.ofNullable(rootNode).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
	}
}
