
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class PaymentProduct.
 */
//@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the product name.
 *
 * @return the product name
 */
@Getter

/**
 * Sets the product name.
 *
 * @param productName the new product name
 */
@Setter
public class PaymentProduct {

    /** The payment type. */
    @JsonProperty("paymentType")
    public String paymentType;
    
    /** The product name. */
    @JsonProperty("productName")
    public String productName;

}
