package com.igtb.ingestion.payment.instruction.util;

import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.igtb.ingestion.payment.instruction.config.VaultProperties;
import com.igtb.jose.JWTUtils;
import com.igtb.jose.model.JWT;

import lombok.Getter;

/**
 * The Class JWTSignedUtil.
 */
@Component
@Getter
public class JoseClientUtil {

	/** The logger. */
	private final static Logger LOGGER = LoggerFactory.getLogger(JoseClientUtil.class);

	/** The private key id. */
	@Value("${igtb.default.privateKeyId}")
	private String privateKeyId;
	
	/** The work claim set. */
	@Value("${igtb.claimSetKey}")
	private String workClaimSet;
	
	/** The work claim set. */
	@Value("${trash.url}")
	private String trashUrl;
	
	/** reject url. */
	@Value("${reject.url}")
	private String rejectUrl;

	/** The work claim set. */
	@Value("${trash.header.key.requestId}")
	private String requestId;
	
	/** The work claim set. */
	@Value("${trash.header.key.sharedKey}")
	private String sharedKey;
	
	/** The work claim set. */
	@Value("${trash.header.key.Authorization}")
	private String authorization;
	
	/** The work claim set. */
	@Value("${gatekeeper_url}")
	private String gatekeeper_url;


	/** The work claim set. */
	@Value("${jwtExpiryInSecs}")
	private Integer jwtExpiryInSecs;
	
	/** The vault properties. */
	@Autowired
	private VaultProperties vaultProperties;

	/**
	 * Jwt signed event.
	 *
	 * @param stringPayload
	 *            the string payload
	 * @return the string
	 */
	public String jwtSignedEvent(final String stringPayload) {
		LOGGER.debug("JWT SIGN USING STRING PAYLOAD");
		final JWT jwtObject = JWTUtils.getJWTBuilder(privateKeyId)
				.claim("https://cbx.igtb.com/custom_claim", stringPayload).expirationSeconds(jwtExpiryInSecs).build();
		return JWTUtils.sign(jwtObject, vaultProperties.getIgtbPrivateKeys().get(privateKeyId));
	}
	
	/**
	 * Wkflw init jwt signed event.
	 *
	 * @param stringPayload the string payload
	 * @return the string
	 */
	public String wkflwInitJwtSignedEvent(final String stringPayload)  {
		LOGGER.debug("wkflwInitJwtSignedEvent JWT SIGN USING STRING PAYLOAD with claimset: {}", workClaimSet);
		final JWT jwtObject = JWTUtils.getJWTBuilder(privateKeyId)
				.claim(workClaimSet, stringPayload).expirationSeconds(240).build();
		final String signedEvent = JWTUtils.sign(jwtObject, vaultProperties.getIgtbPrivateKeys().get(privateKeyId));
		LOGGER.debug("Signed event as: {}", signedEvent);
		return signedEvent;
	}

	/**
	 * Gets the deserialize jwt.
	 *
	 * @param signedEvent the signed event
	 * @return the deserialize jwt
	 */
	public Map<String, Object> getDeserializeJwt(final String signedEvent) {
		LOGGER.debug("JWT UN-SIGNING EVENT");
		final JWT jwt = JWTUtils.deserialize(signedEvent);
		return jwt.getAllClaimsAsMap();
	}
	
	public String generateJWT(String payloadJson) {
		LOGGER.info("Started !");
		JSONObject payloadJsonObj = new JSONObject(payloadJson);

		final JWT jwtObject = JWTUtils.getJWTBuilder(privateKeyId).expirationSeconds(Integer.getInteger("digital.jwtExpiry", 30))
		.claim("context" , payloadJsonObj).build();
		final String signedApprovalDocs = JWTUtils.sign(jwtObject, vaultProperties.getIgtbPrivateKeys().get(privateKeyId));
		       
		LOGGER.info("Generated token : {}",signedApprovalDocs);
		return signedApprovalDocs;
		       
	}
	
}
