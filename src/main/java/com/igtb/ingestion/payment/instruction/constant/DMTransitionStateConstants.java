package com.igtb.ingestion.payment.instruction.constant;

import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;

/**
 * The Class DMTransitionStateConstants to collate all possible channel/backend/workflow events
 * and assign the possiblenewstates and outofseqstates for a respective transition state 
 * and the possible action/transition flags.
 * 
 */
public final class DMTransitionStateConstants {

	/** The Constant BACKEND_CHANNEL_EVENTS. */
	public static final TransitionStateEnum[] BE_CHANNEL_EVENTS = { 
			TransitionStateEnum.backend_ack_success, 
			TransitionStateEnum.backend_rejected, 
			TransitionStateEnum.backend_processed, 
			TransitionStateEnum.trashed
			};

	/** The Constant BACKEND_DATA_EVENTS. */
	public static final TransitionStateEnum[] BE_DATA_EVENTS = { 
			TransitionStateEnum.backend_created, 
			TransitionStateEnum.backend_updated, 
			TransitionStateEnum.backend_deleted
			};

	/** The Constant CHANNEL_STATE_EVENTS. */
	public static final TransitionStateEnum[] CHANNEL_STATE_EVENTS = {
			TransitionStateEnum.draft, 
			TransitionStateEnum.val_success,
			TransitionStateEnum.val_failure, 
			TransitionStateEnum.val_timeout,
			TransitionStateEnum.approved, 
			TransitionStateEnum.rejected, 
			TransitionStateEnum.cancelled,
			TransitionStateEnum.released,
			TransitionStateEnum.release_retry,
			TransitionStateEnum.release_rejected,
			TransitionStateEnum.release_failure,
			TransitionStateEnum.trashed
			};
	
	/** The Constant CHANNEL_WORKFLOW_EVENTS. */
	public static final TransitionStateEnum[] CHANNEL_WORKFLOW_EVENTS = {
			TransitionStateEnum.initiated_wf,
			TransitionStateEnum.verified_wf,
			TransitionStateEnum.approved_wf,
			TransitionStateEnum.released_wf,
			TransitionStateEnum.rejected_wf,
			TransitionStateEnum.trashed_wf
			};
	
	/** The Constant ENTRY_STATES. */
	public static final TransitionStateEnum[] ENTRY_STATES = { 
			TransitionStateEnum.draft, 
			TransitionStateEnum.val_failure, 
			TransitionStateEnum.val_success, 
			TransitionStateEnum.val_timeout};

	
	
	
	/** The Constant Transition History Allowed Channel Events. */
	public static final TransitionStateEnum[] VALID_TH_STATE_EVENTS = {
			TransitionStateEnum.draft, 
			TransitionStateEnum.val_success,
			TransitionStateEnum.val_timeout,
			TransitionStateEnum.released,
			TransitionStateEnum.release_rejected,
			TransitionStateEnum.release_failure,
			TransitionStateEnum.backend_rejected, 
			TransitionStateEnum.backend_processed
			};
	
	/** The Constant Transition History Allowed Workflow Events. */
	public static final TransitionStateEnum[] VALID_TH_WORKFLOW_EVENTS = {
			TransitionStateEnum.initiated_wf,
			TransitionStateEnum.verified_wf,
			TransitionStateEnum.approved_wf,
			TransitionStateEnum.released_wf,
			TransitionStateEnum.rejected_wf
	};

	/**
	 * The Enum TRANSITION_FLAG.
	 */
	public enum TRANSITION_FLAG {

		/** The allowed entry states */
		// when ACTION is first insert.
		ALLOWED_ENTRY_STATE,

		/** The allowed. */
		// when ACTION is to either REPLACE_DOC/or UPDATE_STATE_ONLY
		ALLOWED,

		/** The disallowed. */
		// when ACTION is to skip.
		DISALLOWED,

		/** The out of seq. */
		//when ACTION is to REQUE.
		OUT_OF_SEQ
	}
	
	/**
	 * The Enum Action FLAG.
	 * 
	 * @author vaishali.gupta
	 * @since 07-Jan-2019/12:15:58 AM
	 *
	 */
	public enum ACTION {
		
		/** The action to first insert.*/
		FIRST_INSERT,
		
		/** The action to replace doc.*/
		REPLACE_DOCUMENT,
		
		/** The action to update state only. */
		UPDATE_STATE_ONLY,
		
		/** The action to reque. */
		REQUE_MESSAGE,
		
		/** The action to skip. */
		SKIP_MESSAGE,
		
	}
	
	/**
	 * Instantiates a new ingestion constants.
	 */
	private DMTransitionStateConstants() {
		// empty implementation to avoid instantiation.
	}
}
