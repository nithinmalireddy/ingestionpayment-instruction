package com.igtb.ingestion.payment.instruction.service; // NOPMD moreImport

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.api.ingestion.commons.file.decisionmatrix.FileAction;
import com.igtb.api.ingestion.commons.file.decisionmatrix.FileTransitionData;
import com.igtb.api.ingestion.commons.file.decisionmatrix.FileTransitionDecisionModule;
import com.igtb.api.ingestion.commons.file.eventmatrix.FileEventMatrix;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataPayload;
import com.igtb.ingestion.payment.instruction.models.BackendPayload;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.FileAvailablePayload;
import com.igtb.ingestion.payment.instruction.models.FileTransactionPayload;
import com.igtb.ingestion.payment.instruction.util.FileProcessingChannelUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionHelper;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionRedisUtil;

import lombok.AllArgsConstructor;

/**
 * The Class FileProcessingServiceImpl.
 */
@Component
@AllArgsConstructor
public class FileProcessingServiceImpl {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessingServiceImpl.class);

	/** The ingestion channel util. */
	private FileProcessingChannelUtil ingestionChannelUtil;

	/** The payment ingestion redis util. */
	private PaymentIngestionRedisUtil paymentIngestionRedisUtil;

	/** The payment ingestion helper. */
	private PaymentIngestionHelper paymentIngestionHelper;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	/**
	 * Process rabbit message.
	 *
	 * @param requestMessageJson
	 *            the request message json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void procesasFileRabbitMessage(final JsonNode requestMessageJson)
			throws IOException, RequeueException, DLQException, InterruptedException, MessageProcessingException,
			DataNotValidException, DataNotFoundException {
		// Extracting Context Information from Request Message
		final JsonNode CONTEXTJSON = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		Context context;
		// Converting JsonNode to Context model
		if (CONTEXTJSON == null) {
			context = new Context();
		} else {
			context = objectMapper.readValue(CONTEXTJSON.toString(), Context.class);
		}

		/*
		 * separating the events based on the outcome category[channel-state-updated,
		 * channel-workflow-update, backend-state-updated] for processing since each
		 * channel/routes has its own implementations
		 */
		dispatchRMQMsgBasedOnOutcomeCategory(requestMessageJson, context);

		/*
		 * After successful processing, sending all backend state events to rabbitmq for
		 * other apis to consume.
		 */
		if ("backend-state-updated".equalsIgnoreCase(Optional.ofNullable(context.getEventType())
				.map(eventType -> eventType.getOutcomeCategory()).orElse(null))) {
			paymentIngestionHelper.publishBackendEventsToRabbit(requestMessageJson, context);
		}

	}

	/**
	 * Dispatch RMQ msg based on outcome category.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void dispatchRMQMsgBasedOnOutcomeCategory(final JsonNode message, final Context context)
			throws IOException, RequeueException, DLQException, InterruptedException, MessageProcessingException,
			DataNotValidException, DataNotFoundException {

		final String outcomeCategory = Optional.ofNullable(context.getEventType())
				.map(eventType -> eventType.getOutcomeCategory()).orElse(null);

		final JsonNode PAYLOAD;
		if (message.get(PaymentIngestionConstant.PAYLOAD) == null
				|| message.get(PaymentIngestionConstant.PAYLOAD).isNull()) {
			PAYLOAD = objectMapper.createObjectNode();
			((ObjectNode) message).set("payload", PAYLOAD);
		} else {
			PAYLOAD = message.get(PaymentIngestionConstant.PAYLOAD);
		}

		final String status = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getStatus())
				.orElse(null);

		// validate event status
		if (FileEventMatrix.isIngestionAllowed(message)) {
			if (PAYLOAD != null) {
				if (PaymentIngestionConstant.FILE_AVAILABLE.equalsIgnoreCase(status)) {
					final FileAvailablePayload fileAvailablePayload = objectMapper.readValue(PAYLOAD.toString(),
							FileAvailablePayload.class);
					// validate payload
					if (fileAvailablePayload != null) {
						validateBean(fileAvailablePayload, fileAvailablePayload.getFileId());
					}
					performCommonActions(message, context, fileAvailablePayload, null, null, null, null);

				} else if (Arrays.asList("metadata_available", "metadata_unavailable").contains(status)) {
					final MetadataPayload metadataPayload = objectMapper.readValue(PAYLOAD.toString(),
							MetadataPayload.class);
					// validate payload
					if (metadataPayload != null) {
						validateBean(metadataPayload, metadataPayload.getFileId());
					}
					performCommonActions(message, context, null, metadataPayload, null, null, null);

				} else if (PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)
						|| "channel-file-state-updated".equalsIgnoreCase(outcomeCategory)) {
					final FileTransactionPayload fileTransactionPayload = objectMapper.readValue(PAYLOAD.toString(),
							FileTransactionPayload.class);
					// validate payload
					if (fileTransactionPayload != null) {
						validateBean(fileTransactionPayload, fileTransactionPayload.getFileId());
					}
					performCommonActions(message, context, null, null, fileTransactionPayload, null, null);

				} else if (PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equalsIgnoreCase(outcomeCategory)) {
					final PaymentWF paymentwf = objectMapper.readValue(PAYLOAD.toString(), PaymentWF.class);
					performCommonActions(message, context, null, null, null, paymentwf, null);

				} else if (PaymentIngestionConstant.BACKEND_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)) {
					final BackendPayload backendPayload = objectMapper.readValue(PAYLOAD.toString(),
							BackendPayload.class);
					performCommonActions(message, context, null, null, null, null, backendPayload);
				}
			}
		} else {
			LOGGER.info("isIngestionAllowed false, hence Skipping the event id: {}", message.get("id"));
		}

	}

	/**
	 * Perform common actions.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @param fileAvailablePayload
	 *            the file available payload
	 * @param metadataPayload
	 *            the metadata payload
	 * @param fileTransactionPayload
	 *            the file transaction payload
	 * @param paymentWF
	 *            the payment WF
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void performCommonActions(final JsonNode message, final Context context,
			final FileAvailablePayload fileAvailablePayload, final MetadataPayload metadataPayload,
			final FileTransactionPayload fileTransactionPayload, final PaymentWF paymentWF,
			final BackendPayload backendPayload) throws IOException, RequeueException, DLQException,
			InterruptedException, MessageProcessingException, DataNotFoundException {

		final String id = getSearchId(context, fileAvailablePayload, metadataPayload, fileTransactionPayload);
		final String lockId = StringUtils.isNotBlank(context.getChannelSeqId()) ? context.getChannelSeqId() : id;
		// acquireRedisLock
		if (paymentIngestionRedisUtil.acquirePurgeBatchLock(lockId)) {

			/* Fetching existing PaymentInstruction data from ES if available */
			LOGGER.info("fetching stateinformation of existing payment instruction set for fileId/channelSeqId: {}",
					id);
			final PaymentInstructionSetsES existingPiSetES = ingestionChannelUtil.getExistingPIset(id,
					context.getEventType().getStatus().toLowerCase(Locale.ENGLISH),
					context.getEventType().getOutcomeCategory());

			final StateInfo stateInfo = Optional.ofNullable(existingPiSetES).map(PaymentInstructionSetsES::getStateInfo)
					.orElse(new StateInfo());

			// 2. setting paymentIngestionStateInfoHelper stateinfo
			final FileTransitionData stateTransitionData = new FileTransitionData();

			// Enriching stateTransitionData info from existing record in ES.
			setFromStateTransitionData(stateInfo, stateTransitionData);

			/*
			 * Enriching stateTransitionData info from request payload. passing the
			 * stateTransitionData itself[has "From *" informations] to get enriched with
			 * "To *" informations and return
			 */
			setToStateTransitionData(context, stateTransitionData);

			/*
			 * Action finder call to figure out the action [possible actions are insert,
			 * replace, update, requeue, skip]
			 */
			final FileAction transitionAction = FileTransitionDecisionModule
					.getActionForTransition(stateTransitionData);
			LOGGER.debug("Action receive from common igestion as: {} for id: {}", transitionAction,
					context.getChannelSeqId());

			if (FileAction.FIRST_INSERT.equals(transitionAction)) {
				ingestionChannelUtil.firstInsert(message, context, fileAvailablePayload);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
			} else if (FileAction.BULK_INSTR_INSERT_FILE_UPDATE.equals(transitionAction)) {
				ingestionChannelUtil.processMetadataAvailableEvent(message, context, metadataPayload, existingPiSetES);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
			} else if (FileAction.FILE_INSTR_UPDATE.equals(transitionAction)) {
				ingestionChannelUtil.fileInstrUpdate(message, context, backendPayload, existingPiSetES);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
			} else if (FileAction.FILE_UPDATE.equals(transitionAction)) {
				ingestionChannelUtil.fileUpdate(message, context, metadataPayload, existingPiSetES, paymentWF);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
			} else if (FileAction.REQUEUE.equals(transitionAction)) {
				// releasing the set lock from redis before requeing the message
				releaseRedisLock(lockId);
				final String requeueMsg = "Requeuing this event as per File level common matrix for fileId/channelSeqId: "
						+ lockId;
				ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);
			} else {
				LOGGER.info("<<< Skipping the event with fileId/channelSeqId: {} and outcomecategory {} >>>", lockId,
						context.getEventType().getOutcomeCategory());
				// releasing the set lock from redis after processing
				releaseRedisLock(id);
			}
		} else {

			final String requeueMsg = "Requeuing the event due to unavailability of redis lock, for fileId/channelSeqId: "
					+ lockId;
			LOGGER.info("-------->>>>>>>>> {} <<<<<<<<<<--------", requeueMsg);
			final FileTransitionData stateTransitionData = new FileTransitionData();

			/*
			 * setting only "***********State To***********" Information as this request
			 * hasn't reached a stage to fetch the existing information to derive to an
			 * action that needs to be taken. Enriching stateTransitionData info from
			 * request payload.
			 */
			LOGGER.info("-------->>>>>>>>>Setting only **To State** Information at this stage<<<<<<<<<<--------");
			setToStateTransitionData(context, stateTransitionData);
			ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);

		}

	}

	/**
	 * Release redis lock.
	 *
	 * @param id
	 *            the id
	 * @throws RequeueException
	 *             the requeue exception
	 */
	private void releaseRedisLock(final String id) throws RequeueException {
		if (paymentIngestionRedisUtil.releasePurgeBatchLock(id)) {
			LOGGER.info("-------->>>>>>>>Released the obtained Redis Lock<<<<<<<<--------");
		} else {
			LOGGER.info("-------->>>>>>>>Couldn't release the redis lock<<<<<<<<--------");
		}
	}

	/**
	 * Gets the search id.
	 *
	 * @param context
	 *            the context
	 * @param fileAvailablePayload
	 *            the file available payload
	 * @param metadataPayload
	 *            the metadata payload
	 * @param fileTransactionPayload
	 *            the file transaction payload
	 * @return the search id
	 */
	private String getSearchId(final Context context, final FileAvailablePayload fileAvailablePayload,
			final MetadataPayload metadataPayload, final FileTransactionPayload fileTransactionPayload) {
		if (fileAvailablePayload != null) {
			return fileAvailablePayload.getFileId();
		} else if (metadataPayload != null) {
			return metadataPayload.getFileId();
		} else if (Arrays.asList("draft", "val_success", "val_timeout", "val_failure")
				.contains(context.getEventType().getStatus()) && fileTransactionPayload != null) {
			return fileTransactionPayload.getFileId();
		} else if ("trashed".equalsIgnoreCase(context.getEventType().getStatus())
				&& context.getEventType().getOutcomeCategory().equalsIgnoreCase("channel-file-state-updated")
				&& fileTransactionPayload != null) {
			return fileTransactionPayload.getFileId();
		} else if (StringUtils.isNotBlank(context.getChannelSeqId())) {
			return context.getChannelSeqId();
		}
		return null;
	}

	/**
	 * Validate bean.
	 *
	 * @param object
	 *            the object
	 * @param id
	 *            the channel seq id
	 * @return the list
	 * @throws DLQException
	 *             the DLQ exception
	 */
	private List<String> validateBean(final Object object, final String id) throws DLQException {
		final List<String> errorMessages = new ArrayList<>();

		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		validator.validate(object).stream().forEach(violation -> {
			final StringBuilder messageSet = new StringBuilder(120);
			messageSet.append("\n >>>> Property Path:").append(violation.getPropertyPath())
					.append(" <<<< \n >>>> Value that violated the constraint : ").append(violation.getInvalidValue())
					.append(" <<<< \n >>>> Constraint : ").append(violation.getMessage()).append(" <<<<");
			errorMessages.add(messageSet.toString());
		});

		if (errorMessages.isEmpty()) {
			return null;
		} else {
			LOGGER.info("Validation Failed for fileId/channelSeqId --> {} and the Error Messages {}", id,
					errorMessages);
			throw new DLQException("Bean Validation failed, hence sending to DLQ {}");
		}
	}

	/**
	 * Sets the from state transition data.
	 *
	 * @param stateInfo
	 *            the state info
	 * @param stateTransitionData
	 *            the state transition data
	 */
	private void setFromStateTransitionData(final StateInfo stateInfo, final FileTransitionData stateTransitionData) {

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getLastActivityTs)
				.ifPresent(stateTransitionData::setFromEventTime);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getState)
				.ifPresent(stateTransitionData::setFromState);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getOutcomeCategory)
				.ifPresent(stateTransitionData::setFromOutcomeCategory);

		LOGGER.info("From State {}, From OutCome Category {}, From EventTime {} ", stateTransitionData.getFromState(),
				stateTransitionData.getFromOutcomeCategory(), stateTransitionData.getFromEventTime());
	}

	/**
	 * Sets the to state transition data.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 */
	private void setToStateTransitionData(final Context context, final FileTransitionData stateTransitionData) {
		stateTransitionData.setToEventTime(context.getEventTime());

		stateTransitionData.setToState(context.getEventType().getStatus());

		stateTransitionData.setToOutcomeCategory(context.getEventType().getOutcomeCategory());

		LOGGER.info("To State {}, To OutCome Category {}, To EventTime {} ", stateTransitionData.getToState(),
				stateTransitionData.getToOutcomeCategory(), stateTransitionData.getToEventTime());
	}

	/**
	 * Gets the redis key for purge batch lock.
	 *
	 * @param payloadType
	 *            the payload type
	 * @return the redis key for purge batch lock
	 */
	public static String getRedisKeyToSetLock(final String payloadType) {
		return PaymentIngestionConstant.REDIS_LOCK_KEY_PREFIX + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ PaymentIngestionConstant.REDIS_DOMAIN_KEY + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ "channel-state" + PaymentIngestionConstant.REDIS_KEY_SEPARATOR + payloadType;
	}

}
