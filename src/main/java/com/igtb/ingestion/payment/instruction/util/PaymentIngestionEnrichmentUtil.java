package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.api.ingestion.commons.transhistory.models.TransactionHistoryInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.CBXSupportSatusEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.ResourceEnum;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.SupportingDocumentES;
import com.igtb.ingestion.payment.instruction.es.models.WithholdingTaxES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Creditor;
import com.igtb.ingestion.payment.instruction.models.Debtor;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;

import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import lombok.AllArgsConstructor;

/**
 * The Class PaymentIngestionEnrichmentUtil.
 */
@Component
@AllArgsConstructor
public class PaymentIngestionEnrichmentUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionEnrichmentUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The payment ingestion mapper. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/** The payment ingestion state info util. */
	private final PaymentIngestionHelper stateInfoHelper;

	/** The transaction history doc id. */

	/** The paymnt instr transfomer util. */
	private final PaymntInstrTransformerUtil paymntInstrTransfomerUtil;

	/**
	 * Payment ES enrichment.
	 *
	 * @param paymentsInstructionsES
	 *            the payments instructions ES
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @param requestMessageJson
	 *            the request message json
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @return the payments instructions ES
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public JsonNode paymentESEnrichment(final PaymentsInstructionsES paymentsInstructionsES,
			final PaymentInstruction paymentInstruction, final Context context, final JsonNode requestMessageJson,
			final JsonNode existingStateInfoJson) throws Exception {

		// After final conversion enriching further
		JsonNode userInfoJson = null;

		if (PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(context.getEventType().getOutcomeCategory()) || PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equalsIgnoreCase(context.getEventType().getOutcomeCategory())) {

			// User Enrichment
			final UserInfo userInfo = new UserInfo();
			Optional.ofNullable(context.getRequester()).map(requester -> requester.getDomainId()).ifPresent(userInfo::setDomainName);

			Optional.ofNullable(context.getRequester()).map(requester -> requester.getId()).ifPresent(userInfo::setUserName);
			String entity = null;
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
				entity = Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditor).map(Creditor::getIdentification).orElse(null);
			} else {
				entity = Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getDebtor).map(Debtor::getIdentification).orElse(null);
			}
			//
			if(entity!= null) {
				stateInfoHelper.enrichUserInfo(entity, userInfo);
			}
			

			userInfoJson = objectMapper.convertValue(userInfo, JsonNode.class);

			final StateInfo stateInfo = stateInfoHelper.getStateInfoFromCommonsModule(requestMessageJson, userInfoJson,existingStateInfoJson, "Fund Transfer");

			paymntInstrTransfomerUtil.populatePaymentInstructions(paymentsInstructionsES, paymentInstruction, stateInfo,
					context);

			paymentsInstructionsES.setStateInfo(stateInfo);
			LOGGER.info("ES Model after enrichment:::::::: {}", paymentsInstructionsES);
			LOGGER.info("Payment Instructions returned after enrichmenr:::::: {}",objectMapper.convertValue(paymentsInstructionsES, JsonNode.class));
		}

		return userInfoJson;
	}

	/**
	 * Handle request for other request type.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @param requestMessageJson
	 *            the request message json
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @return the json node
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public JsonNode handleRequestForOtherRequestType(PaymentInstruction paymentInstruction, Context context,
			JsonNode requestMessageJson, JsonNode existingStateInfoJson) throws IOException, DataNotFoundException {

		StateInfo stateInfo = objectMapper.readValue(existingStateInfoJson.toString(), StateInfo.class);
		
		JsonNode userInfoJson = null;

		// Get the enriched active document using the domainSeqId of the incoming
		// request.
		final JsonNode existingActiveDoc = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
		if (existingActiveDoc == null) {
			throw new DataNotFoundException("Context requestType " + context.getEventType().getRequestType() + "request raised for a record has no existing active documents for domainSeqId: {}" + context.getDomainSeqId());
		}
		// User Enrichment
		final UserInfo userInfo = new UserInfo();

		Optional.ofNullable(context.getRequester()).map(requester -> requester.getDomainId()).ifPresent(userInfo::setDomainName);
		Optional.ofNullable(context.getRequester()).map(requester -> requester.getId()).ifPresent(userInfo::setUserName);
		final String entity = existingActiveDoc.get(PaymentIngestionConstant.DEBTOR_ACCOUNT).get(PaymentIngestionConstant.ENTITY).get(PaymentIngestionConstant.ID).asText();
		LOGGER.info("entity::::::{} ",entity);
		stateInfoHelper.enrichUserInfo(entity, userInfo);

		userInfoJson = objectMapper.convertValue(userInfo, JsonNode.class);
		LOGGER.info("userInfoJson::::: {} " , userInfoJson);
		stateInfo = stateInfoHelper.getStateInfoFromCommonsModule(requestMessageJson, userInfoJson,existingStateInfoJson, "Fund Transfer");
		LOGGER.info("stateInfo:::::{} " , stateInfo);
		// update delta of current data and active record data
		((ObjectNode) existingActiveDoc).set(PaymentIngestionConstant.STATE_INFO, objectMapper.convertValue(stateInfo, JsonNode.class));
		((ObjectNode) existingActiveDoc).put(PaymentIngestionConstant.REMARK, paymentInstruction.getRemark());
		((ObjectNode) existingActiveDoc).put(PaymentIngestionConstant.STATUS,UIStatusUtil.getCBXUIStatus(stateInfo, ResourceEnum.paymentInstructions.toString()));
		((ObjectNode) existingActiveDoc).put(PaymentIngestionConstant.TRANSACTION_ID,PaymentIngestionConstant.BLANK);

		// inserting record
		JestResult result = paymentIngestionQueryUtil.indexBuilder(existingActiveDoc, stateInfo.getChannelSeqId(),paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
		LOGGER.info("result::::::{}" , result);
		// Update the active record with the UISupportStatus as '*_INITIATED').(for PI
		// and SI only)
		if (result.isSucceeded()) {
			String activeUIStatus = Optional.ofNullable(existingActiveDoc).map(rec -> rec.get(PaymentIngestionConstant.STATUS)).map(JsonNode::asText).orElse(null);

			// If not 'POSTED', then only perform 'Re-indexing'
			if (activeUIStatus!= null && !PaymentIngestionConstant.POSTED.equals(activeUIStatus)) {
				String requestType = context.getEventType().getRequestType();
				CBXSupportSatusEnum nextActiveStatus = EnumUtils.getEnum(CBXSupportSatusEnum.class,StringUtils.upperCase(requestType).concat("_SUBMITTED"));
				ObjectNode updateActiveDoc = objectMapper.getNodeFactory().objectNode();
				updateActiveDoc.put(PaymentIngestionConstant.CBX_SUPPORT_STATUS, nextActiveStatus.getCbxSupportStatusStr());
				updateActiveDoc.put(PaymentIngestionConstant.CBX_SUPPORT_STATUS_CHANNL_SEQ_ID, stateInfo.getChannelSeqId());
				paymentIngestionQueryUtil.updateBuilder(updateActiveDoc, stateInfo.getDomainSeqId(),paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			}
		}

		// check supporting doc and WHT documents
		List<Index> listSuppDocIndex = buildSupportingDocument(stateInfo.getDomainSeqId(), stateInfo.getChannelSeqId());
		List<Index> withHoldingTxList = buildWithHoldingTxList(stateInfo.getDomainSeqId(), stateInfo.getChannelSeqId());

		// inserting supporting documents
		if (result.isSucceeded() && !listSuppDocIndex.isEmpty() ) {
			LOGGER.info("Inserting SuppDocuments");
			final JestResult suppDocResult = paymentIngestionQueryUtil.insertBulkRecord(
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType(),
					listSuppDocIndex);
			LOGGER.debug("SuppDocuments bulkResult isSucceeded: {}", suppDocResult.isSucceeded());
		}

		// inserting paymentTaxInfo
		if (result.isSucceeded() && !withHoldingTxList.isEmpty()) {
			LOGGER.info("Inserting paymentTaxInfo");
			final JestResult paymentTaxInfoResult = paymentIngestionQueryUtil.insertBulkRecord(paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType(),withHoldingTxList);
			LOGGER.debug("paymentTaxInfo bulkResult isSucceeded: {}", paymentTaxInfoResult.isSucceeded());
		}
		return userInfoJson;
	}

	/**
	 * Builds the supporting document.
	 *
	 * @param domainSeqId
	 *            the domain seq id
	 * @param channelSeqId
	 *            the channel seq id
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 */
	private List<Index> buildSupportingDocument(final String domainSeqId, final String channelSeqId) {
		final List<Index> listSuppDocIndex = new ArrayList<>();

		// check supportingDocument active doc is present or not
		Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("paymentTransactionId", domainSeqId);
		matchStrings.put("paymentDocStatus", PaymentIngestionConstant.DOC_STATUS_ACTIVE);
		JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType());

		if (jestResult.isSucceeded() && StringUtils.isNotBlank(jestResult.getSourceAsString())) {
			final List<SupportingDocumentES> suppDocList = jestResult.getSourceAsObjectList(SupportingDocumentES.class);
			for (SupportingDocumentES supportingDocumentES : suppDocList) {
				supportingDocumentES.setPaymentInstructionId(channelSeqId);
				supportingDocumentES.setPaymentDocStatus(PaymentIngestionConstant.DOC_STATUS_TRANSITION);
				listSuppDocIndex.add(new Index.Builder(supportingDocumentES)
						.id(supportingDocumentES.getDocumentId() + "-" + channelSeqId).build());
			}
		}
		return listSuppDocIndex;
	}

	/**
	 * Builds the with holding tx list.
	 *
	 * @param domainSeqId
	 *            the domain seq id
	 * @param channelSeqId
	 *            the channel seq id
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 */
	private List<Index> buildWithHoldingTxList(final String domainSeqId, final String channelSeqId) {
		final List<Index> withHoldingTxList = new ArrayList<>();

		// check WHT active doc is present or not
		Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("parentTransactionId", domainSeqId);
		matchStrings.put("parentDocStatus", PaymentIngestionConstant.DOC_STATUS_ACTIVE);
		JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType());

		if (jestResult.isSucceeded() && StringUtils.isNotBlank(jestResult.getSourceAsString())) {
			final List<WithholdingTaxES> whtList = jestResult.getSourceAsObjectList(WithholdingTaxES.class);
			for (WithholdingTaxES withHoldingTaxES : whtList) {
				withHoldingTaxES.setParentInstructionId(channelSeqId);
				withHoldingTaxES.setParentDocStatus(PaymentIngestionConstant.DOC_STATUS_TRANSITION);
				withHoldingTxList.add(new Index.Builder(withHoldingTaxES).build());
			}
		}

		return withHoldingTxList;
	}

	/**
	 * Gets the document node.
	 *
	 * @param paymentsInstructionsES
	 *            the payments instructions ES
	 * @return the document node
	 */
	public ObjectNode getDocumentNode(final PaymentsInstructionsES paymentsInstructionsES) {
		final JsonNode node = objectMapper.convertValue(paymentsInstructionsES, JsonNode.class);
		final ObjectNode docNode = JsonNodeFactory.instance.objectNode();
		docNode.set("doc", node);
		return docNode;
	}

	/**
	 * Fetch existing document info from ES.
	 *
	 * @param context
	 *            the context
	 * @return the map
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public Map<String, Object> fetchExistingDocumentInfoFromES(final Context context, final JsonNode message)throws MessageProcessingException {

		try {
			LOGGER.info("Start fetchSelectiveDataByPassingMatchingString() with ChannelSeqId :::::{}", context.getChannelSeqId());
			//final JestResult jestResult = paymentIngestionQueryUtil.fetchExistingDocument(PaymentIngestionConstant.PAYMENT_QUERY_MATCH,context.getChannelSeqId(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
			//added 1 more condition to fetch existing record from transition doc, as sometime data was retrieved from active doc, hence reject or posted status not updating
			final Map<String, String> matchStrings = new HashMap<>();
			matchStrings.put(PaymentIngestionConstant.PAYMENT_QUERY_MATCH, context.getChannelSeqId());
			matchStrings.put(PaymentIngestionConstant.STATEINFO_DOCSTATUS, PaymentIngestionConstant.DOC_STATUS_TRANSITION);
			final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings,
					null, paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());

			LOGGER.info("jestResult.getSourceAsString() ::::::::: {} ", jestResult.getSourceAsString());
			final String stateDocId = paymentIngestionQueryUtil.getDocumentIdFromJestResult(jestResult);
			LOGGER.info("stateDocId :::: {} ", stateDocId);

			PaymentsInstructionsES paymentsInstructionsES = null;
			if (StringUtils.isNotBlank(jestResult.getSourceAsString())) {
				paymentsInstructionsES = objectMapper.readValue(jestResult.getSourceAsString(),new TypeReference<PaymentsInstructionsES>() {
						});
			}

			final Map<String, Object> existingDocumentInfo = new HashMap<>();
			if (paymentsInstructionsES != null) {
				if (PaymentIngestionConstant.SEND_RTP.equals(paymentsInstructionsES.getPaymentTypesObj().getCode()) && null != paymentsInstructionsES.getOriginalGroupInformationAndStatus()) {
					paymentsInstructionsES.getOriginalGroupInformationAndStatus().setClearingSystemReference(context.getChannelSeqId());
				}
				// Need to be Update date into ES on Checker submit 
				ObjectMapper mapper = new ObjectMapper();
				 JsonNode rootNode = mapper.readTree(message.toString());
				if (rootNode.has(PaymentIngestionConstant.SUPPLEMENT)) {
					JsonNode subroot = mapper.readTree(rootNode.get(PaymentIngestionConstant.SUPPLEMENT).toString());
					if (subroot.has(PaymentIngestionConstant.APPROVALSLIP_TAG)) {
						Iterator<Entry<String, JsonNode>> iterator = subroot.get(PaymentIngestionConstant.APPROVALSLIP_TAG).fields();
						while (iterator.hasNext()) {
							Entry<String, JsonNode> childNode = iterator.next();
							if (childNode.getValue() != null) {
								if (childNode.getKey().equalsIgnoreCase(PaymentIngestionConstant.EXPIRY_DATE)) {
									paymentsInstructionsES.setExpiryDate(childNode.getValue().textValue());
								}
								if (childNode.getKey().equalsIgnoreCase(PaymentIngestionConstant.REQUESTED_EXECUTION_DATE)) {
									paymentsInstructionsES.setRequestedExecutionDate(childNode.getValue().textValue());
								}
								if (childNode.getKey().equalsIgnoreCase(PaymentIngestionConstant.CREATION_DATE)) {
									paymentsInstructionsES.setCreationDateTime(childNode.getValue().textValue());
								}
							}
						}
					}
				}
			}
			existingDocumentInfo.put(PaymentIngestionConstant.DOC_ID, stateDocId);
			existingDocumentInfo.put(PaymentIngestionConstant.PAYMNT_INSTR_OBJ, paymentsInstructionsES);
			LOGGER.info("channelSeqId::{} paymentInstructionsESObj :::::::: {} ",context.getChannelSeqId(), paymentsInstructionsES);
			return existingDocumentInfo;
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_036 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_036_Desc);
			LOGGER.error("No Existing Record found for the requested channelseqid {}, exception is: {}", context.getChannelSeqId(),e);
			throw new MessageProcessingException("Exception while fetching records");
		}

	}

	/**
	 * Push transition history to ES.
	 *
	 * @param transactionHistoryInfo
	 *            the transaction history info
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void pushTransitionHistoryToES(final TransactionHistoryInfo transactionHistoryInfo)
			throws IOException, MessageProcessingException, DLQException {

		// initializing this variable to zero to clear out the previously stored value
		final Map<String, String> transactionHistoryDocMap = new HashMap<>();

		// checking for replay of the message with eventId and timestamp of the request
		final Map<String, String> transHistoryMap = new HashMap<>();

		final String eventId = Optional.ofNullable(transactionHistoryInfo.getRequestInfo())
				.map(requestInfo -> requestInfo.getEventId()).map(contextEventId -> contextEventId).orElse(null);
		final String ts = Optional.ofNullable(transactionHistoryInfo.getTs()).orElse(null);
		
//		Date currentDate = new Date();
//        DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT3);
//		transactionHistoryInfo.setTs(df.format(currentDate));
		LOGGER.info("transactionHistoryInfo TS :::::::: {} ",transactionHistoryInfo.getTs());
		transHistoryMap.put(PaymentIngestionConstant.REQINFO_EVNTID, eventId);
		transHistoryMap.put(PaymentIngestionConstant.TS, ts);

		final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(
				transHistoryMap, null, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPITransitionHistory());

		if (jestResult != null) {
			final JsonArray hitsArray = Optional.ofNullable(jestResult.getJsonObject())
					.map(jsonObject -> jsonObject.get(PaymentIngestionConstant.HITS)).map(outerHits -> outerHits.getAsJsonObject())
					.map(innerHitJsonObject -> innerHitJsonObject.get(PaymentIngestionConstant.HITS))
					.map(innerHits -> innerHits.getAsJsonArray()).orElse(null);

			if (hitsArray != null) {
				hitsArray.forEach(hit -> {
					final JsonObject historyJson = hit.getAsJsonObject();
					transactionHistoryDocMap.put(PaymentIngestionConstant.TRANSACTION_HISTORY_DOC_ID,
							Optional.ofNullable(historyJson.get(PaymentIngestionConstant._ID)).map(id -> id.toString()).orElse(null));
				});
			}
		}

		LOGGER.info("Existing Transaction history doc id -->> {}",
				transactionHistoryDocMap.get(PaymentIngestionConstant.TRANSACTION_HISTORY_DOC_ID));

		if (StringUtils.isBlank(transactionHistoryDocMap.get(PaymentIngestionConstant.TRANSACTION_HISTORY_DOC_ID))) {
			final JestResult insertedJestResult = paymentIngestionQueryUtil.indexBuilder(transactionHistoryInfo, null,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPITransitionHistory());
			final String insertedDocId = Optional.ofNullable(insertedJestResult.getJsonObject())
					.map(jsonObject -> jsonObject.get(PaymentIngestionConstant._ID)).map(id -> id.toString()).orElse(null);
			LOGGER.info("Pushed transactionHistory to ES with id :::: {}, channelSeqId:::: {} ",insertedDocId, transactionHistoryInfo.getChannelSeqId());
		} else {
			LOGGER.error("Replay Message for the existing Transaction History with eventId {} and TimeStamp {}",eventId, ts);
			throw new DLQException("Received insert request for the existing Transaction history, hence Sending to DLQ");
		}

	}

	/**
	 * Fetch past channel seq ids.
	 *
	 * @param domainSeqId
	 *            the domain seq id
	 * @return the list
	 */
	public List<String> fetchPastChannelSeqIds(final String domainSeqId) {
		final Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put(PaymentIngestionConstant.STATEINFO_DOMAIN_SEQ_ID, domainSeqId);
		matchStrings.put(PaymentIngestionConstant.STATEINFO_DOCSTATUS, PaymentIngestionConstant.DOC_STATUS_TRANSITION);
		final String[] fetchStrings = { PaymentIngestionConstant.PAYMENT_QUERY_MATCH };
		final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings,
				fetchStrings, paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
		final List<PaymentsInstructionsES> paymentsInstructionsESs = jestResult
				.getSourceAsObjectList(PaymentsInstructionsES.class);
		return paymentsInstructionsESs.stream().filter(Objects::nonNull)
				.map(paymentsInstructionsES -> paymentsInstructionsES.getStateInfo())
				.map(stateInfo -> stateInfo.getChannelSeqId()).collect(Collectors.toList());

	}

}
