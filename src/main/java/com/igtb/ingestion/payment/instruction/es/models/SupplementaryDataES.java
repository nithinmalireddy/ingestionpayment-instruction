/*
 * iGTB CIBC Payments
 * The iGTB Payments  product exposes a key set of foundation Action API's which will enable building contextual digital banking experiences. 
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.igtb.ingestion.payment.instruction.es.models;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * SupplementaryData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-08T12:29:54.954+05:30")
public class SupplementaryDataES {
  @JsonProperty("Cnts")
  private String cnts = null;

  @JsonProperty("autoDeposit")
  private String autoDeposit = null;

  /**
   * Gets or Sets settlementMethod
   */
  public enum SettlementMethodEnum {
    A("A"),
    
    L("L");

    private String value;

    SettlementMethodEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SettlementMethodEnum fromValue(String text) {
      for (SettlementMethodEnum b : SettlementMethodEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("settlementMethod")
  private SettlementMethodEnum settlementMethod = null;

  @JsonProperty("panId")
  private String panId = null;

  @JsonProperty("paymentAuthentication")
  private PaymentAuthenticationES paymentAuthentication = null;

  @JsonProperty("invoiceDetails")
  private InvoiceDetailsES invoiceDetails = null;

  public SupplementaryDataES cnts(String cnts) {
    this.cnts = cnts;
    return this;
  }

   /**
   * Envlp field of sendmoney payload
   * @return cnts
  **/
  @ApiModelProperty(value = "Envlp field of sendmoney payload")
  public String getCnts() {
    return cnts;
  }

  public void setCnts(String cnts) {
    this.cnts = cnts;
  }

  public SupplementaryDataES autoDeposit(String autoDeposit) {
    this.autoDeposit = autoDeposit;
    return this;
  }

   /**
   * Get autoDeposit
   * @return autoDeposit
  **/
  @ApiModelProperty(value = "")
  public String getAutoDeposit() {
    return autoDeposit;
  }

  public void setAutoDeposit(String autoDeposit) {
    this.autoDeposit = autoDeposit;
  }

  public SupplementaryDataES settlementMethod(SettlementMethodEnum settlementMethod) {
    this.settlementMethod = settlementMethod;
    return this;
  }

   /**
   * Get settlementMethod
   * @return settlementMethod
  **/
  @ApiModelProperty(value = "")
  public SettlementMethodEnum getSettlementMethod() {
    return settlementMethod;
  }

  public void setSettlementMethod(SettlementMethodEnum settlementMethod) {
    this.settlementMethod = settlementMethod;
  }

  public SupplementaryDataES panId(String panId) {
    this.panId = panId;
    return this;
  }

   /**
   * Get panId
   * @return panId
  **/
  @ApiModelProperty(value = "")
  public String getPanId() {
    return panId;
  }

  public void setPanId(String panId) {
    this.panId = panId;
  }

  public SupplementaryDataES paymentAuthentication(PaymentAuthenticationES paymentAuthentication) {
    this.paymentAuthentication = paymentAuthentication;
    return this;
  }

   /**
   * Get paymentAuthentication
   * @return paymentAuthentication
  **/
  @ApiModelProperty(value = "")
  public PaymentAuthenticationES getPaymentAuthentication() {
    return paymentAuthentication;
  }

  public void setPaymentAuthentication(PaymentAuthenticationES paymentAuthentication) {
    this.paymentAuthentication = paymentAuthentication;
  }

  public SupplementaryDataES invoiceDetails(InvoiceDetailsES invoiceDetails) {
    this.invoiceDetails = invoiceDetails;
    return this;
  }

   /**
   * Get invoiceDetails
   * @return invoiceDetails
  **/
  @ApiModelProperty(value = "")
  public InvoiceDetailsES getInvoiceDetails() {
    return invoiceDetails;
  }

  public void setInvoiceDetails(InvoiceDetailsES invoiceDetails) {
    this.invoiceDetails = invoiceDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SupplementaryDataES supplementaryData = (SupplementaryDataES) o;
    return Objects.equals(this.cnts, supplementaryData.cnts) &&
        Objects.equals(this.autoDeposit, supplementaryData.autoDeposit) &&
        Objects.equals(this.settlementMethod, supplementaryData.settlementMethod) &&
        Objects.equals(this.panId, supplementaryData.panId) &&
        Objects.equals(this.paymentAuthentication, supplementaryData.paymentAuthentication) &&
        Objects.equals(this.invoiceDetails, supplementaryData.invoiceDetails);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cnts, autoDeposit, settlementMethod, panId, paymentAuthentication, invoiceDetails);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SupplementaryData {\n");
    
    sb.append("    cnts: ").append(toIndentedString(cnts)).append("\n");
    sb.append("    autoDeposit: ").append(toIndentedString(autoDeposit)).append("\n");
    sb.append("    settlementMethod: ").append(toIndentedString(settlementMethod)).append("\n");
    sb.append("    panId: ").append(toIndentedString(panId)).append("\n");
    sb.append("    paymentAuthentication: ").append(toIndentedString(paymentAuthentication)).append("\n");
    sb.append("    invoiceDetails: ").append(toIndentedString(invoiceDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

