package com.igtb.ingestion.payment.instruction.models.quest;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class QuestRequest.
 */
@Getter
@Setter
@ToString
public class QuestRequest implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4549644320349580199L;

	/** The query. */
	private String query;
	
	/** The variables. */
	private transient Object variables;
	
	/** The operation name. */
	private String operationName;
	
}
