package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;

import lombok.Getter;
import lombok.Setter;

/**
 * BackendAddRequest.
 */
@Getter
@Setter
public class BackendAddRequest {

	/** The id. */
	private String id;

	/** The event version. */
	private String eventVersion;

	/** The context. */
	@NotNull
	@Valid
	private Context context;

	/** The payload. */
	@NotNull
	@Valid
	private PaymentInstruction payload;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final BackendAddRequest backendAddRequest = (BackendAddRequest) obj;
		return Objects.equals(this.id, backendAddRequest.id)
				&& Objects.equals(this.eventVersion, backendAddRequest.eventVersion)
				&& Objects.equals(this.context, backendAddRequest.context)
				&& Objects.equals(this.payload, backendAddRequest.payload);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id, eventVersion, context, payload);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(100);
		sb.append("class BackendAddRequest {  id: ").append(toIndentedString(id))
				.append("    " + PaymentIngestionConstant.EVENTVERSION + ": ")
				.append(toIndentedString(PaymentIngestionConstant.EVENTVERSION))
				.append("    " + PaymentIngestionConstant.CONTEXT + ": ")
				.append(toIndentedString(PaymentIngestionConstant.CONTEXT))
				.append("    " + PaymentIngestionConstant.PAYLOAD + ": ")
				.append(toIndentedString(PaymentIngestionConstant.PAYLOAD)).append('}');
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 *
	 * @param obj
	 *            the object
	 * @return the string
	 */
	private String toIndentedString(final Object obj) {
		if (obj == null) {
			return "null";
		}
		return obj.toString().replace("\n", "\n    ");
	}

}
