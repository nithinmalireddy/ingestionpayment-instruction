package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UserES.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class UserES {

	/** The first name. */
	@JsonProperty("first_name")
	private String firstName;

	/** The last name. */
	@JsonProperty("last_name")
	private String lastName;

	/** The domain info. */
	private List<UserDomainInfoES> domainInfo;

	/** The image id. */
	private String imageId;

	/** The communication info. */
	private CommunicationInfoES communicationInfo;
	
}
