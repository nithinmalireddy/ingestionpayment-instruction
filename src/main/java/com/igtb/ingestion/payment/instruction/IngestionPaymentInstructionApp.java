package com.igtb.ingestion.payment.instruction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;

import com.igtb.api.action.commons.cfenv.CFEnvHelper;
import com.igtb.filestorage.config.IgtbStorageProperties;

import io.pivotal.labs.cfenv.CloudFoundryEnvironmentException;
import io.pivotal.spring.cloud.service.config.VaultTokenRenewalAutoConfiguration;

/**
 * The Class IngestionPaymentInstructionApp.
 */
@SpringBootApplication(exclude = { VaultTokenRenewalAutoConfiguration.class })
@ComponentScan(basePackages = { "com.igtb.ingestion.payment.instruction", "com.igtb.filestorage" })
@EnableConfigurationProperties({IgtbStorageProperties.class})
@EnableFeignClients
@EnableRetry
public class IngestionPaymentInstructionApp {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(IngestionPaymentInstructionApp.class);

	/** The context. */
	private static ConfigurableApplicationContext context;

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public final ConfigurableApplicationContext getContext() {
		return context;
	}

	/**
	 * Sets the context.
	 *
	 * @param context
	 *            the new context
	 */
	public final void setContext(final ConfigurableApplicationContext context) {
		IngestionPaymentInstructionApp.context = context;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws CloudFoundryEnvironmentException
	 *             the cloud foundry environment exception
	 */
	public static void main(final String[] args) throws CloudFoundryEnvironmentException {
//		run(args);
		new IngestionPaymentInstructionApp().run(args);
		
	}

	/**
	 * Run.
	 *
	 * @param args
	 *            the args
	 * @throws CloudFoundryEnvironmentException
	 *             the cloud foundry environment exception
	 */
	public void run(final String... args) throws CloudFoundryEnvironmentException {
		extractCFEnvParams();
		LOG.info("Starting ingestion application");
		context = SpringApplication.run(IngestionPaymentInstructionApp.class, args);
	}

	/**
	 * Extract CF env params.
	 *
	 * @throws CloudFoundryEnvironmentException
	 *             the cloud foundry environment exception
	 */
	public static void extractCFEnvParams() throws CloudFoundryEnvironmentException {
		CFEnvHelper.readCfEnv();
	}
}

