/**
 * Version no		Author						Comments
 * 1.0				K1 base version				K1 base version
 * 1.1				Anil Ravva					Changes related to Remove the status field PaymentCenterStatus 
 */
package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.igtb.api.ingestion.commons.stateinfo.models.LastAction;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.api.ingestion.commons.transhistory.models.RejectionInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant.PYMNTHSTRY_STATUS;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.SupportingDocumentES;
import com.igtb.ingestion.payment.instruction.es.models.WithholdingTaxES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.models.BackendPayload;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.irtp.PymntInstrTransHistory;
import com.igtb.ingestion.payment.instruction.models.irtp.RequestInfo;

import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import lombok.AllArgsConstructor;

/**
 * The Class PaymentIngestionBackendUtil.
 */
@Component
@AllArgsConstructor
public class PaymentIngestionBackendUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionBackendUtil.class);

	/** The Payment elastic properties. */
	private PaymentElasticProperties paymentElasticProperties;

	/** The Payment ingestion jest util. */
	private PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	static HashMap<String, Boolean> validTHStatus = new HashMap<>();
	
	/** The file related option. */
	private final List<String> fileRelatedOption = new ArrayList<>(Arrays.asList("file_upload", "payroll"));

	/**
	 * Adds the PI supporting documents and WHT doc.
	 *
	 * @param payloadJson
	 *            the payload json
	 * @param context
	 *            the context
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void addPiSupDocWhtDoc(final ObjectNode payloadJson, final Context context)
			throws IOException, DLQException {
		try {

			LOGGER.info("Strat method addPiSupDocWhtDoc() for domainSeqId::::::::{} and payloadStatus::::::{}",
					context.getDomainSeqId(), payloadJson.get(PaymentIngestionConstant.STATUS));
			List<Index> listSuppDocIndex = null;
			List<Index> withHoldingTxList = null;

			// Validate Backend PI ES bean
			final PaymentsInstructionsES paymentInstructionES = validatingBackendBean(payloadJson, context);
			LOGGER.info("paymentInstructionES after validation::::::::::::{} ", paymentInstructionES);
			// build supportingDoc and WHT for one time only
			if (null != paymentInstructionES) {
				if (PaymentIngestionConstant.SENT_TO_BANK
						.equalsIgnoreCase(payloadJson.get(PaymentIngestionConstant.STATUS).asText())
						|| PaymentIngestionConstant.SENT_TO_RECEIVER
								.equalsIgnoreCase(payloadJson.get(PaymentIngestionConstant.STATUS).asText())) {
					LOGGER.info("Status Is matching SENT TO BANK OR SENT TO RECEIVER for domainSeqId::::::{}",
							context.getDomainSeqId());
					final String parentCreateRequestChannelSeqId = Optional
							.ofNullable(CommonUtil.getValueFromJsonNode(payloadJson, PaymentIngestionConstant.BLANK,
									PaymentIngestionConstant.PARENT_CREATE_REQ_CHANNEL_SEQID))
							.map(Object::toString).orElse(null);
					// if active record present build using transition record
					listSuppDocIndex = buildSupportingDocument(context, parentCreateRequestChannelSeqId,
							paymentInstructionES.getSuppDocsAttachments());
					withHoldingTxList = buildWithHoldingTxList(context, parentCreateRequestChannelSeqId,
							paymentInstructionES.getWhtDocsAttachments());
				}
				Boolean isUpdate = false;
				// for file PI to update state in stateInfo
				if (paymentInstructionES.getOption() != null
						&& fileRelatedOption.contains(paymentInstructionES.getOption().toLowerCase(Locale.ENGLISH))) {
					final StateInfo stateInfo = Optional.ofNullable(paymentInstructionES)
							.map(piEs -> piEs.getStateInfo()).orElse(new StateInfo());
					final TransitionInfo transitionInfo = Optional.ofNullable(stateInfo)
							.map(stInfo -> stInfo.getTransitionInfo()).orElse(new TransitionInfo());
					transitionInfo.setState(paymentInstructionES.getCbxUIStatus());
					stateInfo.setTransitionInfo(transitionInfo);
					paymentInstructionES.setStateInfo(stateInfo);
					isUpdate = true;
				}

				LOGGER.info("transaction be updated::::: ?? {} ", isUpdate);
				final JestResult result;

				if (isUpdate) {
					if (!context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
						result = paymentIngestionQueryUtil.updateBuilder(
								objectMapper.convertValue(paymentInstructionES, ObjectNode.class),
								context.getChannelSeqId(), paymentElasticProperties.getPymtIndex(),
								paymentElasticProperties.getPymtType());
					} else {
						result = paymentIngestionQueryUtil.updateBuilder(
								objectMapper.convertValue(paymentInstructionES, ObjectNode.class),
								context.getDomainSeqId(), paymentElasticProperties.getPymtIndex(),
								paymentElasticProperties.getPymtType());
					}
				} else {
					LOGGER.info("Going to Insert Backend_Created Events Data:::::::{}", context.getDomainSeqId());

					result = paymentIngestionQueryUtil.indexBuilder(
							objectMapper.convertValue(paymentInstructionES, ObjectNode.class), context.getDomainSeqId(),
							paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
					if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
						try {
							final StateInfo stateInfo = Optional.ofNullable(paymentInstructionES)
									.map(piEs -> piEs.getStateInfo()).orElse(new StateInfo());
							final LastAction lastAction = Optional.ofNullable(stateInfo)
									.map(piEs -> piEs.getLastAction()).orElse(new LastAction());
							final UserInfo userInfo = Optional.ofNullable(lastAction).map(piEs -> piEs.getUserInfo())
									.orElse(new UserInfo());

//							jsonObj = addIncomingRtpHistory(context.getDomainSeqId(), userInfo,
//									context.getChannelSeqId(), lastAction.getTs(), "insert");						
								addActiveRecordHistory(context, payloadJson, userInfo,false);
								//for pending acceptance status entry used flag without event
								addActiveRecordHistory(context, payloadJson, userInfo,true);								
							
							
						} catch (Exception e) {
							LOGGER.info("Problem in passing parameter ", e.getCause());
							e.printStackTrace();
						}

					}
					UserInfo userInfo = new UserInfo();	
					if (context.getEventType().getStatus().equals(PaymentIngestionConstant.BACKEND_CREATED)) {
						if(payloadJson.has("status") && payloadJson.get("status").asText().equalsIgnoreCase(PaymentIngestionConstant.WAREHOUSE)) {
//							handling Warehouse message for backend_created
							addActiveRecordHistory(context, payloadJson, userInfo,false);								
						}
					}
					LOGGER.info("Result :::::::{} ", result.isSucceeded());
				}

				LOGGER.info("error on pushing data into ES :::::::{} ", result.getErrorMessage());
				// inserting supporting documents
				if (result.isSucceeded() && listSuppDocIndex != null && !listSuppDocIndex.isEmpty()) {
					LOGGER.info("result after pushing into ES :::::::{} ", result.isSucceeded());
					final JestResult suppDocResult = paymentIngestionQueryUtil.insertBulkRecord(
							paymentElasticProperties.getPymtIndex(),
							paymentElasticProperties.getSupportingDocumentsType(), listSuppDocIndex);
					LOGGER.debug("SuppDocuments bulkResult isSucceeded: {}", suppDocResult.isSucceeded());
				}

				// inserting paymentTaxInfo
				if (result.isSucceeded() && withHoldingTxList != null && !withHoldingTxList.isEmpty()) {
					LOGGER.info("Inserting paymentTaxInfo");
					final JestResult paymentTaxInfoResult = paymentIngestionQueryUtil.insertBulkRecord(
							paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType(),
							withHoldingTxList);
					LOGGER.debug("paymentTaxInfo bulkResult isSucceeded: {}", paymentTaxInfoResult.isSucceeded());
				}
			}
		} catch (Exception ex) {
			LOGGER.error(
					PaymentIngestionErrorCodeConstants.ERR_PAYING_026 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_PAYING_026_Desc);
		}
	}
	
	private String getTs() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		Date date = new Date();
		sdf.format(date);
		return sdf.format(date);
	}
	
	public JSONObject addIncomingRtpHistory(String domainSeqId, UserInfo userInfoDetails, String channelSeqId,
			String ts, String action) {
		LOGGER.info("domainSeqId:::::{} userInfoDetails:{}::::channelSeqId:::{}:::ts:::{}", channelSeqId,
				userInfoDetails, channelSeqId, ts);
		String type = "paymentInstructionTransitionHistory";
		JSONObject jsonObject = new JSONObject();
		PymntInstrTransHistory pymntInstrTransHistory = new PymntInstrTransHistory();
		try {
			pymntInstrTransHistory.setChannelSeqId(channelSeqId);
			pymntInstrTransHistory.setDomainSeqId(domainSeqId);
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setEventId(UUID.randomUUID().toString());
			requestInfo.setLinkedEditedChnlReqIds(null);
			requestInfo.setOutcomeCategory("channel-workflow-update");
			requestInfo.setStatus("Backend_created");
			requestInfo.setSourceIdentity("Interac-0.0.7");
			pymntInstrTransHistory.setRequestInfo(requestInfo);
			pymntInstrTransHistory.setKey("done");
			if (action.equals("insert")) {
				pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.CREATED.getDesc());
			} else {
				pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.UPDATED.getDesc());
			}
			List<UserInfo> userInfo = new ArrayList<>();
			if (null == ts || "".equals(ts)) {
				pymntInstrTransHistory.setTs(getTs());
			} else {
				pymntInstrTransHistory.setTs(ts);
			}
			UserInfo info = new UserInfo();
			if (userInfoDetails.getDomainName() == null) {
				info.setDomainName("SYSTEM");
				info.setUserName("SYSTEM");
			} else {
				info.setDomainName(userInfoDetails.getDomainName());
				info.setUserName(userInfoDetails.getUserName());
			}
			info.setEmail(userInfoDetails.getEmail());
			info.setPhone(userInfoDetails.getPhone());
			info.setImageId(userInfoDetails.getImageId());
			info.setRole(userInfoDetails.getRole());
			userInfo.add(info);
			pymntInstrTransHistory.setUserInfo(userInfo);
			pymntInstrTransHistory.setEntityInfo(null);

			jsonObject = new JSONObject(new Gson().toJson(pymntInstrTransHistory));
			jsonObject.put("rejectionInfo", JSONObject.NULL);
			jsonObject.put("entityInfo", JSONObject.NULL);
			LOGGER.info("final jsonObject>>>{}", jsonObject.toString());
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_027 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_027_Desc);
			LOGGER.error("[getFinalApprover] Exception occuured for channelSeqId:{} Exception:{}", channelSeqId, e);
		}

		return jsonObject;
	}

//	@SuppressWarnings("unchecked")
	public JSONObject addActiveRecordHistory(Context context, ObjectNode payloadJson, UserInfo userInfoDetails, Boolean pendingAcceptanceFlag) {
		LOGGER.info("Insert Active Record History for DomainSeqId:{} and payload:{}",context.getDomainSeqId(),payloadJson);
		final JestResult result1;
		JSONObject jsonObject = new JSONObject();
		PymntInstrTransHistory pymntInstrTransHistory = new PymntInstrTransHistory();
		JsonNode paymentInstData ;
		String statusInPayload = payloadJson.has("status") && Strings.isNotBlank(payloadJson.get("status").asText())? payloadJson.get("status").asText().toUpperCase() : null;
		LOGGER.info("Status In Payload : {}" , statusInPayload);
		String serviceKeyInPayload = context.getEventType().getServiceKey();
		String eventStatusInPayload = context.getEventType().getStatus();
		RejectionInfo rejectionInfo = new RejectionInfo();
		rejectionInfo.setSource("system");
		try {
			if (serviceKeyInPayload.contains(PaymentIngestionConstant.FULFILL_RTP)) {
				LOGGER.info("Inside FRTP...");
				if (eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_CREATED)) {
					pymntInstrTransHistory.setChannelSeqId(payloadJson.get("clearingSystemReference").asText());					
				}else {
					if(context.getChannelSeqId()!=null) {
						paymentInstData = paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
						if(paymentInstData.has(PaymentIngestionConstant.ORGN_GRP_INFO_STS_ID)) {
							if(paymentInstData.get(PaymentIngestionConstant.ORGN_GRP_INFO_STS_ID).has(PaymentIngestionConstant.CLER_SYS_REF)) {
								pymntInstrTransHistory.setChannelSeqId(paymentInstData.get(PaymentIngestionConstant.ORGN_GRP_INFO_STS_ID).get(PaymentIngestionConstant.CLER_SYS_REF).asText());								
							}else {
								pymntInstrTransHistory.setChannelSeqId(paymentInstData.get(PaymentIngestionConstant.CLER_SYS_REF).asText());
							}
						}else {
							pymntInstrTransHistory.setChannelSeqId(paymentInstData.get(PaymentIngestionConstant.CLER_SYS_REF).asText());
						}
						
					}else{
						paymentInstData = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
						String cler_sys_ref = paymentInstData.get(PaymentIngestionConstant.CLER_SYS_REF).asText();
						pymntInstrTransHistory.setChannelSeqId(cler_sys_ref);
					}
					LOGGER.info("Setting Decline Reason {}", paymentInstData.get(PaymentIngestionConstant.REASON));
					rejectionInfo.setMessage(paymentInstData.get(PaymentIngestionConstant.REASON).asText());
					LOGGER.info("Setting RejectionInfo {}", rejectionInfo.toString());
				}
			} else {
				paymentInstData = paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
				pymntInstrTransHistory.setChannelSeqId(context.getChannelSeqId());
			}
			// pymntInstrTransHistory.setChannelSeqId(channelSeqId);
			pymntInstrTransHistory.setDomainSeqId(context.getDomainSeqId());
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setEventId(UUID.randomUUID().toString());
			requestInfo.setLinkedEditedChnlReqIds(null);
			requestInfo.setOutcomeCategory(context.getEventType().getOutcomeCategory());
			requestInfo.setStatus(eventStatusInPayload);
			requestInfo.setSourceIdentity(context.getEventSource().getSourceIdentity());
			pymntInstrTransHistory.setRequestInfo(requestInfo);
			pymntInstrTransHistory.setKey("done");
			LOGGER.info("Starting Status and Desc Mapping ... {}",pymntInstrTransHistory);
			if (eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_CREATED)) {
				LOGGER.info(" eventStatusInPayload... {}",eventStatusInPayload);
				if(pendingAcceptanceFlag) {
					LOGGER.info("Got {} status with Desc {}",PYMNTHSTRY_STATUS.PENDING_ACCEPTANCE.getKey(),PYMNTHSTRY_STATUS.PENDING_ACCEPTANCE.getDesc());
					pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.PENDING_ACCEPTANCE.getDesc());
//					pymntInstrTransHistory.setDesc("pending acceptance:by");
				}else if(statusInPayload!=null & statusInPayload.equalsIgnoreCase(PaymentIngestionConstant.WAREHOUSE)) {
					LOGGER.info("Got {} status with Desc {}",PYMNTHSTRY_STATUS.WAREHOUSE.getKey(),PYMNTHSTRY_STATUS.WAREHOUSE.getDesc());
					pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.WAREHOUSE.getDesc());
//					pymntInstrTransHistory.setDesc("PaymentInstruction warehoused:by");
				}
				else {
					LOGGER.info("Got {} status with Desc {}",PYMNTHSTRY_STATUS.CREATED.getKey(),PYMNTHSTRY_STATUS.CREATED.getDesc());
					pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.CREATED.getDesc());
//					pymntInstrTransHistory.setDesc("PaymentInstruction created:by");
				}
			} else if (eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_UPDATED)) {
				LOGGER.info(" eventStatusInPayload... {}",eventStatusInPayload);
				if (statusInPayload!=null) {
					PYMNTHSTRY_STATUS valueOfStatusMap = PYMNTHSTRY_STATUS.fromKey(statusInPayload);
					LOGGER.info("Got {} status with Desc {}",valueOfStatusMap.getKey(),valueOfStatusMap.getDesc());
						pymntInstrTransHistory.setDesc(valueOfStatusMap.getDesc());
					
//					if (payloadJson.get("status").asText().toLowerCase().equalsIgnoreCase(PaymentIngestionConstant.CANCELLED)) {
//						pymntInstrTransHistory.setDesc("PaymentInstruction cancelled:by");
//					}else if (payloadJson.get("status").asText().toLowerCase().equalsIgnoreCase(PaymentIngestionConstant.COMPLETED)) {
//						pymntInstrTransHistory.setDesc("PaymentInstruction completed:by");
//					}else if (payloadJson.get("status").asText().toLowerCase().equalsIgnoreCase(PaymentIngestionConstant.DECLINED)) {
//						pymntInstrTransHistory.setDesc("PaymentInstruction declined:by");
//					}else if (payloadJson.get("status").asText().toLowerCase().equalsIgnoreCase(PaymentIngestionConstant.EXPIRED)) {
//						pymntInstrTransHistory.setDesc("PaymentInstruction expired:by");
//					}else if (payloadJson.get("status").asText().toLowerCase().equalsIgnoreCase(PaymentIngestionConstant.PENDING_ACCEPTANCE)) {
//						pymntInstrTransHistory.setDesc("PaymentInstruction pending acceptance:by");
//					}else {
//						pymntInstrTransHistory.setDesc("PaymentInstruction updated:by");
//					}
				} else {
					LOGGER.info("Got {} status with Desc {}",PYMNTHSTRY_STATUS.UPDATED.getKey(),PYMNTHSTRY_STATUS.UPDATED.getDesc());
					pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.UPDATED.getDesc());
//					pymntInstrTransHistory.setDesc("PaymentInstruction updated:by");
				}
			}else if(eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_PROCESSED)) {
				LOGGER.info("Got {} status with Desc {}",PYMNTHSTRY_STATUS.COMPLETED.getKey(),PYMNTHSTRY_STATUS.COMPLETED.getDesc());
				pymntInstrTransHistory.setDesc(PYMNTHSTRY_STATUS.COMPLETED.getDesc());
//				pymntInstrTransHistory.setDesc("PaymentInstruction completed:by");
			}

//			Date currentDate = new Date();
//			DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT3);
			pymntInstrTransHistory.setTs(getTs());

			List<UserInfo> userInfo = new ArrayList<>();
			UserInfo info = new UserInfo();
			if (userInfoDetails.getDomainName() == null) {
				info.setDomainName("SYSTEM");
				info.setUserName("SYSTEM");
			} else {
				info.setDomainName(Optional.ofNullable(userInfoDetails).map(UserInfo :: getDomainName).orElse(null));
				info.setUserName(Optional.ofNullable(userInfoDetails).map(UserInfo :: getName).orElse(null));
			}
			info.setEmail(Optional.ofNullable(userInfoDetails).map(UserInfo :: getEmail).orElse(Collections.emptyList()));
			info.setPhone(Optional.ofNullable(userInfoDetails).map(UserInfo :: getPhone).orElse(Collections.emptyList()));
			info.setImageId(Optional.ofNullable(userInfoDetails).map(UserInfo :: getImageId).orElse(null));
			info.setRole(Optional.ofNullable(userInfoDetails).map(UserInfo :: getRole).orElse(null));
			userInfo.add(info);
			LOGGER.info("Got UserInfo {}",userInfo);
			if (serviceKeyInPayload.contains(PaymentIngestionConstant.FULFILL_RTP) && 
					(eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_CREATED) || 
							(eventStatusInPayload.equals(PaymentIngestionConstant.BACKEND_UPDATED) && statusInPayload.equalsIgnoreCase(PaymentIngestionConstant.DECLINED))
							)) {
				LOGGER.info("Setting UserInfo {}","EMPTY_LIST");
				pymntInstrTransHistory.setUserInfo(Collections.emptyList());
			}else {
				LOGGER.info("Setting UserInfo {}",userInfo);
				pymntInstrTransHistory.setUserInfo(userInfo);				
			}
			
			pymntInstrTransHistory.setEntityInfo(null);
			
			if(serviceKeyInPayload.contains(PaymentIngestionConstant.SEND_RTP) && payloadJson.has("reason")) {
				String reason = payloadJson.get("reason").asText();
				if(Strings.isNotBlank(reason)){
					rejectionInfo.setMessage(reason);
				} 
				LOGGER.info("Setting RejectionInfo SOURCE = {}, MESSAGE={}", rejectionInfo.getSource(), rejectionInfo.getMessage());
			}
			if(pymntInstrTransHistory.getRejectionInfo() == null && rejectionInfo.getMessage() == null) {
				LOGGER.info("Setting RejectionInfo {}", "null");
				pymntInstrTransHistory.setRejectionInfo(null);
			} else {
				LOGGER.info("Setting RejectionInfo SOURCE = {}, MESSAGE={}", rejectionInfo.getSource(), rejectionInfo.getMessage());
				pymntInstrTransHistory.setRejectionInfo(rejectionInfo);
			}

			jsonObject = new JSONObject(new Gson().toJson(pymntInstrTransHistory));
//			if((jsonObject.has("rejectionInfo") && jsonObject.isNull("rejectionInfo"))||!jsonObject.has("rejectionInfo")) {
//				jsonObject.put("rejectionInfo", JSONObject.NULL);
//			}
//			jsonObject.put("entityInfo"/, JSONObject.NULL);
			LOGGER.info("Before insert Active record history :::::::{} ", jsonObject);
			result1 = paymentIngestionQueryUtil.indexBuilderHistory(jsonObject);
			LOGGER.info("Resuresult1lt :::::::{} ", result1.isSucceeded());
		} catch (Exception e) {
			LOGGER.error(
					PaymentIngestionErrorCodeConstants.ERR_PAYING_027 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_PAYING_027_Desc);
			LOGGER.error("[addActiveRecordHistory] Exception occuured for channelSeqId:{} Exception:{}",
					context.getChannelSeqId(), e);
			e.printStackTrace();
		}

		return jsonObject;
	}

	/**
	 * Method to do mandatory checks on the incoming backend payload.
	 * 
	 * @param payloadJson
	 * @param context
	 * @return
	 * @throws DLQException
	 */
	private PaymentsInstructionsES validatingBackendBean(final ObjectNode payloadJson, final Context context)
			throws DLQException {
		LOGGER.info("Start inside validatingBackendBean for domainSeqId:::::{}", context.getDomainSeqId());
		final String domainSeqId = Optional.ofNullable(context).map(Context::getDomainSeqId).orElse(null);
		if (StringUtils.isBlank(domainSeqId)) {
			throw new DLQException("Mandatory field domainSeqId is not found.");
		}
		PaymentsInstructionsES paymentInstructionES = null;
		try {
			paymentInstructionES = objectMapper.convertValue(payloadJson, PaymentsInstructionsES.class);
		} catch (IllegalArgumentException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_028 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_028_Desc);
			LOGGER.error("Error occured while mapping payloadJSON to ES:::::{}", e.getMessage());
		}
		validateBean(paymentInstructionES, domainSeqId);
		LOGGER.info("End inside validatingBackendBean for domainSeqId:::::{}", context.getDomainSeqId());
		return paymentInstructionES;
	}

	/**
	 * Common Utility method to validate bean.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param channelSeqId
	 *            the channel seq id
	 * @return the list
	 * @throws DLQException
	 *             the DLQ exception
	 */
	private void validateBean(final PaymentsInstructionsES paymentInstruction, final String domainSeqId)
			throws DLQException {
		LOGGER.info("Start inside validateBean for domainSeqId:::::{}", domainSeqId);
		final List<String> errorMessages = new ArrayList<>();
		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		validator.validate(paymentInstruction).stream().forEach(violation -> {
			final StringBuilder messageSet = new StringBuilder(110);
			messageSet.append("\n >>>> Property Path:").append(violation.getPropertyPath())
					.append(" <<<< \n >>>> Value that violated the constraint : ").append(violation.getInvalidValue())
					.append(" <<<< \n >>>> Constraint : ").append(violation.getMessage()).append(" <<<<");
			errorMessages.add(messageSet.toString());
		});

		if (!errorMessages.isEmpty()) {
			LOGGER.error(
					"Backend Validation Failed for the payment instruction with this domainSeqId --> {} and the Error Messages {}",
					domainSeqId, errorMessages);
			throw new DLQException("Bean Validation failed, hence sending to DLQ {}");
		}
		LOGGER.info("end inside validateBean for domainSeqId:::::{}", domainSeqId);
	}

	/**
	 * Builds the supporting document.
	 *
	 * @param context
	 *            the context
	 * @param parentCreateRequestChannelSeqId
	 *            the parent create request channel seq id
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 */
	private List<Index> buildSupportingDocument(final Context context, final String parentCreateRequestChannelSeqId,
			final List<SupportingDocumentES> suppDocsList) // NOPMD longVariable
	{
		final List<Index> listSuppDocIndex = new ArrayList<>();

		// check supportingDocument active doc is present or not
		Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("paymentTransactionId", context.getDomainSeqId());
		matchStrings.put("paymentDocStatus", PaymentIngestionConstant.DOC_STATUS_ACTIVE);
		JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType());

		if (jestResult.isSucceeded()) {
			final JsonObject resultObj = jestResult.getJsonObject();
			final JsonArray hitsArray = Optional.ofNullable(resultObj.get("hits"))
					.map(jsonObj -> jsonObj.getAsJsonObject()).map(json -> json.get("hits"))
					.map(jsonArray -> jsonArray.getAsJsonArray()).orElse(null);
			if (hitsArray != null && hitsArray.size() == 0) {
				LOGGER.debug("No active record found for supportingDocument as yet.");

				// if received in backend payload
				if (suppDocsList != null && suppDocsList.size() > 0) {
					LOGGER.info("Building suppporting documents using backend send attachments");
					suppDocsList.forEach(suppDoc -> listSuppDocIndex.add(new Index.Builder(suppDoc).build()));
				} else {
					matchStrings = new HashMap<>();
					matchStrings.put("paymentInstructionId", parentCreateRequestChannelSeqId);
					matchStrings.put("paymentDocStatus", PaymentIngestionConstant.DOC_STATUS_TRANSITION);
					jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
							paymentElasticProperties.getPymtIndex(),
							paymentElasticProperties.getSupportingDocumentsType());

					if (jestResult.isSucceeded() && !jestResult.getSourceAsStringList().isEmpty()) {
						LOGGER.info("Building supporting documents using transition attachments");
						final List<SupportingDocumentES> suppDocList = jestResult
								.getSourceAsObjectList(SupportingDocumentES.class);
						for (final SupportingDocumentES supportingDocumentES : suppDocList) {
							supportingDocumentES.setPaymentTransactionId(context.getDomainSeqId());
							supportingDocumentES.setPaymentDocStatus(PaymentIngestionConstant.DOC_STATUS_ACTIVE);
							listSuppDocIndex.add(new Index.Builder(supportingDocumentES)
									.id(supportingDocumentES.getDocumentId() + "-" + context.getDomainSeqId()).build());
						}
					} else {
						LOGGER.info(
								"No supp docs linked[transition] with channelSeqId: {} or linked[active] with domainSeqId: {}",
								parentCreateRequestChannelSeqId, context.getDomainSeqId());
					}
				}
			}
		}
		LOGGER.debug("Returning from buildSupportingDocument with listSuppDocIndex size: {}", listSuppDocIndex.size());
		return listSuppDocIndex;
	}

	/**
	 * Builds the with holding tx list.
	 *
	 * @param context
	 *            the context
	 * @param parentCreateRequestChannelSeqId
	 *            the parent create request channel seq id
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 */
	private List<Index> buildWithHoldingTxList(final Context context, final String parentCreateRequestChannelSeqId,
			final List<WithholdingTaxES> whtDocsList) // NOPMD longVariable
	{
		final List<Index> listWithHoldingTxDocIndex = new ArrayList<>();

		// check WHT active doc is present or not
		Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("parentTransactionId", context.getDomainSeqId());
		matchStrings.put("parentDocStatus", PaymentIngestionConstant.DOC_STATUS_ACTIVE);
		JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType());

		if (jestResult.isSucceeded()) {
			final JsonObject resultObj = jestResult.getJsonObject();
			final JsonArray hitsArray = Optional.ofNullable(resultObj.get("hits"))
					.map(jsonObj -> jsonObj.getAsJsonObject()).map(json -> json.get("hits"))
					.map(jsonArray -> jsonArray.getAsJsonArray()).orElse(null);
			if (hitsArray != null && hitsArray.size() == 0) {
				LOGGER.debug("No active record for withholding documents as yet.");

				// if received in backend payload
				if (whtDocsList != null && whtDocsList.size() > 0) {
					LOGGER.info("Building paymentTaxInfo using backend send attachments");
					whtDocsList.forEach(whtDoc -> listWithHoldingTxDocIndex.add(new Index.Builder(whtDoc).build()));
				} else {
					matchStrings = new HashMap<>();
					matchStrings.put("parentInstructionId", parentCreateRequestChannelSeqId);
					matchStrings.put("parentDocStatus", PaymentIngestionConstant.DOC_STATUS_TRANSITION);
					jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, null,
							paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType());
					// if whtdoc present in transition set
					if (jestResult.isSucceeded() && !jestResult.getSourceAsStringList().isEmpty()) {
						LOGGER.info("Building paymentTaxInfo using transition attachments");
						final List<WithholdingTaxES> whtList = jestResult.getSourceAsObjectList(WithholdingTaxES.class);
						for (final WithholdingTaxES withHoldingTaxES : whtList) {
							withHoldingTaxES.setParentTransactionId(context.getDomainSeqId());
							withHoldingTaxES.setParentDocStatus(PaymentIngestionConstant.DOC_STATUS_ACTIVE);
							listWithHoldingTxDocIndex.add(new Index.Builder(withHoldingTaxES).build());
						}
					}

					else {
						LOGGER.info(
								"No withHoldingTx linked[transition] with channelSeqId: {} or linked[active] with domainSeqId: {}",
								parentCreateRequestChannelSeqId, context.getDomainSeqId());
					}
				}
			}
		}
		LOGGER.debug("Returning from buildWithHoldingTxList with listWithHoldingTxDocIndex size: {}",
				listWithHoldingTxDocIndex.size());
		return listWithHoldingTxDocIndex;
	}

	/**
	 * Update payment instruction.
	 *
	 * @param payloadJson
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult updatePaymentInstruction(final ObjectNode payloadJson, final Context context) throws IOException {
		LOGGER.info("Update domainSeqId:::::::{} for payload::::::{}", context.getDomainSeqId(), payloadJson);
		JestResult lJestResult = null;
		String lNextStatus = null;
		String lCurrentStatus = null;
		String lChnlSeqIdCurrentStatus = null;
		String lProductType = null;
		final String[] fetchFields = { "next_status" };
		String lDomainSeqId = context.getDomainSeqId();
		String lChnlSeqId = context.getChannelSeqId();
		JestResult jestResult = null;
		try {
			lNextStatus = (payloadJson.has(PaymentIngestionConstant.STATUS)?payloadJson.get(PaymentIngestionConstant.STATUS).asText():null);
			LOGGER.info("lNextStatus:::::::::{} ",lNextStatus);
			if (null != lNextStatus && (!lNextStatus.equals("null"))){
				lCurrentStatus = paymentIngestionQueryUtil.getStatusUsingDomainSeqId(context.getDomainSeqId());
				if(null != lChnlSeqId && !lChnlSeqId.equals("") && !lChnlSeqId.equalsIgnoreCase("null"))
				{
					lChnlSeqIdCurrentStatus = paymentIngestionQueryUtil.getStatusUsingDomainSeqId(lChnlSeqId);
				}
				LOGGER.info("lCurrentStatus:::::::::{} & lChnlSeqIdCurrentStatus::::::: {} context:::::{}",lCurrentStatus,lChnlSeqIdCurrentStatus,context);
				
				//CIBCCBX-7171 Removing channelSeqId from active doc if setting status back to PENDING ACCEPTANCE TODO : to be tested
				/*if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
					if(PaymentIngestionConstant.channel_state_updated_incomingrtpstatus.equals(context.getEventSource().getSourceIdentity())
							&& PaymentIngestionConstant.BACKEND_UPDATED.equals(context.getEventType().getStatus()) ) {
						if(PaymentIngestionConstant.PENDING_ACCEPTANCE.equals(lNextStatus) 
								&& PaymentIngestionConstant.IN_TRANSIT.equals(lCurrentStatus)) {
							//removing stateInfo.channelSeqId from active doc
							ObjectNode stateInfoCurr =  (ObjectNode) payloadJson.get(PaymentIngestionConstant.STATE_INFO);
							stateInfoCurr.set(PaymentIngestionConstant.CHANNEL_SQ_ID, null);
							payloadJson.set(PaymentIngestionConstant.STATE_INFO, stateInfoCurr);
							LOGGER.info("After setting channelSeqId to null if showing IRTP back in To Fulfill TAB ::: {}",payloadJson);
						}
					}
				}*/
				
				lProductType = context.getEventType().getServiceKey();
				if (lCurrentStatus != null) {
					//Fix for CIBCCBX-3989 ignore if REJECTED status on active doc to show on UI
					if (PaymentIngestionConstant.REJECTED.equalsIgnoreCase(lNextStatus) && !(PaymentIngestionConstant.REJECTED.equalsIgnoreCase(lChnlSeqIdCurrentStatus))) {
						payloadJson.put("paymentCenterStatus", lNextStatus+"-NULL");//1.1 To be Commented
						payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, PaymentIngestionConstant.DOC_STATUS_TRANSITION+lNextStatus);//1.1
					} else {
						payloadJson.put("paymentCenterStatus", lNextStatus+"-NOTNULL");//1.1 To be Commented
						
						Map<String, String> paramMap = new HashMap<String, String>();
						paramMap.put(PaymentIngestionConstant.DOC_STATUS, PaymentIngestionConstant.DOC_STATUS_ACTIVE);
						paramMap.put(PaymentIngestionConstant.DESC, lNextStatus);
						String fetchQryString = paymentIngestionQueryUtil.getFetchQryStatus(paramMap);
						LOGGER.debug("payloadJson:: "+payloadJson+"fetchQryString: "+fetchQryString+"paramMap: "+paramMap);
						if(!fetchQryString.equalsIgnoreCase(PaymentIngestionConstant.DEFAULT_STR)) {
							payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, fetchQryString);
						}else {
							LOGGER.debug("Not Valid status:: fetchQryString: "+fetchQryString);
						}
//						payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, PaymentIngestionConstant.DOC_STATUS_ACTIVE+lNextStatus);//1.1
					}
					
					jestResult = paymentIngestionQueryUtil.getStatusList(lProductType, lCurrentStatus, "subproduct","current_status", fetchFields, paymentElasticProperties.getPymtIndex(), "statusMaster");
				}
				else{
					LOGGER.error("Invalid request id :" + lDomainSeqId + " received from downstream for request id");
				}
				if (jestResult != null) {
					LOGGER.debug("jestResult of getStatusList():::::::::{} ",jestResult.toString());
					final JsonObject resultObj = jestResult.getJsonObject();
					final JsonArray hitsArray = Optional.ofNullable(resultObj.get(PaymentIngestionConstant.HITS)).map(jsonObj -> jsonObj.getAsJsonObject()).map(json -> json.get(PaymentIngestionConstant.HITS)).map(jsonArray -> jsonArray.getAsJsonArray()).orElse(null);
					LOGGER.debug("hitsArray:::::::::{} ",hitsArray);
					if (hitsArray != null && hitsArray.size() > 0) {
						Iterator<JsonElement> iterator = hitsArray.iterator();
						while (iterator.hasNext()) {
							JsonElement objt = iterator.next();
							String nextStatus = objt.getAsJsonObject().get(PaymentIngestionConstant.SOURCE)
									.getAsJsonObject().get("next_status").getAsString();
							LOGGER.info("nextStatus:::::{}",nextStatus);
							if (lNextStatus.contains(nextStatus)) {
								LOGGER.info("Inside If condition::::");
								lJestResult = paymentIngestionQueryUtil.updateBuilder(payloadJson,context.getDomainSeqId(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
								break;
							}
						}
					}
				}
			} else {
				LOGGER.info("Inside else condition:::::");
				lJestResult = paymentIngestionQueryUtil.updateBuilder(payloadJson, context.getDomainSeqId(),paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			}
			if (lJestResult == null) {
				LOGGER.error("Invalid/Same status: " + lNextStatus + " received from downstream for request id :"+ lDomainSeqId);
			}
			
			 if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
				    
				    String creationDateTime=null;
				    if(payloadJson.get("creationDateTime") != null) {
				     creationDateTime=payloadJson.get("creationDateTime").toString();
				    }
				    LOGGER.info("creationDateTime :::::::{} ", creationDateTime);
					try {
						UserInfo userInfo = new UserInfo()	;		
//					jsonObj = addIncomingRtpHistory(context.getDomainSeqId(), userInfo, context.getChannelSeqId(),
//							creationDateTime, "update");
						if (context.getEventType().getStatus().equals(PaymentIngestionConstant.BACKEND_UPDATED)) {							
							if (payloadJson!=null) {
								if(!payloadJson.has(PaymentIngestionConstant.CLER_SYS_REF) && !payloadJson.has(PaymentIngestionConstant.CLER_SYS_REF_UMM) && !payloadJson.has(PaymentIngestionConstant.STATUS)) {
										addActiveRecordHistory(context, payloadJson, userInfo,false);					
								}else if (payloadJson.has("status")) {
									if(checkStatusToUpdate(payloadJson.get("status").asText())) {
										addActiveRecordHistory(context, payloadJson, userInfo,false);
									}
								}
							}
						}else if(context.getEventType().getStatus().equals(PaymentIngestionConstant.BACKEND_PROCESSED)) {
//							If Decline RTP , do not make entry of processed by in Transition History
							if(!lCurrentStatus.equalsIgnoreCase(PaymentIngestionConstant.DECLINED)) {
							addActiveRecordHistory(context, payloadJson, userInfo,false);
							}
						}
					
					} catch (Exception e) {
						LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_029 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
								+ PaymentIngestionErrorCodeConstants.ERR_PAYING_029_Desc);
						 LOGGER.info("Problem in passing parameter ",e.getCause());
						e.printStackTrace();
					}
				
			}else {
				UserInfo userInfo = new UserInfo();
				if (context.getEventType().getStatus().equals(PaymentIngestionConstant.BACKEND_UPDATED)) {
					if (payloadJson!=null && payloadJson.has("status")) {
						if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_MONEY)) {		
							if(!payloadJson.get(PaymentIngestionConstant.STATUS).asText().equalsIgnoreCase(PaymentIngestionConstant.CANCELLED)) {
								if(checkStatusToUpdate(payloadJson.get("status").asText())) {
									addActiveRecordHistory(context, payloadJson, userInfo,false);							
								}
							}
						}else {
							if(checkStatusToUpdate(payloadJson.get("status").asText())) {
								addActiveRecordHistory(context, payloadJson, userInfo,false);							
							}
						}
					}
				}else if (context.getEventType().getStatus().equals(PaymentIngestionConstant.BACKEND_PROCESSED)) {
					addActiveRecordHistory(context, payloadJson, userInfo,false);												
				}
				
			}
		} catch (Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_030 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_030_Desc);
			LOGGER.error("Excetpion in updatePaymentInstruction::", ex);
		}
		return lJestResult;
	}
		private boolean checkStatusToUpdate(final String status) {
		LOGGER.info("Start isValidTHChannelStatus() with status::::::::{}",status);
		validTHStatus.put(PaymentIngestionConstant.CANCELLED, true);
		validTHStatus.put(PaymentIngestionConstant.COMPLETED, true);
		validTHStatus.put(PaymentIngestionConstant.DECLINED, true);
		validTHStatus.put(PaymentIngestionConstant.EXPIRED, true);
		validTHStatus.put(PaymentIngestionConstant.WAREHOUSE, true);
		validTHStatus.put(PaymentIngestionConstant.PENDING_ACCEPTANCE, true);

		return Optional.ofNullable(validTHStatus.get(status)).orElse(false);
	}
	/**
	 * Update payment instruction for transition doc with channelSeqId.
	 *
	 * @param payloadJson
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult updatePaymentInstructionChannelSeqId(final ObjectNode payloadJson, final Context context)
			throws IOException {
		LOGGER.info("Update payment instruction for channelSeqId: {}", context.getChannelSeqId());
		return paymentIngestionQueryUtil.updateBuilder(payloadJson, context.getChannelSeqId(),paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
	}

	/**
	 * Builds the PI and insert.
	 *
	 * @param payloadJson
	 *            the payload json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public void buildPIAndInsert(final ObjectNode payloadJson) throws IOException, MessageProcessingException {
		LOGGER.info("Building and inserting active PI");
		final BackendPayload backendPayload = objectMapper.readValue(payloadJson.toString(), BackendPayload.class);
		final Set<String> transactionIds = backendPayload.getTxnData().stream()
				.map(txn -> backendPayload.getFileId() + "." + txn.getInternalTxnId()).collect(Collectors.toSet());

		final JestResult jestResult = paymentIngestionQueryUtil.constructMultiGetQuery(
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType(), "transactionId",
				transactionIds);

		if (!jestResult.isSucceeded()) {
			LOGGER.error("Error while fetching existing paymentInstruction for transactionIds: {} as jestResult: {}",
					transactionIds, jestResult);
			throw new MessageProcessingException(
					"Error while fetching existing paymentInstruction for transactionIds: " + transactionIds);
		} else if (transactionIds.size() != jestResult.getSourceAsStringList().size()) {
			LOGGER.error("Count missmatch for existing PI, expected record: {} but found record: {}",
					transactionIds.size(), jestResult.getSourceAsStringList().size());
			throw new MessageProcessingException("Error while fetching existing paymentInstruction, count mismatch");
		}

		final List<String> pIStringList = jestResult.getSourceAsStringList();
		final List<PaymentsInstructionsES> existingPIs = objectMapper.readValue(pIStringList.toString(),
				TypeFactory.defaultInstance().constructCollectionType(List.class, PaymentsInstructionsES.class));
		final List<Index> piIndex = new ArrayList<>();
		StateInfo stateInfo;

		for (final PaymentsInstructionsES paymentInstr : existingPIs) {
			stateInfo = paymentInstr.getStateInfo();
			if (paymentInstr.getStateInfo().getTransitionInfo().getState().equalsIgnoreCase("failed")) {
				paymentInstr.setCbxUIStatus("Failed in validation");
			} else if (paymentInstr.getPaymentTypesObj().getCode().equalsIgnoreCase("RTP")) {
				stateInfo.getTransitionInfo().setState(PaymentIngestionConstant.SENT_TO_RECEIVER);
				paymentInstr.setCbxUIStatus(PaymentIngestionConstant.SENT_TO_RECEIVER);
				paymentInstr.setCbxSupportStatus("none");
			} else {
				stateInfo.getTransitionInfo().setState(PaymentIngestionConstant.SENT_TO_BANK);
				paymentInstr.setCbxUIStatus(PaymentIngestionConstant.SENT_TO_BANK);
				paymentInstr.setCbxSupportStatus("none");
			}

			stateInfo.setDocStatus(PaymentIngestionConstant.DOC_STATUS_ACTIVE);
			stateInfo.setDomainSeqId(paymentInstr.getTransactionId());
			paymentInstr.setStateInfo(stateInfo);
			LOGGER.info("PaymentInstruction to be inserted into ES ::::::::: {} ", paymentInstr);
			piIndex.add(new Index.Builder(objectMapper.writeValueAsString(paymentInstr))
					.id(paymentInstr.getTransactionId()).build());
		}

		// inserting active PI
		if (!piIndex.isEmpty()) {
			LOGGER.debug("Inserting active payment instructions");
			final JestResult result = paymentIngestionQueryUtil.insertBulkRecord(
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType(), piIndex);
			LOGGER.debug("active payment instructions insert bulkResult isSucceeded: {}", result.isSucceeded());
		}

		LOGGER.info("Building and inserting active PI done for {}", piIndex.size());
	}

}
