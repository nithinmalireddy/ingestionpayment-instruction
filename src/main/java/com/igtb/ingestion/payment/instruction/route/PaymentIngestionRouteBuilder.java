package com.igtb.ingestion.payment.instruction.route;

import java.util.Map;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.rabbitmq.RabbitMQConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.mapper.PaymentCamelMapper;

import lombok.AllArgsConstructor;

/**
 * The Class PaymentIngestionRouteBuilder.
 */
@Component
@AllArgsConstructor
public class PaymentIngestionRouteBuilder extends RouteBuilder {

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionRouteBuilder.class);

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.camel.builder.RouteBuilder#configure()
	 */
	@Override
	public void configure() throws Exception {

		//USE for CIBC
		final String signlePI = urlBuilder(
				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.INGESTION_PAYMENT_INSTRUCTION));
		final String singlePIFromKafka = customUrlBuilder(
				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.REQUEUE_STATE_EVENTS_FROM_KAFKA));
		LOGGER.info("PaymentRabbitConsumer connection url1 --->>> {} ", signlePI);
		//end for CIBC
		// For file
//		final String ingestionPISet = customUrlBuilder(
//				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.INGESTION_PI_SET));
//		final String ingestionPISetFromKafka = customUrlBuilder(
//				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.INGESTION_PI_SET_FROM_KAFKA));
//		LOGGER.info("PaymentRabbitConsumer connection url2 --->>> {} ", signlePI);

		// For bulk
//		final String ingestionPIBulk = customUrlBuilder(
//				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.INGESTION_BULK));
//		final String ingestionPIBulkFromKafka = customUrlBuilder(
//				(Map<String, String>) paymentRabbitProperties.getReceiver().get(PaymentIngestionConstant.INGESTION_PI_BULK_FROM_KAFKA));
//		LOGGER.info("PaymentRabbitConsumer connection url3 --->>> {} ", signlePI);

		// For retry (push back to the queue)-CIBC
		onException(RequeueException.class).delay(paymentRabbitProperties.getRmqDelayTime()).asyncDelayed().setHeader(RabbitMQConstants.REQUEUE,
				constant(Boolean.TRUE));

		// For DLQ-CIBC
		onException(DLQException.class).onException(MessageProcessingException.class).maximumRedeliveries(paymentRabbitProperties.getRmqMaxRedelivery())
				.redeliveryDelay(paymentRabbitProperties.getRmqRedelvDelayTime()).asyncDelayedRedelivery()
				.log(LoggingLevel.ERROR, LOGGER,
						"SENDING TO DLQ ->> \"${body}\" for the cause of ->  \"${exception.message}\"")
				.useOriginalMessage().setExchangePattern(ExchangePattern.InOnly).setFaultBody(constant(true))
				.setHeader(RabbitMQConstants.REQUEUE, constant(Boolean.FALSE)).handled(true);

		// For retry (push back to the queue)-CIBC
		onException(Exception.class).maximumRedeliveries(paymentRabbitProperties.getRmqMaxRedelivery()).redeliveryDelay(paymentRabbitProperties.getRmqDelayTime()).asyncDelayedRedelivery()
				.log(LoggingLevel.ERROR, LOGGER,
						"Exception OCCURED ->>  \"${exception.message}\" SENDING TO DLQ ->> \"${body}\" ")
				.useOriginalMessage().setExchangePattern(ExchangePattern.InOnly).setFaultBody(constant(true))
				.setHeader(RabbitMQConstants.REQUEUE, constant(Boolean.FALSE)).handled(false);

		// cbxEvents listener-CIBC
		from(signlePI).startupOrder(PaymentIngestionConstant.CAMEL_START_ORDER_ONE).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_RMQ_PI)
				.log(LoggingLevel.INFO, LOGGER, "Received msg for single PI as ---->>>> ${body}")
				.process(new PaymentCamelMapper()).to("bean:paymentRabbitConsumer?method=rabbitListener(${body})")
				.setExchangePattern(ExchangePattern.InOnly).end();

		// pushing 3 [backend_ack_success, backend_rejected, backend_processed] state
		// transition events from kafka to a Custom rmq endpoint-CIBC
		from("direct:stateeventsfromkafka").startupOrder(PaymentIngestionConstant.CAMEL_START_ORDER_TWO).routeId(PaymentIngestionConstant.KAFKA_EVENT_TO_RMQ_PI)
				.log(LoggingLevel.INFO, LOGGER, "Received state transition message from kafka ${body}")
				.to(ExchangePattern.InOnly, singlePIFromKafka).setExchangePattern(ExchangePattern.InOnly).end();

		// [receives the transition events from a payment specific rmq endpoint]
		from(singlePIFromKafka).startupOrder(PaymentIngestionConstant.CAMEL_START_ORDER_THREE).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_KAFKA_PI)
				.log(LoggingLevel.INFO, LOGGER, "Received message eventHandler ${body}")
				.process(new PaymentCamelMapper()).to("bean:paymentRabbitConsumer?method=rabbitListener(${body})")
				.setExchangePattern(ExchangePattern.InOnly).end();

		/**
		 * File related queue
		 */

		// [receives the transition events from a payment file specific rmq endpoint]
//		from(ingestionPISet).startupOrder(4).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_RMQ_PI_SET)
//				.log(LoggingLevel.INFO, LOGGER, "Received msg for file level PI as ---->>>> ${body}")
//				.process(new PaymentCamelMapper())
//				.to("bean:fileProcessingRabbitConsumer?method=fileRabbitListener(${body})")
//				.setExchangePattern(ExchangePattern.InOnly).end();

		// pushing 3 [backend_ack_success, backend_rejected, backend_processed] state
		// transition events from kafka to a Custom rmq endpoint
//		from("direct:filestateeventsfromkafka").startupOrder(6).routeId(PaymentIngestionConstant.KAFKA_EVENT_TO_RMQ_PI_SET)
//				.to(ExchangePattern.InOnly, ingestionPISetFromKafka).setExchangePattern(ExchangePattern.InOnly).end();

		// [receives the transition events from a payment specific rmq endpoint]
//		from(ingestionPISetFromKafka).startupOrder(5).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_KAFKA_PI_SET)
				// .log(LoggingLevel.INFO, LOGGER, "Received message eventHandler ${body}")
//				.process(new PaymentCamelMapper())
//				.to("bean:fileProcessingRabbitConsumer?method=fileRabbitListener(${body})")
//				.setExchangePattern(ExchangePattern.InOnly).end();

		/**
		 * Bulk related queue
		 */

		// [receives the transition events from a payment bulk specific rmq endpoint]
//		from(ingestionPIBulk).startupOrder(7).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_RMQ_PI_BULK)
//				.log(LoggingLevel.INFO, LOGGER, "Received msg for Bulk PI as ---->>>> ${body}")
//				.process(new PaymentCamelMapper())
//				.to("bean:bulkProcessingRabbitConsumer?method=bulkRabbitListener(${body})")
//				.setExchangePattern(ExchangePattern.InOnly).end();

		// pushing 3 [backend_ack_success, backend_rejected, backend_processed] state
		// transition events from kafka to a Custom rmq endpoint
//		from("direct:bulkstateeventsfromkafka").startupOrder(9).routeId(PaymentIngestionConstant.KAFKA_EVENT_TO_RMQ_PI_BULK)
//				.to(ExchangePattern.InOnly, ingestionPISetFromKafka).setExchangePattern(ExchangePattern.InOnly).end();

		// [receives the transition events from a payment specific rmq endpoint]
//		from(ingestionPIBulkFromKafka).startupOrder(8).routeId(PaymentIngestionConstant.TRANSITION_EVENTS_FROM_KAFKA_PI_BULK)
				// .log(LoggingLevel.INFO, LOGGER, "Received message eventHandler ${body}")
//				.process(new PaymentCamelMapper())
//				.to("bean:bulkProcessingRabbitConsumer?method=bulkRabbitListener(${body})")
//				.setExchangePattern(ExchangePattern.InOnly).end();

	}

	/**
	 * Url builder.
	 *
	 * @param rmqConfig
	 *            the rmq config
	 * @return the string
	 */
	private String urlBuilder(final Map<String, String> rmqConfig) {
		return paymentRabbitProperties.getName() + "://" + paymentRabbitProperties.getHost() + ":"
				+ paymentRabbitProperties.getPort() + "/" + paymentRabbitProperties.getExchange() + "?exchangeType="
				+ paymentRabbitProperties.getExchangeType() + "&username=" + paymentRabbitProperties.getUser()
				+ "&password=" + paymentRabbitProperties.getPassword() + "&autoDelete=false" + "&autoAck=false"
				+ "&automaticRecoveryEnabled=false" + "&routingKey=" + rmqConfig.get("routingKey") + "&queue="
				+ rmqConfig.get("queue") + "&vhost=" + paymentRabbitProperties.getVhost() + "&concurrentConsumers=3"
				+ "&prefetchEnabled=true" + "&prefetchCount=" + paymentRabbitProperties.getPrefatchCount()
				+ "&deadLetterExchange=" + paymentRabbitProperties.getExchange() + "&deadLetterRoutingKey="
				+ rmqConfig.get("deadLetterRoutingKey") + "&deadLetterQueue=" + rmqConfig.get("deadLetterQueue")
				+ "&deadLetterExchangeType=" + paymentRabbitProperties.getExchangeType();
	}

	/**
	 * Custom url builder.
	 *
	 * @param rmqConfig
	 *            the rmq config
	 * @return the string
	 */
	private String customUrlBuilder(final Map<String, String> rmqConfig) {
		return paymentRabbitProperties.getName() + "://" + paymentRabbitProperties.getHost() + ":"
				+ paymentRabbitProperties.getPort() + "/" + rmqConfig.get("exchange") + "?exchangeType="
				+ paymentRabbitProperties.getExchangeType() + "&username=" + paymentRabbitProperties.getUser()
				+ "&password=" + paymentRabbitProperties.getPassword() + "&autoDelete=false" + "&autoAck=false"
				+ "&automaticRecoveryEnabled=false" + "&routingKey=" + rmqConfig.get("routingKey") + "&queue="
				+ rmqConfig.get("queue") + "&vhost=" + paymentRabbitProperties.getVhost() + "&concurrentConsumers=3"
				+ "&prefetchEnabled=true" + "&prefetchCount=" + paymentRabbitProperties.getPrefatchCount()
				+ "&deadLetterExchange=" + rmqConfig.get("deadLetterExchange") + "&deadLetterRoutingKey="
				+ rmqConfig.get("deadLetterRoutingKey") + "&deadLetterQueue=" + rmqConfig.get("deadLetterQueue")
				+ "&deadLetterExchangeType=" + paymentRabbitProperties.getExchangeType();
	}

}
