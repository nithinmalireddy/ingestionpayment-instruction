package com.igtb.ingestion.payment.instruction.config;

import java.util.Optional;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.igtb.api.action.commons.db.redis.client.JedisFactory;
import com.igtb.api.action.commons.exceptions.ConfigException;
import com.igtb.api.action.commons.util.CommonsConstants;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;

import redis.clients.jedis.Jedis;
import redis.clients.util.Pool;

/**
 * The configuration class that sets up the requisite environment for using
 * Jedis to so redis operations.
 */
@Configuration
public class PaymentRedisClientConfig {

	/**
	 * Logger object is initialized for RedisLimitsDataService class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentRedisClientConfig.class);

	/** environment to get the redis application properties. */
	@Autowired
	private Environment environment;

	/** Jedis pool object that manages connection pooling. */
	//public JedisPool jedisPool;
	
	/** The jedis pool instance. */
	//private Pool<Jedis> pool;

	/** actual Jedis client for redis CRUD. */
	//public Jedis jedis;

	@Autowired
	private ApplicationContext appContext;

	/**
	 * Gets the redis connection.
	 *
	 * @return the pool
	 */
	@Bean
	public Pool<Jedis> redisConnection() {
		try {
			final Properties properties = new Properties();

			properties.put(CommonsConstants.CFG_KEY_REDIS_SENTINEL_FLAG,
					environment.getProperty(CommonsConstants.CFG_KEY_REDIS_SENTINEL_FLAG));
			properties.put(CommonsConstants.CFG_KEY_REDIS_DB_URL,
					Optional.ofNullable(environment.getProperty(CommonsConstants.CFG_KEY_REDIS_DB_URL)).orElse(null));
			properties.put(CommonsConstants.CFG_KEY_REDIS_DB_PWD,
					Optional.ofNullable(environment.getProperty(CommonsConstants.CFG_KEY_REDIS_DB_PWD)).orElse(null));
			properties.put(CommonsConstants.CFG_KEY_REDIS_DB_INSTANCE, Optional
					.ofNullable(environment.getProperty(CommonsConstants.CFG_KEY_REDIS_DB_INSTANCE)).orElse(null));
			properties.put(CommonsConstants.CFG_KEY_REDIS_SENTINEL_URLS, Optional
					.ofNullable(environment.getProperty(CommonsConstants.CFG_KEY_REDIS_SENTINEL_URLS)).orElse(null));
			properties.put(CommonsConstants.CFG_KEY_REDIS_MASTER_NAME, Optional
					.ofNullable(environment.getProperty(CommonsConstants.CFG_KEY_REDIS_MASTER_NAME)).orElse(null));
			// Initialize Jedis Factory
			return JedisFactory.initialize(properties);
		} catch (ConfigException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_089 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_089_Desc);
			// throw custom Exception
			LOGGER.error("{} - Exception thrown while initializing JedisFactory", CommonsConstants.SYS_REDIS);
			// cancel startup in case of error
			SpringApplication.exit(appContext);
		}
		return null;
	}
}
