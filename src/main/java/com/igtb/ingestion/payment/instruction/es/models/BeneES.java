package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class BeneES.
 */
@Getter
@Setter
public class BeneES {

	/** The city. */
	private String city;
	
	/** The branch. */
	private String branch;
}
