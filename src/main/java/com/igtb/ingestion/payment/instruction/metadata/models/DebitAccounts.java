package com.igtb.ingestion.payment.instruction.metadata.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class DebitAccounts.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitAccounts {

    /** The ids. */
    private List<String> ids;

}
