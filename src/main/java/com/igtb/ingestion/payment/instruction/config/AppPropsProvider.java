package com.igtb.ingestion.payment.instruction.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class AppPropsProvider.
 */
@Component
@Getter
@Setter
public class AppPropsProvider {

	/** The quest router name. */
	@Value("${digital.quest.routerName}")
	private String questRouterName;
	
	/** The quest uri. */
	@Value("${digital.quest.uri}")
	private String questUri;
	
	/** The quest header key. */
	@Value("${digital.quest.secret.header.key}")
	private String questHeaderKey;
	
	/** The quest header value. */
	@Value("${digital.quest.secret.header.value}")
	private String questHeaderValue;
	
	/** The target base ccy. */
	@Value("${digital.targetCcy}")
	private String targetBaseCcy;
	
	/** The get eq amount query. */
	@Value("${digital.query.getEqAmountQuery}")
	private String getEqAmountQuery;
		
}
