package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Details of a user/system whose request has resulted into this event.
 */

@Getter
@Setter
public class Requester {
  
  /** The id. */
  @JsonProperty("id")
  private String id;

  /** The domain id. */
  @JsonProperty("domainId")
  private String domainId;

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final Requester requester = (Requester) obj;
    return Objects.equals(this.id, requester.id) &&
        Objects.equals(this.domainId, requester.domainId);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(id, domainId);
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(50);
    sb.append("class Requester {  id: ").append(toIndentedString(id))
      .append("    domainId: ").append(toIndentedString(domainId))
      .append('}');
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param obj the obj
   * @return the string
   */
  private String toIndentedString(final Object obj) {
    if (obj == null) {
      return "null";
    }
    return obj.toString().replace("\n", "\n    ");
  }

}

