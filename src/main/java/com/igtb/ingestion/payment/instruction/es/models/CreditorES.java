package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class CreditorES.
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
public class CreditorES {

	/** The scheme name. */
	//to be used to only for schemeNames for PI and SI
	@JsonIgnore
	private String schemeName;
	
	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	/** The contact address. */
	private ContactAdressES contactAddress;
	
	/** The designation. */
	private String designation;
	
	/** The communication info. */
	private CommInfoES communicationInfo;
	
	private String orgId;
}
