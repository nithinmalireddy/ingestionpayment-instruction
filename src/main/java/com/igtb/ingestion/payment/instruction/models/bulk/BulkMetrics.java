package com.igtb.ingestion.payment.instruction.models.bulk;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class BulkMetrics.
 */
@Setter
@Getter
@ToString
public class BulkMetrics {

	/** The approved amount. */
	private Double approvedAmount;
	
	/** The max amount. */
	private Double maxAmount;
	
	/** The min amount. */
	private Double minAmount;
	
	/** The rejected amount. */
	private Double rejectedAmount;
	
	/** The skipped amount. */
	private Double skippedAmount;
	
	/** The total amount. */
	private Double totalAmount;
	
	/** The rejected validation count. */
	private Integer rejectedValidationCount;

	/** The rejected validation amount. */
	private Double rejectedValidationAmount;
	
	/** The total count. */
	private Integer totalCount;
}
