package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class RejectError.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RejectError {

	/** The code. */
	private String code;
	
	/** The field. */
	private String field;
	
	/** The message. */
	private String message;
	
}
