package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CorrespondentES.
 */
@Getter
@Setter
public class CorrespondentES {
	
	/** The city. */
	private String city;
	
	/** The bank. */
	private String bank;
	
	/** The branch. */
	private String branch;
}
