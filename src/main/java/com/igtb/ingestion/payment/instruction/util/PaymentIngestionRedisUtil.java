package com.igtb.ingestion.payment.instruction.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.api.action.commons.db.redis.client.JedisFactory;
import com.igtb.api.action.commons.util.ValUtil;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRedisProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * The Class ContactIngestionRedisUtil.
 */
@Component
public class PaymentIngestionRedisUtil {

	/** The contact redis properties. */
	@Autowired
	private PaymentRedisProperties paymentRedisProperties;

	/** The Constant LOGGER. */
	private static final Logger logger = LoggerFactory.getLogger(PaymentIngestionRedisUtil.class);

	/** Redis Sentinel Failover timeout message. */
	private static final String FO_TIMEOUT_MSG = "Exceeded retry limit {}ms for this Redis data operation...giving up!";

	/**
	 * Message constant indicating connection to be retried due to disconnection.
	 */
	private static final String CONN_RETRY_MSG = "Redis Connection Error...retry : {}";

	/** is redis sentinal enabled. */
	private static boolean sentinelEnabled;

	/**
	 * Gets the redis key
	 * 
	 * @param channelSeqId
	 *            the channel seq id
	 * @return the redis key
	 */

	public static String getRedisKeyForPurgeBatchLock(final String channelSeqId) {
		return PaymentIngestionConstant.REDIS_LOCK_KEY_PREFIX + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ PaymentIngestionConstant.REDIS_DOMAIN_KEY + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ "state-ingestion" + PaymentIngestionConstant.REDIS_KEY_SEPARATOR + channelSeqId;
	}

	/*	*//**
			 * Gets the redis key
			 * 
			 * @param accountId
			 *            the accountId
			 * @return the redis key
			 * 
			 * 
			 * 
			 *         Acquire purge batch lock.
			 *
			 * @param channelSeqId
			 *            the channel seq id
			 * @return true, if successful
			 */
	public boolean acquirePurgeBatchLock(final String channelSeqId) {

		logger.info("Start acquirePurgeBatchLock() for ChannelSeqId:::::::{}", channelSeqId);
		logger.debug("called for payloadType={}", channelSeqId);
		final String lockKey = getRedisKeyForPurgeBatchLock(channelSeqId);
		return acquireLock(lockKey);
	}

	/*	*//**
			 * Release lock.
			 *
			 * @param channelSeqId
			 *            the channel seq id
			 * @return true, if successful
			 * @throws RequeueException
			 *             the RequeueException Release lock.
			 *
			 * @param channelSeqId
			 *            the channel seq id
			 * @return true, if successful
			 * @throws RequeueException
			 *             the RequeueException
			 */
	public boolean releasePurgeBatchLock(final String channelSeqId) throws RequeueException {

		logger.info("Start releasePurgeBatchLock() for ChannelSeqId:::::::{}", channelSeqId);
		logger.debug("called for debug={}", channelSeqId);
		final String lockKey = getRedisKeyForPurgeBatchLock(channelSeqId);
		return releaseLock(lockKey);
	}

	/*	*//**
			 * Acquire lock.
			 * 
			 * @param accountId
			 *            the accountId
			 * @return true, if successful
			 * @throws RequeueException
			 *             the RequeueException
			 * @param lockKey
			 *            the lockKey
			 * @return true, if successful
			 */
	private boolean acquireLock(final String lockKey) {

		boolean acquired = false;
		logger.info("“Lock obtained on {}", lockKey);

		// Create lock entry into the state store with expiry time
		// perform data operation until succeeds/timeouts
		final long startTime = System.currentTimeMillis();
		long endTime = 0;
		while (true) {
			Pipeline pipeline = null;
			try (Jedis jedis = JedisFactory.getInstance()) {

				pipeline = jedis.pipelined(); // open pipeline
				pipeline.watch(lockKey); // ensure it doesn't get updated in parallel
				pipeline.multi();
				final int keyExpiryInSec = paymentRedisProperties.getMaxWaitTime();
				final Response<String> lockStrResp = pipeline.set(lockKey,"Purge Batch key - will be removed on completion or expiry - " + keyExpiryInSec + " sec", "NX","EX", keyExpiryInSec);
				pipeline.exec(); // close transaction
				pipeline.sync(); // execute and close pipeline

				final String response = lockStrResp.get();
				if (ValUtil.strFieldIsEmpty(response)) {
					// perhaps, lock already acquired by other instance
					logger.error("Purge Batch key {} couldn't be locked - response {}", lockKey, response);
				} else if (PaymentIngestionConstant.OK_MSG.equalsIgnoreCase(response)) {
					acquired = true;
				} else {
					logger.error("Unsupported Response [{}] seen while locking key {}", response, lockKey);
				}

				break; // break; as this data operation succeeded
			} catch (JedisConnectionException ce) {
				logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_053 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_053_Desc);
				logger.warn(CONN_RETRY_MSG, ce.getMessage(), ce); // potentially, Failover in progress
				if (!sentinelEnabled) {
					return false;
				}
				try {
					Thread.sleep(paymentRedisProperties.getRetryIntervalInMs());
				} catch (Exception ex) {
					logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_054 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_PAYING_054_Desc);
					logger.error(ex.getMessage(), ex);
				} // between each failure retry
			} catch (Exception ex) {
				logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_055 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_055_Desc);
				logger.error("Exception during Redis Lock operation : {}", ex.getMessage());
				logger.error(ex.getMessage(), ex);
				return false;
			} finally {
				if (pipeline != null && pipeline.isInMulti()) {
					try {
						pipeline.discard();
					} catch (Exception ex) {
						logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_056 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
								+ PaymentIngestionErrorCodeConstants.ERR_PAYING_056_Desc);
						logger.error(ex.getMessage(), ex);
					}
				}
			}
			// Return if retry limit exceeded
			endTime = System.currentTimeMillis();
			if (endTime >= startTime + paymentRedisProperties.getMaxWaitTime()) {
				logger.error(FO_TIMEOUT_MSG, paymentRedisProperties.getMaxWaitTime());
				return false;
			}
		}

		logger.debug("returning acquiredFlag={}", acquired);
		return acquired;
	}

	/**
	 * @param lockKey
	 *            the lockKey
	 * @return true, if successful
	 */
	private boolean releaseLock(final String lockKey) throws RequeueException {

		boolean released = false;
		// Delete existing lock entry from state store
		// perform data operation until succeeds/timeouts
		final long startTime = System.currentTimeMillis();
		long endTime = 0;
		while (true) {
			Pipeline pipeline = null;
			try (Jedis jedis = JedisFactory.getInstance()) {

				pipeline = jedis.pipelined(); // open pipeline
				pipeline.watch(lockKey); // ensure it doesn't get updated in parallel
				pipeline.multi(); // open transaction
				final Response<Long> lockStrResp = pipeline.del(lockKey); // remove lock
				pipeline.exec(); // close transaction
				pipeline.sync(); // execute and close pipeline

				final Long cnt = lockStrResp.get();
				if (cnt == null) {
					logger.warn("Batch key {} couldn't be released", lockKey);
				} else if (cnt.intValue() >= 1) {
					released = true;
				} else {
					logger.error("Unsupported Response [{}] seen while releasing key {}", cnt.intValue(), lockKey);
				}

				break; // break; as this data operation succeeded
			} catch (JedisConnectionException ce) {
				logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_057 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_057_Desc);
				logger.warn(CONN_RETRY_MSG, ce.getMessage(), ce); // potentially, Failover in progress
				if (!sentinelEnabled) {
					throw new RequeueException("Exception thrown while taking redis lock hence requing the event");
				}
				try {
					Thread.sleep(paymentRedisProperties.getRetryIntervalInMs());
				} catch (Exception ex) {
					logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_058 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_PAYING_058_Desc);
					logger.error(ex.getMessage(), ex);
				} // between each failure retry
			} catch (Exception ex) {
				logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_059 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_059_Desc);
				logger.error("Exception during Redis Lock operation : {}", ex.getMessage());
				logger.error(ex.getMessage(), ex);
				return false;
			} finally {
				if (pipeline != null && pipeline.isInMulti()) {
					try {
						pipeline.discard();
					} catch (Exception ex) {
						logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_060+ PaymentIngestionErrorCodeConstants.ERR_SPLITER
								+ PaymentIngestionErrorCodeConstants.ERR_PAYING_060_Desc);
						logger.error(ex.getMessage(), ex);
					}
				}
			}
			// Return if retry limit exceeded
			endTime = System.currentTimeMillis();
			if (endTime >= startTime + paymentRedisProperties.getMaxWaitTime()) {
				logger.error(FO_TIMEOUT_MSG, paymentRedisProperties.getMaxWaitTime());
				return false;
			}
		}

		logger.debug("returning releasedFlag={}", released);
		return released;
	}
	public void pushBackendCreateDataToRedis(String pKeyId,String requestMessageJson) {
		Jedis jedis = null;
		String lRedisStatus = null;
		try  {
			jedis = JedisFactory.getInstance();
			lRedisStatus = jedis.set(pKeyId, requestMessageJson);
			logger.debug("lRedisStatus:::{}:::: For Key::{}",lRedisStatus,pKeyId);
		}catch(Exception ex) {
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_061 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_061_Desc);
			logger.error("Exception during pushBackendCreateDataToRedis {}", ex);
		}finally {
			if (null != jedis)
				jedis.close();
		}
	}
	public JsonNode pullBackendCreateDataToRedis(String pKeyId) {
		Jedis jedis = null;
		ObjectMapper mapper = null;
		JsonNode actualObj = null;
		try  {
			  jedis = JedisFactory.getInstance();
			  logger.debug("pKeyId:"+pKeyId);
			  String lResult = jedis.get(pKeyId);
			  logger.debug("lResult:"+lResult);
			  if(lResult!=null) {
				  mapper = new ObjectMapper();
			      actualObj = mapper.readTree(lResult);
			      logger.debug("actualObj:"+actualObj);
			  }
		}catch(Exception ex) {
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_062 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_062_Desc);
			logger.error("Exception during pushBackendCreateDataToRedis {}", ex);
		}finally {
			if (null != jedis)
				jedis.close();
		}
		return actualObj;
	}
	public String pullDataToRedis(String pKeyId) {
		Jedis jedis = null;
		String lResult = null;
		try  {
			  jedis = JedisFactory.getInstance();
			  logger.debug("pKeyId:"+pKeyId);
			  lResult = jedis.get(pKeyId);
			  logger.debug("lResult:"+lResult);
		}catch(Exception ex) {
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_063 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_063_Desc);
			logger.error("Exception during pushBackendCreateDataToRedis {}", ex);
		}finally {
			if (null != jedis)
				jedis.close();
		}
		return lResult;
	}
	public void removeKeyFromRedis(String pKeyId) {
		Jedis jedis = null;
		try  {
			  jedis = JedisFactory.getInstance();
			  logger.debug("pKeyId:"+pKeyId);
			  Long lResult = jedis.del(pKeyId);
			  logger.debug("lResult:"+lResult);
		}catch(Exception ex) {
			logger.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_064 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_064_Desc);
			logger.error("Exception during pushBackendCreateDataToRedis {}", ex);
		}finally {
			if (null != jedis)
				jedis.close();
		}
	}
}
