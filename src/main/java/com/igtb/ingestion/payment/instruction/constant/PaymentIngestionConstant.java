package com.igtb.ingestion.payment.instruction.constant;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.igtb.api.action.commons.util.CommonsConfig;

/**
 * The Class PaymentIngestionConstant.
 */
public final class PaymentIngestionConstant {

	/** The Constant FLA. */
	public static final String FLA = "FLA";

	/** The Constant ID. */
	public static final String ID = "id";

	/** The Constant _ID. */
	public static final String _ID = "_id";

	/** The Constant KEY. */
	public static final String KEY = "key";

	/** The Constant NAME. */
	public static final String NAME = "name";

	/** The Constant BACKEND. */
	public static final String BACKEND = "backend";

	/** The Constant CHANNEL_SQ_ID. */
	public static final String CHANNEL_SQ_ID = "channelSeqId";

	/** The Constant StateInfo. */
	public static final String STATE_INFO = "stateInfo";

	/** The Constant rejectionInfo. */
	public static final String REJECTION_INFO = "rejectionInfo";

	/** The Constant REQUESTER. */
	public static final String REQUESTER = "requester";

	/** The Constant REQUESTER. */
	public static final String DOMAINID = "domainId";

	/** The Constant type. */
	public static final String TYPE = "type";

	/** The Constant PAYLOAD. */
	public static final String PAYLOAD = "payload";

	/** The Constant eventVersion. */
	public static final String EVENTVERSION = "eventVersion";
	
	/** The Constant DELETE. */
	public static final String DELETE = "DELETE";

	/** The Constant REPLACE. */
	public static final String REPLACE = "REPLACE";

	/** The Constant REQUEUE. */
	public static final String REQUEUE = "requeue";

	/** The Constant SKIP. */
	public static final String SKIP = "skip";

	/** The Constant AMEND. */
	public static final String AMEND = "AMEND";

	/** The Constant UPDATE. */
	public static final String UPDATE = "UPDATE";

	/** The Constant UPDATE. */
	public static final String STATEUPDATE = "STATEUPDATE";

	/** The Constant ADD. */
	public static final String ADD = "ADD";

	/** The Constant CAD. */
	public static final String CAD_CURRENCY = "CAD";

	/** The Constant INSERT. */
	public static final String INSERT = "INSERT";

	/** The Constant EVENTTIME. */
	public static final String EVENTTIME = "eventTime";

	/** The Constant REQUEST_TYPE. */
	public static final String REQUEST_TYPE = "requestType";

	/** The Constant EVENT_TYPE. */
	public static final String EVENT_TYPE = "eventType";

	/** The Constant EVENT_SOURCE. */
	public static final String EVENT_SOURCE = "eventSource";

	/** The Constant SOURCE_IDENTITY. */
	public static final String SOURCE_IDENTITY = "sourceIdentity";

	/** The Constant NEXTACTION. */
	public static final String NEXTACTION = "nextAction";

	/** The Constant CURRENTACTION. */
	public static final String CURRENTACTION = "currentAction";

	/** The Constant NONE. */
	public static final String NONE = "none";

	/** The Constant CONTEXT. */
	public static final String CONTEXT = "context";
	
	/** The Constant supplement. */
	public static final String SUPPLEMENT = "supplement";

	/** The Constant CFG_KEY_ES_DB_PASSWORD. */
	public static final String CFG_KEY_ES_DB_PASSWORD = "ES.DB.Password";

	/** The Constant CFG_KEY_ES_DB_USER. */
	public static final String CFG_KEY_ES_DB_USER = "ES.DB.User";

	/** The Constant CFG_KEY_ENRICH_ES_DB_PASSWORD. */
	public static final String CFG_KEY_ENRICH_ES_DB_PASSWORD = "ES.Enrich.DB.Password"; // NOPMD longVariable

	/** The Constant CFG_KEY_ENRICH_ES_DB_USER. */
	public static final String CFG_KEY_ENRICH_ES_DB_USER = "ES.Enrich.DB.User";

	/** The Constant CFG_KEY_ES_DB_PORT. */
	public static final String CFG_KEY_ES_DB_PORT = "ES.DB.Port";

	/** The Constant DATA_INVALID. */
	public static final String DATA_INVALID = "DATA_INVALID";

	/** The Constant CFG_KEY_ES_DB_HOST. */
	public static final String CFG_KEY_ES_DB_HOST = "ES.DB.Host";

	/** The Constant CFG_KEY_ENRICHMENT_ES_DB_HOST. */
	public static final String CFG_KEY_ENRICHMENT_ES_DB_HOST = "ES.Enrich.DB.Host"; // NOPMD longVariable

	/** The Constant CFG_KEY_ES_DB_PROTOCOL. */
	public static final String CFG_KEY_ES_DB_PROTOCOL = "ES.DB.Protocol";

	/** The Constant NOT_FOUND_ERROR_CODE. */
	public static final int NOT_FOUND_ERROR_CODE = 404;

	/** The Constant SERVICE_KEY. */
	public static final String SERVICE_KEY = "serviceKey";

	/** The Constant DOMAIN_SEQ_ID. */
	public static final String DOMAIN_SEQ_ID = "domainSeqId";

	/** The Constant JSON_VAL_FAIL. */
	public static final String JSON_VAL_FAIL = "Json Validation Failed";

	/** The Constant BACKEND_STATE_UPDATED. */
	public static final String BACKEND_STATE_UPDATED = "backend-state-updated";

	/** The Constant BACKEND_CREATED. */
	public static final String BACKEND_CREATED = "backend_created";

	/** The Constant BACKEND_UPDATED. */
	public static final String BACKEND_UPDATED = "backend_updated";

	/** The Constant BACKEND_PROCESSED. */
	public static final String BACKEND_PROCESSED = "backend_processed";

	/** The Constant BACKEND_REJECTED. */
	public static final String BACKEND_REJECTED = "backend_rejected";

	/** The Constant BACKEND_ACK_SUCC. */
	public static final String BACKEND_ACK_SUCC = "backend_ack_success";

	/** The Constant CHANNEL_STATE_UPDATED. */
	public static final String CHANNEL_STATE_UPDATED = "channel-state-updated";

	/** The Constant CHANNEL_WORKFLOW_UPDATE. */
	public static final String CHANNEL_WORKFLOW_UPDATE = "channel-workflow-update";

	/** The Constant OUTCOMECATEGORY. */
	public static final String OUTCOMECATEGORY = "outcomeCategory";

	/** The Constant STATUS. */
	public static final String STATUS = "status";

	/** The Constant INITIATED. */
	public static final String INITIATED = "initiated";

	/** The Constant REJECTED. */
	public static final String REJECTED = "rejected";
	public static final String CREJECTED = "Rejected";

	/** The Constant INITIATE. */
	public static final String INITIATE = "initiate";

	/** The Constant CANNOTPROCESSMESSAGE. */
	public static final String CANNOTPROCESSMESSAGE = "Cannot process or action the message on Elasticsearch.";

	/** The Constant FAILEDENRICHMENT. */
	public static final String FAILEDENRICHMENT = "{} Enrichment Failed ";

	/** The Constant DEF_SERVER_PORT. */
	public static final int DEF_SERVER_PORT = 6379;

	/** The Constant REDIS_KEY_SEPARATOR. */
	// Key names for storing Requests in Redis
	public static final String REDIS_KEY_SEPARATOR = ":";

	/** The Constant REDIS_REQUEST_KEY_PREFIX. */
	public static final String REDIS_REQUEST_KEY_PREFIX = "action-requests";

	/** The Constant REDIS_INDEX_KEY_PREFIX. */
	public static final String REDIS_INDEX_KEY_PREFIX = "indexes";

	/** The Constant REDIS_LOCK_KEY_PREFIX. */
	public static final String REDIS_LOCK_KEY_PREFIX = "locks";

	/** The Constant REDIS_TEMP_KEY_PREFIX. */
	public static final String REDIS_TEMP_KEY_PREFIX = "temp";

	/** The Constant REDIS_DOMAIN_KEY. */
	public static final String REDIS_DOMAIN_KEY = "paymentInstruction";

	// Key expiry time in seconds
	/** The Constant for header request id expiry (seconds). */
	public static final int REDIS_EXPRY_HDR_REQ_ID_KEY = Integer.parseInt(CommonsConfig.getHdrReqIdLckExpryInSec()); // NOPMD
																														// longVariable

	/** The Constant for approved batch lock expiry (seconds). */
	/*
	 * public static final int REDIS_EXPRY_APPR_BATCH_KEY = 30*60; // for test
	 * purpose - 60
	 * 
	 *//** The Constant for retry batch lock expiry (seconds). *//*
																	 * public static final int
																	 * REDIS_EXPRY_RETRY_BATCH_KEY = 30*60; // for test
																	 * purpose - 60
																	 */

	/** The Constant for release request locks (seconds). */
	public static final int REDIS_EXPRY_REQ_REL_KEY = Integer.parseInt(CommonsConfig.getReqRelLckExpryInSec());

	/** The Constant for purge batch locks (seconds). */
	// public static final int REDIS_EXPRY_PURGE_BATCH_KEY = Integer.parseInt(CommonsConfig.getPurgeBatchLckExpryInSec());

	public static final String CFG_KEY_REDIS_DB_URL = "Redis.DB.Url";

	/** The Constant CFG_KEY_REDIS_DB_PWD. */
	public static final String CFG_KEY_REDIS_DB_PWD = "Redis.DB.Password";

	/** The Constant CFG_KEY_REDIS_DB_INSTANCE. */
	public static final String CFG_KEY_REDIS_DB_INSTANCE = "Redis.DB.Instance";

	/** The Constant CFG_KEY_REDIS_SENTINEL_FLAG. */
	public static final String CFG_KEY_REDIS_SENTINEL_FLAG = "Redis.Sentinel.Enabled"; // NOPMD longVariable

	/** The Constant CFG_KEY_REDIS_SENTINEL_URLS. */
	public static final String CFG_KEY_REDIS_SENTINEL_URLS = "Redis.Sentinel.Urls"; // NOPMD longVariable

	/** The Constant CFG_KEY_REDIS_MASTER_NAME. */
	public static final String CFG_KEY_REDIS_MASTER_NAME = "Redis.DB.Master.Name";

	/** The Constant CFG_KEY_REDIS_FO_RETRY_MS. */
	public static final String CFG_KEY_REDIS_FO_RETRY_MS = "Redis.Sentinel.ConnRetryIntervalMS";

	/** The Constant CFG_KEY_REDIS_SNTNL_FO_WAIT_MS. */
	public static final String CFG_KEY_REDIS_SNTNL_FO_WAIT_MS = "Redis.Sentinel.FailoverWaitTimeMS"; // NOPMD
																										// longVariable

	/** The Constant CFG_KEY_REDIS_DB_CONN_TOUT_MS. */
	public static final String CFG_KEY_REDIS_DB_CONN_TOUT_MS = "Redis.DB.ConnTimeoutInMs"; // NOPMD longVariable

	/** The Constant DEF_RELEASE_BATCH_SIZE. */
	public static final String DEF_RELEASE_BATCH_SIZE = "1000";

	/** The Constant DEF_REDIS_DB_INSTANCE. */
	public static final String DEF_REDIS_DB_INSTANCE = "0";

	/** The Constant DEF_REDIS_SNTNL_FO_WAIT_MS. */
	public static final String DEF_REDIS_SNTNL_FO_WAIT_MS = "10000"; // NOPMD longVariable

	/** The Constant DEF_REDIS_FO_RETRY_MS. */
	public static final String DEF_REDIS_FO_RETRY_MS = "250";

	/** The Constant DEF_REDIS_DB_CONN_TIMEOUT_MS. */
	public static final String DEF_REDIS_DB_CONN_TIMEOUT_MS = "10000"; // NOPMD longVariable

	/** The es index setting file name. */
	public static final String ES_INDEX_SETTING_FILE_NAME = "EsIndexSetting.json"; // NOPMD longVariable

	/** The es index mapping file name. */
	public static final String ES_PYMT_INSTRUCTION_MAPPING_FILE_NAME = "ESPaymentInstructionMapping.json"; // NOPMD
																											// longVariable

	/** The es index mapping file name. */
	public static final String ES_PYMT_INSTRUCTION_SETS_MAPPING_FILE_NAME = "ESpaymentInstructionSetsMapping.json"; // NOPMD
																													// longVariable

	/** The es index mapping file name. */
	public static final String ES_PYMT_TRANS_HISTORY_MAPPING_FILE_NAME = "ESPITransitionHistoryMapping.json"; // NOPMD
																												// longVariable

	/** The max time (in seconds) a message to be retried for. */
	public static final String MAX_TIME_SEC = "86400";

	/** The default sleep time (in milliseconds) between each message retry. */
	public static final String RETRY_SLEEP_MS = "250";

	/** The transition doc status. */
	public static final String DOC_STATUS_TRANSITION = "transition";

	/** The Constant DOC_STATUS_ACTIVE. */
	public static final String DOC_STATUS_ACTIVE = "active";

	/** Type- Payment. */
	public static final String TYPE_PAYMENT = "Payment";

	/** txnInitiationType - PAYMENT. */
	public static final String PAYMENT = "PAYMENT";

	/** schemeName - GUID. */
	public static final String GUID = "GUID";

	/** schemeName - ADHOC. */
	public static final String ADHOC = "ADHOC";

	/** Account action - update. */
	public static final String PAYMENT_QUERY_MATCH = "stateInfo.channelSeqId";

	/** Date format variable. */
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	/** The Constant CREDITOR_ACCOUNT. */
	public static final String CREDITOR_ACCOUNT = "creditorAccount";

	/** The Constant OPTION_PAYMENT. */
	public static final String OPTION_PAYMENT = "PAYMENT";
	
	/** The Constant OPTION_PAYMENT_RTP. */
	public static final String OPTION_PAYMENT_RTP = "RTP";

	/** The Constant SOURCE. */
	public static final String SOURCE = "_source";
	/** The Constant SOURCE. */
	public static final String REJECTIONINFO_SOURCE = "source";

	/** The Constant DATE_FORMAT_ACTION 0. */
	public static final String DATE_FORMAT_ACTION0 = "yyyy-MM-dd";

	/** The Constant DATE_FORMAT_ACTION 1. */
	public static final String DATE_FORMAT_ACTION1 = "dd-MMM-yyyy";

	/** The Constant DATE_FORMATTER 0. */
	public static final SimpleDateFormat ACTION_API_FORMATTER0 = new SimpleDateFormat(DATE_FORMAT_ACTION0,Locale.ENGLISH);

	/** The Constant DATE_FORMATTER 1. */
	public static final SimpleDateFormat ACTION_API_FORMATTER1 = new SimpleDateFormat(DATE_FORMAT_ACTION1,Locale.ENGLISH);

	/** The Constant ZERO. */
	public static final String ZERO = "0";

	/** The Constant COLON. */
	public static final String COLON = ":";
	
	/** The Constant COMMA. */
	public static final String COMMA = ",";

	/** The Constant DOT. */
	public static final String DOT = ".";

	/** The Constant TEN. */
	public static final int TEN = 10;

	/** The Constant HUNDRED. */
	public static final int HUNDRED = 100;

	/** The Constant SI_SUFFIX. */
	public static final String SI_SUFFIX = "_si";

	/** The Constant TEMPLATE_SUFFIX. */
	public static final String TEMPLATE_SUFFIX = "_template";

	/** The Constant BLANK. */
	public static final String BLANK = "";

	public static final String UNDER_SCORE = "_";

	/** The Constant TIMESTAMP_FORMAT 0. */
	public static final String TIMESTAMP_FORMAT0 = "yyyy-MM-dd'T'HH:mm:ss.s";

	/** The Constant TIMESTAMP_FORMAT 1. */
	public static final String TIMESTAMP_FORMAT1 = "yyyy-MM-dd'T'HH:mm:ss.s'Z'";
	
	/** The Constant TIMESTAMP_FORMAT 2. */
	public static final String TIMESTAMP_FORMAT2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";

	public static final String TIMESTAMP_FORMAT3 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

	/** The Constant ISO_FORMATTER 0 for "yyyy-MM-dd'T'HH:mm:ss.s". */
	public static final SimpleDateFormat TIMESTAMP_FORMATTER0 = new SimpleDateFormat(TIMESTAMP_FORMAT0, Locale.ENGLISH);

	/** The Constant ISO_FORMATTER 1 for "yyyy-MM-dd'T'HH:mm:ss.s'Z'". */
	public static final SimpleDateFormat TIMESTAMP_FORMATTER1 = new SimpleDateFormat(TIMESTAMP_FORMAT1, Locale.ENGLISH);

	/** The Constant DEBTOR_ACCOUNT. */
	public static final String DEBTOR_ACCOUNT = "debtorAccount";

	/** The Constant HITS. */
	public static final String HITS = "hits";

	/** The Constant RATE. */
	public static final String RATE = "rate";

	/** The Constant FXRATETYPE. */
	public static final String FXRATETYPE = "rateType";

	/** The Constant CURRENCY. */
	public static final String CURRENCY = "currency";

	/** The Constant CONTRACT_ID. */
	public static final String CONTRACT_ID = "contractId";

	/** The Constant TARGET_CURRENCY. */
	public static final String TARGET_CURRENCY = "targetCcy";

	/** The Constant CODE. */
	public static final String CODE = "code";

	/** The Constant DESCRIPTION. */
	public static final String DESCRIPTION = "description";

	/** The Constant PAYMENT_TYPES. */
	public static final String PAYMENT_TYPES = "paymentTypes";

	/** The Constant PAYMENT_REASONS. */
	public static final String PAYMENT_REASONS = "paymentReasons";

	/** The Constant DRAFT_STATE. */
	public static final String DRAFT_STATE = "draft";

	/** The Constant UPLOAD_USER_ORG_ID. */
	public static final String UPLOAD_USER_ORG_ID = "uploadUserOrgId";

	/** The Constant MULTIPLE. */
	public static final String MULTIPLE = "multiple";

	/** The Constant FILE_AVAILABLE. */
	public static final String FILE_AVAILABLE = "file_available";

	/** The Constant STUB. */
	public static final String STUB = "STUB";

	/**
	 * The standard error code can be used when failures occur while establishing
	 * network or transport-level connections..
	 */
	public static final String SYS_CONN_TARGETFAILED = "SYS_CONN_TARGETFAILED";

	/**
	 * The standard error code can be used when unsupported functionality invoked.
	 */
	public static final String SYS_UNSUP_FUNC = "SYS_UNSUP_FUNC";

	/** The Constant QUEST_UNEXPECTED_EXP. */
	public static final String QUEST_UNEXPECTED_EXP = "Quest: Unexpected response received, sending for retry";

	/** The string constant representing corresponding Http Status code. */
	public static final String OK_CODE_200 = "200";

	/** The string constant representing corresponding Http Status code. */
	public static final String ERR_CODE_400_BAD_REQ = "400";

	/** The Constant TOTAL_AMT. */
	public static final String TOTAL_AMT = "TOTAL_AMT";

	/** The Constant MIN_AMT. */
	public static final String MIN_AMT = "MIN_AMT";

	/** The Constant MAX_AMT. */
	public static final String MAX_AMT = "MAX_AMT";

	/** The Constant VALID_AMT. */
	public static final String VALID_AMT = "VALID_AMT";

	/** The Constant INVALID_AMT. */
	public static final String INVALID_AMT = "INVALID_AMT";

	/** The Constant APPROVED_AMT. */
	public static final String APPROVED_AMT = "APPROVED_AMT";

	/** The Constant REJECTED_AMT. */
	public static final String REJECTED_AMT = "REJECTED_AMT";

	/** The Constant UNACTED_AMT. */
	public static final String UNACTED_AMT = "UNACTED_AMT";

	/** The Constant INDENTIFICATION. */
	public static final String INDENTIFICATION = "identification";

	/** The Constant BULK_ACTION_ADD. */
	public static final String BULK_ACTION_ADD = "ADD";

	/** The Constant BULK_ACTION_UNCHANGED. */
	public static final String BULK_ACTION_UNCHANGED = "UNCHANGED";

	/** The Constant BULK_ACTION_DELETE. */
	public static final String BULK_ACTION_DELETE = "DELETE";

	/** The Constant BULK_ACTION_UPDATE. */
	public static final String BULK_ACTION_UPDATE = "UPDATE";

	/** The Constant FILE. */
	public static final String FILE = "FILE";

	/** The Constant SINGLE. */
	public static final String SINGLE = "SINGLE";

	/** The Constant BULK. */
	public static final String BULK = "BULK";

	// added by vinay
	public static final String INGESTION_PAYMENT_INSTRUCTION = "ingestionPaymentInstruction";
	public static final String REQUEUE_STATE_EVENTS_FROM_KAFKA = "requeueStateEventsFromKafka";
	public static final String INGESTION_PI_SET = "ingestionPISet";
	public static final String INGESTION_PI_SET_FROM_KAFKA = "ingestionPISetFromKafka";
	public static final String INGESTION_BULK = "ingestionBulk";
	public static final String INGESTION_PI_BULK_FROM_KAFKA = "ingestionPIBulkFromKafka";
	public static final String TRANSITION_EVENTS_FROM_RMQ_PI = "TRANSITION_EVENTS_FROM_RMQ_PI";
	public static final String KAFKA_EVENT_TO_RMQ_PI = "KAFKA_EVENT_TO_RMQ_PI";
	public static final String TRANSITION_EVENTS_FROM_KAFKA_PI = "TRANSITION_EVENTS_FROM_KAFKA_PI";
	public static final String TRANSITION_EVENTS_FROM_RMQ_PI_SET = "TRANSITION_EVENTS_FROM_RMQ_PISet";
	public static final String KAFKA_EVENT_TO_RMQ_PI_SET = "KAFKA_EVENT_TO_RMQ_PISet";
	public static final String TRANSITION_EVENTS_FROM_KAFKA_PI_SET = "TRANSITION_EVENTS_FROM_KAFKA_PISet";
	public static final String TRANSITION_EVENTS_FROM_RMQ_PI_BULK = "TRANSITION_EVENTS_FROM_RMQ_PIBulk";
	public static final String KAFKA_EVENT_TO_RMQ_PI_BULK = "KAFKA_EVENT_TO_RMQ_PIBulk";
	public static final String TRANSITION_EVENTS_FROM_KAFKA_PI_BULK = "TRANSITION_EVENTS_FROM_KAFKA_PIBulk";
	public static final String PAYMENT_INSTRUCTION_EVENTS = "paymentInstructionEvents";
	public static final String SUPPORTED_REQUESTD_TYPE = "supportedRequestType";
	public static final String SUPPORTED_SERVICE_KEY = "supportedServiceKey";
	public static final String FILE_EVENTS = "fileEvents";
	public static final String BULK_EVENTS = "bulkEvents";
	public static final String BACKEND_UPDATED_FAE = "backend_updated-fae";
	public static final String DOC_ID = "docId";
	public static final String FULFILL_RTP = "fulfillrtp";
	public static final String SEND_RTP = "sendrtp";
	public static final String SEND_MONEY = "sendmoney";
	public static final String SEND_WIRE = "sendwires";
	public static final String Y = "Y";
	public static final String DECLINE_RTP = "declinertp";
	public static final String VAL_SUCCESS = "val_success";
	public static final String APPROVED = "approved";
	public static final String PENDING_APPROVAL = "PENDING APPROVAL";
	public static final String PENDING_ACCEPTANCE = "PENDING ACCEPTANCE";
	public static final String SENT_TO_BANK = "SENT TO BANK";
	public static final String SENT_TO_RECEIVER = "SENT TO RECEIVER";
	public static final String BCC_ACTION = "bccAction";
	public static final String AE_RELEASE = "AERelease";
	public static final String AE_HOLD = "AEHold";
	public static final String OK_MSG = "OK";
	public static final String SUPPLEMENT_TAG = "supplement";
	public static final String APPROVALSLIP_TAG = "approvalSlip";
	public static final String DOC = "doc";
	public static final String FUND_TRANSFER = "Fund Transfer";
	public static final String DOC_STATUS = "docStatus";
	public static final String COMPLETED = "COMPLETED";
	public static final String DECLINED = "DECLINED";
	public static final String WAREHOUSE = "WAREHOUSED";
	public static final String EXPIRED = "EXPIRED";
	public static final String CANCELLED = "CANCELLED";
	public static final String IN_PROGRESS = "in-progress";
	public static final String AUTHORIZATION = "Authorization";
	public static final String BUSINESS = "business";
	public static final String PLAIN = "plain";
	public static final String PSH_SOURCE_IDENTITY = "PSH-1.0.0";
	public static final String IIL_SOURCE_IDENTITY = "Olive-Fabric-1.0.0";
	public static final String REGION = "North-America";
	public static final String COUNTRY_CANADA = "Canada";
	public static final String VERSION = "1.0";
	public static final String SERVICE_ID = "serviceId";
	public static final String BSDN_DTM = "bsdnDtTm";
	public static final String CUT_OFF_TYPE = "cutOffType";
	public static final String PROD_TYPE = "prodType";
	public static final String SERVICE_ID_VAL = "003";
	public static final String CUT_OFF_VAL = "Client";
	public static final String PROD_TYPE_VAL = "Wires";
	public static final String REF_DATA_REQ_TAG = "RefDataValidationRequest";
	public static final String REF_DATA_RESP_TAG = "RefDataValidationResponse";
	public static final String VALIDATE_KEY = "validateKey3";
	public static final String WIRE_CUTOFF_DTM_12FORMATE ="yyyy-MM-dd hh:mm:ss";
	public static final String WIRE_CUTOFF_DTM_24FORMATE ="yyyy-MM-dd HH:mm:ss";
	public static final String VALID_STATUS ="validStatus";
	public static final int ACCEPTED_CODE =202;
	public static final String TRANSACTION_ID ="transactionId";
	public static final String ORGN_GRP_INFO_STS_ID ="originalGroupInformationAndStatus";
	public static final String END_TO_END_IDNTFCN ="endToEndIdentification";
	public static final String INSTRCTED_AMNT ="instructedAmount";
	public static final String EQUIVLNT_AMNT ="equivalentAmount";
	public static final String CDTR ="creditor";
	public static final String DBTR_DETAILS ="debtorDetails";
	public static final String INSTRUCTION_ID ="instructionId";
	public static final String PYMNT_RAIL_OBJ ="paymentRailObj";
	public static final String PYMNT_TYP_OBJ ="paymentTypesObj";
	public static final String PYMNT_REASON ="paymentReason";
	public static final String REASON ="reason";
	public static final String OPTION ="option";
	public static final String INITIATING_PRTY ="initiatingParty";
	public static final String PYMNT_METHOD ="paymentMethod";
	public static final String FILE_AUTH_PRTY ="fileAuthstnPrtry";
	public static final String INCMNG_RTP ="incomingRTP";
	public static final String CLER_SYS_REF ="clearingSystemReference";
	public static final String CLER_SYS_REF_UMM ="clearingSystemReferenceFromUMM";
	public static final int CAMEL_START_ORDER_ONE =1;
	public static final int CAMEL_START_ORDER_TWO =2;
	public static final int CAMEL_START_ORDER_THREE =3;
	public static final String WORKFLOW ="_wf";
	public static final String PAYMNT_INSTR_OBJ ="paymentInstructionObj";
	public static final String VARIFIED ="verified";
	public static final String RELEASED ="released";
	public static final String TRASHED ="trashed";
	public static final String REF_DATA_INQRY_RESP ="RefDataInquiryResponse";
	public static final String RMQ_ROUTING_KEY ="rabbitmq.ROUTING_KEY";
	public static final String RMQ_DELIVERY_MODE ="rabbitmq.DELIVERY_MODE";
	public static final String RMQ_TIMPSTAMP ="rabbitmq.TIMESTAMP";
	public static final String REQINFO_EVNTID ="requestInfo.eventId";
	public static final String STATEINFO_DOCSTATUS ="stateInfo.docStatus";
	public static final String STATEINFO_DOMAIN_SEQ_ID ="stateInfo.domainSeqId";
	public static final String TS ="ts";
	public static final String YES ="Yes";
	public static final String CRDTR_AGENT ="creditorAgent";
	public static final String DBTR_AGENT ="debtorAgent";
	public static final String ACCP_PNDNG_APROVL ="ACCEPTED PENDING APPROVAL";
	public static final String DCLIN_PNDNG_APROVL ="DECLINED PENDING APPROVAL";
	public static final String PAYROLL ="PAYROLL";
	public static final String FILE_UPLOAD ="FILE_UPLOAD";
	public static final String SFILE ="file";
	public static final String RECV_ON ="RECEIVEON";
	public static final String SEND_ON ="SENDON";
	public static final String FAILED ="failed";
	public static final String SYSTEM ="system";
	public static final String UNCORELETED ="uncorrelated";
	public static final String VALIDATED ="validated";
	public static final String CORELETED ="correlated";
	public static final String TRF ="TRF";
	public static final String HYPHEN_SEPRATOR ="-";
	public static final String DOMAIN_INFO ="domainInfo";
	public static final String CURR_CODE ="currencyCode";
	public static final String VALUE ="value";
	public static final String INSTR_AMNT_BASE_CCY ="instructedAmountBaseCcy";
	public static final String OFF_SET ="offset";
	public static final String PARTITION ="partition";
	public static final String CONSUMER_APP_NAME ="consumerAppName";
	public static final String CONSUMER_GRP_ID ="consumerGroupId";
	public static final String ERR_TYPE ="errorType";
	public static final String RECIEVED_MSG ="receivedMessage";
	public static final String MESSAGE ="message";
	public static final String BACKEND_REJECTION ="Backend rejection";
	public static final String PAYMENT_STATE_INGESTION ="PAYMENT_STATE_INGESTION";
	public static final String BANK ="bank";
	public static final String ACC_NO ="accountNo";
	public static final String LGL_NAME ="legalName";
	public static final String SHRT_ACC_NO ="shortAccountNo";
	public static final String ALIAS ="alias";
	public static final String ACC_NUM_TYPE ="accNumType";
	public static final String TAGS ="tags";
	public static final String ENTITY ="entity";
	public static final String ISO_3CHAR_CODE ="iso_3char_code";
	public static final String ACCT_CCY ="acctCcy";
	public static final String BANK_PROXY ="bankProxy";
	public static final String BIC ="bic";
	public static final String BANK_BRANCH_CD ="BANK_BRANCH_CD";
	public static final String BANK_BRANCH_NM ="BANK_BRANCH_NM";
	public static final String CONTACT ="contact";
	public static final String MAX_PER_TXN_LMT ="maxPerTxnLimit";
	public static final String PARENT_CREATE_REQ_CHANNEL_SEQID ="parentCreateRequestChannelSeqId";
	public static final String REMARK ="remark";
	public static final String POSTED ="POSTED";
	public static final String CUSTOMER_TXN_REF_NO ="customerTransactionRefNo";
	public static final String REMITTANCEINFORMATION_1 ="remittanceInformation_1";
	public static final String REMITTANCEINFORMATION_2 ="remittanceInformation_2";
	public static final String REMITTANCEINFORMATION_3 ="remittanceInformation_3";
	public static final String REMITTANCEINFORMATION_4 ="remittanceInformation_4";
	public static final String REASON_CODE ="reasonCode";
	public static final String OTHR_REMNG_INFO ="otherRemainingInfo";
	public static final String CBX_SUPPORT_STATUS ="cbxSupportStatus";
	public static final String CBX_SUPPORT_STATUS_CHANNL_SEQ_ID ="cbxSupportStatusChannelSeqId";
	public static final String CATEGORY_PURPOSE ="categoryPurpose";
	public static final String SUPPLEMENTRY_DATA ="supplementaryData";
	public static final String INVOICE_NO ="invoiceNo";
	public static final String INVOICE_DATE ="invoiceDate";
	public static final String BACKEND_CREJECTED ="BACKENDREJECTED";
	public static final String INTERAC_ID ="interac_id";
	public static final String EVENT ="event";
	public static final String NOTF_TYPE ="notf_type";
	public static final String NOTF_RECEIVE_RTP ="receive_rtp";
	public static final String NOTF_TYPE_ES ="notfType";
	public static final String ACCOUNT ="account";
	public static final String DR ="DR";
	public static final String CR ="CR";
	public static final String NULL ="NULL";
	public static final String HASH ="#";
	public static final String PAYMNT ="paymnt";
	public static final String IN_TRANSIT ="IN-TRANSIT";
	public static final boolean FALSE = false;
	public static final boolean TRUE = true;
	public static final String CUST_ID ="custId";
	public static final String CUST_NAME ="custName";
	public static final String PU_ID ="puId";
	public static final String DIRECT_CREDIT ="Direct Credit";
	public static final String LOCAL_INST_CODE ="localInstrumentCode";
	public static final String BTR ="BTR";
	public static final String SMALL_ADD ="add";
	public static final String REQUESTER_ID ="PSH";
	public static final String REQUESTER_DOMAINID ="PSH Group";
	public static final String EVNT_SOURCE_INDENTITY ="action-payments";
	public static final String CUMULATIVE_STATUS ="cumulativeStatus";
	public static final String UIDISPLAY_STATUS ="uiDisplayStatus";
	public static final String FAE_INDICATOR ="faeIndicator";
	public static final String FILE_CTRL_SUM ="fileCtrlSum";
	public static final String FILE_NUM_TXN ="fileNbOfTxs";
	public static final String BATCH_NUM_TXN ="batchNbOfTxs";
	public static final String BATCH_CTRL_SUM ="batchCtrlSum";
	public static final String CREATION_DATE_TIME ="creationDateTime";
	public static final String USER_DATE_TIME ="userDateTime";
	public static final String CINITIATED ="INITIATED";
	
	
	
	
	
	// added by durgesh
	public static final String ADDRESS = "address";
	public static final String ADDRESS1 = "address1";
	public static final String ADDRESS2 = "address2";
	public static final String ADDRESS3 = "address3";
	public static final String CONTACT_ADDRESS = "contactAddress";
	public static final String COUNTRY = "country";
	public static final String CITY = "city";
	public static final String APPROVAL_SCOPE = "approvalScope";
	public static final String HAS_SETS = "hasSets";
	public static final String LAST_ACTIVITYTS = "lastActivityTs";
	public static final String PARTIAL_APPROVAL = "partialApproval";
	public static final String SETELEMENT_INFO = "setElementInfo";
	public static final String STATE = "state";
	public static final String TRANSITION_INFO = "transitionInfo";
	public static final String FULFILLRTP = "fulfillrtp";
	public static final String TRANSACTION_HISTORY_DOC_ID = "transactionHistoryDocId";
	public static final String ERROR_WHILE_PROCESSING_BULK_RECORD = "Error while processing bulk record: ";
	public static final String RELEASE_TO_BANK = "release-to-bank";
	public static final String METADATA_AVAILABLE = "metadata_available";
	public static final String EXPIRY_DATE = "expiryDate";
	public static final String REQUESTED_EXECUTION_DATE= "requestedExecutionDate";
	public static final String CREATION_DATE= "creationDate";
	
	// use for contataccount query
	public static final String ALIAS_MOBILE_NO= "mobileNo";
	public static final String ALIAS_EMAIL= "email.keyword"; //without keyword not search the record

	public static final String NT_HOLD = "NT_HOLD";
	public static final String LAST_ACTION = "lastAction";

	public static final String REQ_CHANNEL_SEQ_ID = "reqChannelSeqId";
	public static final String NOTIFICATION_LANGUAGE = "notificationLanguage";
	public static final String LOGICAL_END_STATGES = "CANCELLED,EXPIRED,REJECTED,BACKENDREJECTED,DECLINED,COMPLETED";

	public static final String TRASHED_STATUS = "TRASHED";

	public static final String REJECTED_STATUS = "REJECTED";

	public static final String channel_state_updated_incomingrtpstatus = "channel-state-updated-incomingrtpstatus";

	public static final String TRASH_ACTION = "trash";
	
	public static final String BCC_PENDING = "PENDING";
	public static final String IRTP_STR = "IRTP";
	public static final String INVOICE_DETAILS = "invoiceDetails";
	public static final String RELEASE_DATE = "releaseDate";
	
	public static final String DESC = "desc";
	public static final String DOC_STATUS_STR = "{DOC_STATUS_STR}";
	public static final String TRANSITION_DESC = "{TRANSITION_DESC}";
	public static final String CBX_UI_STATUS = "{CBX_UI_STATUS}";
	public static final String FAE_IND = "{FAE_IND}";
	public static final String BCC_ACTION_STR = "{BCC_ACTION_STR}";
	public static final String FETCH_QUERY_STATUS = "fetchQueryStatus";
	public static final String BLANK_STR ="{BLANK_STR}";
	public static final String DEFAULT_STR ="{DEFAULT_STR}";
	public static final String FETCH_QUERY_DEFAULT_STR = "{DOC_STATUS_STR}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR}";
	//DOC_STATUS+TRANSITION_DESC+CBX_UI_STATUS+FAE_IND+BCC_ACTION+IRTP
	
	/**
	 * Instantiates a new constants.
	 */
	private PaymentIngestionConstant() {
		// empty implementation to avoid instantiation.
	}
	

    // Payment Transition History
    public static final Map<PYMNTHSTRY_STATUS, String> statusDescMap = new HashMap<>();
    
    public enum PYMNTHSTRY_STATUS{
    	CREATED("CREATED","PaymentInstruction created:by"),
    	CANCELLED("CANCELLED","PaymentInstruction cancelled:by"),
    	COMPLETED("COMPLETED","PaymentInstruction completed:by"),
    	DECLINED("DECLINED","PaymentInstruction declined:by"),
    	EXPIRED("EXPIRED","PaymentInstruction expired:by"),
    	PENDING_ACCEPTANCE("PENDING_ACCEPTANCE","PaymentInstruction pending acceptance:by"),
    	UPDATED("UPDATED","PaymentInstruction updated:by"),
    	WAREHOUSE("WAREHOUSE","PaymentInstruction warehoused:by"),
    	REJECTED("REJECTED","PaymentInstruction rejected:by"),
    	;
    	
    	private String key;
        private String desc;

        PYMNTHSTRY_STATUS(String key, String desc) {
          this.key = key;
          this.desc = desc;
        }

        @JsonValue
        public String getKey() {
          return key;
        }
        
        @JsonValue
        public String getDesc() {
          return desc;
        }

        @Override
        public String toString() {
        	final StringBuilder sb = new StringBuilder();
            sb.append("PYMNTHSTRY_STATUS");
            sb.append("{key=").append(key);
            sb.append(", desc='").append(desc);
            sb.append('}');
            return sb.toString();
        }

        @JsonCreator
        public static PYMNTHSTRY_STATUS fromKey(String text) {
          for (PYMNTHSTRY_STATUS b : PYMNTHSTRY_STATUS.values()) {
            if (String.valueOf(b.key).equals(text)) {
              return b;
            }
          }
          return null;
        }
    }


	/** The Constant DRAFT STATUS. */
	public static final String DRAFT_STATUS = "draft";

}
