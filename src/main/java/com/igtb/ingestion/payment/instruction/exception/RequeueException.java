package com.igtb.ingestion.payment.instruction.exception;

/**
 * The Class RequeueException.
 */
public class RequeueException extends Exception{

    /**  Serial Version. */
    private static final long serialVersionUID = 2582722819300428882L;


    /**
     * Instantiates a new requeue exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public RequeueException(final String message, final Throwable cause) {
        super(message, cause);
    }


    /**
     * Instantiates a new requeue exception.
     *
     * @param message the message
     */
    public RequeueException(final String message) {
        super(message);
    }
}
