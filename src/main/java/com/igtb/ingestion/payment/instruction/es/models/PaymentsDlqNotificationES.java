package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class PaymentInstruction.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@Setter
public class PaymentsDlqNotificationES {
	
	
	/** The event. */
	private String eventES = null;

	/** The notf_id. */
	private String requestIdES = null;
	
	private String eventTypeES = null;

	private String messageES = null;
	private String exceptionTypeES = null;
	private String   creationTimeES = null;



}
