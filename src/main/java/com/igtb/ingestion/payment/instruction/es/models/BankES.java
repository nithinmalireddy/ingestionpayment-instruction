package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * BankES Class.
 *
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
public class BankES {
	
	/** The account bank id. */
//	@NotNull
	private String id;
	
	/** The account bank name. */
	private String name;
	
	/** The account bank key. */
	private String key;
	
}
