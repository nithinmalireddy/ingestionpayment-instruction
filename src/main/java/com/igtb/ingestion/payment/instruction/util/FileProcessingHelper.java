package com.igtb.ingestion.payment.instruction.util; //NOPMD excessiveImports

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.api.action.commons.util.DateUtil;
import com.igtb.api.ingestion.commons.file.models.FileStateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.ingestion.payment.instruction.config.AppPropsProvider;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.DebtorAccount;
import com.igtb.ingestion.payment.instruction.es.models.EntityES;
import com.igtb.ingestion.payment.instruction.es.models.Metrics;
import com.igtb.ingestion.payment.instruction.es.models.MetricsObj;
import com.igtb.ingestion.payment.instruction.es.models.ParentSet;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentRailES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentReasonES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentTypeES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.SetElementInfo;
import com.igtb.ingestion.payment.instruction.es.models.SetElementInfoES;
import com.igtb.ingestion.payment.instruction.es.models.SetInfo;
import com.igtb.ingestion.payment.instruction.es.models.Template;
import com.igtb.ingestion.payment.instruction.es.models.TxnCurrency;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.metadata.models.BusinessField;
import com.igtb.ingestion.payment.instruction.metadata.models.Metadata;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataFile;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataPayload;
import com.igtb.ingestion.payment.instruction.metadata.models.Records;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.FileAvailablePayload;

import lombok.AllArgsConstructor;

/**
 * The Class FileProcessingHelper.
 */
@Component

/**
 * Instantiates a new file processing helper.
 *
 * @param stateInfoHelper
 *            the state info helper
 * @param objectMapper
 *            the object mapper
 * @param fileStorageManager
 *            the file storage manager
 * @param accInfoUtil
 *            the acc info util
 * @param paymentInstructionBuilder
 *            the payment instruction builder
 * @param paymntInstrTransformerUtil
 *            the paymnt instr transformer util
 */
@AllArgsConstructor
public class FileProcessingHelper {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessingHelper.class);

	/** The payment ingestion state info util. */
	private final PaymentIngestionHelper stateInfoHelper;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/** The acc info util. */
	private AccountsInfoUtil accInfoUtil;

	/** The payment instruction builder. */
	private PaymentInstructionBuilder paymentInstructionBuilder;

	/** The payment instruction transformer util. */
	private PaymntInstrTransformerUtil paymntInstrTransformerUtil; // NOPMD longVariable

	/** The igtb blob stor util. */
	private IgtbBlobStorUtil igtbBlobStorUtil;

	/** The event validation config. */
	private EventValidationConfig eventValidationConfig;

	/** The props. */
	private AppPropsProvider props;

	/**
	 * Builds the pymt instr sets.
	 *
	 * @param payload
	 *            the payload
	 * @param context
	 *            the context
	 * @return the payment instruction sets ES
	 */
	public PaymentInstructionSetsES buildPymtInstrSets(final FileAvailablePayload payload, final Context context) {
		final PaymentInstructionSetsES paymentInstructionSetsES = new PaymentInstructionSetsES();
		paymentInstructionSetsES.setRecordSetId(payload.getFileId());
		paymentInstructionSetsES.setType("file");
		paymentInstructionSetsES.setCorporateEntity(new EntityES(null, null, payload.getEntity()));

		if (payload.getTemplate() != null) {
			paymentInstructionSetsES
					.setTemplate(new Template(payload.getTemplate().getId(), payload.getTemplate().getName()));
		}

		paymentInstructionSetsES.setUploadFileName(payload.getFileName());

		final Map<String, Object> additionalInfo = new HashMap<>();
		additionalInfo.put(PaymentIngestionConstant.UPLOAD_USER_ORG_ID, payload.getUploadUser().getOrgId());
		if (payload.getAdditionalProperties() != null) {
			additionalInfo.putAll(payload.getAdditionalProperties());
		}
		paymentInstructionSetsES.setAdditionalInfo(additionalInfo);

		paymentInstructionSetsES.setPartialApproval(false);
		paymentInstructionSetsES.setHasSets(false);
		paymentInstructionSetsES.setParentSet(null);
		paymentInstructionSetsES.setCreated(context.getEventTime());
		paymentInstructionSetsES.setFileSizeInBytes(payload.getFileSize());
		paymentInstructionSetsES.setFileProcessingFlag("FLA");

		// Setting default setName start with Payment as here serviceKey is paymnt/any
		paymentInstructionSetsES.setSetName("Payment file " + paymentInstructionSetsES.getRecordSetId());
		LOGGER.debug("paymentInstructionSetsES build as: {}", paymentInstructionSetsES);
		return paymentInstructionSetsES;
	}

	/**
	 * Builds the state info.
	 *
	 * @param entity
	 *            the entity
	 * @param event
	 *            the event
	 * @param context
	 *            the context
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @param moduleName
	 *            the module name
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param metadataStatus
	 *            the metadata status
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void buildFileStateInfo(final String entity, final JsonNode event, final Context context,
			final JsonNode existingStateInfoJson, final String moduleName,
			final PaymentInstructionSetsES paymentInstructionSetsES, final String metadataStatus) throws IOException {
		// User Enrichment
		UserInfo userInfo = null;
		if (Arrays.asList(PaymentIngestionConstant.METADATA_AVAILABLE, "metadata_unavailable")
				.contains(context.getEventType().getStatus())) {
			userInfo = Optional.ofNullable(paymentInstructionSetsES.getStateInfo())
					.map(stateInfo -> stateInfo.getInitiated()).map(initiate -> initiate.getUserInfo()).orElse(null);
		}

		if (userInfo == null) {
			userInfo = new UserInfo();
			Optional.ofNullable(context.getRequester()).map(requester -> requester.getDomainId())
					.ifPresent(userInfo::setDomainName);

			Optional.ofNullable(context.getRequester()).map(requester -> requester.getId())
					.ifPresent(userInfo::setUserName);

			final EntityES enrichedEntity = stateInfoHelper.enrichUserInfo(entity, userInfo);
			if (Arrays.asList("file_available", PaymentIngestionConstant.METADATA_AVAILABLE, "metadata_unavailable")
					.contains(context.getEventType().getStatus())) {
				paymentInstructionSetsES.setCorporateEntity(enrichedEntity);
			}
		}
		final JsonNode userInfoJson = objectMapper.convertValue(userInfo, JsonNode.class);

		FileStateInfo fileStateInfo;

		if (context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.METADATA_AVAILABLE)
				&& metadataStatus.equalsIgnoreCase("NONE_VALID")) {
			fileStateInfo = new FileStateInfo();
			fileStateInfo.setStateInfo(paymentInstructionSetsES.getStateInfo());
			fileStateInfo.setHasSets(paymentInstructionSetsES.getHasSets());
			fileStateInfo.setPartialApproval(paymentInstructionSetsES.getPartialApproval());

			fileStateInfo = stateInfoHelper.getFileStateInfoAndMarkRejectedAsNoValidRecord(fileStateInfo);
		} else {
			fileStateInfo = stateInfoHelper.getFileStateInfoFromCommonsModule(event, userInfoJson,
					existingStateInfoJson, moduleName);
		}
		paymentInstructionSetsES.setStateInfo(fileStateInfo.getStateInfo());
		paymentInstructionSetsES.setHasSets(fileStateInfo.isHasSets());
		paymentInstructionSetsES.setPartialApproval(fileStateInfo.isPartialApproval());

	}

	/**
	 * Builds the PI set and PI by metadata file.
	 *
	 * @param payload
	 *            the payload
	 * @param context
	 *            the context
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @return the map
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public Map<String, Object> buildPISetAndPIByMetadataFile(final MetadataPayload payload, final Context context,
			final PaymentInstructionSetsES paymentInstructionSetsES)
			throws IOException, MessageProcessingException, DataNotFoundException {
		final MetadataFile metadataFile = igtbBlobStorUtil.readMetadataFile(payload.getMetadataFileId());
		buildExistingPISetFile(paymentInstructionSetsES, metadataFile);
		final Map<String, PaymentInstructionSetsES> businessFieldMap = buildPISetBulk(metadataFile,
				paymentInstructionSetsES);
		final List<PaymentInstructionSetsES> payInstructionSetBulk = new ArrayList<>(businessFieldMap.values());
		final List<PaymentsInstructionsES> payInstructions = buildPIUsingRecords(metadataFile, payload.getFileId(),
				businessFieldMap, payInstructionSetBulk, paymentInstructionSetsES, context);

		final Map<String, Object> pIAndPISetMap = new HashMap<>();
		pIAndPISetMap.put("pISets", payInstructionSetBulk);
		pIAndPISetMap.put("payInstr", payInstructions);
		pIAndPISetMap.put("fileServiceKey", metadataFile.getMetadata().getServiceKey());
		return pIAndPISetMap;
	}

	/**
	 * Builds the PI set by metadata file.
	 *
	 * @param payload
	 *            the payload
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public void buildPISetByMetadataFile(final MetadataPayload payload,
			final PaymentInstructionSetsES paymentInstructionSetsES) throws IOException, MessageProcessingException {
		final MetadataFile metadataFile = igtbBlobStorUtil.readMetadataFile(payload.getMetadataFileId());
		buildExistingPISetFile(paymentInstructionSetsES, metadataFile);
	}

	/**
	 * Builds the PI using records.
	 *
	 * @param metadataFile
	 *            the metadata file
	 * @param fileId
	 *            the file id
	 * @param businessFieldMap
	 *            the business field map
	 * @param payInstructionSetBulk
	 *            the pay instruction set bulk
	 * @param pISetFile
	 *            the i set file
	 * @param context
	 *            the context
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	private List<PaymentsInstructionsES> buildPIUsingRecords(final MetadataFile metadataFile, final String fileId,
			final Map<String, PaymentInstructionSetsES> businessFieldMap,
			final List<PaymentInstructionSetsES> payInstructionSetBulk, final PaymentInstructionSetsES pISetFile,
			final Context context) throws IOException, DataNotFoundException, MessageProcessingException {
		LOGGER.debug("building PI using metadata records");
		final List<PaymentsInstructionsES> payInstructions = new ArrayList<>();
		final Map<String, List<String>> bpIdSetElementMap = new HashMap<>();
		final Map<String, AccountES> debtorAccCache = new HashMap<>();
		final Map<String, PaymentReasonES> paymentReasonCache = new HashMap<>();
		final Map<String, PaymentRailES> paymentRailCache = new HashMap<>();
		final Map<String, PaymentTypeES> paymentTypesObjCache = new HashMap<>();
		final Set<String> requestedExecutionDates = new HashSet<>();
		final Map<String, Set<String>> requestedExecutionDatesMap = new HashMap<>();
		Boolean isControlEleSet = true;
		if (metadataFile.getRecords() != null) {
			for (final Records record : metadataFile.getRecords()) {

				// build PI
				final PaymentsInstructionsES paymentInstructionES = paymntInstrTransformerUtil.buildPIByMetadataRecord(
						record, fileId, context, debtorAccCache, paymentReasonCache, paymentRailCache,
						paymentTypesObjCache);

				// build setInfo
				final SetInfo setInfo = new SetInfo(pISetFile.getRecordSetId(), pISetFile.getSetName(),
						pISetFile.getType(), pISetFile.getMetrics().getValid().getCount());
				paymentInstructionES.setSetInfo(setInfo);

				final String bpId = record.getBusinessProductId().getValue().toUpperCase(Locale.ENGLISH);

				// build setElementInfo
				final PaymentInstructionSetsES pISetBulk = businessFieldMap.get(bpId);
				if (pISetBulk == null) {
					LOGGER.error(" {} not found in BusinessProduct map", bpId);
					throw new MessageProcessingException("BusinessProductId miss match");
				}
				final SetElementInfo setElementInfo = new SetElementInfo(pISetBulk.getRecordSetId(),
						pISetBulk.getSetName(), pISetBulk.getType(), pISetBulk.getMetrics().getValid().getCount(),
						false);

				// build setElementInfo for bulk
				if (bpIdSetElementMap.get(bpId) == null) {
					bpIdSetElementMap.put(bpId, new ArrayList<>(
							Arrays.asList(pISetFile.getRecordSetId() + "." + record.getInternalTxnId().getValue())));
				} else {
					final List<String> setElement = bpIdSetElementMap.get(bpId);
					setElement.add(pISetFile.getRecordSetId() + "." + record.getInternalTxnId().getValue());
					bpIdSetElementMap.put(bpId, setElement);
				}
				if (isControlEleSet
						&& paymentInstructionES.getStateInfo().getTransitionInfo().getState().equals("validated")) {
					setElementInfo.setControlElement(true);
					isControlEleSet = false;
				}

				// requestedExecutionDate at bulk
				if (requestedExecutionDatesMap.get(bpId) == null) {
					requestedExecutionDatesMap.put(bpId,
							new HashSet<>(Arrays.asList(paymentInstructionES.getRequestedExecutionDate())));
				} else {
					final Set<String> dates = requestedExecutionDatesMap.get(bpId);
					dates.add(paymentInstructionES.getRequestedExecutionDate());
					requestedExecutionDatesMap.put(bpId, dates);
				}

				// requestedExecutionDate at file
				requestedExecutionDates.add(paymentInstructionES.getRequestedExecutionDate());

				paymentInstructionES.setSetElementInfo(setElementInfo);
				paymentInstructionES.setTxnCurrency(pISetBulk.getTxnCurrency());
				payInstructions.add(paymentInstructionES);
			}

			// setting requestedExecutionDate for PISet File and PISet Bulk
			if (requestedExecutionDates.size() == 1) {
				pISetFile.setRequestedExecutionDate(requestedExecutionDates.iterator().next());
			}

			// updating setElementInfo for PI set bulk
			for (final PaymentInstructionSetsES paymentInstructionSetsES : payInstructionSetBulk) {
				String bpId = paymentInstructionSetsES.getPaymentReason().getCode().toUpperCase(Locale.ENGLISH);

				paymentInstructionSetsES.setSetElementInfo(
						new SetElementInfoES(bpIdSetElementMap.get(bpId), new ArrayList<>(), new ArrayList<>()));

				if (requestedExecutionDatesMap.get(bpId) == null && requestedExecutionDatesMap.get(bpId).size() == 1) {
					paymentInstructionSetsES
							.setRequestedExecutionDate(requestedExecutionDatesMap.get(bpId).iterator().next());
				}

			}

			if (paymentTypesObjCache.size() == 1) {
				Set<String> keys = paymentTypesObjCache.keySet();
				pISetFile.setPaymentTypesObj(paymentTypesObjCache.get(keys.iterator().next()));
			}
		}
		LOGGER.debug("building PI using metadata records completed, records: {}", payInstructions.size());
		return payInstructions;
	}

	/**
	 * Creates the bulk id.
	 *
	 * @return the string
	 */

	/**
	 * Builds the PI set bulk.
	 *
	 * @param metadataFile
	 *            the metadata file
	 * @param pISetFile
	 *            the i set file
	 * @return the map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private Map<String, PaymentInstructionSetsES> buildPISetBulk(final MetadataFile metadataFile,
			final PaymentInstructionSetsES pISetFile) throws IOException {
		LOGGER.debug("building PI Set for bulk");
		final List<BusinessField> businessFields = metadataFile.getMetadata().getBusinessFields();
		final Map<String, PaymentInstructionSetsES> businessFieldMap = new HashMap<>();
		final Map<String, Object> reusableData = new HashMap<>();
		for (final BusinessField businessField : businessFields) {
			final PaymentInstructionSetsES paymentInstructionSetsES = new PaymentInstructionSetsES();
			paymentInstructionSetsES.setRecordSetId(pISetFile.getRecordSetId() + "." + businessField.getBpId());
			paymentInstructionSetsES.setType("bulk");
			paymentInstructionSetsES.setTemplate(pISetFile.getTemplate());
			paymentInstructionSetsES.setUploadFileName(pISetFile.getUploadFileName());
			paymentInstructionSetsES.setPartialApproval(false);
			paymentInstructionSetsES.setHasSets(false);
			paymentInstructionSetsES.setParentSet(
					new ParentSet(pISetFile.getRecordSetId(), pISetFile.getSetName(), pISetFile.getType()));
			paymentInstructionSetsES.setCreated(DateUtil.dateToISOString(new Date()));
			paymentInstructionSetsES.setLastModified(DateUtil.dateToISOString(new Date()));
			paymentInstructionSetsES.setFileProcessingFlag("FLA");

			// get payment reason
			final String[] serviceKeySplit = businessField.getServiceKey().split("/");
			if (serviceKeySplit.length > 0) {
				paymentInstructionSetsES
						.setPaymentReason(paymentInstructionBuilder.getPaymentReason(serviceKeySplit[1]));
			}

			// buildDebtorAccount
			final Set<String> debitAccounts = new HashSet<>(businessField.getDebitAccounts().getIds());
			if (!debitAccounts.isEmpty()) {
				Boolean isMultiple = true;
				if (debitAccounts.size() == 1) {
					isMultiple = false;
				}

				final String debitId = businessField.getDebitAccounts().getIds().get(0);
				final DebtorAccount debtorAccount;
				if (reusableData.get(debitId) != null) {
					debtorAccount = (DebtorAccount) reusableData.get(debitId);
					LOGGER.debug("Reusing debtor account details for {}", debitId);
				} else {
					LOGGER.debug("Deriving debtor account details for {}", debitId);
					debtorAccount = getDebtorAccount(businessField.getDebitAccounts().getIds().get(0), isMultiple);
					reusableData.put(debitId, debtorAccount);
				}

				paymentInstructionSetsES.setDebtorAccount(debtorAccount);
				paymentInstructionSetsES.setCorporateEntity(debtorAccount.getEntity());
			}

			final Metadata metadata = metadataFile.getMetadata();
			final String currency = businessField.getCurrency().getCode();
			paymentInstructionSetsES.setTxnCurrency(new TxnCurrency(currency, currency));
			paymentInstructionSetsES
					.setInstructedAmountBaseCcy(new AmountES(metadata.getFileMetrics().getTotalAmount().getValue(),
							metadata.getFileMetrics().getTotalAmount().getCurrencyCode()));

			// build file metrics
			final Metrics fileMetrics = new Metrics();
			fileMetrics
					.setTotal(new MetricsObj(businessField.getSuccessfulTxnCount() + businessField.getFailedTxnCount(),
							businessField.getSuccessfulTxnAmt() + businessField.getFailedTxnAmt(), currency));
			fileMetrics.setValid(new MetricsObj(businessField.getSuccessfulTxnCount(),
					businessField.getSuccessfulTxnAmt(), currency));
			fileMetrics.setInvalid(
					new MetricsObj(businessField.getFailedTxnCount(), businessField.getFailedTxnAmt(), currency));
			fileMetrics.setMin(new MetricsObj(null, businessField.getMinTxnAmount(), currency));
			fileMetrics.setMax(new MetricsObj(null, businessField.getMaxTxnAmount(), currency));
			paymentInstructionSetsES.setMetrics(fileMetrics);

			String setFileType = "Payment";
			if (eventValidationConfig.getPayrollProduct()
					.contains(businessField.getBpId().toLowerCase(Locale.getDefault()))) {
				setFileType = "Payroll";
			}

			paymentInstructionSetsES.setSetName(setFileType + " bulk " + paymentInstructionSetsES.getRecordSetId());

			businessFieldMap.put(businessField.getBpId().toUpperCase(Locale.ENGLISH), paymentInstructionSetsES);
		}
		LOGGER.debug("building PI Set for bulk completed with number of businessField: {}", businessFieldMap.size());
		return businessFieldMap;
	}

	/**
	 * Builds the existing PI set file.
	 *
	 * @param paymentInstructionSetsES
	 *            the payment instruction sets ES
	 * @param metadataFile
	 *            the metadata file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void buildExistingPISetFile(final PaymentInstructionSetsES paymentInstructionSetsES,
			final MetadataFile metadataFile) throws IOException {
		LOGGER.debug("building existing PI Set using metadata file");
		// buildDebtorAccount
		final Set<String> debitAccounts = new HashSet<>();
		final Set<String> serviceKeys = new HashSet<>();
		String setFileType = "Payment";
		for (final BusinessField businessField : metadataFile.getMetadata().getBusinessFields()) {
			debitAccounts.addAll(businessField.getDebitAccounts().getIds());
			serviceKeys.add(businessField.getServiceKey());
			if (eventValidationConfig.getPayrollProduct()
					.contains(businessField.getBpId().toLowerCase(Locale.getDefault()))) {
				setFileType = "Payroll";
			}
		}

		if (!debitAccounts.isEmpty()) {
			Boolean isMultiple = true;
			if (debitAccounts.size() == 1) {
				isMultiple = false;
			}
			final String id = debitAccounts.iterator().next();
			if (StringUtils.isNoneEmpty(id)) {
				final DebtorAccount debtorAccount = getDebtorAccount(id, isMultiple);
				paymentInstructionSetsES.setDebtorAccount(debtorAccount);
				paymentInstructionSetsES.setCorporateEntity(debtorAccount.getEntity());
				paymentInstructionSetsES.setHasSets(true);
			}

		}

		// setName
		paymentInstructionSetsES.setSetName(setFileType + " file " + paymentInstructionSetsES.getRecordSetId());

		// get payment reason
		if (serviceKeys.size() == 1) {
			final String[] serviceKeySplit = serviceKeys.iterator().next().split("/");
			if (serviceKeySplit.length > 0) {
				paymentInstructionSetsES
						.setPaymentReason(paymentInstructionBuilder.getPaymentReason(serviceKeySplit[1]));
			}
		} else {
			paymentInstructionSetsES.setPaymentReason(
					new PaymentReasonES(PaymentIngestionConstant.MULTIPLE, PaymentIngestionConstant.MULTIPLE));
		}

		final Map<String, Object> additionalInfo = Optional.ofNullable(paymentInstructionSetsES.getAdditionalInfo())
				.orElse(new HashMap<>());
		additionalInfo.put("customerRefNo", metadataFile.getMetadata().getCustomerFileRefNo());
		paymentInstructionSetsES.setAdditionalInfo(additionalInfo);

		final Metadata metadata = metadataFile.getMetadata();

		// build txnCurrency
		if (metadataFile.getRecords() != null) {
			final Set<String> currencies = new HashSet<>();
			metadataFile.getRecords().stream().filter(record -> currencies.add(record.getCur().getValue()))
					.collect(Collectors.toSet());

			if (currencies.size() == 1) {
				final String currency = currencies.iterator().next();
				paymentInstructionSetsES.setTxnCurrency(new TxnCurrency(currency, currency));
			} else {
				final String currency = metadata.getFileMetrics().getTotalAmount().getCurrencyCode();
				paymentInstructionSetsES.setTxnCurrency(new TxnCurrency(currency, currency));
			}
		}

		paymentInstructionSetsES
				.setInstructedAmountBaseCcy(new AmountES(metadata.getFileMetrics().getTotalAmount().getValue(),
						metadata.getFileMetrics().getTotalAmount().getCurrencyCode()));

		// build file metrics
		final Metrics fileMetrics = new Metrics();
		fileMetrics.setTotal(new MetricsObj(
				metadata.getFileMetrics().getValidRecords().getCount()
						+ metadata.getFileMetrics().getInvalidRecords().getCount(),
				metadata.getFileMetrics().getTotalAmount().getValue(),
				getCurrency(metadata.getFileMetrics().getTotalAmount().getValue(),
						metadata.getFileMetrics().getTotalAmount().getCurrencyCode())));
		fileMetrics.setValid(new MetricsObj(metadata.getFileMetrics().getValidRecords().getCount(),
				metadata.getFileMetrics().getValidRecords().getAmount().getValue(),
				getCurrency(metadata.getFileMetrics().getValidRecords().getAmount().getValue(),
						metadata.getFileMetrics().getValidRecords().getAmount().getCurrencyCode())));
		fileMetrics.setInvalid(new MetricsObj(metadata.getFileMetrics().getInvalidRecords().getCount(),
				metadata.getFileMetrics().getInvalidRecords().getAmount().getValue(),
				getCurrency(metadata.getFileMetrics().getInvalidRecords().getAmount().getValue(),
						metadata.getFileMetrics().getInvalidRecords().getAmount().getCurrencyCode())));
		paymentInstructionSetsES.setMetrics(fileMetrics);

		paymentInstructionSetsES.setLastModified(DateUtil.dateToISOString(new Date()));

		LOGGER.debug("building existing PI Set completed");
	}

	/**
	 * Gets the currency.
	 *
	 * @param amount
	 *            the amount
	 * @param currencyCode
	 *            the currency code
	 * @return the currency
	 */
	private String getCurrency(final Double amount, final String currencyCode) {
		if (amount == 0 && StringUtils.isEmpty(currencyCode)) {
			return props.getTargetBaseCcy();
		}
		return currencyCode;
	}

	/**
	 * Gets the debtor account.
	 *
	 * @param debtorId
	 *            the debtor id
	 * @param isMultiple
	 *            the is multiple
	 * @return the debtor account
	 */
	private DebtorAccount getDebtorAccount(final String debtorId, final Boolean isMultiple) {
		try {
			final AccountES accountData = accInfoUtil.getAccountDetails(null, "", debtorId, "", "",PaymentIngestionConstant.GUID, null, "");
			final DebtorAccount debtorAccount = new DebtorAccount();
			if (isMultiple) {
				debtorAccount.setId(PaymentIngestionConstant.MULTIPLE);
				debtorAccount.setName(PaymentIngestionConstant.MULTIPLE);
				debtorAccount.setAccountNo(null);
				debtorAccount.setTags(new ArrayList<>());
			} else {
				debtorAccount.setId(accountData.getId());
				debtorAccount.setName(accountData.getName());
				debtorAccount.setAccountNo(accountData.getAccountNo());
				debtorAccount.setTags(accountData.getTags());
			}
			debtorAccount.setBank(accountData.getBank());
			debtorAccount.setEntity(accountData.getEntity());
			debtorAccount.setStatus(accountData.getStatus());
			debtorAccount.setCountry(accountData.getCountry());

			return debtorAccount;
		} catch (IOException | DataNotFoundException e) {

			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_016 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_016_Desc);
			LOGGER.error("Debtor account not found for id: {} with exception: {}", debtorId, e);
			throw new NullPointerException(e.getMessage());
		}
//		return null;
	}

}
