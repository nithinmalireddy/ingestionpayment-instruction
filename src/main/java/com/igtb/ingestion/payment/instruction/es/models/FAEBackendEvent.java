
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.HashMap;
import java.util.Map;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "eventVersion", "context", "payload" })
public class FAEBackendEvent {

	@JsonProperty("id")
	private String id;
	@JsonProperty("eventVersion")
	private String eventVersion;
	@JsonProperty("context")
	private Context context;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("eventVersion")
	public String getEventVersion() {
		return eventVersion;
	}

	@JsonProperty("eventVersion")
	public void setEventVersion(String eventVersion) {
		this.eventVersion = eventVersion;
	}

	@JsonProperty("context")
	public Context getContext() {
		return context;
	}

	@JsonProperty("context")
	public void setContext(Context context) {
		this.context = context;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
