package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.filestorage.util.FileStorageManager;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataFile;

@Component
public class IgtbBlobStorUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IgtbBlobStorUtil.class);

	/** The file storage manager. */
	@Autowired
	private FileStorageManager fileStorageManager;

	/**
	 * Read metadata file.
	 *
	 * @param fileId
	 *            the file id
	 * @return the metadata file
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public MetadataFile readMetadataFile(final String fileId) throws IOException, MessageProcessingException {
		LOGGER.debug("Downloading metadata file using fileStorageManager for metadata fileId: {}", fileId);
		InputStream inputStream = null;
		try {
			inputStream = fileStorageManager.getS3Store().get(fileId);
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_020 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_020_Desc);
			LOGGER.error("Exception while downloading metadata file as: {}", e);
			throw new MessageProcessingException("Exception while downloading metadata file as: " + e.getMessage());
		}
		final ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(inputStream, MetadataFile.class);
	}

}
