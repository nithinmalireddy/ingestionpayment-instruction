package com.igtb.ingestion.payment.instruction.metadata.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class MetadataFile.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetadataFile {

    /** The metadata. */
    private Metadata metadata;
    
    /** The records. */
    private List<Records> records;

}
