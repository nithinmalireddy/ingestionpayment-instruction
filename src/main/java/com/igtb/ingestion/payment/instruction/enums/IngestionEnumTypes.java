package com.igtb.ingestion.payment.instruction.enums;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.util.IgtbBlobStorUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Enum to define the collection of all valid RequestTypes, 
 * ResourceTypes, TransitionStates, TransitionStatus and some CBX specific utility enums and methods.
 * 
 */
public final class IngestionEnumTypes{
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IgtbBlobStorUtil.class);
	/**
	 * Enum listing all request Types.
	 * 
	 */
	
	/** The Constant log. */
	@Slf4j
	public enum RequestEnum {
		
		/** The add. */
		add,
		
		/** The amend. */
		amend,
		
		/** The hold. */
		hold,
		
		/** The unhold. */
		unhold,

		/** The cancel. */
		delete;
		
		

		/**
		 * Get Request Type Enum. 
		 * 
		 * @param reqType
		 * 			The respective request Type.
		 * 
		 * @return {@link RequestEnum}
		 */
		public static RequestEnum getRequestEnumType(final String reqType) {
			try {
				return reqType==null?null:Enum.valueOf(RequestEnum.class, reqType);
			}catch(IllegalArgumentException ex) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_021 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_021_Desc);
				log.info(
						">>>>>> Could not get process further because of unacceptable operation type <<<<<<<");
			}
			return null;
		}
		
		public static boolean isHUCRequestType(final RequestEnum reqType) {
			switch(reqType) {
			case add:
				return false;
			case amend:
				return false;
			case delete:
				return true;
			case hold:
				return true;
			case unhold:
				return true;
		}
		return false;
	}
	}
	
	/**
	 * Enum listing all resource types.
	 * 
	 */
	
	/** The Constant log. */
	@Slf4j
	public enum ResourceEnum {

		/** The payment instructions. */
		paymentInstructions("Fund Transfer", "PI");
		
		/** Worflow specific name for the defined enum.*/
		private String workflowNm;
		
		/** The abbrev. */
		private String abbrev;
		
		/**
		 * Enum Constructor to instantiate the called enum.
		 *
		 * @param str the str
		 */
		private ResourceEnum(final String str) {
			workflowNm = str;
		}
		
		/**
		 * Enum Constructor to instantiate the called enum.
		 *
		 * @param str the str
		 * @param abbrev the abbrev
		 */
		private ResourceEnum(final String str, final String abbrev) {
			this(str);
			this.abbrev = abbrev;
		}
		
		/**
		 * Gets the workflow name.
		 *
		 * @return the workflow name
		 */
		public String getWorkflowName() {
			return workflowNm;
		}

		/**
		 * Gets the abbrev.
		 *
		 * @return the abbrev
		 */
		public String getAbbrev() {
			return abbrev;
		}
		
		/**
		 * Gets the resource enum type.
		 *
		 * @param resourceType the resource type
		 * @return the resource enum type
		 */
		public static ResourceEnum getResourceEnumType(final String resourceType){
			try {
				return resourceType==null?null:Enum.valueOf(ResourceEnum.class, resourceType);
			}catch(IllegalArgumentException ex) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_022 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_022_Desc);
				log.info(
						">>>>>> Could not get process further because of unacceptable resource request <<<<<<<");
			}
			return null;
		}
		
	}
	
	
	
	/**
	 * Enum listing all the transition status.
	 * 
	 */
	
	@Slf4j
	public enum TransitionStateEnum {
		
		/** The draft. */
		draft,
		
		/** The val success. */
		val_success,
		
		/** The val timeout. */
		val_timeout,
		
		/** The val failure. */
		val_failure,
		
		/** The cancelled. */
		cancelled,
		
		/** The rejected. */
		rejected,
		
		/** The approved. */
		approved,
		
		/** The released. */
		released,
		
		/** The release retry. */
		release_retry,
		
		/** The release rejected. */
		release_rejected,
		
		/** The release failure. */
		release_failure,
		
		/** The backend ack success. */
		backend_ack_success,
		
		/** The backend rejected. */
		backend_rejected,
		
		/** The backend processed. */
		backend_processed,
		
		/** The backend created. */
		backend_created,
		
		/** The backend updated. */
		backend_updated,
		
		/** The backend deleted. */
		backend_deleted,
		
		/** The trashed. */
		trashed,
		
		/** The initiated wf. */
		// WF state
		initiated_wf,
		
		/** The verified wf. */
		verified_wf, 
		
		/** The approved wf. */
		approved_wf,
		
		/** The released wf. */
		released_wf,
		
		/** The rejected wf. */
		rejected_wf,
		
		/** The trashed wf. */
		trashed_wf; 
		

		/**
		 * Get Transition State Type Enum. 
		 *
		 * @param transitionState transitionState
		 * @return {@link TransitionStateEnum}
		 */
		public static TransitionStateEnum getTransitionStateEnumType(final String transitionState){
			try {
				return transitionState==null?null:Enum.valueOf(TransitionStateEnum.class, transitionState);
			}catch(IllegalArgumentException ex) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_023 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_023_Desc);
				log.info(
						">>>>>> Could not get process further because of unacceptable transition state request <<<<<<<");
			}
			return null;
		}

	}
	
	/**
	 * Enum listing all the outcome catgeory.
	 * 
	 * @author vaishali.gupta
	 * @since 11-Mar-2019/9:59:42 PM
	 *
	 */
	public enum OutcomeCategoryEnum{
		
		/** The backend state update. */
		BACKEND_STATE_UPDATE("backend-state-updated"),
		
		/** The channel state update. */
		CHANNEL_STATE_UPDATE("channel-state-updated"),
		
		/** The channel workflow update. */
		CHANNEL_WORKFLOW_UPDATE("channel-workflow-update");
		
		/** The outcome category str. */
		private String outcomeCategoryStr;

		/**
		 * Instantiates a new outcome category enum.
		 *
		 * @param outcomeCategoryStr the outcome category str
		 */
		private OutcomeCategoryEnum(final String outcomeCategoryStr) {
			this.outcomeCategoryStr = outcomeCategoryStr;
		}

		/**
		 * Gets the outcomeCategoryStr. 
		 * @return the outcomeCategoryStr
		 */
		public String getOutcomeCategoryStr() {
			return outcomeCategoryStr;
		}


		/**
		 * Gets the enum by string.
		 *
		 * @param desc the desc
		 * @return the enum by string
		 */
		//Get the value of the Enum name from a respective description. 
		public static OutcomeCategoryEnum getEnumByString(final String desc){
	        for(final OutcomeCategoryEnum e : OutcomeCategoryEnum.values()){
	            if(e.getOutcomeCategoryStr().equals(desc)) {return e;}
	        } 
	        return null; 
	    } 
	}
	
	/**
	 * Enum listing all the transition status.
	 * 
	 */
	public enum TransitionStatusEnum{
		
		/** The in progress. */
		IN_PROGRESS("in-progress"),
		
		/** The completed. */
		COMPLETED("completed");
		
		/** The transition status enum str. */
		private String transitionStatusEnumStr;

		/**
		 * Instantiates a new transition status enum.
		 *
		 * @param transitionStatusEnumStr the transition status enum str
		 */
		private TransitionStatusEnum(final String transitionStatusEnumStr) {
			this.transitionStatusEnumStr = transitionStatusEnumStr;
		}

		/**
		 * Gets the transitionStatusEnumStr. 
		 * @return the transitionStatusEnumStr
		 */
		public String getTransitionStatusEnumStr() {
			return transitionStatusEnumStr;
		}
		
		/**
		 * Gets the enum by string.
		 *
		 * @param desc the desc
		 * @return the enum by string
		 */
		//Get the value of the Enum name from a respective description. 
		public static TransitionStatusEnum getEnumByString(final String desc){
	        for(final TransitionStatusEnum e : TransitionStatusEnum.values()){
	            if(e.getTransitionStatusEnumStr().equals(desc)) {
	            	return e;
	            }
	        } 
	        return null; 
	    }
		
	}
	
	
	
	/** Enum listing all the allowed mode of creation for a template. */
	public enum MODE{
		
		/** The single. */
		SINGLE,
		
		/** The expert. */
		EXPERT,
		
		/** The batch. */
		BATCH
	}
	
	
	/** Enum listing all the backend support statuses(for hold/unhold/cancel submitted from CBX). */
	public enum CBXSupportSatusEnum{
		
		/** The hold submitted. */
		HOLD_SUBMITTED("Hold Request"),
		
		/** The unhold submitted. */
		UNHOLD_SUBMITTED("Unhold Request"),
		
		/** The cancel submitted. */
		CANCEL_SUBMITTED("Stop Request"),
		
		/** The none. */
		NONE("none");
		
		/** CBX Hold Request Message to be reflected on the screen. */
		private String cbxSupportStatusStr;
		
		/**
		 * Enum Constructor to instantiate the called enum.
		 *
		 * @param str the str
		 */
		private CBXSupportSatusEnum(final String str) {
			cbxSupportStatusStr = str;
		}

		/**
		 * Gets the cbxSupportStatusStr. 
		 * @return the cbxSupportStatusStr
		 */
		public String getCbxSupportStatusStr() {
			return cbxSupportStatusStr;
		}
		
		/**
		 * Gets the enum by string.
		 *
		 * @param code the code
		 * @return the enum by string
		 */
		//Get the value of the Enum name for a respective description. 
		public static CBXSupportSatusEnum getEnumByString(final String code){
	        for(final CBXSupportSatusEnum e : CBXSupportSatusEnum.values()){
	            if(e.getCbxSupportStatusStr().equals(code)) {
	            	return e;
	            }
	        } 
	        return null; 
	    } 
	}
}
