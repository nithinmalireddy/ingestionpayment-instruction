package com.igtb.ingestion.payment.instruction.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class BackendPayload.
 */
@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BackendPayload {

	/** The file id. */
	private String fileId;
	
	/** The txn data. */
	private List<TxnData> txnData;
	
	/** The reject reason. */
	private String rejectReason;
	
}
