
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UploadUser.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadUser {

    /** The id. */
    private String id;
    
    /** The name. */
    private String name;
    
    /** The domain name. */
    private String domainName;
    
    /** The org id. */
    private String orgId;

}
