package com.igtb.ingestion.payment.instruction.commons.decision.cibc;

import com.google.common.collect.Maps;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.FromStates;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.StateTransitionData;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.StateTransitionDecisionModule;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.ToStates;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.TransitionAction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StateTransitionDecisionModule
{
  private static final Logger LOGGER = LoggerFactory.getLogger(StateTransitionDecisionModule.class);
  private static final String CHANNEL_WORKFLOW_UPDATE = "channel-workflow-update";

  public static TransitionAction getActionForTransitionCIBC(StateTransitionData stateTransitionData)
  {
    LOGGER.info(">>>>>> Decision Matrix Begins <<<<<<");

    LOGGER.info("From State {}, From OutCome Category {}, From EventTime {} ", new Object[] { stateTransitionData.getFromState(), stateTransitionData.getFromOutcomeCategory
      (), stateTransitionData.getFromEventTime() });

    LOGGER.info("To State {}, To OutCome Category {}, To EventTime {} ", new Object[] { stateTransitionData.getToState(), stateTransitionData.getToOutcomeCategory
      (), stateTransitionData.getToEventTime() });

    String derivedFromOutcome = (String)Optional.ofNullable(stateTransitionData.getFromOutcomeCategory()).orElse
      ("NOOUTCOME");

    String derivedFromState = resolveFromState(stateTransitionData.getFromState(), derivedFromOutcome);

    String derivedToOutcome = (String)Optional.ofNullable(stateTransitionData.getToOutcomeCategory()).orElse("NOOUTCOME");

    String derivedToState = resolveToState(stateTransitionData.getToState(), derivedToOutcome);

    Date fromEventTime = getFormattedDate(stateTransitionData.getFromEventTime());

    Date toEventTime = getFormattedDate(stateTransitionData.getToEventTime());

    Boolean isRequestLatest = resolveTimeCheck(toEventTime, fromEventTime);

    if ((isRequestLatest.booleanValue()) && 
      (((Boolean)Optional.ofNullable(initialiazeAcceptedWorkflowEvents().get(derivedFromState)).orElse(Boolean.valueOf(false))).booleanValue()) && 
      (((Boolean)Optional.ofNullable(initialiazeWorkflowEvents().get(derivedToState)).orElse(Boolean.valueOf(false))).booleanValue()))
    {
      return TransitionAction.UPDATE;
    }

    Map fromStateMap = Maps.newHashMapWithExpectedSize(FromStates.values().length);
    Map toStateMap = Maps.newHashMapWithExpectedSize(ToStates.values().length);
    Object[] localObject = FromStates.values(); int i = localObject.length; 
    for (int j = 0; j < i; ++j) {
      FromStates fromState = (FromStates) localObject[j];
      fromStateMap.put(fromState.name(), fromState);
    }

    localObject = ToStates.values(); i = localObject.length; 
    for (int j = 0; j < i; ++j) { 
      ToStates toState = (ToStates) localObject[j];
      toStateMap.put(toState.name(), toState);
    }

    FromStates fromStateEnum = (FromStates)fromStateMap.get(derivedFromState.toUpperCase());

    ToStates toStateEnum = (ToStates)toStateMap.get(derivedToState.toUpperCase());

    LOGGER.info(">>>>>> Decision Matrix Ends <<<<<<");

    TransitionAction instrAction = TransitionAction.of(fromStateEnum, toStateEnum);
    return ((instrAction != null) ? instrAction : (TransitionAction)TransitionAction.SKIP);
  }

  private static Date getFormattedDate(String eventTime)
  {
    if (eventTime != null) {
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
      try {
        return formatter.parse(eventTime);
      } catch (ParseException e) {
        LOGGER.info("Parse Exception during string to date conversion at Decision Layer");

        return null; }
    }
    return null;
  }

  private static Boolean resolveTimeCheck(Date toEventTime, Date fromEventTime)
  {
    if ((toEventTime != null) && (fromEventTime != null)) {
      if (toEventTime.after(fromEventTime))
        return Boolean.valueOf(true);

      return Boolean.valueOf(false);
    }
    if ((toEventTime != null) && (fromEventTime == null))
      return Boolean.valueOf(true);

    return Boolean.valueOf(false);
  }

  private static String resolveFromState(String fromState, String fromOutCome)
  {
    if ("channel-workflow-update".equalsIgnoreCase(fromOutCome))
      return fromState.toUpperCase(Locale.ENGLISH) + "_WF";
    if (fromState == null)
      return "NO_STATE";

    return fromState;
  }

  private static String resolveToState(String toState, String toOutCome)
  {
    if ("channel-workflow-update".equalsIgnoreCase(toOutCome))
      return toState.toUpperCase(Locale.ENGLISH) + "_WF";
    if (toState == null)
      return "NO_STATE";

    return toState;
  }

  private static Map<String, Boolean> initialiazeAcceptedWorkflowEvents()
  {
    Map workFlowState = new HashMap();
    workFlowState.put("INITIATED_WF", Boolean.valueOf(true));
    workFlowState.put("VERIFIED_WF", Boolean.valueOf(true));
    workFlowState.put("APPROVED_WF", Boolean.valueOf(true));
    workFlowState.put("RELEASED_WF", Boolean.valueOf(true));
    workFlowState.put("TRASHED_WF", Boolean.valueOf(true));
    return workFlowState;
  }

  private static Map<String, Boolean> initialiazeWorkflowEvents()
  {
    Map workFlowState = new HashMap();
    workFlowState.put("INITIATED_WF", Boolean.valueOf(true));
    workFlowState.put("VERIFIED_WF", Boolean.valueOf(true));
    workFlowState.put("APPROVED_WF", Boolean.valueOf(true));
    workFlowState.put("RELEASED_WF", Boolean.valueOf(true));
    workFlowState.put("TRASHED_WF", Boolean.valueOf(true));
    workFlowState.put("REJECTED_WF", Boolean.valueOf(true));
    return workFlowState;
  }
}