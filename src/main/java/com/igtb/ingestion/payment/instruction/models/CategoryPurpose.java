
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CategoryPurpose.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CategoryPurpose {

    /** The code. */
    @JsonProperty("code")
    public String code;
    
    /** The proprietary. */
    @JsonProperty("proprietary")
    public Object proprietary;

}
