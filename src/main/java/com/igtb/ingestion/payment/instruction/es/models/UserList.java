package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UserList.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class UserList {

	/** The user id. */
	private String userId;
	
	/** The user name. */
	private String userName;
	
	/** The role. */
	private String role;
	
	/** The role desc. */
	private String roleDesc;
	
	/** The transaction role. */
	private String transactionRole;
	
	/** The image id. */
	private String imageId;
	
	/** The email id. */
	private String emailId;
	
	/** The phone no. */
	private String phoneNo;
	
}
