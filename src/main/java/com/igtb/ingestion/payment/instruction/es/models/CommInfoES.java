package com.igtb.ingestion.payment.instruction.es.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class CommInfoES.
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommInfoES {

	/** The mobile. */
	private List<String> mobile;
	
	/** The phones. */
	private List<String> phone;
	
	/** The fax. */
	private String fax;
	
	/** The emails. */
	private List<String> email;
	
	@SuppressWarnings("unchecked")
	@JsonProperty("phone")
	private void getPhone(final Object phone) { // NOPMD unused private method
		if (phone != null) {
			if (phone.getClass().equals(ArrayList.class)) {
				this.phone = (List<String>) phone;
			} else {
				this.phone = Arrays.asList(phone.toString());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@JsonProperty("email")
	private void getEmail(final Object email) { // NOPMD unused private method
		if (email != null) {
			if (email.getClass().equals(ArrayList.class)) {
				this.email = (List<String>) email;
			} else {
				this.email = Arrays.asList(email.toString());
			}
		}
	}

	@SuppressWarnings("unchecked")
	@JsonProperty("mobile")
	private void getmobile(final Object mobile) { // NOPMD unused private method
		if (mobile != null) {
			if (mobile.getClass().equals(ArrayList.class)) {
				this.mobile = (List<String>) mobile;
			} else {
				this.mobile = Arrays.asList(mobile.toString());
			}
		}
	}
}
