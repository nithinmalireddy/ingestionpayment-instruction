
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class FxDetail.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class FxDetail {

    /** The fx rate. */
    @JsonProperty("fxRate")
    public Double fxRate;
    
    /** The fx contract id. */
    @JsonProperty("fxContractId")
    public String fxContractId;
    
    /** The fx rate type. */
    @JsonProperty("fxRateType")
    public String fxRateType;
    
    /** The fx amount. */
    @JsonProperty("fxAmount")
    public String fxAmount;

    /** The fx currency. */
    private String fxCurrency;
}
