package com.igtb.ingestion.payment.instruction.models.bulk;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class TxnData.
 */
@Setter
@Getter
@ToString
public class TxnSlip {

	/** The internal txn id. */
	private String internalTxnId;
	
	/** The action. */
	private String action;
	
	/** The reject remark. */
	private String rejectRemark;
}
