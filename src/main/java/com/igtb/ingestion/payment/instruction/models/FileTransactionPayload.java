package com.igtb.ingestion.payment.instruction.models;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class FileTransactionPayload.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileTransactionPayload {

	/** The file id. */
	@NotEmpty
	private String fileId;
	
}