package com.igtb.ingestion.payment.instruction.mapper;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DLQException;

/**
 * The Class PaymentCamelMapper.
 */
public class PaymentCamelMapper implements Processor {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentCamelMapper.class);
	
	/** The object mapper. */
	private ObjectMapper objectMapper;

	/* (non-Javadoc)
	 * @see org.apache.camel.Processor#process(org.apache.camel.Exchange)
	 */
	@Override
	public void process(final Exchange exchange) throws Exception {

		initialize(exchange);
		try {
			final String eventMessage = exchange.getIn().getBody(String.class);
			final JsonNode root = objectMapper.readTree(eventMessage);
			exchange.getIn().setBody(root, JsonNode.class);
		} catch (JsonProcessingException | IllegalArgumentException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_024 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_024_Desc);
			LOGGER.error("Exception while converting request of string type to JsonNode: {}", e.getMessage());
			throw new DLQException("Invalid payload, sending to DLQ");
		}
	}

	/**
	 * Initialize.
	 *
	 * @param exchange the exchange
	 */
	public synchronized void initialize(final Exchange exchange) {

		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
			objectMapper.setSerializationInclusion(Include.NON_NULL);
		}
	}
}
