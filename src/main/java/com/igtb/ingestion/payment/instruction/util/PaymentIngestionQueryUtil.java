package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.net.ProtocolException;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;

import io.searchbox.action.Action;
import io.searchbox.action.BulkableAction;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Bulk;
import io.searchbox.core.BulkResult;
import io.searchbox.core.BulkResult.BulkResultItem;
import io.searchbox.core.Delete;
import io.searchbox.core.DeleteByQuery;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.MultiGet;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;
import io.searchbox.core.UpdateByQuery;
import io.searchbox.core.UpdateByQueryResult;

/**
 * The Class PaymentIngestionQueryUtil.
 */
@Component
@SuppressWarnings({ "unchecked", "rawtypes" })
public class PaymentIngestionQueryUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionQueryUtil.class);

	/** The jest client. */
	@Autowired
	private JestClient jestClient;

	/** The object mapper. */
	@Autowired
	private ObjectMapper objectMapper;

	/** The payment elastic properties. */
	@Autowired
	private PaymentElasticProperties paymentElasticProperties;

	@Autowired
	private EventValidationConfig eventValidationConfig;

	/**
	 * Enrich with three fields with nested query.
	 *
	 * @param input1
	 *            the input 1
	 * @param input2
	 *            the input 2
	 * @param matchString1
	 *            the match string 1
	 * @param matchString2
	 *            the match string 2
	 * @param fetchStrings
	 *            the fetch strings
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult enrichUserInfoUsingTermNestedQuery(final String input1, final String input2,final String matchString1, final String matchString2, final String[] fetchStrings, final String index,final String type) throws IOException {
		try {
			LOGGER.info("Start enrichUserInfoUsingTermNestedQuery()::::::{}", input1);
			final BoolQueryBuilder query = QueryBuilders.boolQuery();
			final BoolQueryBuilder domainNameQuery = QueryBuilders.boolQuery();
			final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			query.must(QueryBuilders.termQuery(matchString1, input1));
			query.must(QueryBuilders.nestedQuery(PaymentIngestionConstant.DOMAIN_INFO,domainNameQuery.must(QueryBuilders.termQuery(matchString2, input2))));
			searchSourceBuilder.query(query).fetchSource(fetchStrings, null);
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(index).addType(type).build();
			return jestClient.execute(search);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_040 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_040_Desc);
			LOGGER.error(PaymentIngestionConstant.FAILEDENRICHMENT, type);
			throw new NullPointerException();
		}

	}

	/**
	 * Index builder.
	 *
	 * @param object
	 *            the object
	 * @param id
	 *            the id
	 * @param esIndex
	 *            the es index
	 * @param esType
	 *            the es type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the MessageProcessingException
	 */
	public JestResult indexBuilder(final Object object, final String id, final String esIndex, final String esType)
			throws IOException, MessageProcessingException {

		final ObjectNode objectNode = objectMapper.convertValue(object, ObjectNode.class);

		if (id == null) {
			final Index index = new Index.Builder(objectNode.toString()).index(esIndex).type(esType).refresh(true)
					.build();
			final JestResult jestResult = jestClient.execute(index);
			if (jestResult.getErrorMessage() == null) {
				final String docId = jestResult.getValue(PaymentIngestionConstant._ID).toString();
				LOGGER.info("Inserted the message into ES with _id {} on this ES-Type ->{}",docId, esType);
				return jestResult;
			} else {
				LOGGER.error("Exception thrown  message into Elasticsearch and the exception message is {}",jestResult.getErrorMessage());
				throw new MessageProcessingException(jestResult.getErrorMessage());
			}
		} else {
			final Index index = new Index.Builder(objectNode.toString()).index(esIndex).type(esType).id(id).refresh(true).build();
			final JestResult jestResult = jestClient.execute(index);

			if (jestResult.getErrorMessage() == null) {
				final String docId = jestResult.getValue(PaymentIngestionConstant._ID).toString();
				LOGGER.info("Inserted the message into ES with _id {} on this ES-Index ->{} and on this ES-Type ->{}",docId, esIndex, esType);
				return jestResult;
			} else {
				LOGGER.error("Exception thrown while inserting the {} message into Elasticsearch and the exception message is {}",esType, jestResult.getErrorMessage());
				throw new MessageProcessingException(jestResult.getErrorMessage());
			}
		}
	}
	
	public synchronized JestResult indexBuilderHistory(final JSONObject payload) throws IOException {
		Index index = null;
			LOGGER.info("inserting backend_created events without Id :::::::");
			index = new Index.Builder(payload.toString()).index("quest.pay").type("paymentInstructionTransitionHistory").build();
		
		LOGGER.info("ES Insertion :::: type ::::: {}", index.getId());
		return jestClient.execute(index);
	}


	/**
	 * Bulk action builder.
	 *
	 * @param accountIndexList
	 *            the account index list
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult bulkActionBuilder(final List<BulkableAction> accountIndexList, final String index,
			final String type) throws IOException {
		final Bulk bulk = new Bulk.Builder().defaultIndex(index).defaultType(type).addAction(accountIndexList)
				.refresh(true).build();
		final BulkResult result = jestClient.execute(bulk);
		if (!result.isSucceeded()) {
			LOGGER.error("Bulk record inserted error msg if any: ");
			for (final Iterator<BulkResultItem> iterator = result.getItems().iterator(); iterator.hasNext();) {
				final BulkResultItem bulkResultItem = (BulkResultItem) iterator.next();
				LOGGER.error(bulkResultItem.error);
			}
			throw new IOException(PaymentIngestionConstant.ERROR_WHILE_PROCESSING_BULK_RECORD + result.getJsonString());
		}
		return result;
	}

	/**
	 * Insert bulk record.
	 *
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @param recordsIndexList
	 *            the records index list
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult insertBulkRecord(final String index, final String type, final List<Index> recordsIndexList)
			throws IOException {
		final Bulk bulk = new Bulk.Builder().defaultIndex(index).defaultType(type).addAction(recordsIndexList)
				.refresh(true).build();
		final BulkResult result = jestClient.execute(bulk);

		if (!result.isSucceeded()) {
			LOGGER.error("Bulk record inserted error msg if any: ");
			for (final Iterator<BulkResultItem> iterator = result.getItems().iterator(); iterator.hasNext();) {
				final BulkResultItem bulkResultItem = (BulkResultItem) iterator.next();
				LOGGER.error(bulkResultItem.error);
			}
			throw new IOException(PaymentIngestionConstant.ERROR_WHILE_PROCESSING_BULK_RECORD + result.getJsonString());
		}
		return result;
	}

	/**
	 * Bulk action update builder.
	 *
	 * @param indexList
	 *            the account index list
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult bulkActionExecutor(final List<BulkableAction> indexList, final String index, final String type)
			throws IOException {
		final Bulk bulk = new Bulk.Builder().defaultIndex(index).defaultType(type).addAction(indexList).refresh(true)
				.build();
		final BulkResult result = jestClient.execute(bulk);
		if (!result.isSucceeded()) {
			LOGGER.error("Bulk record action: {} error msg if any: ", indexList.get(0).getBulkMethodName());
			for (final Iterator<BulkResultItem> iterator = result.getItems().iterator(); iterator.hasNext();) {
				final BulkResultItem bulkResultItem = (BulkResultItem) iterator.next();
				LOGGER.error(bulkResultItem.error);
			}
			throw new IOException(PaymentIngestionConstant.ERROR_WHILE_PROCESSING_BULK_RECORD + result.getJsonString());
		}
		return result;
	}

	/**
	 * Update builder action.
	 *
	 * @param object
	 *            the object
	 * @param elasticSearchId
	 *            the elastic search id
	 * @param indexName
	 *            the index name
	 * @param type
	 *            the type
	 * @param removalId
	 *            the removal id
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult updateBuilderAction(final PaymentInstruction object, final String elasticSearchId,
			final String indexName, final String type, final String removalId) throws IOException {
		final JsonNode node = objectMapper.convertValue(object, JsonNode.class);
		final ObjectNode docNode = JsonNodeFactory.instance.objectNode();
		docNode.set("doc", node);
		final Update update = new Update.Builder(docNode.toString()).index(indexName).type(type).id(elasticSearchId)
				.refresh(true).build();
		try {
			LOGGER.info("Updating record with id {}", elasticSearchId);
			return jestClient.execute(update);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_041 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_041_Desc);
			LOGGER.error("Exception thrown while updating the record");
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * State update builder action.
	 *
	 * @param object
	 *            the object
	 * @param elasticSearchId
	 *            the elastic search id
	 * @param indexName
	 *            the index name
	 * @param type
	 *            the type
	 * @param removalId
	 *            the removal id
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult updateExistingDocument(final Object object, final String elasticSearchId, final String indexName,
			final String type, final String removalId) throws IOException {
		final JsonNode jsonUpdateNode = new ObjectMapper().convertValue(object, JsonNode.class);
		try {
			LOGGER.info("Updating record with id {} and payload {}", elasticSearchId,jsonUpdateNode);
			return updateBuilder(jsonUpdateNode, elasticSearchId, indexName, type);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_042 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_042_Desc);
			LOGGER.error("Exception thrown while updating the record");
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * Contact delete action.
	 *
	 * @param documentId
	 *            the document id
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult paymentDeleteAction(final String documentId, final String index, final String type)
			throws IOException {
		final Delete delete = new Delete.Builder(documentId).index(index).type(type).build();
		try {
			LOGGER.info("Deleting the record with id {}", documentId);
			return jestClient.execute(delete);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_043 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_043_Desc);
			LOGGER.error("Exception thrown while Deleting the record");
			throw new IOException(e.getMessage());
		}
	}

	/**
	 * Fetch existing document.
	 *
	 * @param matchString
	 *            the match string
	 * @param channelSeqId
	 *            the channel seq id
	 * @param indexName
	 *            the index name
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult fetchExistingDocument(final String matchString, final String channelSeqId, final String indexName,final String type) throws IOException {

		try {
			final SearchSourceBuilder searchSourceBuilder = constructSelectQuery(matchString, channelSeqId);
			LOGGER.info("###Final search query for channelSequenceId::{} for fetchExistingDocumentInfoFromES().....{}",channelSeqId,searchSourceBuilder);
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(indexName).addType(type).build();
			return jestClient.execute(search);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_044 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_044_Desc);
			throw new IOException("Exception thrown while fetching existing document for "+channelSeqId);
		}

	}

	/**
	 * Gets the document by match query.
	 *
	 * @param matchFieldQuery
	 *            the match field query
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the document by match query
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult getDocumentByMatchQuery(final String matchFieldQuery, final String index, final String type)
			throws IOException {
		final Search search = new Search.Builder(matchFieldQuery).addIndex(index).addType(type).build();
		return jestClient.execute(search);
	}

	/**
	 * Construct select query.
	 *
	 * @param matchField
	 *            the match field
	 * @param matchValue
	 *            the match value
	 * @return the search source builder
	 */
	public SearchSourceBuilder constructSelectQuery(final String matchField, final String matchValue) {
		final BoolQueryBuilder termQuery = QueryBuilders.boolQuery();
		final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		termQuery.must(QueryBuilders.termQuery(matchField, matchValue));
		searchSourceBuilder.query(termQuery);
		return searchSourceBuilder;
	}

	/**
	 * Construct multi get query.
	 *
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @param matchField
	 *            the match field
	 * @param matchValues
	 *            the match value
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult constructMultiGetQuery(final String index, final String type, final String matchField,
			final Set<String> matchValues) throws IOException {
		final Action action = new MultiGet.Builder.ById(index, type).addId(matchValues).build();
		return jestClient.execute(action);
	}

	/**
	 * Gets the document id from jest result.
	 *
	 * @param jestResult
	 *            the jest result
	 * @return the document id from jest result
	 */
	public String getDocumentIdFromJestResult(final JestResult jestResult) {
		final JsonObject resultObj = jestResult.getJsonObject();
		final JsonArray hitsArray = Optional.ofNullable(resultObj.get(PaymentIngestionConstant.HITS)).map(jsonObj -> jsonObj.getAsJsonObject()).map(json -> json.get(PaymentIngestionConstant.HITS)).map(jsonArray -> jsonArray.getAsJsonArray()).orElse(null);
		if (hitsArray != null && hitsArray.size() > 0) {
			Iterator<JsonElement> iterator = hitsArray.iterator();
            while (iterator.hasNext()) {
            	JsonElement objt = iterator.next();
            	String docStatus = objt.getAsJsonObject().get(PaymentIngestionConstant.SOURCE).getAsJsonObject().get(PaymentIngestionConstant.STATE_INFO).getAsJsonObject().get(PaymentIngestionConstant.DOC_STATUS).getAsString();
            	LOGGER.info("docStatus:::::::::{}",docStatus);
            	if(docStatus.equalsIgnoreCase(PaymentIngestionConstant.DOC_STATUS_TRANSITION))
            	{
            		LOGGER.info("docStatus:::::::::_id:::::::{}",objt.getAsJsonObject().get(PaymentIngestionConstant._ID));
            		return Optional.ofNullable(objt.getAsJsonObject()).map(jsonObject -> jsonObject.get(PaymentIngestionConstant._ID)).map(docId -> docId.toString().replace("\"", "")).orElse(null);
            	}
            }
			//return Optional.ofNullable(hitsArray.get(0).getAsJsonObject()).map(jsonObject -> jsonObject.get("_id")).map(docId -> docId.toString().replace("\"", "")).orElse(null);
		}
		return null;
	}

	/**
	 * Delete by query.
	 *
	 * @param searchBuilder
	 *            the accounts search builder
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult deleteByQuery(final SearchSourceBuilder searchBuilder, final String index, final String type)
			throws IOException {
		final DeleteByQuery deleteAction = new DeleteByQuery.Builder(searchBuilder.toString()).addIndex(index)
				.addType(type).build();

		return jestClient.execute(deleteAction);
	}

	/**
	 * Gets the index builder.
	 *
	 * @param documentId
	 *            the document id
	 * @param object
	 *            the object
	 * @param esIndex
	 *            the es index
	 * @param esType
	 *            the es type
	 * @return the index builder
	 */
	public BulkableAction getIndexBuilder(final String documentId, final Object object, final String esIndex,
			final String esType) {

		if (documentId == null) {
			return new Index.Builder(object).index(esIndex).type(esType).refresh(true).build();
		} else {
			return new Index.Builder(object).index(esIndex).type(esType).id(documentId).refresh(true).build();
		}
	}

	/**
	 * Gets the update builder.
	 *
	 * @param documentId
	 *            the document id
	 * @param object
	 *            the object
	 * @return the update builder
	 */
	public BulkableAction getUpdateBuilder(final String documentId, final Object object) {
		final JsonNode node = objectMapper.convertValue(object, JsonNode.class);
		final ObjectNode docNode = JsonNodeFactory.instance.objectNode();
		docNode.set("doc", node);
		final BulkableAction updateIndex = new Update.Builder(docNode.toString()).id(documentId).build();

		return updateIndex;

	}

	/**
	 * Gets the delete builder.
	 *
	 * @param documentId
	 *            the document id
	 * @param esIndex
	 *            the es index
	 * @param esType
	 *            the es type
	 * @return the delete builder
	 */
	public BulkableAction getDeleteBuilder(final String documentId, final String esIndex, final String esType) {
		final BulkableAction bulkableAction = new Delete.Builder(documentId).index(esIndex).type(esType).build();
		return bulkableAction;
	}

	/**
	 * Fetch selective data by passing matching string.
	 *
	 * @param matchStrings
	 *            the match strings
	 * @param fetchStrings
	 *            the fetch strings
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 */
	public JestResult fetchSelectiveDataByPassingMatchingString(final Map<String, String> matchStrings,
			final String[] fetchStrings, final String index, final String type) {
		try {
			final BoolQueryBuilder query = QueryBuilders.boolQuery();
			final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			matchStrings.forEach((key, value) -> {
				query.must(QueryBuilders.termQuery(key, value));
			});
			searchSourceBuilder.query(query);
			if (fetchStrings != null) {
				searchSourceBuilder.fetchSource(fetchStrings, null);
			}
			LOGGER.info("##[fetchSelectiveDataByPassingMatchingString]searchSourceBuilder::{}",searchSourceBuilder);
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(index).addType(type)
					.build();
			return jestClient.execute(search);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_045 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_045_Desc);
			LOGGER.error(PaymentIngestionConstant.FAILEDENRICHMENT, type);
			return null;
		}

	}

	/**
	 * Exculde selective data by passing matching string.
	 *
	 * @param matchStrings
	 *            the match strings
	 * @param exculdeStrings
	 *            the exculde strings
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @return the jest result
	 */
	public JestResult exculdeSelectiveDataByPassingMatchingString(final Map<String, String> matchStrings,
			final String[] exculdeStrings, final String index, final String type) {
		try {
			final BoolQueryBuilder query = QueryBuilders.boolQuery();
			final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			matchStrings.forEach((key, value) -> {
				query.must(QueryBuilders.termQuery(key, value));
			});
			searchSourceBuilder.query(query);
			if (exculdeStrings != null) {
				searchSourceBuilder.fetchSource(null, exculdeStrings);
			}
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(index).addType(type)
					.build();
			return jestClient.execute(search);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_046 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_046_Desc);
			LOGGER.error(PaymentIngestionConstant.FAILEDENRICHMENT, type);
			return null;
		}

	}

	/**
	 * Gets the accounts dtls from ES.
	 *
	 * @param accountGUID
	 *            the account GUID
	 * @param typeOfAccount
	 *            the type of account
	 * @param option
	 *            the option
	 * @param piType
	 *            the payment insturction type
	 * @return the accounts dtls from ES
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JsonNode getAccountsDtlsFromES(final String accountGUID, final String typeOfAccount, final String option,
			final String piType, final String serviceKey) throws IOException {
		LOGGER.info("Searching for the {} :::: with option:::: {}", typeOfAccount, option);
		// For creditorAccount
		LOGGER.info("accountGUID :::: {}" ,accountGUID);
		LOGGER.info("typeOfAccount::::: {}",typeOfAccount);
		LOGGER.info("piType::::::{} ",piType);
		if (serviceKey.contains(PaymentIngestionConstant.SEND_RTP)) {
			if (PaymentIngestionConstant.CREDITOR_ACCOUNT.equalsIgnoreCase(typeOfAccount)) {
				return getCashAccounts(accountGUID);
			} else {
				return getContactsAccounts(accountGUID);
			}

		} else if (PaymentIngestionConstant.CREDITOR_ACCOUNT.equalsIgnoreCase(typeOfAccount)) {
			// check option=PAYMENT, search in Contact Accounts
			if (Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT))
					.isPresent() || Optional.ofNullable(option).filter(obj -> obj.equals(PaymentIngestionConstant.OPTION_PAYMENT_RTP))
					.isPresent()
					|| (piType.equals(PaymentIngestionConstant.SFILE) && !eventValidationConfig.getCashAccountSupportedProduct()
							.contains(Optional.ofNullable(option).map(op -> op.toLowerCase()).orElse("")))) {
				return getContactsAccounts(accountGUID);
			}
			// else option=TRANSFER, search in Corporate Accounts
			else {
				return getCashAccounts(accountGUID);
			}
		}
		// For debtorAccount, search only in Corporate Accounts
		return getCashAccounts(accountGUID);
	}

	public JsonNode getAccountsDtlsFromES2(final String accountGUID, final String typeOfAccount, final String option,final String piType) {
		LOGGER.info("Searching for the::::: {} with option::: {}", typeOfAccount, option);
		// For creditorAccount
		LOGGER.info("accountGUID::::  {}" , accountGUID);
		LOGGER.info("typeOfAccount :::: {}" , typeOfAccount);
		LOGGER.info("piType :::: {} " , piType);
		return getContactsAccounts(accountGUID);
	}

	/**
	 * Gets the cash account details for a given account guid.
	 * 
	 * @param accountGUID
	 *            account guid
	 * @return {@link JsonNode} cashaccount json
	 * @throws IOException
	 *             exception
	 */
	private JsonNode getCashAccounts(final String accountGUID) {
		LOGGER.debug("Searching cash account details for the account guid :::: [{}].............", accountGUID);
		final String querystring1 = PaymentIngestionConstant._ID;
		// Mudasir
		final QueryBuilder query = QueryBuilders.termQuery(querystring1, accountGUID);
		// final JsonNode sourceNode = jestGetESBuilder(accountGUID,
		// paymentElasticProperties.getAcctIndexName(),
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getAcctIndexName(),paymentElasticProperties.getAccountType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.debug("returning with [{}]", sourceNode);
		return sourceNode;
	}

	/**
	 * Jest get ES builder.
	 *
	 * @param id
	 *            the id
	 * @param indexName
	 *            the index name
	 * @param typeName
	 *            the type name
	 * @return the json node
	 */
	public JsonNode jestGetESBuilder(final String id, final String indexName, final String typeName) {
		Get getBuilder = new Get.Builder(indexName, id).type(typeName).build();
		JsonNode rootNode = null;
		try {
			final DocumentResult docResult = jestClient.execute(getBuilder);
			if (!docResult.isSucceeded() && docResult.getErrorMessage() != null&& docResult.getErrorMessage().length() > 0) {
				LOGGER.error("Exception occured while getting the documents with errorMessages : [{}]",docResult.getErrorMessage());
			}
			final String jsonResult = Optional.ofNullable(docResult).map(result -> result.getJsonString()).orElse(null);
			LOGGER.debug("Current state search result::::  {}", jsonResult);
			rootNode = null;
			if (jsonResult != null)
				rootNode = objectMapper.readTree(jsonResult);
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_047 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_047_Desc);
			LOGGER.error("Exception occured in ElasticUtil GetBuilder due to reason :::: [{}]",e.getMessage());
		}
		return Optional.ofNullable(rootNode).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
	}

	/**
	 * Gets the contact account details for a given account guid.
	 * 
	 * @param accountGUID
	 * @return {@link JsonNode} contactaccount json
	 * @throws IOException
	 *             exception
	 */
	private JsonNode getContactsAccounts(final String accountGUID) {
		LOGGER.debug("Searching contact account details for the account guid >>> [{}].............", accountGUID);
		// 1. Get the contact account using guid
		final JsonNode contactAccNode = jestGetESBuilder(accountGUID, paymentElasticProperties.getOrgIndex(),paymentElasticProperties.getContactAccountsType());

		// 2. if contact account found in ES org DB
		if (contactAccNode != null) {
			final String contactId = Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactAccNode, PaymentIngestionConstant.CONTACT, PaymentIngestionConstant.ID)).map(Object::toString).map(String::trim).orElse(null);

			final JsonNode contactNode = jestGetESBuilder(contactId, paymentElasticProperties.getOrgIndex(),paymentElasticProperties.getContactsType());
			if (contactNode != null) {
				final JsonNode accCcytNode = JsonNodeFactory.instance.objectNode();
				final JsonNode countryNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode, PaymentIngestionConstant.CONTACT_ADDRESS,PaymentIngestionConstant.COUNTRY);
				final JsonNode entityNode = (JsonNode) CommonUtil.getValueFromJsonNode(contactNode,PaymentIngestionConstant.BLANK, PaymentIngestionConstant.ENTITY);
				final String currencyCode = Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactNode, PaymentIngestionConstant.MAX_PER_TXN_LMT, PaymentIngestionConstant.CURR_CODE)).map(Object::toString).orElse(null);
				final String alias = Optional.ofNullable(CommonUtil.getValueFromJsonNode(contactNode, PaymentIngestionConstant.BLANK, PaymentIngestionConstant.ALIAS)).map(Object::toString).orElse(null);

				// populate acctCcyt in contact account jsonnode
				((ObjectNode) accCcytNode).put(PaymentIngestionConstant.CODE, currencyCode);
				((ObjectNode) accCcytNode).put(PaymentIngestionConstant.NAME, PaymentIngestionConstant.BLANK);
				((ObjectNode) contactAccNode).set(PaymentIngestionConstant.ACCT_CCY, accCcytNode);

				// populate country and entity in contact account jsonnode
				((ObjectNode) contactAccNode).set(PaymentIngestionConstant.COUNTRY, countryNode);
				((ObjectNode) contactAccNode).set(PaymentIngestionConstant.ENTITY, entityNode);
				((ObjectNode) contactAccNode).put(PaymentIngestionConstant.ALIAS, alias);

				LOGGER.info("Returning with accounts details:::: {}", contactAccNode);
				return contactAccNode;
			}
		}
		return null;
	}

	/**
	 * Jest search ES builder.
	 *
	 * @param searchQuery
	 *            the search query
	 * @param indexName
	 *            the index name
	 * @param typeName
	 *            the type name
	 * @return the json node
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private JsonNode jestSearchESBuilder(final QueryBuilder searchQuery, final String indexName, final String typeName)
	 {
		JsonNode hitsNode=null;
		try {
			
			final SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
			searchBuilder.query(searchQuery.toString());
			final Search stateSearch = new Search.Builder(searchBuilder.toString()).addIndex(indexName).addType(typeName)
					.build();
			final SearchResult searchResult = jestClient.execute(stateSearch);
			final String jsonResult = searchResult.getJsonString();
			hitsNode = Optional.ofNullable(objectMapper.readTree(jsonResult)).map(obj -> obj.get(PaymentIngestionConstant.HITS)).map(obj -> obj.get(PaymentIngestionConstant.HITS)).orElse(null);
		}catch(IOException ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_048 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_048_Desc);
		}
		return hitsNode;
	}

	/**
	 * Gets the description.
	 *
	 * @param code
	 *            the code
	 * @param typeName
	 *            the type name
	 * @return the description
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String getDescription(final String code, final String typeName) throws IOException {
		LOGGER.info("Searching for the {} description with code {}.............", typeName, code);
		final QueryBuilder query = QueryBuilders.matchQuery(PaymentIngestionConstant.CODE, code);
		// Gets total hits.
		final JsonNode hitsNode = jestSearchESBuilder(query, paymentElasticProperties.getPymtIndex(), typeName);
		final String description = Optional.ofNullable(hitsNode).map(obj -> obj.get(0))
				.map(obj -> obj.get(PaymentIngestionConstant.SOURCE))
				.map(obj -> obj.get(PaymentIngestionConstant.DESCRIPTION)).map(JsonNode::asText).orElse(null);
		LOGGER.info("Returning with {} description.. ", description);
		return description;
	}

	/**
	 * Insert payment instruction.
	 *
	 * @param paymentsInstructionsES
	 *            the payments instructions ES
	 * @param id
	 *            the id
	 * @param esIndex
	 *            the es index
	 * @param esType
	 *            the es type
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public JestResult insertPaymentInstruction(final PaymentsInstructionsES paymentsInstructionsES, final String id,
			final String esIndex, final String esType) throws IOException, MessageProcessingException {
		final ObjectNode paymentsInstrESNode = objectMapper.convertValue(paymentsInstructionsES, ObjectNode.class);

		final Index index = new Index.Builder(paymentsInstrESNode.toString()).index(esIndex).id(id).type(esType)
				.refresh(true).build();
		final JestResult jestResult = jestClient.execute(index);
		if (jestResult.getErrorMessage() == null) {
			final String docId = jestResult.getValue("_id").toString();
			LOGGER.info("Inserted the message into ES with _id {} on this ES-Index ->{} and on this ES-Type ->{}",
					docId, esIndex, esType);
			return jestResult;
		} else {
			LOGGER.error("Exception thrown while inserting the {} message into Elasticsearch and the exception message is {}",esType, jestResult.getErrorMessage());
			throw new MessageProcessingException(jestResult.getErrorMessage());
		}
	}

	/**
	 * Gets the initiator of A create request for A txn.
	 *
	 * @param payloadNode
	 *            the payload node
	 * @return the initiator of A create request for A txn
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JsonNode getInitiatorOfACreateRequestForATxn(final JsonNode payloadNode) throws IOException {
		LOGGER.info("Enterning getInitiatorOfACreateRequestForATxn:::::");
		final String parentCreateRequestChannelSeqId = Optional.ofNullable(CommonUtil.getValueFromJsonNode(payloadNode,PaymentIngestionConstant.BLANK, PaymentIngestionConstant.PARENT_CREATE_REQ_CHANNEL_SEQID)).map(Object::toString).orElse(null);
		if (StringUtils.isNotBlank(parentCreateRequestChannelSeqId))
			LOGGER.debug("Get initiatror information for the incoming transaction with parentCreateRequestChannelSeqId:::: {}",parentCreateRequestChannelSeqId);
		else
			LOGGER.info("Transaction created from backend. Hence, not required to fetch the initiator information");
		JsonNode initiatedJson = null;
		if (StringUtils.isNotBlank(parentCreateRequestChannelSeqId)) {
			JsonNode sourceNode = jestGetESBuilder(parentCreateRequestChannelSeqId,paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			initiatedJson = Optional.ofNullable(sourceNode).map(t -> t.get(PaymentIngestionConstant.STATE_INFO)).map(stInfo -> stInfo.get(PaymentIngestionConstant.INITIATED)).orElse(null);
			LOGGER.info("Initiated JSON ::::{} ",initiatedJson);
		}
		return initiatedJson;
	}

	/**
	 * Index builder.
	 *
	 * @param payload
	 *            the payload
	 * @param id
	 *            the id
	 * @param esIndex
	 *            the es index
	 * @param typeName
	 *            the type name
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public synchronized JestResult indexBuilder(final JsonNode payload, final String id, final String esIndex,
			final String typeName) throws IOException {
		Index index = null;
		if (StringUtils.isBlank(id)) {
			LOGGER.info("inserting backend_created events without Id :::::::");
			index = new Index.Builder(payload.toString()).index(esIndex).type(typeName).build();
		} else {
			LOGGER.info("inserting backend_created events with Id  ::::::: {} " , id);
			index = new Index.Builder(payload.toString()).index(esIndex).id(id).type(typeName).build();
		}
		LOGGER.info("ES Insertion :::: type {}, id ::::: {}", typeName, index.getId());
		return jestClient.execute(index);
	}

	/**
	 * Update builder.
	 *
	 * @param payload
	 *            the payload
	 * @param id
	 *            the id
	 * @param esIndex
	 *            the es index
	 * @param typeName
	 *            the type name
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult updateBuilder(final JsonNode payload, final String id, final String esIndex,
			final String typeName) throws IOException {
		final ObjectNode docNode = JsonNodeFactory.instance.objectNode();
		docNode.set("doc", payload);
		LOGGER.debug(">>>>> ES Updation :: type -> {}, doc -> {}, id -> {}", typeName, docNode, id);
		final Update update = new Update.Builder(docNode.toString()).index(esIndex).type(typeName).id(id).refresh(true).build();
		final JestResult result = jestClient.execute(update);
		if (!result.isSucceeded() && result.getResponseCode() == 404) {
			LOGGER.debug("Calling index builder for id: {}", id);
			indexBuilder(payload, id, esIndex, typeName);
		}
		LOGGER.debug(">>>>> ES Updation result :: {}", result.getJsonString());
		return result;
	}

	/**
	 * Fetch past channel seq ids.
	 *
	 * @param domainSeqId
	 *            the domain seq id
	 * @return the list
	 */
	public JsonNode fetchActiveRecord(final String domainSeqId) {
		LOGGER.info("Enter inside fetchActiveRecord with domainId::::   {}",domainSeqId);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOMAIN_SEQ_ID, domainSeqId)).must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOCSTATUS, PaymentIngestionConstant.DOC_STATUS_ACTIVE));
		// Gets total hits.
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchActiveRecord with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}

	public JsonNode fetchTransitionRecordFromDomain(final String domainSeqId) {
		LOGGER.info("Enter inside fetchTransitionRecordFromDomain with domainId::::   {}",domainSeqId);
		final QueryBuilder query = QueryBuilders.boolQuery()
				.must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOMAIN_SEQ_ID, domainSeqId))
				.must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOCSTATUS,
						PaymentIngestionConstant.DOC_STATUS_TRANSITION));
		// Gets total hits.
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.debug("Returning from fetchTransitionRecordFromDomain with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}

	public JsonNode fetchTransitionRecord(final String domainSeqId) {
		LOGGER.info("Enter inside fetchTransitionRecord with domainId::::   {}",domainSeqId);
		final QueryBuilder query = QueryBuilders.boolQuery()
				.must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOMAIN_SEQ_ID, domainSeqId))
				.must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOCSTATUS,
						PaymentIngestionConstant.DOC_STATUS_TRANSITION))
				.mustNot(QueryBuilders.termQuery(PaymentIngestionConstant.STATUS,
						PaymentIngestionConstant.TRASHED_STATUS))
				.mustNot(QueryBuilders.termQuery(PaymentIngestionConstant.STATUS,
						PaymentIngestionConstant.REJECTED_STATUS));
		// Gets total hits.
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchTransitionRecord with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	// added by durgesh
	public JsonNode fetchExistingContactAccountsFromES(final String alias) {
		LOGGER.info("Enter inside fetchExistingContactAccountsFromES with alias ::::   {}", alias);
		final QueryBuilder query = QueryBuilders.boolQuery()
				.should(QueryBuilders.termQuery(PaymentIngestionConstant.ALIAS_MOBILE_NO, alias))
				.should(QueryBuilders.termQuery(PaymentIngestionConstant.ALIAS_EMAIL, alias));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getOrgIndex(),
				paymentElasticProperties.getContactAccountsType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).orElse(null);
				
		LOGGER.info("Returning from fetchExistingContactAccountsFromES with sourceNode ::::   {}", sourceNode);
		return sourceNode;
	}
	// added by vinay line (899 -907)
	public JsonNode fetchExistingRecordFromES(final String channelSeqId) {
		LOGGER.info("Enter inside fetchExsitingRecordOnChannelSeqId with channelSeqId ::::   {}",channelSeqId);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant._ID, channelSeqId));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchExsitingRecordOnChannelSeqId with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	public JsonNode fetchExistingRecordFromESForNotification(final String clearingSysNum) {
		LOGGER.info("Enter inside fetchExistingRecordFromESForNotification with clearingSysRefNo ::::   {}",clearingSysNum);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant.CLER_SYS_REF, clearingSysNum))
				.must(QueryBuilders.termQuery(PaymentIngestionConstant.STATEINFO_DOCSTATUS,
						PaymentIngestionConstant.DOC_STATUS_ACTIVE));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchExistingRecordFromESForNotification with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	public JsonNode fetchCustomerInfofromCorporates(final String accountEntityId) {
		LOGGER.info("Enter inside fetchCustomerInfofromCorporates with accountEntityId ::::   {}",accountEntityId);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant._ID, accountEntityId));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getOrgIndex(),paymentElasticProperties.getCorporates());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchCustomerInfofromCorporates with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	public JsonNode fetchCustomerInfofromActiveRecord(final String clearingSysRef) {
		LOGGER.info("Enter inside fetchCustomerInfofromActiveRecord with clearingSystemReference ::::   {}",clearingSysRef);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant._ID, clearingSysRef));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchCustomerInfofromCorporates with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	public JsonNode fetchAccountTypeFromNotificationEventConfig(String event,String ntfcnType) {
		LOGGER.info("Enter inside fetchAccountTypeFromNotificationMaster with event & ntfcnType::::   {}",event,ntfcnType);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant.EVENT, event)).must(QueryBuilders.termQuery(PaymentIngestionConstant.NOTF_TYPE_ES, ntfcnType));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPaynotificnEvntCofigType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchAccountTypeFromNotificationMaster with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}

	/**
	 * Common method using jest search builder to get total hits using builder
	 * queries.
	 *
	 * @param searchQuery
	 *            the search query
	 * @param indexName
	 *            the index name
	 * @param typeName
	 *            the type name
	 * @return the json node
	 */
	public JsonNode jestSearchESBuilder(final String searchQuery, final String indexName, final String typeName) {
		final SearchSourceBuilder searchBuilder = new SearchSourceBuilder();
		searchBuilder.query(searchQuery);
		LOGGER.debug("query ---> {}", searchBuilder);
		final Search stateSearch = new Search.Builder(searchBuilder.toString()).addIndex(indexName).addType(typeName)
				.build();
		JsonNode rootNode = null;
		try {
			final SearchResult searchResult = jestClient.execute(stateSearch);
			final String jsonResult = Optional.ofNullable(searchResult).map(result -> result.getJsonString())
					.orElse(null);
			rootNode = null;
			if (jsonResult != null)
				rootNode = objectMapper.readTree(jsonResult);
		} catch (IOException e) {
			LOGGER.warn(">>>>>>>>>> Exception occured in ElasticUtil::SearchBuilder due to reason : [{}], cause : [{}]",
					e.getMessage(), e.getCause());
		}
		final JsonNode hitsNode = Optional.ofNullable(rootNode).map(t -> t.get(PaymentIngestionConstant.HITS)).map(t -> t.get(PaymentIngestionConstant.HITS)).orElse(null);
		return hitsNode;
	}

	/**
	 * Gets the bank information from ES.
	 *
	 * @param agentId
	 *            the agent id
	 * @param schemeName
	 *            the scheme name
	 * @return the bank information from ES
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JsonNode getBankInformationFromES(final String agentId, final String schemeName) throws IOException {
		LOGGER.debug("called with creditorAgentId=>{}, schemeName=>{}", agentId, schemeName);
		/*
		 * 1. Existing bene agent:: can be fetched from ES using >>>>creditorAgentId<<<<
		 * - with bank code and branch code and country code - with BIC
		 *
		 * 2. ADHOC bene agent(not to be fetched from ES) - with bank name and branch
		 * name and country code
		 */
		JsonNode sourceNode = null;
		if (StringUtils.isNotBlank(agentId)) {
			sourceNode = jestGetESBuilder(agentId, paymentElasticProperties.getOrgIndex(),
					paymentElasticProperties.getBankType());
		}
		LOGGER.debug("Returning with bank document from org -> [{}]", sourceNode);
		return sourceNode;
	}

	/**
	 * Gets the update query script for trash.
	 *
	 * @param conditionKey
	 *            the condition key
	 * @param conditionValues
	 *            the condition values
	 * @return the update query script for trash
	 */
	public JsonObject getUpdateQueryScriptForTrash(final String conditionKey, final String[] conditionValues) {

		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.termsQuery(conditionKey, conditionValues));

		// Script
		JsonObject script = new JsonObject();
		JsonObject paramData = new JsonObject();
		paramData.addProperty("state", "trashed");
		paramData.addProperty("status", "completed");

		final String inlineString = "ctx._source.stateInfo.transitionInfo.state=params.state;ctx._source.stateInfo.transitionInfo.status=params.status";
		script.addProperty("inline", inlineString);
		script.addProperty("lang", "painless");
		script.add("params", paramData);

		JsonElement element = new JsonParser().parse(boolQueryBuilder.toString()).getAsJsonObject();
		JsonObject reqJson = new JsonObject();
		reqJson.add("query", element);
		reqJson.add("script", script);
		LOGGER.debug("Update script:{}", reqJson);
		return reqJson;

	}

	/**
	 * Partial update record.
	 *
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @param data
	 *            the data
	 * @return the update by query result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public UpdateByQueryResult partialUpdateRecord(final String index, final String type, final Object data)
			throws IOException {

		UpdateByQueryResult queryResult = updateByQuery(index, type, data);

		if (!queryResult.isSucceeded()) {
			LOGGER.error("Error while updating record:{} and error msg::::{}", queryResult.getJsonString(),queryResult.getErrorMessage());
			if (queryResult.getResponseCode() == 409) {
				LOGGER.debug("Retrying update query");
				queryResult = updateByQuery(index, type, data);
				if (!queryResult.isSucceeded()) {
					LOGGER.error("Retrying failed:::::{}",queryResult.getErrorMessage());
				}
			}
		}

		LOGGER.debug("{} record updated by query resCode: {}, isSucceeded:{}", queryResult.getUpdatedCount(),queryResult.getResponseCode(), queryResult.isSucceeded());
		return queryResult;
	}

	/**
	 * Update by query.
	 *
	 * @param index
	 *            the index
	 * @param type
	 *            the type
	 * @param data
	 *            the data
	 * @return the update by query result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private UpdateByQueryResult updateByQuery(final String index, final String type, final Object data)
			throws IOException {
		UpdateByQueryResult queryResult;
		if (StringUtils.isEmpty(type)) {
			queryResult = jestClient.execute(new UpdateByQuery.Builder(data.toString()).addIndex(index).ignoreUnavailable(true).build());
		} else {
			queryResult = jestClient.execute(new UpdateByQuery.Builder(data.toString()).addType(type).addIndex(index).ignoreUnavailable(true).build());
		}
		return queryResult;
	}
	
	public void deleteESDocumentByIndexType(String esIndexName, String esTypeName) throws IOException {
		try{
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(QueryBuilders.matchAllQuery());
			LOGGER.info("Executing query {} to delete elastic Document from elastic index name {} and elastic types {} ", searchSourceBuilder, esIndexName, esTypeName);
			DeleteByQuery deleteQuery = new DeleteByQuery.Builder(searchSourceBuilder.toString()).addIndex(esIndexName).addType(esTypeName).build(); 
			jestClient.execute(deleteQuery);
		}catch(IOException ex){
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_049 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_049_Desc);
			LOGGER.error("Throwing IOException while deleting records for Index -- {} and Types -- {} ",esIndexName, esTypeName);
		}catch(Exception ex){
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_050 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_050_Desc);
			LOGGER.error("Throwing Exception while deleting records for Index -- {} and Types -- {} ",esIndexName, esTypeName);
		}
	}
	
	public JsonNode fetchRecordFromESUsingDomainSeqId(final String domainSeqId) {
		LOGGER.info("Enter inside fetchExsitingRecordOnChannelSeqId with channelSeqId ::::   {}",domainSeqId);
		final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.termQuery(PaymentIngestionConstant._ID, domainSeqId));
		JsonNode hitsNode = jestSearchESBuilder(query.toString(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
		JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0)).map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
		LOGGER.info("Returning from fetchExsitingRecordOnChannelSeqId with sourceNode ::::   {}",sourceNode);
		return sourceNode;
	}
	public String getStatusUsingDomainSeqId(final String domainSeqId) {
		JsonNode lResultJsonNode = fetchRecordFromESUsingDomainSeqId(domainSeqId);
		if(lResultJsonNode!=null)
			return Optional.ofNullable(lResultJsonNode.get("status").textValue()).orElse(null);
		else 
			return null;
	}
	public JestResult getStatusList(final String pPaymentType,final String pCurrentStatus, String pMatchingProdType, 
			                  String pMatchingCurrentStatus, String[] fetchFields, String pIndex, String pDocType) {
		JestResult lJestResult = null;
		try {
			LOGGER.info("Start getStatusList()::::::{}::::::{}", pPaymentType,pCurrentStatus);
			LOGGER.info("Start getStatusList()::::::{}::::::{}::::{}::::{}", pMatchingProdType,pMatchingCurrentStatus,pIndex,pDocType);
			final BoolQueryBuilder query = QueryBuilders.boolQuery();
			final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			query.must(QueryBuilders.termQuery(pMatchingProdType, pPaymentType));
			query.must(QueryBuilders.termQuery(pMatchingCurrentStatus, pCurrentStatus));
			LOGGER.info("query:"+query);
			searchSourceBuilder.query(query).fetchSource(fetchFields, null);
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(pIndex).addType(pDocType).build();
			lJestResult = jestClient.execute(search);
		}catch(Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_051 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_051_Desc);
			LOGGER.error(PaymentIngestionConstant.FAILEDENRICHMENT, pMatchingProdType);
		}
		return lJestResult;
	}
	
	
	public String getFetchQryStatus(Map<String, String> paramMap) {
		String fetchQryStatus = null;
		//String keyArray[] = new String[] { "status", "serviceKey", "docStatus", "outcomeCategory", "bccaction","faeIndicator","desc" };
		String keyArray[] = new String[] { 	PaymentIngestionConstant.STATUS, PaymentIngestionConstant.SERVICE_KEY, PaymentIngestionConstant.DOC_STATUS, 
											PaymentIngestionConstant.OUTCOMECATEGORY, PaymentIngestionConstant.BCC_ACTION,PaymentIngestionConstant.FAE_INDICATOR,
											PaymentIngestionConstant.DESC };
		StringBuilder keyBuilder = new StringBuilder();
		List<String> keyList = Arrays.asList(keyArray);
		LOGGER.info("Start getFetchQryStatus()::::::{}::::::{***}", paramMap+" keyList: "+keyList);
		for (String key : keyList) {
			if(paramMap.containsKey(key) && paramMap.get(key)!= null) {
				keyBuilder.append(paramMap.get(key));
			}else {
				keyBuilder.append("NULL");
			}
		}
		
		BuildStatusConfiguration buildStatusConfiguration = new BuildStatusConfiguration();
		LOGGER.info(" buildStatusConfiguration.getConfigMap(): "+buildStatusConfiguration.getConfigMap());		
		LOGGER.info("KEYBUILDER:: ::"+keyBuilder.toString()+" buildStatusConfiguration.getConfigMap(): "+buildStatusConfiguration.getConfigMap());
		fetchQryStatus = buildStatusConfiguration.getConfigMap().get(keyBuilder.toString());
		if(fetchQryStatus != null) {
			LOGGER.info("Cumulative status from ES:::::{}",fetchQryStatus);
		}else {
			//fetchQryStatus = "{DOC_STATUS_STR}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR}";
			fetchQryStatus = PaymentIngestionConstant.DEFAULT_STR;
		}
		return fetchQryStatus;
	}
	
/*	public String getCumulativeStatus(Map<String, String> paramMap) {
		JestResult lJestResult = null;
		String pIndex = paymentElasticProperties.getPymtIndex();
		final String[] fetchFields = { "cumulative_status" };
		String pDocType ="cumulative_status_master";
		String cumulativeStatus = null;
		
		//String keyArray[] = new String[] { "status", "serviceKey", "docStatus", "outcomeCategory", "bccaction","faeIndicator","desc" };
		String keyArray[] = new String[] { 	PaymentIngestionConstant.STATUS, PaymentIngestionConstant.SERVICE_KEY, PaymentIngestionConstant.DOC_STATUS, 
											PaymentIngestionConstant.OUTCOMECATEGORY, PaymentIngestionConstant.BCC_ACTION,PaymentIngestionConstant.FAE_INDICATOR,
											PaymentIngestionConstant.DESC };
		try {
			List<String> keyList = Arrays.asList(keyArray);
			LOGGER.info("Start getCumulativeStatus()::::::{}::::::{}", paramMap+" keyList: "+keyList);
			final BoolQueryBuilder query = QueryBuilders.boolQuery();
			final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			
			for (String key : keyList) {
				if(paramMap.containsKey(key) && paramMap.get(key)!= null) {
					query.must(QueryBuilders.termQuery(key, paramMap.get(key)));
				}else {
					query.must(QueryBuilders.termQuery(key, "NULL"));
				}
			}
			LOGGER.info("query for cumulative status:"+query);
			searchSourceBuilder.query(query).fetchSource(fetchFields, null);
			final Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(pIndex).addType(pDocType).build();
			lJestResult = jestClient.execute(search);
			
			if (lJestResult != null) {
				LOGGER.debug("lJestResult of getCumulativeStatus():::::::::{} ",lJestResult.toString());
				final JsonObject resultObj = lJestResult.getJsonObject();
				final JsonArray hitsArray = Optional.ofNullable(resultObj.get(PaymentIngestionConstant.HITS)).map(jsonObj -> jsonObj.getAsJsonObject()).map(json -> json.get(PaymentIngestionConstant.HITS)).map(jsonArray -> jsonArray.getAsJsonArray()).orElse(null);
				LOGGER.debug("hitsArray:::::::::{} ",hitsArray);
				if (hitsArray != null && hitsArray.size() > 0) {
					Iterator<JsonElement> iterator = hitsArray.iterator();
					while (iterator.hasNext()) {
						JsonElement objt = iterator.next();
						cumulativeStatus = objt.getAsJsonObject().get(PaymentIngestionConstant.SOURCE)
								.getAsJsonObject().get("cumulative_status").getAsString();
						LOGGER.info("Cumulative:::::{}",cumulativeStatus);
					}
				}
			}
		}catch(Exception ex) {
			LOGGER.error("Exception Occured"+String.valueOf(ex.getMessage()));
		}
		
		if(cumulativeStatus != null) {
			LOGGER.info("Cumulative status from ES:::::{}",cumulativeStatus);
		}else {
			cumulativeStatus = "DOC_STATUS_STR+CBX_UI_STATUS+FAE_IND+BCC_ACTION_STR";
		}
		return cumulativeStatus;
	}*/
	
	
	/** Gets the Payment Instruction Group ID.
	 * 
	 * @param channelSeqId
	 * @param index
	 * @param typeName
	 * @return
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public String getPymntInstnGrpID( final String channelSeqId, final String index, final String typeName) throws IOException {
		LOGGER.info("Searching for groupId with channelSqId {} in index: {} of type: {}.............", channelSeqId, index, typeName);
		//final QueryBuilder query = QueryBuilders.matchQuery(PaymentIngestionConstant.CHANNEL_SQ_ID, channelSeqId);
		final QueryBuilder query = QueryBuilders.termQuery(PaymentIngestionConstant.CHANNEL_SQ_ID, channelSeqId);
		LOGGER.info("Query to get Group Id from ES:::{}",query);
		// Gets total hits.
		JsonNode hitsNode = jestSearchESBuilder(query, index, typeName);
		LOGGER.info("O/p from ES for Group Id::{}",hitsNode);
		//If not found, try again after 1000 ms
		
		for (int i = 0; i < 20; i++) {
			if (!(hitsNode.has(0))) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_052 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_PAYING_052_Desc);
					e.printStackTrace();
				}
				LOGGER.error("###Trying again to search in ES### for channelSeqId {} and count is {}",channelSeqId,i);
				hitsNode = jestSearchESBuilder(query, index, typeName);
			} else {
				LOGGER.info("###Found data in ES...... for channelSeqId {} and count is {}",channelSeqId,i);
				break;
			}
		}
		final String pymtInstnGroupId = Optional.ofNullable(hitsNode).map(obj -> obj.get(0))
				.map(obj -> obj.get(PaymentIngestionConstant.SOURCE))
				.map(obj -> obj.get(PaymentIngestionConstant.REQ_CHANNEL_SEQ_ID)).map(JsonNode::asText).orElse(null);
		LOGGER.info("Returning with {} paymentInstructionGroupId.. ", pymtInstnGroupId);
		return pymtInstnGroupId;
	}
	
	/**
	 * To check if a user is auto approve user or not
	 * reference from /CIBC-ENTITLEMENT/entitlement/autoApprove
	 * Temporary call to be removed when passed from auto approve workflow
	 * @param userName
	 * @param serviceKey
	 * @return
	 */
	public boolean isUserAutoApprove(String userName, String serviceKey) {
		String index = "quest.entl", type = "entitlement";
		LOGGER.info("[isUserAutoApprove] User Name : {} ServiceKey {}",userName,serviceKey);
		try 
		{
			final SearchSourceBuilder searchSrcBuilder = new SearchSourceBuilder();
			final QueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("userId", userName))
																.must(QueryBuilders.matchQuery("serviceKey", serviceKey))
																.must(QueryBuilders.matchQuery("autoApprove", "Y"));
			searchSrcBuilder.query(query);
			LOGGER.debug("[isUserAutoApprove] searchSrcBuilder : {} ",searchSrcBuilder.toString());
			final Search search = new Search.Builder(searchSrcBuilder.toString()).addIndex(index).addType(type).build();
			final JestResult result = jestClient.execute(search);
			if (result.isSucceeded()) {
				LOGGER.debug("[isUserAutoApprove] result succeeded");
				int countOfRecords =  result.getJsonObject().get("hits").getAsJsonObject().get("total").getAsInt();
				LOGGER.info("[isUserAutoApprove] countOfRecords  : "+countOfRecords );
				if(countOfRecords > 0) {
					return true;
				}
			}
			return false;
		}
		catch(Exception e) {
			if (e.getClass().isInstance(ProtocolException.class)) {
				LOGGER.error("[isUserAutoApprove] ERR_ENTL_ES_002 :: Unable to establish a connection with elasticsearch due to incorrect configuration:{}",
						e);
			} else if (e.getClass().isInstance(SocketException.class)) {
				LOGGER.error("[isUserAutoApprove] ERR_ENTL_ES_001 :: Unable to establish a connection with elasticsearch :{}",
						e);
			} else if (e.getClass().isInstance(IOException.class)) {
				LOGGER.error("[isUserAutoApprove] ERR_ENTL_ES_003 :: Problem while connecting elasticsearch :{}",e);
			} else {
				LOGGER.error("[isUserAutoApprove] Exception Ocurred:{}", e);
			}
		}
		return false;
	}
}
