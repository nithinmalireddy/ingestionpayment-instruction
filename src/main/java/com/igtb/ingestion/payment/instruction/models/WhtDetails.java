
package com.igtb.ingestion.payment.instruction.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class WhtDetails.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class WhtDetails { // NOPMD TooManyFields

    /** The serial number. */
    @JsonProperty("serialNumber")
    public String serialNumber;
    
    /** The book number. */
    @JsonProperty("bookNumber")
    public String bookNumber;
    
    /** The sequence number. */
    @JsonProperty("sequenceNumber")
    public String sequenceNumber;
    
    /** The form type. */
    @JsonProperty("formType")
    public String formType;
    
    /** The deduction type. */
    @JsonProperty("deductionType")
    public String deductionType;
    
    /** The deduction type description. */
    @JsonProperty("deductionTypeDescription")
    public String deductionTypeDescription;
    
    /** The tax withholder name. */
    @JsonProperty("taxWithholderName")
    public String taxWithholderName;
    
    /** The tax withholder branch code. */
    @JsonProperty("taxWithholderBranchCode")
    public String taxWithholderBranchCode;
    
    /** The tax withholder tax ID. */
    @JsonProperty("taxWithholderTaxID")
    public String taxWithholderTaxID;
    
    /** The tax withholder address. */
    @JsonProperty("taxWithholderAddress")
    public String taxWithholderAddress;
    
    /** The alternative payer name. */
    @JsonProperty("alternativePayerName")
    public String alternativePayerName;
    
    /** The alternative payer branch code. */
    @JsonProperty("alternativePayerBranchCode")
    public String alternativePayerBranchCode; //NOPMD longVariable
    
    /** The alternative payer tax ID. */
    @JsonProperty("alternativePayerTaxID")
    public String alternativePayerTaxID;
    
    /** The alternative payer address. */
    @JsonProperty("alternativePayerAddress")
    public String alternativePayerAddress;
    
    /** The taxable party name. */
    @JsonProperty("taxablePartyName")
    public String taxablePartyName;
    
    /** The taxable party tax ID. */
    @JsonProperty("taxablePartyTaxID")
    public String taxablePartyTaxID;
    
    /** The taxable party address. */
    @JsonProperty("taxablePartyAddress")
    public String taxablePartyAddress;
    
    /** The delivery method. */
    @JsonProperty("deliveryMethod")
    public String deliveryMethod;
    
    /** The delivery location. */
    @JsonProperty("deliveryLocation")
    public String deliveryLocation;
    
    /** The tax details. */
    @JsonProperty("taxDetails")
    public List<TaxDetail> taxDetails;

}
