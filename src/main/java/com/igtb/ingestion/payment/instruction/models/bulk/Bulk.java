package com.igtb.ingestion.payment.instruction.models.bulk;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.igtb.ingestion.payment.instruction.models.TxnData;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class Bulk.
 */
@Setter
@Getter
@ToString
public class Bulk {

	/** The bp id. */
	@NotEmpty
	private String bpId;
	
	/** The bulk action. */
	@NotEmpty
	private String bulkAction;
	
	/** The bulk id. */
	@NotEmpty
	private String bulkId;
	
	/** The bulk metrics. */
	private BulkMetrics bulkMetrics;
	
	/** The file id. */
	@NotEmpty
	private String fileId;
	
	/** The partial processing. */
	@NotNull
	private Boolean partialProcessing;
	
	/** The service key. */
	@NotEmpty
	private String serviceKey;
	
	/** The txn data. */
	private List<TxnData> txnData = null;
	
	/** The txn slip. */
	private List<TxnSlip> txnSlip = null;
}
