package com.igtb.ingestion.payment.instruction.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class WkflwContext.
 */
@Getter
@Setter
@ToString
public class WkflwContext {
	
	/** The action. */
	private String action;
	
	/** The function. */
	private String function;
	
	/** The service key. */
	private String serviceKey;
	
	/** The user id. */
	private String userId;
	
	/** The org id. */
	private String orgId;
	
	/** The channel seq id. */
	private String channelSeqId;
	
	/** The domain id. */
	private String domainId;
	
	/** The request type. */
	private String requestType;
	
	/** The domain seq id. */
	private String domainSeqId;
	
	/** The initiator user id. */
	private String initiatorUserId;
	
	/** The initiator auth 0 user id. */
	private String initiatorAuth0UserId;
	
	/** The related chnl requests. */
	private String relatedChnlRequests;
	
	/** The other rltd ch seq id. */
	private String otherRltdChSeqId;
	
	/** The sub action. */
	private String subAction;
		
}
