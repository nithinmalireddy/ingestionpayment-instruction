package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.igtb.ingestion.payment.instruction.models.PostalAddress;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntermediaryAgentES {

	@JsonProperty("postalAddress")
	private PostalAddress postalAddress;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("bic")
	private String bic = null;

}
