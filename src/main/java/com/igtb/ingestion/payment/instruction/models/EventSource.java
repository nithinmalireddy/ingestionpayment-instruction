package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Microservice identifier example action-payment-instruction.
 */
@Getter
@Setter
public class EventSource {
  
  /** The source identity. */
  @JsonProperty("sourceIdentity")
  private String sourceIdentity;

  /** The region. */
  @JsonProperty("region")
  private String region;

  /** The country. */
  @JsonProperty("country")
  private String country;

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final EventSource eventSource = (EventSource) obj;
    return Objects.equals(this.sourceIdentity, eventSource.sourceIdentity) &&
        Objects.equals(this.region, eventSource.region) &&
        Objects.equals(this.country, eventSource.country);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(sourceIdentity, region, country);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(80);
    sb.append("class EventSource { sourceIdentity: ").append(toIndentedString(sourceIdentity))
      .append("    region: ").append(toIndentedString(region))
      .append("    country: ").append(toIndentedString(country))
      .append('}');
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param obj the obj
   * @return the string
   */
  private String toIndentedString(final Object obj) {
    if (obj == null) {
      return "null";
    }
    return obj.toString().replace("\n", "\n    ");
  }

}

