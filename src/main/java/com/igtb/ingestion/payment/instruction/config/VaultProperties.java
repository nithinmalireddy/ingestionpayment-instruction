package com.igtb.ingestion.payment.instruction.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import java.util.Map;

/**
 * The Class VaultProperties.
 */
@Configuration
@ConfigurationProperties
public class VaultProperties {

    /** The igtb certificates. */
    private Map<String, String> igtbCertificates;
    
    /** The igtb private keys. */
    private Map<String, String> igtbPrivateKeys;

    /**
     * Gets the igtb certificates.
     *
     * @return the igtb certificates
     */
    public Map<String, String> getIgtbCertificates() {
        return igtbCertificates;
    }

    /**
     * Sets the igtb certificates.
     *
     * @param igtbCertificates the igtb certificates
     */
    public void setIgtbCertificates(final Map<String, String> igtbCertificates) {
        this.igtbCertificates = igtbCertificates;
    }

    /**
     * Gets the igtb private keys.
     *
     * @return the igtb private keys
     */
    public Map<String, String> getIgtbPrivateKeys() {
        return igtbPrivateKeys;
    }

    /**
     * Sets the igtb private keys.
     *
     * @param igtbPrivateKeys the igtb private keys
     */
    public void setIgtbPrivateKeys(final Map<String, String> igtbPrivateKeys) {
        this.igtbPrivateKeys = igtbPrivateKeys;
    }
}
