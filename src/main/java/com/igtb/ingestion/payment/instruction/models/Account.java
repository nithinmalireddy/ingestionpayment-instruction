
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CreditorAccount.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the account type.
 *
 * @return the account type
 */
@Getter

/**
 * Sets the account type.
 *
 * @param accountType the new account type
 */
@Setter
public class Account {

    /** The scheme name. */
    @JsonProperty("schemeName")
    public String schemeName;
    
    /** The identification. */
    @JsonProperty("identification")
    public String identification;
    
    /** The name. */
    @JsonProperty("name")
    public String name;
    
    /** The account type. */
    @JsonProperty("accNumType")
    public String accountType;
    
    @JsonProperty("accountCreationDate")
    public String accountCreationDate;

    /** The account currency*/
    @JsonProperty("currency")
    public String currency;
    
    /** The account accountNo*/
    @JsonProperty("accountNo")
    public String accountNo;
    
    /** The account legalName*/
    @JsonProperty("legalName")
    public String legalName ;
    
    /** The account alias*/
    @JsonProperty("alias")
    public String alias;
    
    /** The account aliasHandleType*/
    @JsonProperty("aliasHandleType")
    public String aliasHandleType;
    
    
    @JsonProperty("contactName")
    private String contactName;
	
    @JsonProperty("isAddedConactBookFlag")
	private String isAddedConactBookFlag;
        
    @JsonProperty("schemeNameCode")
   	private String schemeNameCode;
    
}
