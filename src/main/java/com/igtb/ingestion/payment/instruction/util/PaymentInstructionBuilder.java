package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.AgentES;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.BankProxyES;
import com.igtb.ingestion.payment.instruction.es.models.CreditorES;
import com.igtb.ingestion.payment.instruction.es.models.DebtorES;
import com.igtb.ingestion.payment.instruction.es.models.FxRateES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentRailES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentReasonES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentTypeES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.RemainingInfoES;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.metadata.models.RecordData;
import com.igtb.ingestion.payment.instruction.metadata.models.RecordRateAmount;
import com.igtb.ingestion.payment.instruction.metadata.models.Records;
import com.igtb.ingestion.payment.instruction.models.Account;
import com.igtb.ingestion.payment.instruction.models.Agent;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Creditor;
import com.igtb.ingestion.payment.instruction.models.Debtor;
import com.igtb.ingestion.payment.instruction.models.EquivalentAmount;
import com.igtb.ingestion.payment.instruction.models.FxDetail;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;

/**
 * PaymentInstructionBuilder
 * 
 */
@Component
public class PaymentInstructionBuilder {

	/** Logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentInstructionBuilder.class);

	/** AccountInfo Utility. */
	@Autowired
	private AccountsInfoUtil accInfoUtil;

	/** The payment ingestion query util. */
	@Autowired
	private PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The payment elastic properties. */
	@Autowired
	private PaymentElasticProperties paymentElasticProperties;

	/**
	 * Gets the release Date(send-on date).
	 *
	 * @param date
	 *            the date
	 * @return the release date
	 * @throws DataNotValidException
	 *             the data not valid exception
	 */
	public String getReleaseDate(final String date) throws DataNotValidException {
		if (date != null) {
			// first parse the date with the formatter and then return
			return Optional.ofNullable(CommonUtil.getParseableDate(date, PaymentIngestionConstant.ACTION_API_FORMATTER0)).map(parsedDate -> PaymentIngestionConstant.ACTION_API_FORMATTER0.format(parsedDate)).orElse(null);
		}
		return null;
	}
/**
	 * Gets the processing Date(send-on date).
	 *
	 * @param date
	 *            the date
	 * @return the processing date
	 * @throws DataNotValidException
	 *             the data not valid exception
	 */
	public String getProcessingDate(final String date) throws DataNotValidException {
		if (date != null) {
	// first parse the date with the formatter and then return
			return Optional.ofNullable(CommonUtil.getParseableDate(date, PaymentIngestionConstant.ACTION_API_FORMATTER0)).map(parsedDate -> PaymentIngestionConstant.ACTION_API_FORMATTER0.format(parsedDate)).orElse(null);
		}
		return null;
	}

	/**
	 * Get the requestedExecutionDate.(receiveOn date)
	 *
	 * @param date
	 *            the date
	 * @param requestType
	 *            the request type
	 * @param transitionState
	 *            the transition state
	 * @return the requested execution date
	 * @throws DataNotValidException
	 *             exception
	 */
	public String getRequestedExecutionDate(final String date, final String requestType, final String transitionState)
			throws DataNotValidException {

		if (date == null && PaymentIngestionConstant.ADD.equalsIgnoreCase(requestType) && !PaymentIngestionConstant.DRAFT_STATE.equalsIgnoreCase(transitionState)) {
			throw new DataNotValidException(">>>>>>>>> Requested Execution date is not received. <<<<<<<<<<<");
		} else if (date != null) {
			return Optional.ofNullable(CommonUtil.getParseableDate(date, PaymentIngestionConstant.ACTION_API_FORMATTER0,PaymentIngestionConstant.ACTION_API_FORMATTER1)).map(parsedDate -> PaymentIngestionConstant.ACTION_API_FORMATTER0.format(parsedDate)).orElse(null);
		}

		return null;
	}

	/**
	 * Get UserDateTime.
	 *
	 * @param date
	 *            the date
	 * @param initiatedTs
	 *            the initiated ts
	 * @return the user date time
	 * @throws DataNotValidException
	 *             exception
	 */
	public String getUserDateTime(final String date, final String initiatedTs) throws DataNotValidException {
		final String dateStr;
		if (date != null) {
			dateStr = date;
		} else {
			dateStr = PaymentIngestionConstant.ACTION_API_FORMATTER0
					.format(CommonUtil.getParseableDate(initiatedTs, PaymentIngestionConstant.TIMESTAMP_FORMATTER1));
		}
		return new DateTime(dateStr, DateTimeZone.UTC).toString();
	}

	/**
	 * Get Option.
	 *
	 * @param txnInitiationType
	 *            the txn initiation type
	 * @param requestType
	 *            the request type
	 * @return the option
	 */
	public String getOption(final String txnInitiationType, final String requestType, final String serviceKey) {
		// "txnInitiationType" cant be "HOLD/UNHOLD" in case of hold/unhold
		if (PaymentIngestionConstant.ADD.equalsIgnoreCase(requestType)) {
			return txnInitiationType;
		} else if (PaymentIngestionConstant.DELETE.equalsIgnoreCase(requestType)
				&& serviceKey.contains(PaymentIngestionConstant.FULFILL_RTP)) {
			return txnInitiationType;
		}
		return null;
	}

	/**
	 * Gets the creditor ES.
	 *
	 * @param creditor
	 *            the creditor
	 * @param option
	 *            the option
	 * @param piType
	 *            the pi type
	 * @return the creditor ES
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public DebtorES getDebtorES(final Debtor debtor, final String option, final String piType)
			throws DataNotFoundException {
		DebtorES debtorES = null;
		final String debtorId = Optional.ofNullable(debtor).map(Debtor::getIdentification).orElse(null);
		String debtorIdSchemeName = Optional.ofNullable(debtor).map(Debtor::getSchemeName).orElse(null);
		debtorES = accInfoUtil.getDebtorDetails(debtor, debtorId, option, debtorIdSchemeName, piType);
		return debtorES;
	}

	/**
	 * Gets the creditor ES.
	 *
	 * @param creditor
	 *            the creditor
	 * @param option
	 *            the option
	 * @param piType
	 *            the pi type
	 * @return the creditor ES
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public CreditorES getCreditorES(final Creditor creditor, final String option, final String piType)
			throws DataNotFoundException {
		CreditorES creditorES = null;
		final String creditorId = Optional.ofNullable(creditor).map(Creditor::getIdentification).orElse(null);
		String creditorSchemeName = Optional.ofNullable(creditor).map(Creditor::getSchemeName).orElse(null);
		creditorES = accInfoUtil.getCreditorDetails(creditor, creditorId, option, creditorSchemeName, piType);
		return creditorES;
	}

	/**
	 * Gets the creditor account.
	 *
	 * @param account
	 *            the account
	 * @param creditorAccID
	 *            the creditor acc ID
	 * @param creditorAccSchemeName
	 *            the creditor acc scheme name
	 * @param option
	 *            the option
	 * @param piType
	 *            the pi type
	 * @param bankProxyES
	 *            the bank proxy ES
	 * @return the creditor account
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public AccountES getCreditorAccount(final Account account, final String creditorAccID,
			final String creditorAccSchemeName, final String option, final String piType, BankProxyES bankProxyES,
			final String serviceKey) throws IOException, DataNotFoundException {
		AccountES creditorAccount = null;
		if (StringUtils.isNotBlank(creditorAccID) || creditorAccSchemeName.equals(PaymentIngestionConstant.ADHOC)) {
			creditorAccount = accInfoUtil.getAccountDetails(account, PaymentIngestionConstant.CREDITOR_ACCOUNT,
					creditorAccID, option, piType, creditorAccSchemeName, bankProxyES, serviceKey);
		}
		return creditorAccount;
	}

	/**
	 * Gets the creditor agent.
	 *
	 * @param agent
	 *            the agent
	 * @param agentType
	 *            the agent type
	 * @param bankProxyES
	 *            the bank proxy ES
	 * @return the creditor agent
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AgentES getCreditorAgent(final Agent agent, final String agentType, BankProxyES bankProxyES)
			throws IOException {
		return accInfoUtil.getAgentDetails(agent, agentType, bankProxyES);
	}

	/**
	 * Gets the debtor.
	 *
	 * @param payload
	 *            the payload
	 * @return the debtor
	 */
	public String getDebtor(final JsonNode payload) {
		return Optional
				.ofNullable(
						CommonUtil.getValueFromJsonNode(payload, "debtor", PaymentIngestionConstant.INDENTIFICATION))
				.map(Object::toString).orElse(null);
	}

	/**
	 * Gets the debtor account.
	 *
	 * @param account
	 *            the account
	 * @param debtorAccID
	 *            the debtor acc ID
	 * @param debtorAccSchemeName
	 *            the debtor acc scheme name
	 * @param option
	 *            the option
	 * @return the debtor account
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public AccountES getDebtorAccount(final Account account, final String debtorAccID, final String debtorAccSchemeName,
			final String option, final String serviceKey) throws IOException, DataNotFoundException {
		LOGGER.info("serviceKey>>>>>>>>" +serviceKey);
		LOGGER.info("debtorAccID>>>>>>>>" +debtorAccID);
		
		AccountES debtorAccount = null;
		//CIBCCBX-6528 - Adhoc- commented below if
//		if (StringUtils.isNotBlank(debtorAccID)) {
			debtorAccount = accInfoUtil.getAccountDetails(account, PaymentIngestionConstant.DEBTOR_ACCOUNT, debtorAccID,
					option, "", debtorAccSchemeName, null, serviceKey);
//		}
		return debtorAccount;
	}

	/**
	 * Gets the debtor agent.
	 *
	 * @param agent
	 *            the agent
	 * @param agentType
	 *            the agent type
	 * @return the debtor agent
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AgentES getDebtorAgent(final Agent agent, final String agentType) throws IOException {
		return accInfoUtil.getAgentDetails(agent, agentType, null);
	}

	/**
	 * Get Charge Amount.
	 *
	 * @param chargeAmounVal
	 *            the charge amoun val
	 * @param chargeAmtCurrencyCode
	 *            the charge amt currency code
	 * @return the charge amount
	 */
	public AmountES getChargeAmount(final Double chargeAmounVal, final String chargeAmtCurrencyCode) {
		AmountES chargeAmount = null;
		if (chargeAmounVal != null && chargeAmtCurrencyCode != null) {
			chargeAmount = new AmountES();
			chargeAmount.setValue(chargeAmounVal);
			chargeAmount.setCurrencyCode(chargeAmtCurrencyCode);
		}

		return chargeAmount;
	}

	/**
	 * Get Instructed Amount.
	 *
	 * @param instrAmtValue
	 *            the instr amt value
	 * @param instrAmtCurrenyCode
	 *            the instr amt curreny code
	 * @return the instructed amount
	 */
	public AmountES getInstructedAmount(final Double instrAmtValue, final String instrAmtCurrenyCode) {
		AmountES instructedAmount = null;
		if (instrAmtValue != null && instrAmtCurrenyCode != null) {
			instructedAmount = new AmountES();
			instructedAmount.setValue(instrAmtValue);
			instructedAmount.setCurrencyCode(instrAmtCurrenyCode);
		}
		return instructedAmount;
	}

	/**
	 * Get Equivalent Amount.
	 *
	 * @param debitAmountFlag
	 *            the debit amount flag
	 * @param equivalentAmountFmPayload
	 *            the equivalent amount fm payload
	 * @param instructedAmount
	 *            the instructed amount
	 * @param debitorAccount
	 *            the debitor account
	 * @param fxDetailsList
	 *            the fx details list
	 * @return the equivalent amount
	 */
	public AmountES getEquivalentAmount(final Boolean debitAmountFlag, final EquivalentAmount equivalentAmountFmPayload,
			final AmountES instructedAmount, final AccountES debitorAccount, final List<FxRateES> fxDetailsList) {
		Double equiAmtVal = null;

		final String equiAmtCurr = Optional.ofNullable(equivalentAmountFmPayload).map(EquivalentAmount::getCurrency).orElse(null);
		// if debit amount flag is present = TRUE
		if (debitAmountFlag != null && debitAmountFlag) {

			if (equivalentAmountFmPayload != null) {
				equiAmtVal = equivalentAmountFmPayload.getAmount();
			}

		} else {// otherwise debitamountflag = FALSE
			final Double instrAmount = Optional.ofNullable(instructedAmount).map(AmountES::getValue).orElse(null);

			// if same currency txns- debtorAccount -> accCcy -> code = instructedamount >
			// currencyCode
			if (Optional.ofNullable(debitorAccount).map(AccountES::getAcctCcy).map(acc -> acc.getCode()).orElse("").equalsIgnoreCase(Optional.ofNullable(instructedAmount).map(AmountES::getCurrencyCode).orElse(""))) {
				equiAmtVal = instrAmount;
			} else {// else
				final List<Double> fxRatesStrList = new ArrayList<>();

				Optional.ofNullable(fxDetailsList).ifPresent(fxDetails -> fxDetails.forEach(
						fxDtlsDAO -> Optional.ofNullable(fxDtlsDAO).map(fxDtls -> fxDtls.getFxRate()).orElse(null)));

				if (fxRatesStrList.isEmpty()) {// if no fxrates found
					equiAmtVal = null;
				} else {
					// if single fxrates is present, then calculate the equivlent amount
					if (instrAmount != null && fxRatesStrList.size() == 1) {
						equiAmtVal = instrAmount * fxRatesStrList.get(0);
					} else {
						// for multiple fxrates and no fxrates passed, equivalent amount should be null
						equiAmtVal = null;
					}
				}
			}
		}

		AmountES equivalentAmount = null;
		if (equiAmtVal != null && equiAmtCurr != null) {
			equivalentAmount = new AmountES();
			equivalentAmount.setValue(equiAmtVal);
			equivalentAmount.setCurrencyCode(equiAmtCurr);
		}
		return equivalentAmount;

	}

	/**
	 * Get PaymentRail.
	 *
	 * @param payRailCd
	 *            the pay rail cd
	 * @return the payment rail
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public PaymentRailES getPaymentRail(final String payRailCd) throws IOException {
		PaymentRailES paymentRail = null;
		if (payRailCd != null) {
			paymentRail = new PaymentRailES();
			final String paymentRailDesc = paymentIngestionQueryUtil.getDescription(payRailCd,
					paymentElasticProperties.getPaymentRailsType());
			paymentRail.setCode(payRailCd);
			paymentRail.setDescription(paymentRailDesc);
		}
		return paymentRail;
	}

	/**
	 * Get PaymentType.
	 *
	 * @param payTypeCd
	 *            the pay type cd
	 * @return the payment type
	 * @throws IOException
	 *             exception
	 */
	public PaymentTypeES getPaymentType(final String payTypeCd) throws IOException {
		PaymentTypeES paymentType = null;
		if (StringUtils.isNotEmpty(payTypeCd)) {
			paymentType = new PaymentTypeES();
			final String paymentTypeDesc = paymentIngestionQueryUtil.getDescription(payTypeCd,
					paymentElasticProperties.getPaymentTypes());
			paymentType.setCode(payTypeCd);
			paymentType.setDescription(paymentTypeDesc);
		}
		return paymentType;
	}

	/**
	 * Get PaymentReason.
	 *
	 * @param payReasonCd
	 *            the pay reason cd
	 * @return the payment reason
	 * @throws IOException
	 *             exception
	 */
	public PaymentReasonES getPaymentReason(final String payReasonCd) throws IOException {
		PaymentReasonES paymentReason = null;
		if (payReasonCd != null) {
			paymentReason = new PaymentReasonES();
			final String paymentReasonDesc = paymentIngestionQueryUtil.getDescription(
					payReasonCd.toUpperCase(Locale.ENGLISH), paymentElasticProperties.getPaymentReasonsType());
			paymentReason.setCode(payReasonCd);
			paymentReason.setDescription(paymentReasonDesc);
		}
		return paymentReason;
	}

	/**
	 * Get debit time.
	 *
	 * @param debitTime
	 *            the debit time
	 * @param releaseDate
	 *            the release date
	 * @return the debit time
	 */
	public String getDebitTime(final String debitTime, final String releaseDate) {

		if (StringUtils.isNotEmpty(releaseDate) && StringUtils.isNotEmpty(debitTime)) {// send-on-date to be present if debittime expected
			final Integer numOfhours = Optional.ofNullable(debitTime).filter(obj -> obj.length() >= 2).map(obj -> obj.substring(0, 2)).map(Integer::parseInt).orElse(0);
			final Integer numOfminutes = Optional.ofNullable(debitTime).filter(obj -> obj.length() > 2 && obj.length() <= 4).map(obj -> obj.substring(2, obj.length())).map(Integer::parseInt).orElse(0);
			DateTime debitDateTime;
			try {
				debitDateTime = new DateTime(releaseDate, DateTimeZone.UTC); // "2010-05-23" ->"2010-05-23T00:00:00.000Z"
			} catch (final Exception e) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_076 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_076_Desc);
				LOGGER.error("Error in generating dateTime as: {}", e);
				debitDateTime = null;
			}

			debitDateTime = debitDateTime.plusHours(numOfhours);
			debitDateTime = debitDateTime.plusMinutes(numOfminutes);
			return debitDateTime.toString();

		}
		return debitTime;
	}

	/**
	 * Gets the fx details.
	 *
	 * @param fxDetails
	 *            the fx details
	 * @return the fx details
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<FxRateES> getFxDetails(final List<FxDetail> fxDetails) throws IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final String jsonString = mapper.writeValueAsString(fxDetails);
		return mapper.readValue(jsonString, new TypeReference<List<FxRateES>>() {
		});
	}

	/**
	 * Gets the fx details.
	 *
	 * @param exchangeRate
	 *            the exchange rate
	 * @param exchangeRateType
	 *            the exchange rate type
	 * @param exchangeRateAmount
	 *            the exchange rate amount
	 * @return the fx details
	 */
	public List<FxRateES> getFxDetails(final RecordRateAmount exchangeRate, final RecordData exchangeRateType,
			final RecordRateAmount exchangeRateAmount) {
		final FxRateES fxRateES = new FxRateES();
		fxRateES.setFxRate(exchangeRate.getValue());
		fxRateES.setFxCurrency(exchangeRate.getCurrencyCode());
		fxRateES.setFxRateType(exchangeRateType.getValue());
		fxRateES.setFxAmount(exchangeRateAmount.getValue());
		return Arrays.asList(fxRateES);
	}

	/**
	 * Gets the enriched remaining info with all the scheme names and account num
	 * types of creditor/debtor.
	 *
	 * @param creditorSchemeName
	 *            the creditor scheme name
	 * @param creditorAccSchemeName
	 *            the creditor acc scheme name
	 * @param creditorAgentSchemeName
	 *            the creditor agent scheme name
	 * @param debtorAgentSchemeName
	 *            the debtor agent scheme name
	 * @param creditorAccNumType
	 *            the creditor acc num type
	 * @return the other remaining info
	 */
	public RemainingInfoES getOtherRemainingInfo(final String creditorSchemeName, final String creditorAccSchemeName,
			final String creditorAgentSchemeName, final String debtorAgentSchemeName, final String creditorAccNumType) {
		RemainingInfoES remInfo = new RemainingInfoES();
		remInfo.setCreditorSchemeName(creditorSchemeName);
		remInfo.setCreditorAccSchemeName(creditorAccSchemeName);
		remInfo.setCreditorAgentSchemeName(creditorAgentSchemeName);
		remInfo.setDebtorAgentSchemeName(debtorAgentSchemeName);
		remInfo.setCreditorAccNumType(creditorAccNumType);
		return remInfo;
	}

	/**
	 * Gets the creditor agent file PI.
	 *
	 * @param record
	 *            the record
	 * @param bankProxyES
	 *            the bank proxy ES
	 * @return the creditor agent file PI
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AgentES getCreditorAgentFilePI(final Records record, final BankProxyES bankProxyES) throws IOException {
		final Agent agent = new Agent();
		agent.setAddress1(
				Optional.ofNullable(record.getBeneficiaryBankAddress1()).map(obj -> obj.getValue()).orElse(null));
		agent.setAddress2(
				Optional.ofNullable(record.getBeneficiaryBankAddress2()).map(obj -> obj.getValue()).orElse(null));
		agent.setAddress3(
				Optional.ofNullable(record.getBeneficiaryBankAddress3()).map(obj -> obj.getValue()).orElse(null));
		agent.setBankAddnlInfo1(Optional.ofNullable(record.getBankId()).map(obj -> obj.getValue()).orElse(null));
		agent.setBankCode(Optional.ofNullable(record.getBeneficiaryBankBranch()).map(obj -> obj.getId()).orElse(null));
		agent.setBankEntityId(Optional.ofNullable(record.getBankEntityId()).map(obj -> obj.getValue()).orElse(null));
		agent.setBankIdType(
				Optional.ofNullable(record.getBeneficiaryClearingIdType()).map(obj -> obj.getValue()).orElse(null));
		agent.setBankName(Optional.ofNullable(record.getBeneficiaryBankName()).map(obj -> obj.getValue()).orElse(null));
		agent.setBranchCode(
				Optional.ofNullable(record.getBeneficiaryBankBranch()).map(obj -> obj.getId()).orElse(null));
		agent.setBranchName(
				Optional.ofNullable(record.getBeneficiaryBankBranch()).map(obj -> obj.getValue()).orElse(null));
		agent.setCity(Optional.ofNullable(record.getBeneficiaryBankCity()).map(obj -> obj.getValue()).orElse(null));
		agent.setCountry(
				Optional.ofNullable(record.getBeneficiaryBankCountry()).map(obj -> obj.getValue()).orElse(null));
		agent.setIdentification(
				Optional.ofNullable(record.getBeneficiaryClearingId()).map(obj -> obj.getValue()).orElse(null));
		agent.setIsHostBank(
				Optional.ofNullable(record.getBeneficiaryIshostbank()).map(obj -> obj.getValue()).orElse(null));
		agent.setSchemeName(
				Optional.ofNullable(record.getBeneficiaryClearingIdType()).map(obj -> obj.getValue()).orElse(null));

		return getCreditorAgent(agent, "creditorAgent", bankProxyES);
	}

	/**
	 * Gets the debtor agent file PI.
	 *
	 * @param record
	 *            the record
	 * @return the debtor agent file PI
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public AgentES getDebtorAgentFilePI(final Records record) throws IOException {
		final Agent agent = new Agent();
		agent.setBankAddnlInfo1(Optional.ofNullable(record.getDrBankId()).map(obj -> obj.getValue()).orElse(null));
		agent.setBankCode(Optional.ofNullable(record.getDrBankCode()).map(obj -> obj.getId()).orElse(null));
		agent.setBankName(Optional.ofNullable(record.getDrBankName()).map(obj -> obj.getValue()).orElse(null));
		agent.setBranchCode(Optional.ofNullable(record.getDrBranchcode()).map(obj -> obj.getId()).orElse(null));
		agent.setBranchName(Optional.ofNullable(record.getDrBranchName()).map(obj -> obj.getValue()).orElse(null));
		agent.setIsHostBank("true");
		agent.setType("bank");

		return getDebtorAgent(agent, "debtorAgent");
	}

	/**
	 * Gets the creditor agent file PI.
	 *
	 * @param creditorAccID
	 *            the creditor acc ID
	 * @param paymentRail
	 *            the payment rail
	 * @return the creditor agent file PI
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public CreditorES getCreditorAgentFilePI(final String creditorAccID, final String paymentRail) throws IOException {
		return accInfoUtil.getCreditorFilePI(creditorAccID, paymentRail);

	}
	public void enrichCustomerInformation(final PaymentsInstructionsES paymentInstructionES,final Context context) throws IOException, DataNotFoundException {
		LOGGER.info("EntityAccountid For ChannelSeqId::::::{} and ServicesKey:::::::{}",context.getChannelSeqId(),context.getEventType().getServiceKey());
		JsonNode corporatesPayload = null;
		JsonNode activeRecord = null;
		String accountEntityId = null;
		String clearingSysRef = null;
		// Extracting payload Information from existing
		if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_MONEY) || context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_WIRE)) {
			accountEntityId =paymentInstructionES.getDebtorAccount().getEntity().getId();
			LOGGER.info("DebtorAccount EntityId::::::{} for ServicesKey:::::::{}",accountEntityId,context.getEventType().getServiceKey());
			corporatesPayload=paymentIngestionQueryUtil.fetchCustomerInfofromCorporates(accountEntityId);
		}
		else if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)){
			accountEntityId =paymentInstructionES.getCreditorAccount().getEntity().getId();
			LOGGER.info("CreditorAccount EntityId::::::{} for ServicesKey:::::::{}",accountEntityId,context.getEventType().getServiceKey());
			corporatesPayload=paymentIngestionQueryUtil.fetchCustomerInfofromCorporates(accountEntityId);
		}
		else if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)){
			clearingSysRef =paymentInstructionES.getClearingSystemReferenceES();
			LOGGER.info("EntityId::::::{} for ServicesKey:::::::{}",clearingSysRef,context.getEventType().getServiceKey());
			activeRecord=paymentIngestionQueryUtil.fetchCustomerInfofromActiveRecord(clearingSysRef);
		}
		if(corporatesPayload != null) {
			LOGGER.info("corporatesPayload ::::::{} ",corporatesPayload);
			paymentInstructionES.setCustIdES(corporatesPayload.get(PaymentIngestionConstant.KEY).asText());
			paymentInstructionES.setCustNameES(corporatesPayload.get(PaymentIngestionConstant.NAME).asText());
			paymentInstructionES.setPuIdES(corporatesPayload.get(PaymentIngestionConstant.PU_ID).asText());
		}
		else if(activeRecord != null)
		{
			LOGGER.info("activeRecordPayload ::::::{} ",activeRecord);
			paymentInstructionES.setCustIdES(activeRecord.get(PaymentIngestionConstant.CUST_ID).asText());
			paymentInstructionES.setCustNameES(activeRecord.get(PaymentIngestionConstant.CUST_NAME).asText());
			paymentInstructionES.setPuIdES(activeRecord.get(PaymentIngestionConstant.PU_ID).asText());
		}
		else {
			LOGGER.error("corporatesPayload is ::::::{} ",corporatesPayload);
			LOGGER.error("activeRecordPayload is ::::::{} ",activeRecord);
		}
	}
	
	/**
	 * Get Payment Instruction Group ID.
	 * 
	 * @param channelSeqId
	 * @return
	 * @throws IOException
	 */
	public String getPymntInstnGrpID(final String channelSeqId) throws IOException {
		LOGGER.info("Fetching Group Seq Id for Channel Sequence ID: {}",channelSeqId);
		String wistore_index = "quest.wistore";
		String typeName = "conductorTriggerMappings";
		final String pymntInstnGrpID = paymentIngestionQueryUtil.getPymntInstnGrpID(channelSeqId, wistore_index,
				typeName);
		if(null== pymntInstnGrpID) {
			LOGGER.error("Payment Instruction Group ID is NULL for Channel Seqence Id {}", channelSeqId);
		}
		return pymntInstnGrpID;
	}
	
	/**
	 * To check if a user is auto approve user or not
	 * @param userName
	 * @param serviceKey
	 * @return
	 */
	public boolean isUserAutoApprove(String userName, String serviceKey) {
		return paymentIngestionQueryUtil.isUserAutoApprove(userName,serviceKey);
	}
}
