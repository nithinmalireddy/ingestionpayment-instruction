
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Debtor.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Debtor {

    /** The name. */
    @JsonProperty("name")
    public String name;
    
    /** The postal address. */
    @JsonProperty("postalAddress")
    public PostalAddress postalAddress;
    
    /** The identification. */
    @JsonProperty("identification")
    public String identification;

    @JsonProperty("AnyBIC")
    public String anyBIC;
    
    /** The designation. */
    @JsonProperty("designation")
    private String designation;
    
    /** The country of residence. */
    @JsonProperty("countryOfResidence")
    public String countryOfResidence;
    
    /** The scheme name. */
    @JsonProperty("schemeName")
    public String schemeName;
    
    /** The phne nb. */
    @JsonProperty("phneNb")
    public String phneNb;
    
    /** The mob nb. */
    @JsonProperty("mobNb")
    public String mobNb;
    
    /** The email adr. */
    @JsonProperty("emailAdr")
    public String emailAdr;

}
