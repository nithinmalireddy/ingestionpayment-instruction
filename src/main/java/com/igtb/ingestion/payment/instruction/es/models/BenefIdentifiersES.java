package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the charge types.
 *
 * @return the charge types
 */
@Getter

/**
 * Sets the charge types.
 *
 * @param chargeTypes
 *            the new charge types
 */
@Setter
public class BenefIdentifiersES {

	/** The charge types. */
	@JsonProperty("nationalIdType")
	public String nationalIdType;

	/** The charge types. */
	@JsonProperty("idNumber")
	public String idNumber;

	/** The charge types. */
	@JsonProperty("nameInID")
	public String nameInID;

}
