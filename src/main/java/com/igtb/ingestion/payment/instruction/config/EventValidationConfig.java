package com.igtb.ingestion.payment.instruction.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class EventValidationConfig
 */
@Component
@EnableConfigurationProperties
@Setter
@Getter
@ConfigurationProperties(prefix = "event")
public class EventValidationConfig {

	/** The event validation. */
	private Map<String, HashMap<String, ArrayList<String>>> validation = new HashMap<String, HashMap<String, ArrayList<String>>>();
	
	/** The cash account supported product. */
	private List<String> cashAccountSupportedProduct; //NOPMD longVariable 
	
	/** The payroll product. */
	private List<String> payrollProduct;
}
