package com.igtb.ingestion.payment.instruction.models.bulk;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.igtb.ingestion.payment.instruction.models.TxnData;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class BulkActionApiPayload.
 */
@Setter
@Getter
@ToString
public class BulkActionApiPayload {

	/** The bp id. */
	private String bpId;
	
	/** The bulk id. */
	@NotEmpty
	private String bulkId;
	
	/** The bulk metrics. */
	private BulkMetrics bulkMetrics;
	
	/** The file id. */
	@NotEmpty
	private String fileId;
	
	/** The partial processing. */
	@NotNull
	private Boolean partialProcessing;
	
	/** The txn data. */
	private List<TxnData> txnData = null;
	
}
