package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * AccCyyES Class.
 * 
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
public class AcctCyyES {
	
	/** The account currency code. */
	private String code;
	
	/** The account currency name. */
	private String name;
	
}
