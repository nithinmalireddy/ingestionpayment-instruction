package com.igtb.ingestion.payment.instruction.es.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class RemainingInfoES.
 */
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
public class RemainingInfoES {
	
	/** The creditor agent scheme name. */
//	@NotNull
	private String creditorAgentSchemeName;
	
	/** The creditor acc scheme name. */
	private String creditorAccSchemeName;
	
	/** The creditor scheme name. */
	private String creditorSchemeName;
	
	/** The debtor agent scheme name. */
//	@NotNull
	private String debtorAgentSchemeName;
	
	/** The creditor acc num type. */
	private String creditorAccNumType;

}
