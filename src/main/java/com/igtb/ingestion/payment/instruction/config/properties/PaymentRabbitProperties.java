package com.igtb.ingestion.payment.instruction.config.properties;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * ElasticSearch Property file. During boot startup, binding takes place with
 * the respective external properties.
 * 
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "rabbitMq")

/**
 * Gets the receiver.
 *
 * @return the receiver
 */
@Getter
public class PaymentRabbitProperties {
	
	
	/** The name. */
	@Value("${Digital.RabbitMQ.name}")
	private String name;

	/** The host. */
	@Value("${Digital.RabbitMQ.Host}")
	private String host;

	/** The port. */
	@Value("${Digital.RabbitMQ.Port}")
	private String port;

	/** The uname. */
	@Value("${Digital.RabbitMQ.User}")
	private String user;

	/** The password. */
	@Value("${Digital.RabbitMQ.Password}")
	private String password;

	/** The vhost. */
	@Value("${Digital.RabbitMQ.VHost}")
	private String vhost;
	
	/** The exchange. */
	@Value("${Digital.RabbitMQ.exchange}")
	private String exchange;
	
	/** The exchange type. */
	@Value("${Digital.RabbitMQ.exchangeType}")
	private String exchangeType;
	
	/** The retry wait time. */
	@Value("${RMQ_REQUEUE_EXPIRY_TIME_IN_MIN}")
	private Long rmqRequeueExpTime;
	
	/** The retry sleep time. */
	@Value("${RESEQ_RETRY_SLEEP_MS}")
	private Long retrySleepTime;
	
	/** The prefatch count. */
	@Value("${Digital.RabbitMQ.prefatchCount}")
	private Long prefatchCount;
	
	/** The delay time. */
	@Value("${RMQ_DELAY_TIME}")
	private Long rmqDelayTime;
	
	/** The max redelivery. */
	@Value("${RMQ_MAX_REDELIVERY}")
	private int rmqMaxRedelivery;
	
	/** The redelivery delay time. */
	@Value("${RMQ_REDELV_DELAY_TIME}")
	private Long rmqRedelvDelayTime;

	/** The receiver. */
	private Map<String, Map<String, String>> receiver = new HashMap<String, Map<String, String>>();
	
	/** The sender. */
	private Map<String, Map<String, String>> sender = new HashMap<String, Map<String, String>>();
}
