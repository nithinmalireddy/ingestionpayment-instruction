package com.igtb.ingestion.payment.instruction.consumer;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.igtb.api.action.commons.exceptions.ProcessingException;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.service.FileProcessingServiceImpl;
import com.igtb.ingestion.payment.instruction.util.EventValidationUtil;

import lombok.AllArgsConstructor;

/**
 * The Class PaymentRabbitConsumer.
 */
@Component
@AllArgsConstructor
public class FileProcessingRabbitConsumer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FileProcessingRabbitConsumer.class);

	/** the service implementation class for limits. */
	private final FileProcessingServiceImpl serviceImpl;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;
	
	/** The event validation util. */
	private final EventValidationUtil eventValidationUtil;
	
	/** The event validation config. */
	private final EventValidationConfig eventValidationConfig;

	/**
	 * Rabbit listener.
	 *
	 * @param event the json node
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ProcessingException the processing exception
	 * @throws RequeueException the requeue exception
	 * @throws DLQException the DLQ exception
	 * @throws InterruptedException the interrupted exception
	 * @throws MessageProcessingException the message processing exception
	 */
	public void fileRabbitListener(final @Body JsonNode event) throws IOException, ProcessingException, RequeueException,
			DLQException, InterruptedException, MessageProcessingException {

		try {
			LOGGER.debug("Validating event for file PI for event: {}", event.get("id"));
			if (event != null) {
				final Map<String, ArrayList<String>> fileEventsConfig = eventValidationConfig.getValidation().get("fileEvents");
				if (eventValidationUtil.isValidEvent(event,
						fileEventsConfig.get("supportedRequestType"), fileEventsConfig.get("supportedServiceKey"))) {
					try {
						serviceImpl.procesasFileRabbitMessage(event);
					} catch (DataNotValidException | DataNotFoundException e) {
						LOGGER.error("Error in possing for event id: {} as : {}", event.get("id"), e);
						throw new DLQException(e.getMessage()); //NOPMD PreserveStackTrace
					}
					LOGGER.info("------>>>>>>Request is processed for event: {} <<<<<<<------", event.get("id"));

				} else {
					LOGGER.info("For file PI, Invalid Service Key or request type or Source Identity, hence ignoring the event.");
				}
			}
		} catch (IOException e) {

			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_018 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_018_Desc);
			LOGGER.error("Exception recevied at fileRabbitListener for eventId: {} as ===> {}", event.get("id"), e);
			validateRequeueExpirationForIOExceptionThread(event.get(PaymentIngestionConstant.CONTEXT));
		}
	}

	/**
	 * Validate requeue expiration for IO exception thread.
	 *
	 * @param contextNode the context node
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 * @throws DLQException the DLQ exception
	 * @throws RequeueException the requeue exception
	 */
	private void validateRequeueExpirationForIOExceptionThread(final JsonNode contextNode)
			throws IOException, InterruptedException, DLQException, RequeueException {

		/*
		 * This method will check and execute requeue as long as the difference between
		 * event time of the request and the current time is lesser than 24hrs. Upon
		 * crossing 24hrs of requeuing, the request will be pushed to dlq.
		 */

		final String contextEventTime = Optional.ofNullable(contextNode.get(PaymentIngestionConstant.EVENTTIME))
				.map(eventTime -> eventTime.toString()).orElse(null);

		final String channelSeqId = Optional.ofNullable(contextNode.get(PaymentIngestionConstant.CHANNEL_SQ_ID))
				.map(eventTime -> eventTime.toString()).orElse(null);
		if (contextEventTime == null) {
			LOGGER.error(
					"EventTime is missing in the request for the channelSeqId ---> {}, hence pushing the event to DLQ",
					channelSeqId);
			throw new DLQException("Inbound request eventTime is missing");
		} else {
			// formatting the eventtime to localdatetime type
			final LocalDateTime formattedEventTime = getFormattedDate(contextEventTime);
			final LocalDateTime currentDateTime = LocalDateTime.now();
			final Duration d1 = Duration.between(formattedEventTime, currentDateTime);
			final Duration d2 = Duration.ofHours(24);
			if (d1.compareTo(d2) > 0) {
				LOGGER.error("Requeue Time expired for the channelSeqId ---> {}, hence pushing the event to DLQ",
						channelSeqId);
				throw new DLQException("Requeue Time expired");
			} else {
				Thread.sleep(paymentRabbitProperties.getRetrySleepTime());
				throw new RequeueException("Requeuing the event with this channelseqid -->>> " + channelSeqId);
			}
		}
	}

	/**
	 * Gets the formatted date.
	 *
	 * @param eventTime
	 *            the event time
	 * @return the formatted date
	 */
	private LocalDateTime getFormattedDate(final String eventTime) {
		if (eventTime != null) {
			try {
				final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				return LocalDateTime.parse(eventTime, formatter);
			} catch (DateTimeParseException e) {

				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_019 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_019_Desc);
				LOGGER.error("----->>>>>> Invalid EventTime format <<<<<-----");
				return null;
			}
		}
		return null;
	}

}
