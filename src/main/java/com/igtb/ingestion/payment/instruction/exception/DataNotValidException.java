package com.igtb.ingestion.payment.instruction.exception;

/**
 * Exception thrown when elastic search typename is not valid as per expected/ or date received is unparseable or , or an incorrect operation type is suggested..
 * Allowable channel events : {paymentInstructions/standingInstructions/paymentFiles}
 * Allowable request types: {add/amend/delete/hold/unhold/cancel}
 * 
 */
public class DataNotValidException extends Exception{

	/** Serial Version */
	private static final long serialVersionUID = -1019374980870273656L;

	
	/**Constructs a new exception with the specified detail message. 
	 * The cause is not initialized, and may subsequently be initialized by a call.
	 * @param message The exception message
	 */
	public DataNotValidException(final String message) {
		super(message);
	}
}
