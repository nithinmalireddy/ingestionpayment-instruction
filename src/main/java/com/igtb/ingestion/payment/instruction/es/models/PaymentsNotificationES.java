package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class PaymentInstruction.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@Setter
public class PaymentsNotificationES {
	
	
	/** The event. */
	@JsonProperty("event")
	private String eventES = null;

	/** The notf_id. */
	@JsonProperty("notificationId")
	private String notificationIdES = null;
	
	@JsonProperty("notfType")
	private String notfTypeES = null;

	@JsonProperty("ntfcnCategory")
	private String ntfcnCategoryES = null;

	@JsonProperty("notfName")
	private String notfNameES = null;

	@JsonProperty("name")
	private String nameES = null;

	@JsonProperty("nameType")
	private String nameTypeES = null;

	/** The debtor account. */
	@JsonProperty("accountNumber")
	private String accoutNumberES = null;

	@JsonProperty("accountType")
	private String accoutTypeES = null;

	@JsonProperty("instAmtcurrency")
	private String instAmtcurrencyES = null;

	/** The instructed amount. */
	@JsonProperty("instAmtamount")
	private Double instAmtamountES;

	@JsonProperty("participantUserId")
	private String participantUserIdES = null;

	@JsonProperty("interacId")
	private String interacIdES = null;

	@JsonProperty("message")
	private String messageES = null;

	@JsonProperty("priority")
	private Integer priorityES;
	
	@JsonProperty("docStatus")
	private String docStatusES = null;
	
	@JsonProperty("domainSeqId")
	private String domainSeqIdES = null;
	
	@JsonProperty("channelSeqId")
	private String channelSeqIdES = null;

	@JsonProperty("creationDate")
	private String creationDateES = null;
	
	@JsonProperty("referenceNumber")
	private String referenceNumberES = null;
	
	@JsonProperty("decline_reason")
	private String declinereason = null;
	
	@JsonProperty("participantId")
	private String participantIdES = null;

	@JsonProperty("paymntTypeAccId")
	private String paymntTypeAccIdES = null;

	@JsonProperty("ntfcnStatus")
	private String ntfcnStatusES = null;
	
	@JsonProperty("account_alias_handle")
	private String accountAliasHandleES = null;

	@JsonProperty("account_alias_reference")
	private String accountAliasReferenceES = null;

}
