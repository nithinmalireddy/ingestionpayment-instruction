
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class ChargesAccount.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class ChargesAccount {

    /** The currency. */
    @JsonProperty("currency")
    public String currency;
    
    /** The number. */
    @JsonProperty("number")
    public String number;

}
