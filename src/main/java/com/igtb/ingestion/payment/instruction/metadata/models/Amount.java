package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class Amount.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Amount {

    /** The value. */
    private Double value;
    
    /** The currency code. */
    private String currencyCode;

}
