package com.igtb.ingestion.payment.instruction.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.igtb.ingestion.payment.instruction.config.FeignClientConfig;
import com.igtb.ingestion.payment.instruction.models.quest.QuestRequest;
import com.igtb.ingestion.payment.instruction.models.quest.QuestResponse;


/**
 * The Interface GatekeeperFeignClient.
 */
@FeignClient(name = "${digital.quest.appName}", configuration = FeignClientConfig.class)
public interface DigitalFeignClient {
	
	/**
	 * Gets the user details from quest.
	 *
	 * @param questPayload the quest payload
	 * @param headers the headers
	 * @return the user details from quest
	 */
	@RequestMapping(method = RequestMethod.POST, value = "${digital.quest.uri}")
    ResponseEntity<QuestResponse> getDetailsFromQuest(@RequestBody QuestRequest questPayload, @RequestHeader HttpHeaders headers);

}
