
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class ChargeAccount.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the scheme name.
 *
 * @return the scheme name
 */
@Getter

/**
 * Sets the scheme name.
 *
 * @param schemeName the new scheme name
 */
@Setter
public class ChargeAccount {

    /** The identification. */
    @JsonProperty("identification")
    public String identification;
    
    /** The name. */
    @JsonProperty("name")
    public String name;
    
    /** The scheme name. */
    @JsonProperty("schemeName")
    public String schemeName;
    @JsonProperty("currency")
    public String currency;


}
