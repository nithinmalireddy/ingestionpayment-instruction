package com.igtb.ingestion.payment.instruction.metadata.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class Metadata.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {

    /** The file hash fn. */
    private String fileHashFn;
    
    /** The file name. */
    private String fileName;
    
    /** The file metrics. */
    private FileMetrics fileMetrics;
    
    /** The file hash. */
    private String fileHash;
    
    /** The business fields. */
    private List<BusinessField> businessFields;
    
    /** The template id. */
    private String templateId;
    
    /** The upload user id. */
    private String uploadUserId;
    
    /** The upload date. */
    private String uploadDate;
    
    /** The template name. */
    private String templateName;
    
    /** The upload user name. */
    private String uploadUserName;
    
    /** The file uri. */
    private String fileUri;
    
    /** The service key. */
    private String serviceKey;
    
    /** The file type. */
    private String fileType;
    
    /** The file id. */
    private String fileId;
    
    /** The customer file ref no. */
    private String customerFileRefNo;
    
}
