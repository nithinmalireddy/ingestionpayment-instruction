package com.igtb.ingestion.payment.instruction.models.bulk;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class BulkEventPayload.
 */
@Setter
@Getter
@ToString
public class BulkEventPayload {

	/** The bulks. */
	@NotEmpty
	private List<Bulk> bulks = null;
}
