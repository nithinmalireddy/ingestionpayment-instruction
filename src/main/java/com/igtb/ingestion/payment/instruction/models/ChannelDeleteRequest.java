package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * ChannelDeleteContactRequest.
 */
@Getter
@Setter
public class ChannelDeleteRequest {
  
  /** The id. */
  @JsonProperty("id")
  private String id;

  /** The event version. */
  @JsonProperty("eventVersion")
  private String eventVersion;

  /** The context. */
  @JsonProperty("context")
  @NotNull
  @Valid
  private Context context;

  /** The payload. */
  @JsonProperty("payload")
  @NotNull
  @Valid
  private PaymentInstruction payload;

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final ChannelDeleteRequest channelDeleteRequest = (ChannelDeleteRequest) obj;
    return Objects.equals(this.id, channelDeleteRequest.id) &&
        Objects.equals(this.eventVersion, channelDeleteRequest.eventVersion) &&
        Objects.equals(this.context, channelDeleteRequest.context) &&
        Objects.equals(this.payload, channelDeleteRequest.payload);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(id, eventVersion, context, payload);
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(100);
    sb.append("class ChannelDeleteRequest {  id: ").append(toIndentedString(id))
      .append("    eventVersion: ").append(toIndentedString(eventVersion))
      .append("    context: ").append(toIndentedString(context))
      .append("    payload: ").append(toIndentedString(payload))
      .append('}');
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param obj the obj
   * @return the string
   */
  private String toIndentedString(final Object obj) {
    if (obj == null) {
      return "null";
    }
    return obj.toString().replace("\n", "\n    ");
  }

}

