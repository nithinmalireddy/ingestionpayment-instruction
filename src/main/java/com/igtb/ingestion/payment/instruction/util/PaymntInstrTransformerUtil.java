/**
 * Version no		Author						Comments
 * 1.0				K1 base version				K1 base version
 * 1.1				Anil Ravva					Changes related to modify the combination of fields for cumulativeStatus and added  new status column
 */

package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.igtb.api.ingestion.commons.stateinfo.models.RejectionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.ResourceEnum;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.AgentES;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.BankProxyES;
import com.igtb.ingestion.payment.instruction.es.models.CancellationDetailsES;
//import com.igtb.ingestion.payment.instruction.es.models.ChargeAccountES;
import com.igtb.ingestion.payment.instruction.es.models.ChargesAccountES;
import com.igtb.ingestion.payment.instruction.es.models.CreditorES;
import com.igtb.ingestion.payment.instruction.es.models.EmailES;
import com.igtb.ingestion.payment.instruction.es.models.IncomingRTPES;
import com.igtb.ingestion.payment.instruction.es.models.InitiatingPartyES;
import com.igtb.ingestion.payment.instruction.es.models.InvoiceDetailsES;
import com.igtb.ingestion.payment.instruction.es.models.NotificationChannelES;
import com.igtb.ingestion.payment.instruction.es.models.OriginalGroupInformationAndStatusES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentAuthenticationES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentConditionES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentRailES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentReasonES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentTypeES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.PhoneES;
import com.igtb.ingestion.payment.instruction.models.Phone;
import com.igtb.ingestion.payment.instruction.models.Email;
import com.igtb.ingestion.payment.instruction.es.models.RecommendationCostES;
import com.igtb.ingestion.payment.instruction.es.models.SupplementaryDataES;
import com.igtb.ingestion.payment.instruction.es.models.WhtDetailES;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.metadata.models.Records;
import com.igtb.ingestion.payment.instruction.models.Account;
import com.igtb.ingestion.payment.instruction.models.CancellationDetails;
import com.igtb.ingestion.payment.instruction.models.CategoryPurpose;
import com.igtb.ingestion.payment.instruction.models.ChargeAccount;
import com.igtb.ingestion.payment.instruction.models.ChargeBearer;
import com.igtb.ingestion.payment.instruction.models.ChargesAccount;
import com.igtb.ingestion.payment.instruction.models.City;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Country;
import com.igtb.ingestion.payment.instruction.models.IncomingRTP;
import com.igtb.ingestion.payment.instruction.models.InitiatingParty;
import com.igtb.ingestion.payment.instruction.models.InstructedAmount;
import com.igtb.ingestion.payment.instruction.models.IntermediaryAgent;
import com.igtb.ingestion.payment.instruction.models.NotificationChannel;
import com.igtb.ingestion.payment.instruction.models.OriginalGroupInformationAndStatus;
import com.igtb.ingestion.payment.instruction.models.PaymentAuthentication;
import com.igtb.ingestion.payment.instruction.models.PaymentCondition;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;
import com.igtb.ingestion.payment.instruction.models.PostalAddress;
import com.igtb.ingestion.payment.instruction.models.RecommendationCost;
import com.igtb.ingestion.payment.instruction.models.RemittanceInformation;
import com.igtb.ingestion.payment.instruction.models.State;
import com.igtb.ingestion.payment.instruction.models.SupplementaryData;
import com.igtb.ingestion.payment.instruction.models.SupplementaryData.SettlementMethodEnum;
import com.igtb.ingestion.payment.instruction.models.WhtDetails;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Payment Instructions payload transformation Utility.
 * 
 */
@Component
public class PaymntInstrTransformerUtil {
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymntInstrTransformerUtil.class);

	/** The object mapper. */
	private ObjectMapper objectMapper;
	
	/** The payment instruction builder. */
	@Autowired
	private PaymentInstructionBuilder paymentInstructionBuilder;

	/** The config. */
	@Autowired
	private EventValidationConfig config;
	
	@Autowired
	PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/**
	 * Populating payment instructions field.
	 *
	 * @param paymentInstructionES
	 *            the payment instruction ES
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param stateInfo
	 *            State Information.
	 * @return the payments instructions ES
	 * @throws DataNotValidException
	 *             The exception.
	 * @throws IOException
	 *             exception
	 * @throws DataNotFoundException
	 *             exception
	 */
	public PaymentsInstructionsES populatePaymentInstructions(final PaymentsInstructionsES paymentInstructionES,final PaymentInstruction paymentInstruction, final StateInfo stateInfo, final Context context)throws Exception {
		LOGGER.info("***************Begin populatePaymentInstructions****************");
		LOGGER.info("inside populatePaymentInstructions()::::::::::::::: {} ", paymentInstruction);
		LOGGER.info("stateInfo ::::::::::::::::: {} ", stateInfo);
		LOGGER.info("serviceKey inside populatePaymentInstructions():::: {}", context.getEventType().getServiceKey());
		final String requestType = stateInfo.getTransitionInfo().getRequestType();
		final String transitionState = stateInfo.getTransitionInfo().getState();

		try {
			paymentInstructionES.setProcessingDate(paymentInstructionBuilder.getProcessingDate(paymentInstruction.getProcessingDate()));
			paymentInstructionES.setReleaseDate(paymentInstructionBuilder.getReleaseDate(paymentInstruction.getReleaseDate()));
			paymentInstructionES.setRequestedExecutionDate(paymentInstructionBuilder.getRequestedExecutionDate(paymentInstruction.getRequestedExecutionDate(), requestType, transitionState));
			paymentInstructionES.setRemark(paymentInstruction.getRemark());
			paymentInstructionES.setUserDateTime(paymentInstructionBuilder.getUserDateTime(paymentInstruction.getUserDateTime(), stateInfo.getInitiated().getTs()));
			paymentInstructionES.setCategoryPurpose(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCategoryPurpose).map(CategoryPurpose::getCode).orElse(null));
			paymentInstructionES.setChargeBearer(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargeBearer).map(ChargeBearer::getChargeTypes).orElse(null));
			paymentInstructionES.setOption(paymentInstructionBuilder.getOption(paymentInstruction.getTxnInitiationType(), requestType, context.getEventType().getServiceKey()));
			//chargeAccount
//			ChargeAccountES chargeAccountES =null;
//			if(paymentInstruction.getChargeAccount() != null)
//			{
//				chargeAccountES = new ChargeAccountES();
//				chargeAccountES.setIdentificationES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargeAccount).map(ChargeAccount::getIdentification).orElse(null));
//				chargeAccountES.setNameES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargeAccount).map(ChargeAccount::getName).orElse(null));
//				chargeAccountES.setSchemeNameES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargeAccount).map(ChargeAccount::getSchemeName).orElse(null));
//				chargeAccountES.setCurrencyES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargeAccount).map(ChargeAccount::getCurrency).orElse(null));
//				paymentInstructionES.setChargeAccount(chargeAccountES);
//			}
//			else {
//				paymentInstructionES.setChargeAccount(null);
//			}
			//chargesAccount for CP
			ChargesAccountES chargesAccountES =null;
			if(paymentInstruction.getChargesAccount() != null)
			{
				chargesAccountES = new ChargesAccountES();
				chargesAccountES.setNumberES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargesAccount).map(ChargesAccount::getNumber).orElse(null));
				chargesAccountES.setCurrencyES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getChargesAccount).map(ChargesAccount::getCurrency).orElse(null));
				paymentInstructionES.setChargesAccount(chargesAccountES);
			}
			else {
				paymentInstructionES.setChargesAccount(null);
			}
			// creditor
			paymentInstructionES.setCreditor(paymentInstructionBuilder.getCreditorES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditor).orElse(null),paymentInstructionES.getOption(), PaymentIngestionConstant.BLANK));

			// creditorAccount
			BankProxyES bankProxyES = null;
			paymentInstructionES.setCreditorAccount(paymentInstructionBuilder.getCreditorAccount(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditorAccount).orElse(null),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditorAccount).map(Account::getIdentification).orElse(null),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditorAccount).map(Account::getSchemeName).orElse(null),paymentInstructionES.getOption(), PaymentIngestionConstant.BLANK, bankProxyES, context.getEventType().getServiceKey()));
			// creditorAgent
			paymentInstructionES.setCreditorAgent(paymentInstructionBuilder.getCreditorAgent(paymentInstruction.getCreditorAgent(), PaymentIngestionConstant.CRDTR_AGENT, bankProxyES));

			// debtorDetails
			paymentInstructionES.setDebtorDetails(paymentInstructionBuilder.getDebtorES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getDebtor).orElse(null),paymentInstructionES.getOption(), PaymentIngestionConstant.BLANK));

			// debtorAccount
			paymentInstructionES.setDebtorAccount(paymentInstructionBuilder.getDebtorAccount(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getDebtorAccount).orElse(null),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getDebtorAccount).map(Account::getIdentification).orElse(null),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getDebtorAccount).map(Account::getSchemeName).orElse(null),paymentInstructionES.getOption(), context.getEventType().getServiceKey()));
			// debtorAgent
			paymentInstructionES.setDebtorAgent(paymentInstructionBuilder.getDebtorAgent(paymentInstruction.getDebtorAgent(), PaymentIngestionConstant.DBTR_AGENT));

			// otherRemainingInfo
			paymentInstructionES.setOtherRemainingInfo(paymentInstructionBuilder.getOtherRemainingInfo(Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditor).map(CreditorES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAgent).map(AgentES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getDebtorAgent).map(AgentES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getAccNumType).orElse(null)));

			//Notification Channel Code start Durgesh
			PhoneES phoneES=new PhoneES();
			phoneES.setValue((Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getNotificationChannel).map(NotificationChannel::getPhone).map(Phone::getValue).orElse(null)));
			phoneES.setChecked((Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getNotificationChannel).map(NotificationChannel::getPhone).map(Phone::getChecked).orElse(null)));
			EmailES emailES=new EmailES();
			emailES.setValue((Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getNotificationChannel).map(NotificationChannel::getEmail).map(Email::getValue).orElse(null)));
			emailES.setChecked((Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getNotificationChannel).map(NotificationChannel::getEmail).map(Email::getChecked).orElse(null)));
			NotificationChannelES notificationChannelES=new NotificationChannelES();
			notificationChannelES.setEmail(emailES);
			notificationChannelES.setPhone(phoneES);
			paymentInstructionES.setNotificationChannel(notificationChannelES);
			//End
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
				paymentInstructionES.setEndToEndIdentification(stateInfo.getChannelSeqId());
			} else if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_WIRE) || context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_MONEY)) {
				paymentInstructionES.setEndToEndIdentification(stateInfo.getChannelSeqId());
			} else {
				paymentInstructionES.setEndToEndIdentification(paymentInstruction.getEndToEndIdentification());
			}
			paymentInstructionES.setFxDetails(paymentInstructionBuilder.getFxDetails(paymentInstruction.getFxDetails()));
			paymentInstructionES.setClearingSystemReferenceES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getClearingSystemReference).orElse(null));
			paymentInstructionES.setChargeAmount(paymentInstructionBuilder.getChargeAmount(paymentInstruction.getChargeAmount(),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInstructedAmount).map(InstructedAmount::getCurrency).orElse(null)));
			paymentInstructionES.setInstructedAmount(paymentInstructionBuilder.getInstructedAmount(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInstructedAmount).map(InstructedAmount::getAmount).orElse(null),Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInstructedAmount).map(InstructedAmount::getCurrency).orElse(null)));
			paymentInstructionES.setEquivalentAmount(paymentInstructionBuilder.getEquivalentAmount(paymentInstruction.getDebitAmountFlag(),paymentInstruction.getEquivalentAmount(), paymentInstructionES.getInstructedAmount(),paymentInstructionES.getDebtorAccount(), paymentInstructionES.getFxDetails()));

			// intermediaryAgent for wire payment
			IntermediaryAgent intermediaryAgentES = new IntermediaryAgent();
			PostalAddress address = new PostalAddress();
			Country countryES = new Country();
			State stateES = new State();
			City cityES = new City();

			address.setAddress1(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getAddress1).orElse(null));
			address.setAddress2(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getAddress2).orElse(null));
			address.setAddress3(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getAddress3).orElse(null));
			countryES.setName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getCountry).map(Country::getName).orElse(null));
			countryES.setCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getCountry).map(Country::getCode).orElse(null));
			stateES.setName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getState).map(State::getName).orElse(null));
			stateES.setCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getState).map(State::getCode).orElse(null));
			cityES.setName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getCity).map(City::getName).orElse(null));
			cityES.setCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getPostalAddress).map(PostalAddress::getCity).map(City::getCode).orElse(null));
			address.setCity(cityES);
			address.setState(stateES);
			address.setCountry(countryES);
			intermediaryAgentES.setPostalAddress(address);
			intermediaryAgentES.setBic(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getBic).orElse(null));
			intermediaryAgentES.setName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIntermediaryAgent).map(IntermediaryAgent::getName).orElse(null));
			paymentInstructionES.setPaymentMethod(paymentInstruction.getPaymentMethod());
			if (paymentInstruction.getLimitUtilizeDateTime() != null) {
				paymentInstructionES.setLimitUtilizeDateTime(paymentInstruction.getLimitUtilizeDateTime());
			}
			if (paymentInstruction.getLimitUtilizeFlag() != null) {
				paymentInstructionES.setLimitUtilizeFlag(paymentInstruction.getLimitUtilizeFlag());
			}

			if (paymentInstruction.getRemittanceInformation() != null) {
				final RemittanceInformation remittanceInformation = paymentInstruction.getRemittanceInformation();
				paymentInstructionES.setRemittanceInformation1(remittanceInformation.getRemitInfo1());
				paymentInstructionES.setRemittanceInformation2(remittanceInformation.getRemitInfo2());
				paymentInstructionES.setRemittanceInformation3(remittanceInformation.getRemitInfo3());
				paymentInstructionES.setRemittanceInformation4(remittanceInformation.getRemitInfo4());
			}

			paymentInstructionES.setPaymentRailObj(paymentInstructionBuilder.getPaymentRail(paymentInstruction.getPaymentProduct() == null ? null : paymentInstruction.getPaymentProduct().getPaymentType()));
			//paymentInstructionES.setPaymentRailObj(paymentInstructionBuilder.getPaymentRail(paymentInstruction.getPaymentProduct() == null ? null : paymentInstruction.getPaymentProduct().getProductName()));
			paymentInstructionES.setPaymentTypesObj(paymentInstructionBuilder.getPaymentType(paymentInstruction.getPaymentProduct() == null ? null : paymentInstruction.getPaymentProduct().getPaymentType()));

			paymentInstructionES.setAcceptTnCFlag(true);
			paymentInstructionES.setIntermediaryAgent1BIC(paymentInstruction.getIntermediaryAgent1());
			paymentInstructionES.setInstructionId(stateInfo.getChannelSeqId());
			paymentInstructionES.setTransactionId(null);
			
			paymentInstructionES.setStandingInstructionId(null);

			// send advice flag
			paymentInstructionES.setSendAdviceFlag(PaymentIngestionConstant.Y.equals(paymentInstruction.getSendAdviceFlag()));
			// Payment reason is same to PaymentTypeObj code thats why paymentReason
			// change to PaymentTypeObj
			if (paymentInstruction.getPaymentTypesObj() != null && paymentInstruction.getPaymentTypesObj().getCode() != null) {
				paymentInstructionES.setPaymentReason(paymentInstructionBuilder.getPaymentReason(paymentInstruction.getPaymentTypesObj().getCode()));
			}
			paymentInstructionES.setFileId(null);
			paymentInstructionES.setBatchId(null);
			paymentInstructionES.setBankCommissionAmount(null);

			if (paymentInstruction.getWhtDetails() != null) {
				final WhtDetails whtDetails = paymentInstruction.getWhtDetails();
				final WhtDetailES whtDetailES = new WhtDetailES();
				whtDetailES.setSerialNumber(whtDetails.getSerialNumber());
				whtDetailES.setBookNumber(whtDetails.getBookNumber());
				whtDetailES.setSequenceNumber(whtDetails.getSequenceNumber());
				whtDetailES.setFormType(whtDetails.getFormType());
				whtDetailES.setDeductionType(whtDetails.getDeductionType());
				whtDetailES.setDeductionTypeDescription(whtDetails.getDeductionTypeDescription());
				whtDetailES.setTaxWithholderName(whtDetails.getTaxWithholderName());
				whtDetailES.setTaxWithholderBranchCode(whtDetails.getTaxWithholderBranchCode());
				whtDetailES.setTaxWithholderTaxID(whtDetails.getTaxWithholderTaxID());
				whtDetailES.setTaxWithholderAddress(whtDetails.getTaxWithholderAddress());
				whtDetailES.setAlternativePayerName(whtDetails.getAlternativePayerName());
				whtDetailES.setAlternativePayerBranchCode(whtDetails.getAlternativePayerBranchCode());
				whtDetailES.setAlternativePayerTaxID(whtDetails.getAlternativePayerTaxID());
				whtDetailES.setAlternativePayerAddress(whtDetails.getAlternativePayerAddress());
				whtDetailES.setTaxablePartyName(whtDetails.getTaxablePartyName());
				whtDetailES.setTaxablePartyTaxID(whtDetails.getTaxablePartyTaxID());
				whtDetailES.setTaxablePartyAddress(whtDetails.getTaxablePartyAddress());
				whtDetailES.setDeliveryMethod(whtDetails.getDeliveryMethod());
				whtDetailES.setDeliveryLocation(whtDetails.getDeliveryLocation());

				paymentInstructionES.setWhtDtls(whtDetailES);
			}

			paymentInstructionES.setDateTypeIndicator(paymentInstruction.getDateTypeIndicator());
			paymentInstructionES.setDebitTimeFlag(Optional.ofNullable(paymentInstruction.getDebitTime()).isPresent());
			paymentInstructionES.setDebitTime(paymentInstructionBuilder.getDebitTime(paymentInstruction.getDebitTime(),paymentInstruction.getProcessingDate()));

			paymentInstructionES.setCbxUIStatus(UIStatusUtil.getCBXUIStatus(stateInfo, ResourceEnum.paymentInstructions.toString()));
            //enrich customerInformation on puId final PaymentsInstructionsES paymentInstructionES
			LOGGER.info("Calling enrichCustomerInformation::::: {}",context.getChannelSeqId());
			paymentInstructionBuilder.enrichCustomerInformation(paymentInstructionES,context);
			LOGGER.info("default STATUS updated to::::: {}", paymentInstructionES.getCbxUIStatus());
			LOGGER.info("paymentInstructionES inside populatePaymentInstructions()::::: {} ", paymentInstructionES);
			// By Naveen S.
			Map<String, String> checkerStatusMap = new HashMap<>();
			checkerStatusMap.put(PaymentIngestionConstant.SEND_RTP, PaymentIngestionConstant.SENT_TO_RECEIVER);
			checkerStatusMap.put(PaymentIngestionConstant.SEND_MONEY, PaymentIngestionConstant.SENT_TO_BANK);
			checkerStatusMap.put(PaymentIngestionConstant.SEND_WIRE, PaymentIngestionConstant.SENT_TO_BANK);
			checkerStatusMap.put(PaymentIngestionConstant.FULFILL_RTP, PaymentIngestionConstant.ACCP_PNDNG_APROVL);
			checkerStatusMap.put(PaymentIngestionConstant.DECLINE_RTP, PaymentIngestionConstant.DCLIN_PNDNG_APROVL);
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
				if (stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.VAL_SUCCESS) && stateInfo.getTransitionInfo().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.ADD)) 
				{
					paymentInstructionES.setCbxUIStatus(checkerStatusMap.get(PaymentIngestionConstant.FULFILL_RTP));
					LOGGER.info("Updating status to ACCEPTED PEND APRVL for fulfillrtp");
				} else if (stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.VAL_SUCCESS) && stateInfo.getTransitionInfo().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.DELETE)) 
				{
					paymentInstructionES.setCbxUIStatus(checkerStatusMap.get(PaymentIngestionConstant.DECLINE_RTP));
					JsonNode activeDocData =  paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
					if(activeDocData!=null) {
//						final PaymentInstruction paymentInstructionActiveDocData = objectMapper.readValue(activeDocData.toString(),PaymentInstruction.class);
//						LOGGER.info("activeDoc Data in Case of FRTP {}", paymentInstructionActiveDocData);
						LOGGER.info("Updating remittance information from activeDoc");
//						paymentInstructionES.setRemittanceInformation1(paymentInstructionActiveDocData.getRemittanceInformation().getRemitInfo1());
//						paymentInstructionES.setRemittanceInformation2(paymentInstructionActiveDocData.getRemittanceInformation().getRemitInfo2());
//						paymentInstructionES.setRemittanceInformation3(paymentInstructionActiveDocData.getRemittanceInformation().getRemitInfo3());
//						paymentInstructionES.setRemittanceInformation4(paymentInstructionActiveDocData.getRemittanceInformation().getRemitInfo4());
						if(activeDocData.has(PaymentIngestionConstant.REMITTANCEINFORMATION_1) && activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_1)!=null) {
							paymentInstructionES.setRemittanceInformation1(activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_1).asText());							
						}
						if(activeDocData.has(PaymentIngestionConstant.REMITTANCEINFORMATION_2) && activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_2)!=null) {
							paymentInstructionES.setRemittanceInformation2(activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_2).asText());							
						}
						if(activeDocData.has(PaymentIngestionConstant.REMITTANCEINFORMATION_3) && activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_3)!=null) {
							paymentInstructionES.setRemittanceInformation3(activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_3).asText());							
						}
						if(activeDocData.has(PaymentIngestionConstant.REMITTANCEINFORMATION_4) && activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_4)!=null) {
							paymentInstructionES.setRemittanceInformation4(activeDocData.get(PaymentIngestionConstant.REMITTANCEINFORMATION_4).asText());						
						}
						LOGGER.info("Updating status to DECLINE PEND APRVL for declinertp");
						paymentInstructionES.setReason(paymentInstruction.getReasonCode());
						LOGGER.info("Updating  decline reason for declinertp");
					}
				} else {
					LOGGER.info("conditions not met for fulfillrtp status update");
				}
			} else if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
				LOGGER.info("State info-> RequestType:::::: {}" , stateInfo.getChannelSeqId());
				if ((!stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.REJECTED) && !stateInfo.getTransitionInfo().getState().equalsIgnoreCase("val_failure") && !stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.VAL_SUCCESS) && !stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.INITIATED) && !stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.DRAFT_STATE)) && stateInfo.getTransitionInfo().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.ADD)) 
				{
					LOGGER.info("Updating status to SENT TO RECEIVER");
					paymentInstructionES.setCbxUIStatus(checkerStatusMap.get(PaymentIngestionConstant.SEND_RTP));
				}
				else if (stateInfo.getTransitionInfo().getState().equalsIgnoreCase(PaymentIngestionConstant.REJECTED) || stateInfo.getTransitionInfo().getState().equalsIgnoreCase("val_failure")) 
				{
					LOGGER.info("Updating status to Rejected");
					paymentInstructionES.setCbxUIStatus("REJECTED");
				}
			}
			LOGGER.info("CIBC STATUS updated to:::::::: {} ", paymentInstructionES.getCbxUIStatus());
			paymentInstructionES.setTxnLevelAddinlInfo(paymentInstruction.getTxnLevelAddinlInfo());
			paymentInstructionES.setMultiSetAddnlInfo(paymentInstruction.getMultiSetAddnlInfo());

			// Single fields Added for Parent payload
			if(PaymentIngestionConstant.BLANK.equals(paymentInstruction.getExpiryDate())) {
				paymentInstruction.setExpiryDate(null);
			}
			paymentInstructionES.setExpiryDate(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getExpiryDate).orElse(null));

			// MsgId
			LOGGER.info("populatePaymentInstructions::::::::MsgId {} ", paymentInstruction.getMsgId());
			paymentInstructionES.setMsgId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getMsgId).orElse(null));
			paymentInstructionES.setFileNbOfTxs(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getFileNbOfTxs).orElse(null));
			paymentInstructionES.setFileCtrlSum(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getFileCtrlSum).orElse(null));
			paymentInstructionES.setFileAuthstnPrtry(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getFileAuthstnPrtry).orElse(null));

			paymentInstructionES.setPmtInfId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getPmtInfId).orElse(null));
			paymentInstructionES.setBatchNbOfTxs(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getBatchNbOfTxs).orElse(null));
			paymentInstructionES.setBatchCtrlSum(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getBatchCtrlSum).orElse(null));

			// instructedAgentId
			paymentInstructionES.setInstructedAgentId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInstructedAgentId).orElse(null));
			paymentInstructionES.setInstructingAgentId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInstructingAgentId).orElse(null));

			paymentInstructionES.setNotificationLanguage(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getNotificationLanguage).orElse(null));

			// Single fields Added for Parent payload-Ends
			PaymentConditionES paymentConditionES = new PaymentConditionES();
			paymentConditionES.setAmountModificationAllowed(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getPaymentCondition).map(PaymentCondition::getAmountModificationAllowed).orElse(null));
			paymentConditionES.setEarlyPaymentAllowed(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getPaymentCondition).map(PaymentCondition::getEarlyPaymentAllowed).orElse(null));
			paymentConditionES.setGuaranteedPaymentRequested(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getPaymentCondition).map(PaymentCondition::getGuaranteedPaymentRequested).orElse(null));
			paymentInstructionES.setPaymentCondition(paymentConditionES);

			// paymentAuthentication
			PaymentAuthenticationES paymentAuthenticationES = new PaymentAuthenticationES();
			paymentAuthenticationES.setSecurityQuestion(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getPaymentAuthentication).map(PaymentAuthentication::getSecurityQuestion).orElse(null));
			paymentAuthenticationES.setSecurityAnswer(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getPaymentAuthentication).map(PaymentAuthentication::getSecurityAnswer).orElse(null));
			paymentAuthenticationES.setHashType(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getPaymentAuthentication).map(PaymentAuthentication::getHashType).orElse(null));
			paymentAuthenticationES.setHashSalt(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getPaymentAuthentication).map(PaymentAuthentication::getHashSalt).orElse(null));

			// invoice details
			InvoiceDetailsES invoiceDetailsES = new InvoiceDetailsES();
			invoiceDetailsES.setInvoiceDate(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInvoiceDate).orElse(null));
			invoiceDetailsES.setInvoiceNo(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInvoiceNo).orElse(null));
			// SupplementaryData
			SupplementaryDataES supplementaryDataES = new SupplementaryDataES();
			supplementaryDataES.setPaymentAuthentication(paymentAuthenticationES);
			supplementaryDataES.setPanId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getPanId).orElse(null));
			supplementaryDataES.setCnts(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getCnts).orElse(null));
			supplementaryDataES.setAutoDeposit(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getAutoDeposit).orElse(null));
			supplementaryDataES.setSettlementMethod(com.igtb.ingestion.payment.instruction.es.models.SupplementaryDataES.SettlementMethodEnum.fromValue(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getSupplementaryData).map(SupplementaryData::getSettlementMethod).map(SettlementMethodEnum::getValue).orElse(null)));
			supplementaryDataES.setInvoiceDetails(invoiceDetailsES);
			paymentInstructionES.setSupplementaryData(supplementaryDataES);

			// cancellationDetails
			CancellationDetailsES cancellationDetailsES = new CancellationDetailsES();
			cancellationDetailsES.setAssignmentID(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getAssignmentID).orElse(null));
			cancellationDetailsES.setAssignerName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getAssignerName).orElse(null));
			cancellationDetailsES.setAssignerID(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getAssignerID).orElse(null));
			cancellationDetailsES.setGroupCancellation(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getGroupCancellation).orElse(null));
			cancellationDetailsES.setPaymentInformationCancellation(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getPaymentInformationCancellation).orElse(null));
			cancellationDetailsES.setCancellationId(
					Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCancellationDetails)
							.map(CancellationDetails::getCancellationId).orElse(null));
			cancellationDetailsES.setCaseId(Optional.ofNullable(paymentInstruction)
					.map(PaymentInstruction::getCancellationDetails).map(CancellationDetails::getCaseId).orElse(null));
			paymentInstructionES.setCancellationDetails(cancellationDetailsES);

			// IncomingRTP
			IncomingRTPES incomingRTPES = new IncomingRTPES();
			incomingRTPES.setRtPFulfillmentIndicator(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIncomingRTP).map(IncomingRTP::getRtpFulfillmentIndicator).orElse(null));
			incomingRTPES.setOriginatorFIRTPRefNum(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getIncomingRTP).map(IncomingRTP::getOriginatorFIRTPRefNum).orElse(null));
			paymentInstructionES.setIncomingRTP(incomingRTPES);

			// OriginalGroupInformationAndStatus
			OriginalGroupInformationAndStatusES originalGroupInformationAndStatusES = new OriginalGroupInformationAndStatusES();
			originalGroupInformationAndStatusES.setClearingSystemReference(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getClearingSystemReference).orElse(null));
			originalGroupInformationAndStatusES.setOriginalMessageIdentification(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalMessageIdentification).orElse(null));
			originalGroupInformationAndStatusES.setOriginalMessageNameIdentification(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalMessageNameIdentification).orElse(null));
			originalGroupInformationAndStatusES.setOriginalCreationDateTime(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalCreationDateTime).orElse(null));
			originalGroupInformationAndStatusES.setOriginalPaymentInformationIdentification(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalPaymentInformationIdentification).orElse(null));
			originalGroupInformationAndStatusES.setOriginalInstructionIdentification(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalInstructionIdentification).orElse(null));
			originalGroupInformationAndStatusES.setOriginalEndToEndIdentification(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getOriginalEndToEndIdentification).orElse(null));
			originalGroupInformationAndStatusES.setDownStreamTransactionStatus(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getDownStreamTransactionStatus).orElse(null));
			originalGroupInformationAndStatusES.setAcceptanceDateTime(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getAcceptanceDateTime).orElse(null));
			originalGroupInformationAndStatusES.setProprietary(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getOriginalGroupInformationAndStatus).map(OriginalGroupInformationAndStatus::getProprietary).orElse(null));
			paymentInstructionES.setOriginalGroupInformationAndStatus(originalGroupInformationAndStatusES);

			// added two field instaPayment
			paymentInstructionES.setCreditorAccPaymentType(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditorAccPaymentType).orElse(null));
			paymentInstructionES.setCreditorAccAliasRefNo(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getCreditorAccAliasRefNo).orElse(null));
			// InteracTransactionAmountLimit not in used
			// AmountInteractES amountInteractES = new AmountInteractES();
			// amountInteractES.setCurrency(Optional.ofNullable(paymentInstruction)
			// .map(PaymentInstruction::getInteracTransactionAmountLimit).map(Amount::getCurrency).orElse(null));
			// amountInteractES.setAmount(Optional.ofNullable(paymentInstruction)
			// .map(PaymentInstruction::getInteracTransactionAmountLimit).map(Amount::getAmount).orElse(null));
			// paymentInstructionES.setInteracTransactionAmountLimit(amountInteractES);

			// InitiatingParty
			InitiatingPartyES initiatingPartyES = new InitiatingPartyES();
			initiatingPartyES.setName(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInitiatingParty).map(InitiatingParty::getName).orElse(null));
			initiatingPartyES.setId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInitiatingParty).map(InitiatingParty::getId).orElse(null));
			initiatingPartyES.setInitPtyOrgId(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getInitiatingParty).map(InitiatingParty::getInitPtyOrgId).orElse(null));

			// faeIndicatior
			paymentInstructionES.setFaeIndicator(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getFaeIndicator).orElse(null));

			/* New BCC Action field set to capture BCC action for FAE */
			paymentInstructionES.setBccAction(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getBccAction).orElse(null));

			paymentInstructionES.setInitiatingParty(initiatingPartyES);

			setOldStateInfoValues(stateInfo, paymentInstructionES);
			// added to avoid exception
			PaymentTypeES pytypES = new PaymentTypeES();
			if (paymentInstruction.getPaymentTypesObj() != null) {
				pytypES.setCode(paymentInstruction.getPaymentTypesObj().getCode());
				pytypES.setDescription(paymentInstruction.getPaymentTypesObj().getDescription());
			}
			// code by durgesh
			paymentInstructionES.setServiceLvlCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getServiceLvlCode).orElse(null));
			paymentInstructionES.setLocalInstrumentCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getLocalInstrumentCode).orElse(null));
			paymentInstructionES.setBusinessProductCode(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getBusinessProductCode).orElse(null));
			paymentInstructionES.setBankRemark(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getBankRemark).orElse(null));
			paymentInstructionES.setAuditIdES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getAuditId).orElse(null));
			paymentInstructionES.setTypeOfAmountES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getTypeOfAmount).orElse(null));
			//CP related changes
			paymentInstructionES.setIsCPFlagOnES(paymentInstruction.getIsCPFlagOn() == null?PaymentIngestionConstant.FALSE:paymentInstruction.getIsCPFlagOn());
			RecommendationCostES recommendationCostES=null;
			if(paymentInstructionES.getIsCPFlagOnES() != null && paymentInstruction.getIsCPFlagOn()!=null)
			{
				if(paymentInstruction.getIsCPFlagOn()) {					
				recommendationCostES = new RecommendationCostES();
				recommendationCostES.setCurrencyES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getRecommendationCost).map(RecommendationCost::getCurrency).orElse(null));
				recommendationCostES.setValueES(Optional.ofNullable(paymentInstruction).map(PaymentInstruction::getRecommendationCost).map(RecommendationCost::getValue).orElse(null));
				}
			}
			paymentInstructionES.setRecommendationCostES(recommendationCostES);
			paymentInstructionES.setPaymentTypesObj(pytypES);
			/** The cumulativeStatus. */
			//1.1 starts
			//To be removed
			paymentInstructionES.setCumulativeStatusES((stateInfo.getDocStatus()!= null?stateInfo.getDocStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getCbxUIStatus()!=null?paymentInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getFaeIndicator()!=null?paymentInstructionES.getFaeIndicator():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getBccAction()!=null?paymentInstructionES.getBccAction():PaymentIngestionConstant.NULL));//1.1
			paymentInstructionES.setFetchQueryStatus((stateInfo.getDocStatus()!= null?stateInfo.getDocStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getCbxUIStatus()!=null?paymentInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getFaeIndicator()!=null?paymentInstructionES.getFaeIndicator():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(paymentInstructionES.getBccAction()!=null?paymentInstructionES.getBccAction():PaymentIngestionConstant.NULL));//1.1
			//1.1 ends
			paymentInstructionES.setUiDisplayStatusES(paymentInstructionES.getCbxUIStatus()!=null?paymentInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL);
			//Adding new field CAD Equivalent Amount in ES.
			paymentInstructionES.setCadEquivalentAmount(paymentInstruction.getCadEquivalentAmount());
//			Populate Group Channel Seq Id for all
			paymentInstructionES.setGroupChannelSeqId(paymentInstruction.getGroupChannelSeqId());
			LOGGER.info("PaymentTypesObj updated to:::::::::{} ", paymentInstructionES.getPaymentTypesObj());

		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_090 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_090_Desc);
			LOGGER.info("Exception while setting into ES POJO:::::: {}", e);

		}

		return paymentInstructionES;
	}

	/**
	 * Sets the old state info values.
	 *
	 * @param stateInfo
	 *            the state info
	 * @param paymentInstructionES
	 *            the payment instruction ES
	 */
	// TODO: need to remove status, MakerUserId, MakerDomainId, ApproverUserId,
	// ApproverDomainId
	private void setOldStateInfoValues(final StateInfo stateInfo, final PaymentsInstructionsES paymentInstructionES) {
		paymentInstructionES.setMakerUserId(stateInfo.getInitiated().getUserInfo() == null ? null: stateInfo.getInitiated().getUserInfo().getUserName());
		paymentInstructionES.setMakerUserDomainId(stateInfo.getInitiated().getUserInfo() == null ? null: stateInfo.getInitiated().getUserInfo().getDomainName());
		if (stateInfo.getLastAction() != null && stateInfo.getLastAction().getKey().equalsIgnoreCase(PaymentIngestionConstant.APPROVED)) {
			paymentInstructionES.setApproverUserId(stateInfo.getLastAction().getUserInfo().getUserName());
			paymentInstructionES.setApproverDomainId(stateInfo.getLastAction().getUserInfo().getDomainName());
		}
	}

	/**
	 * Builds the PI by metadata record.
	 *
	 * @param record
	 *            the record
	 * @param fileId
	 *            the file id
	 * @param context
	 *            the context
	 * @param debtorAccCache
	 *            the debtor acc cache
	 * @param paymentReasonCache
	 *            the payment reason cache
	 * @param paymentRailCache
	 *            the payment rail cache
	 * @param paymentTypesObjCache
	 *            the payment types obj cache
	 * @return the payments instructions ES
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public PaymentsInstructionsES buildPIByMetadataRecord(final Records record, final String fileId,final Context context, final Map<String, AccountES> debtorAccCache,final Map<String, PaymentReasonES> paymentReasonCache, final Map<String, PaymentRailES> paymentRailCache,final Map<String, PaymentTypeES> paymentTypesObjCache) throws IOException, DataNotFoundException 
	{
		final PaymentsInstructionsES paymentInstructionES = new PaymentsInstructionsES();
		paymentInstructionES.setReleaseDate(record.getSendOnDate().getValue());
		paymentInstructionES.setRequestedExecutionDate(record.getArriveOnDate().getValue());
		paymentInstructionES.setRemark(null);
		paymentInstructionES.setUserDateTime(null);

		paymentInstructionES.setCategoryPurpose(record.getPurposeCode().getValue());
		paymentInstructionES.setChargeBearer(record.getChargeType().getValue());
		paymentInstructionES.setChargesAccount(null);

		if (config.getPayrollProduct().contains(record.getPaymentReason().getValue().toLowerCase(Locale.getDefault()))) {
			paymentInstructionES.setOption(PaymentIngestionConstant.PAYROLL);
		} else {
			paymentInstructionES.setOption(PaymentIngestionConstant.FILE_UPLOAD);
		}

		final Map<String, String> additionalInfo = new HashMap<>();
		additionalInfo.put(PaymentIngestionConstant.CUSTOMER_TXN_REF_NO, record.getCustomerTransactionRefNo().getValue());
		paymentInstructionES.setAdditionalInfo(additionalInfo);

		final String paymentRailKey = record.getPaymentRail().getValue();
		final PaymentRailES paymentRailES;
		if (paymentRailCache.get(paymentRailKey) != null) {
			paymentRailES = paymentRailCache.get(paymentRailKey);
		} else {
			paymentRailES = paymentInstructionBuilder.getPaymentRail(paymentRailKey);
			paymentRailCache.put(paymentRailKey, paymentRailES);
		}
		paymentInstructionES.setPaymentRailObj(paymentRailES);

		final String paymentCode = Optional.ofNullable(record.getPaymentTypeCode()).map(obj -> obj.getValue()).orElse(PaymentIngestionConstant.BLANK);
		LOGGER.info("paymentCode:::::::{}", paymentCode);
		final PaymentTypeES paymentTypeES;
		if (paymentTypesObjCache.get(paymentCode) != null) {
			paymentTypeES = paymentTypesObjCache.get(paymentCode);
		} else {
			paymentTypeES = paymentInstructionBuilder.getPaymentType(paymentCode);
			if (paymentTypeES != null) {
				paymentTypesObjCache.put(paymentCode, paymentTypeES);
			}
		}
		paymentInstructionES.setPaymentTypesObj(paymentTypeES);

		BankProxyES bankProxyES = null;
		final String creditorAccSchemeName;
		final String creditorAccID;
		final Account account = new Account();
		if (StringUtils.isNotEmpty(record.getCreditAccountNo().getId())) {
			creditorAccSchemeName = PaymentIngestionConstant.GUID;
			creditorAccID = record.getCreditAccountNo().getId();
		} else {
			creditorAccSchemeName = ""; // NOT GUID
			creditorAccID = record.getCreditAccountNo().getValue();
			account.setName(record.getCreditAccountName().getValue());
		}
		// file txns will always be GUID(known contact accounts)
		paymentInstructionES.setCreditorAccount(paymentInstructionBuilder.getCreditorAccount(account, creditorAccID,creditorAccSchemeName, paymentInstructionES.getPaymentRailObj().getCode(), PaymentIngestionConstant.SFILE, bankProxyES, PaymentIngestionConstant.BLANK));
		paymentInstructionES.setCreditorAgent(paymentInstructionBuilder.getCreditorAgentFilePI(record, bankProxyES));

		if (creditorAccSchemeName.equals(PaymentIngestionConstant.GUID)) {
			paymentInstructionES.setCreditor(paymentInstructionBuilder.getCreditorAgentFilePI(creditorAccID,paymentInstructionES.getPaymentRailObj().getCode()));
		} else {
			paymentInstructionES.setCreditor(null);
		}
		// file txns will always be GUID(known contact accounts)
		final AccountES debtorAcc;
		final String debtorId = record.getDebitAccountNo().getId();
		if (debtorAccCache.get(debtorId) != null) {
			debtorAcc = debtorAccCache.get(debtorId);
		} else {
			debtorAcc = paymentInstructionBuilder.getDebtorAccount(null, record.getDebitAccountNo().getId(),PaymentIngestionConstant.GUID, paymentInstructionES.getOption(), "");
			debtorAccCache.put(debtorId, debtorAcc);
		}
		paymentInstructionES.setDebtorAccount(debtorAcc);
		if (creditorAccSchemeName.equals(PaymentIngestionConstant.GUID)) {
			// paymentInstructionES.setDebtor(paymentInstructionBuilder.getDebtorES(agent,
			// agentType));
		} else {
			paymentInstructionES.setDebtor(null);
		}

		paymentInstructionES.setDebtorAgent(null);

		paymentInstructionES.setEndToEndIdentification(null);
		paymentInstructionES.setFxDetails(paymentInstructionBuilder.getFxDetails(record.getExchangeRate(),record.getExchangeRateType(), record.getExchangeRateAmount()));

		paymentInstructionES.setChargeAmount(new AmountES(record.getTotalCharges().getValue(), record.getTotalCharges().getCurrencyCode()));
		paymentInstructionES.setInstructedAmount(new AmountES(record.getPymntAmnt().getValue(), record.getCur().getValue()));
		paymentInstructionES.setEquivalentAmount(new AmountES(record.getDebitAmount().getValue(), record.getDebitAmount().getCurrencyCode()));
		paymentInstructionES.setInstructedAmountBaseCcy(paymentInstructionES.getInstructedAmount());
		paymentInstructionES.setPaymentMethod(PaymentIngestionConstant.TRF);
		paymentInstructionES.setRemittanceInformation1(record.getCreditDescription().getValue());
		paymentInstructionES.setRemittanceInformation2(null);
		paymentInstructionES.setRemittanceInformation3(null);
		paymentInstructionES.setRemittanceInformation4(null);

		paymentInstructionES.setAcceptTnCFlag(true);
		paymentInstructionES.setIntermediaryAgent1BIC(null);
		paymentInstructionES.setInstructionId(null);
		paymentInstructionES.setTransactionId(record.getInternalTxnId().getValue());
		paymentInstructionES.setStandingInstructionId(null);

		final String paymentReasonKey = record.getPaymentReason().getValue();
		final PaymentReasonES paymentReason;
		if (paymentReasonCache.get(paymentReasonKey) != null) {
			paymentReason = paymentReasonCache.get(paymentReasonKey);
		} else {
			paymentReason = paymentInstructionBuilder.getPaymentReason(paymentReasonKey);
		}
		paymentInstructionES.setPaymentReason(paymentReason);

		paymentInstructionES.setFileId(fileId);
		paymentInstructionES.setBatchId(null);
		paymentInstructionES.setBankCommissionAmount(null);

		if (StringUtils.isEmpty(paymentInstructionES.getRequestedExecutionDate())) {
			paymentInstructionES.setDateTypeIndicator(PaymentIngestionConstant.RECV_ON);
		} else {
			paymentInstructionES.setDateTypeIndicator(PaymentIngestionConstant.SEND_ON);
		}

		// otherRemainingInfo
		paymentInstructionES.setOtherRemainingInfo(paymentInstructionBuilder.getOtherRemainingInfo(Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditor).map(CreditorES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAgent).map(AgentES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getDebtorAgent).map(AgentES::getSchemeName).orElse(null),Optional.ofNullable(paymentInstructionES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getAccNumType).orElse(null)));
		paymentInstructionES.setDebitTimeFlag(null);
		paymentInstructionES.setDebitTime(null);

		// build state info for PI
		final StateInfo stateInfo = new StateInfo();
		final TransitionInfo transitionInfo = new TransitionInfo();
		if (record.getSt().getValue().equalsIgnoreCase(PaymentIngestionConstant.CREJECTED)) {
			transitionInfo.setState(PaymentIngestionConstant.FAILED);
			transitionInfo.setStatus(PaymentIngestionConstant.COMPLETED);
			final RejectionInfo rejectionInfo = new RejectionInfo();
			rejectionInfo.setSource(PaymentIngestionConstant.SYSTEM);
			rejectionInfo.setMessage(record.getErr().getValue());
			transitionInfo.setRejectionInfo(rejectionInfo);
			stateInfo.setDocStatus(PaymentIngestionConstant.UNCORELETED);
		} else {
			transitionInfo.setState(PaymentIngestionConstant.VALIDATED);
			transitionInfo.setStatus(PaymentIngestionConstant.IN_PROGRESS);
			stateInfo.setDocStatus(PaymentIngestionConstant.CORELETED);
		}
		transitionInfo.setRequestType(context.getEventType().getRequestType());
		transitionInfo.setOutcomeCategory(context.getEventType().getOutcomeCategory());
		transitionInfo.setLastActivityTs(context.getEventTime());
		transitionInfo.setApprovalScope(PaymentIngestionConstant.SFILE);
		stateInfo.setTransitionInfo(transitionInfo);
		stateInfo.setDomainSeqId(record.getInternalTxnId().getValue());
		paymentInstructionES.setStateInfo(stateInfo);

		return paymentInstructionES;
	}
}
