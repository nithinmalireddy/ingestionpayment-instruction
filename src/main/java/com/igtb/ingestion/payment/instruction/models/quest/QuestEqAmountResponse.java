package com.igtb.ingestion.payment.instruction.models.quest;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


/**
 * The Class QuestEqAmountResponse.
 */
@Getter
@Setter
public class QuestEqAmountResponse {

	/** The get equivalent amounts. */
	private List<AmountVariable> getEquivalentAmounts;
}
