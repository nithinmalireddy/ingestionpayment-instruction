package com.igtb.ingestion.payment.instruction.models.quest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class AmountVariable.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AmountVariable {

	/** The value. */
	public Double value;
	
	/** The currency code. */
	public String currencyCode;
	
	/** The ref. */
	public String ref;
}
