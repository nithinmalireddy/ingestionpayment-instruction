package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IntermediaryAgent {

	@JsonProperty("postalAddress")
	private PostalAddress postalAddress;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("bic")
	private String bic = null;

}
