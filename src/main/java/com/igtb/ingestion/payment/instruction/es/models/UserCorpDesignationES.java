package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UserCorpDesignationES.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class UserCorpDesignationES {

	/** The corporate. */
	private CorporateES corporate;

	/** The designation. */
	private DesignationES designation;
	
}
