package com.igtb.ingestion.payment.instruction.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.validation.Validation;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.exception.DLQException;

/**
 * The Class EventValidationUtil.
 */
@Component
public class EventValidationUtil {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(EventValidationUtil.class);
	
	/** The payment elastic properties. */
	@Autowired
	private PaymentElasticProperties paymentElasticProperties;
	
	/**
	 * Checks if is valid event.
	 *
	 * @param jsonNode the json node
	 * @param supportedRequestType the supported request type
	 * @param supportedServiceKey the supported service key
	 * @return true, if is valid event
	 */
	public boolean isValidEvent(final JsonNode jsonNode, final List<String> supportedRequestType,
			final List<String> supportedServiceKey) {
		
		/*
		 * This method will verify if the request can be consumed for processing or not
		 * based on the service type and requestType 
		 */

		final String serviceType = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.CONTEXT))
				.map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE))
				.map(eventType -> eventType.get(PaymentIngestionConstant.SERVICE_KEY))
				.map(serviceKey -> serviceKey.textValue()).orElse(null);
		
		final String requestType = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.CONTEXT))
				.map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE))
				.map(eventType -> eventType.get(PaymentIngestionConstant.REQUEST_TYPE))
				.map(reqType -> reqType.textValue()).orElse(null);

		final String sourceIdentity = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.CONTEXT))
				.map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_SOURCE))
				.map(eventSource -> eventSource.get(PaymentIngestionConstant.SOURCE_IDENTITY))
				.map(srcIdentity -> srcIdentity.textValue()).orElse(null);

		LOGGER.info("serviceType {} and requestType {} and sourceIdentity {}",serviceType,requestType,sourceIdentity);
		LOGGER.info("isValidEvent {}",(serviceType != null 
				&& Optional.ofNullable(supportedServiceKey.contains(serviceType.toLowerCase(Locale.getDefault()))).orElse(Boolean.FALSE)
				&& requestType != null 
				&& Optional.ofNullable(supportedRequestType.contains(requestType.toLowerCase(Locale.getDefault()))).orElse(Boolean.FALSE)
				&& sourceIdentity != null
				&& !sourceIdentity.startsWith(paymentElasticProperties.getApplicationName())));
		return  serviceType != null 
				&& Optional.ofNullable(supportedServiceKey.contains(serviceType.toLowerCase(Locale.getDefault()))).orElse(Boolean.FALSE)
				&& requestType != null 
				&& Optional.ofNullable(supportedRequestType.contains(requestType.toLowerCase(Locale.getDefault()))).orElse(Boolean.FALSE)
				&& sourceIdentity != null
				&& !sourceIdentity.startsWith(paymentElasticProperties.getApplicationName());

	}

	/**
	 * Validate bean.
	 *
	 * @param obj the obj
	 * @param id the id
	 * @return the list
	 * @throws DLQException the DLQ exception
	 */
	public List<String> validateBean(final Object obj, final String id) throws DLQException {
		final List<String> errorMessages = new ArrayList<>();

		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		validator.validate(obj).stream().forEach(violation -> {
			final StringBuilder messageSet = new StringBuilder(110);
			messageSet.append("\n >>>> Property Path:").append(violation.getPropertyPath())
			         .append(" <<<< \n >>>> Value that violated the constraint : ")
					.append(violation.getInvalidValue()).append(" <<<< \n >>>> Constraint : ")
					.append(violation.getMessage()).append(" <<<<");
			errorMessages.add(messageSet.toString());
		});

		if (errorMessages.isEmpty()) {
			return null;
		} else {
			LOGGER.info("Validation Failed for the payment instruction with this id --> {} and the Error Messages {}",
					id, errorMessages);
			throw new DLQException("Bean Validation failed, hence sending to DLQ {}");
		}
	}

}
