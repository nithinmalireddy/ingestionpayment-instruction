package com.igtb.ingestion.payment.instruction.config;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Data;

@Configuration
@Data
@ConfigurationProperties("fetchKey")
public class PshcReferenceDataInquiryProperties {
	
	private Map<String,String> fetchKeyEsTypeMap = new HashMap<>();
	private String esIndexName ;

}
