package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

	/**
	 * The Class ChargeAccount.
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Getter
	@Setter
	@ToString
	public class ChargeAccountES {

		/** The identification. */
	    @JsonProperty("identification")
	    public String identificationES;
	    
	    /** The name. */
	    @JsonProperty("name")
	    public String nameES;
	    
	    /** The scheme name. */
	    @JsonProperty("schemeName")
	    public String schemeNameES;
	    
	    /** The currency. */
	    @JsonProperty("currency")
	    public String currencyES;
	    


	}



