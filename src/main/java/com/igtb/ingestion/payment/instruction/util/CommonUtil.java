package com.igtb.ingestion.payment.instruction.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;

import lombok.extern.slf4j.Slf4j;

/**
 * The Utility Class providing having common methods.
 * 
 */
@Slf4j
public final class CommonUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkProcessingChannelUtil.class);
	/** Private Constructor. */
	private CommonUtil() {
		// To prevent instantiation.
	}

	
	/**
	 * Gets the value from json record
	 * 
	 * @param record
	 *            the json record
	 * @param valueType
	 *            type of value to fetch from json
	 * @return {@link String} the value fetched from record
	 */
	public static String getValue(final JsonNode record, final String valueType) {
		JsonNode data = null;

		final JsonNode context = Optional.ofNullable(record).map(rc -> rc.get(PaymentIngestionConstant.CONTEXT)).orElse(null);

		final Optional<JsonNode> eventType = Optional.ofNullable(context)
				.map(ctx -> ctx.get(PaymentIngestionConstant.EVENT_TYPE));
		final Optional<JsonNode> eventSource = Optional.ofNullable(context)
				.map(ctx -> ctx.get(PaymentIngestionConstant.EVENT_SOURCE));

		if (context != null) {
			data = context.get(valueType);
		}
		if (data == null && eventType.isPresent()) {
			data = eventType.get().get(valueType);
		}
		if (data == null && eventSource.isPresent()) {
			data = eventSource.get().get(valueType);
		}
		return data == null || data.isNull() ? null : data.textValue().trim();
	}

	
	/**
	 * Gets the value from json record
	 * 
	 * @param record
	 * 			JsonNode record.
	 * @param parentType
	 * 			Parent type.
	 * @param childType
	 * 			Child type.
	 * @return {@link Object} the value fetched from record
	 */
	public static Object getValueFromJsonNode(final JsonNode record, final String parentType, final String childType) {
		JsonNode data = null;

		if (parentType.isEmpty()) {
			data = Optional.ofNullable(record).map(rc -> rc.get(childType)).orElse(null);
		} else {
			final JsonNode parent = Optional.ofNullable(record).map(rc -> rc.get(parentType)).orElse(null);

			if (data == null && parent != null && parent.isArray()) {
				data = parent.get(0).get(childType);
			}
			if (data == null && parent != null && parent.isObject()) {
				data = parent.get(childType);
			}
		}

		if (data != null && data.isInt())
			{return data.asInt();}
		else if (data != null && data.isDouble())
			{return data.asDouble();}
		else if (data != null && data.isTextual())
			{return data.textValue().trim();}
		else if (data != null && data.isBoolean())
			{return data.asBoolean();}
		return data;
	}

	/**
	 * 
	 * Get ISO format from Date.
	 * 
	 * @param date
	 * 		The informed date.
	 * @return {@link String} ISO formatted date
	 * @throws DataNotValidException exception
	 * Get ISO format parseable string.
	 * 
	 * @param dateStr The informed date.
	 * @param formatter formatters
	 * @return {@link String} Parseable Date.
	 * @throws DataNotValidException exception date invalid
	 */
	public static Date getParseableDate(final String dateStr, final SimpleDateFormat ...formatter) throws DataNotValidException {
	
		try {
	    	return formatter[0].parse(dateStr);//if parsed by first formatter, means it is valid date
	    }
	    catch(ParseException e1) {
	    	try {
	    		LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_009 + " "
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_009_Desc);
	    		final Date actionApiDate = formatter[1].parse(dateStr);
	    		return formatter[0].parse(formatter[0].format(actionApiDate));//format and then parse by other formatter
	    	}
	    	catch(ParseException e2) {
	    		// TODO Auto-generated catch block
	    		LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_010 + " "
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_010_Desc);
				throw new DataNotValidException("Date received is not either of these expected formats (yyyy-MM-dd)/(dd-MMM-yyyy)");
	    	}
	    }
	}

	
	/**
	 * Get Concatted date time.
	 * 
	 * @param dateStr The date String.
	 * @return {@link String} the concatted string as per ISO format
	 */
	
	/**
	 * Gets the In-Flight Time of the events by calculating the diff between 
	 * current timestamp and the time event was generated.
	 * 
	 * @param body jsonbody received
	 * @return {@link Long} In-flight time of the request.
	 * @throws ParseException exception
	 */
	public static Long getInFlightTimeInSec(final JsonNode body) throws ParseException {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PaymentIngestionConstant.TIMESTAMP_FORMAT0);
		final String eventTime = CommonUtil.getValue(body, PaymentIngestionConstant.EVENTTIME);
		final LocalDateTime fromDate = LocalDateTime.parse(PaymentIngestionConstant.TIMESTAMP_FORMATTER0
				.format(PaymentIngestionConstant.TIMESTAMP_FORMATTER1.parse(eventTime)), formatter);
		if(log.isDebugEnabled()){log.debug("fromDate ->  "+  fromDate);}
		
		final LocalDateTime toDate = LocalDateTime.now();
		if(log.isDebugEnabled()) {log.debug("toDate ->  "+  toDate);}
		
		final long days = ChronoUnit.DAYS.between(fromDate, toDate);
		final long hours = ChronoUnit.HOURS.between(fromDate, toDate)%24;
		final long minutes = ChronoUnit.MINUTES.between(fromDate, toDate)%60;
		final long secs = ChronoUnit.SECONDS.between(fromDate, toDate)%60;
		
		log.info("Inflight time of the event is ->> {} days, {} hours, {} minutes, {} seconds.", days, hours, minutes, secs);
		return (long) ((days*24+hours)*60)+ minutes * 60 +secs;
	}
}
