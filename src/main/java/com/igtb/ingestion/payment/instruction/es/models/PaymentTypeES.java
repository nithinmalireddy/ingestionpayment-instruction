package com.igtb.ingestion.payment.instruction.es.models;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Payment Types DAO class.
 * 
 */
@ToString
@Getter
@Setter
public class PaymentTypeES {
	
	/** The payment type code. */
//	@NotNull
	private String code;
	
	/** The payment type description. */
	private String description;
		
}
