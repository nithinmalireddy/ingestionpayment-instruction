package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class AgentES.
 */
@ToString
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
public class AgentES {
	
	/** Creditor agent Id. */
//	@NotNull
	private String id;
	
	/** The bic. */
	private String bic;
	
	/** The key. */
	private String key;
	
	/** The name. */
	private String name;
	/** The address. */
	private AddressES address;
	
	/** The org. */
	private OrganisationES org;
	
	/** The scheme name. */
	//to be used to only for schemeNames for PI and SI
	@JsonIgnore
	private String schemeName;
	
	/** The type. */
	private String type;
    
    /** The is host bank. */
	private Boolean isHostBank;
    
    /** The parent bank. */
	private ParentBankES parentBank;
    
    /** The clearing code. */
	private String clearingCode;
    
    /** The clearing code type. */
	private String clearingCodeType;
    
    /** The bank proxy ES. */
	private BankProxyES bankProxyES;
	
    /** The String. */
    private String bicfi;
    
}
