package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CityES.
 */
@Getter
@Setter
public class CityES {
	
	/** The city code. */
	private String code;
	/** The city name. */
	private String name;

}
