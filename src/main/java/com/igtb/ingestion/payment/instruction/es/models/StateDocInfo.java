package com.igtb.ingestion.payment.instruction.es.models;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;


/**
 * The Class StateDocInfo.
 */
@Getter
@Setter
public class StateDocInfo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The doc status. */
	private String docStatus;
	
	/** The channel seq id. */
	private String channelSeqId;
	
	/** The type. */
	private String type;

}
