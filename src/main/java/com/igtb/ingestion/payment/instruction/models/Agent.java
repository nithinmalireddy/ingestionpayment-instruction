
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CreditorAgent.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Agent {

    /** The scheme name. */
    @JsonProperty("schemeName")
    public String schemeName;
    
    /** The identification. */
    @JsonProperty("identification")
    public String identification;
    
    /** The bank entity id. */
    @JsonProperty("bankEntityId")
    private String bankEntityId;

    /** The bank addnl info 1. */
    @JsonProperty("bankAddnlInfo1")
    private String bankAddnlInfo1;
    
    /** The bank code. */
    @JsonProperty("bankCode")
    public String bankCode;
    
    /** The branch code. */
    @JsonProperty("branchCode")
    public String branchCode;
    
    /** The bank name. */
    @JsonProperty("bankName")
    public String bankName;
    
    /** The branch name. */
    @JsonProperty("branchName")
    public String branchName;
    
    /** The address 1. */
    @JsonProperty("address1")
    public String address1;
    
    /** The address 2. */
    @JsonProperty("address2")
    public String address2;
    
    /** The address 3. */
    @JsonProperty("address3")
    public String address3;
    
    /** The city. */
    @JsonProperty("city")
    public String city;

    /** The country. */
    @JsonProperty("country")
    public String country;
    
    /** The type. */
    @JsonProperty("type")
    private String type;
    
    /** The String. */
    @JsonProperty("isHostBank")
    private String isHostBank;
    
    /** The String. */
    @JsonProperty("bicfi")
    private String bicfi;
    
    /** The bank id type. */
    @JsonProperty("bankIdType")
    private String bankIdType;
}
