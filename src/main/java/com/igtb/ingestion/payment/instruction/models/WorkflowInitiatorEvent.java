package com.igtb.ingestion.payment.instruction.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * The Class WorkflowInitiatorEvent.
 */
@Getter
@Setter
@ToString
public class WorkflowInitiatorEvent {
	
	/** The channel seq id. */
	private String channelSeqId;
	
	/** The domain seq id. */
	private String domainSeqId;
	
	/** The channel id. */
	private String channelId;
	
	/** The payload type. */
	private String payloadType;
	
	/** The conductor triggers. */
	private List<WkflwConductorTriggers> conductorTriggers;
	
}
