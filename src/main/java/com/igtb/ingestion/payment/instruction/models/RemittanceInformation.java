
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class RemittanceInformation.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class RemittanceInformation {

	/** The remit info 1. */
	@JsonProperty("remitInfo1")
	public String remitInfo1;
	
	/** The remit info 2. */
	@JsonProperty("remitInfo2")
	public String remitInfo2;
	
	/** The remit info 3. */
	@JsonProperty("remitInfo3")
	public String remitInfo3;
	
	/** The remit info 4. */
	@JsonProperty("remitInfo4")
	public String remitInfo4;
	
	/** The creditor reference information. */
	@JsonProperty("creditorReferenceInformation")
	public String creditorReferenceInformation; //NOPMD longVariable

}
