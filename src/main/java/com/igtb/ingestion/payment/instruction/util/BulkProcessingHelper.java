package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.api.action.commons.util.DateUtil;
import com.igtb.api.ingestion.commons.bulk.models.BulkStateInfo;
import com.igtb.api.ingestion.commons.bulk.stateInfo.BulkStateInfoUtil;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.ingestion.payment.instruction.config.AppPropsProvider;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.Metrics;
import com.igtb.ingestion.payment.instruction.es.models.MetricsObj;
import com.igtb.ingestion.payment.instruction.es.models.ParentSet;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.SetElementInfoES;
import com.igtb.ingestion.payment.instruction.es.models.TxnCurrency;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.bulk.Bulk;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class BulkProcessingHelper {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkProcessingHelper.class);
	
	/** The payment ingestion state info util. */
	private final PaymentIngestionHelper stateInfoHelper;
	
	/** The payment instruction builder. */
	private PaymentInstructionBuilder paymentInstructionBuilder;
	
	/** The props. */
	private final AppPropsProvider props;
	
	/** The object mapper. */
	private final ObjectMapper objectMapper;
	
	public List<BulkStateInfo> buildBulkStateInfo(final JsonNode event, final Context context, final String entity,
			final List<BulkStateInfo> existingStateInfoList) throws  IOException {
		LOGGER.debug("Enriching bulkStateInfo");
		UserInfo userInfo = new UserInfo();
		Optional.ofNullable(context.getRequester()).map(requester -> requester.getDomainId())
				.ifPresent(userInfo::setDomainName);

		Optional.ofNullable(context.getRequester()).map(requester -> requester.getId())
				.ifPresent(userInfo::setUserName);

		stateInfoHelper.enrichUserInfo(entity, userInfo);
		
		 // Call bulk state info population
	    List<BulkStateInfo> bulkStateInfoList = BulkStateInfoUtil.bulkStateInfoEnrichment(event, userInfo,
	    		existingStateInfoList, "PI Bulk", null);
	    
	    LOGGER.debug("bulkStateInfoList from common: {}", bulkStateInfoList);
	    
	    return bulkStateInfoList;
	}

	/**
	 * Builds the PI set bulk.
	 *
	 * @param bulkRequest the bulk request
	 * @param bulkStateInfo the bulk state info
	 * @param existingPiSet the existing pi set
	 * @return the payment instruction sets ES
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public PaymentInstructionSetsES buildPISetBulk(final Bulk bulkRequest, final BulkStateInfo bulkStateInfo,
			final PaymentInstructionSetsES existingPiSet) throws IOException {

		final PaymentInstructionSetsES paymentInstructionSetsES = new PaymentInstructionSetsES();
		paymentInstructionSetsES.setRecordSetId(bulkRequest.getBulkId());
		paymentInstructionSetsES.setType("bulk");
		paymentInstructionSetsES.setTemplate(existingPiSet.getTemplate());
		paymentInstructionSetsES.setUploadFileName(existingPiSet.getUploadFileName());
		paymentInstructionSetsES.setPartialApproval(bulkRequest.getPartialProcessing());
		paymentInstructionSetsES.setHasSets(bulkStateInfo.isHasSets());
		paymentInstructionSetsES.setStateInfo(bulkStateInfo.getStateInfo());
		
		if(existingPiSet.getType().equals("file")) {
			paymentInstructionSetsES.setParentSet(new ParentSet(existingPiSet.getRecordSetId(), existingPiSet.getSetName(), existingPiSet.getType()));
		} else {
			paymentInstructionSetsES.setParentSet(existingPiSet.getParentSet());
		}
		
		paymentInstructionSetsES.setCreated(DateUtil.dateToISOString(new Date()));
		paymentInstructionSetsES.setLastModified(DateUtil.dateToISOString(new Date()));
		
		if(bulkRequest.getPartialProcessing()) {
			paymentInstructionSetsES.setFileProcessingFlag("TLA");
		} else {
			paymentInstructionSetsES.setFileProcessingFlag("FLA");
		}
		
		//get payment reason
		final String[] serviceKeySplit = Optional.ofNullable(bulkRequest.getServiceKey()).map(key -> key.split("/")).orElse(new String[0]);
		if(serviceKeySplit.length > 0) {
			paymentInstructionSetsES.setPaymentReason(paymentInstructionBuilder.getPaymentReason(serviceKeySplit[1]));
		}
		
		paymentInstructionSetsES.setDebtorAccount(existingPiSet.getDebtorAccount());
		paymentInstructionSetsES.setCorporateEntity(existingPiSet.getCorporateEntity());
	
		final String currency = existingPiSet.getTxnCurrency().getCode();
		paymentInstructionSetsES.setTxnCurrency(new TxnCurrency(currency, currency));
		paymentInstructionSetsES.setInstructedAmountBaseCcy(new AmountES(bulkRequest.getBulkMetrics().getTotalAmount(),
				currency));
		
		//build file metrics
		final Metrics metrics = new Metrics();
		metrics.setTotal(new MetricsObj(bulkRequest.getBulkMetrics().getTotalCount(), 
				bulkRequest.getBulkMetrics().getTotalAmount(), currency));
		metrics.setValid(new MetricsObj(bulkRequest.getBulkMetrics().getTotalCount() - bulkRequest.getBulkMetrics().getRejectedValidationCount(), 
				bulkRequest.getBulkMetrics().getTotalAmount(), currency));
		metrics.setInvalid(new MetricsObj(bulkRequest.getBulkMetrics().getRejectedValidationCount(), 
				bulkRequest.getBulkMetrics().getRejectedValidationAmount(), currency));
		metrics.setRejected(new MetricsObj(null, bulkRequest.getBulkMetrics().getRejectedAmount(), currency));
		metrics.setApproved(new MetricsObj(null, bulkRequest.getBulkMetrics().getApprovedAmount(), currency));
		metrics.setMin(new MetricsObj(null, bulkRequest.getBulkMetrics().getMinAmount(), currency));
		metrics.setMax(new MetricsObj(null, bulkRequest.getBulkMetrics().getMaxAmount(), currency));
		paymentInstructionSetsES.setMetrics(metrics);

		paymentInstructionSetsES.setSetName("Bulk " + paymentInstructionSetsES.getRecordSetId());
		
		List<String> elements = bulkRequest.getTxnData().stream().map(txn -> bulkRequest.getFileId()+"."+txn.getInternalTxnId()).collect(Collectors.toList());
		paymentInstructionSetsES.setSetElementInfo(new SetElementInfoES(elements, null, null));
		
		LOGGER.debug("building PI Set for bulkId: {} completed", bulkRequest.getBulkId());

		return paymentInstructionSetsES;
	}

	public ObjectNode buildBulkUpdateObj(final Bulk bulkRequest, final BulkStateInfo bulkStateInfo, 
			final Metrics existingMetrics, final SetElementInfoES existingSetEleInfo) throws IOException {
		// build file metrics
		final String currency = props.getTargetBaseCcy();
		final Metrics metrics = new Metrics();
		final Integer rejectedCount = Optional.ofNullable(bulkRequest).map(obj -> obj.getTxnSlip().stream()
				.filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase("rejected")).count()).orElse((long) 0).intValue();
		final Integer skippedCount =  Optional.ofNullable(bulkRequest).map(obj -> obj.getTxnSlip().stream()
				.filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase("skipped")).count()).orElse((long) 0).intValue();
		final Integer approvedCount = existingMetrics.getValid().getCount() - (rejectedCount + skippedCount);

		metrics.setTotal(new MetricsObj(approvedCount+rejectedCount, bulkRequest.getBulkMetrics().getTotalAmount(), currency));
		metrics.setRejected(new MetricsObj(rejectedCount, bulkRequest.getBulkMetrics().getRejectedAmount(), currency));
		metrics.setApproved(new MetricsObj(approvedCount, bulkRequest.getBulkMetrics().getApprovedAmount(), currency));
		metrics.setUnacted(new MetricsObj(skippedCount, bulkRequest.getBulkMetrics().getSkippedAmount(), currency));
		metrics.setMin(new MetricsObj(null, bulkRequest.getBulkMetrics().getMinAmount(), currency));
		metrics.setMax(new MetricsObj(null, bulkRequest.getBulkMetrics().getMaxAmount(), currency));

		//build SetElementInfoES, ignoring skipped action
		List<String> skippedElements = Optional.ofNullable(bulkRequest).map(obj -> obj.getTxnSlip().stream()
				.filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase("skipped"))
				.map(txn -> bulkRequest.getFileId()+"."+txn.getInternalTxnId()).collect(Collectors.toList()))
				.orElse(new ArrayList<>());
		
		List<String> rejectedElements = Optional.ofNullable(bulkRequest).map(obj -> obj.getTxnSlip().stream()
				.filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase("rejected"))
				.map(txn -> bulkRequest.getFileId()+"."+txn.getInternalTxnId()).collect(Collectors.toList()))
				.orElse(new ArrayList<>());
		
		existingSetEleInfo.getElements().removeAll(skippedElements);
		
		final SetElementInfoES setElementInfoES = new SetElementInfoES(existingSetEleInfo.getElements(), rejectedElements, skippedElements);
		
		final ObjectNode bulkUpdateObj = objectMapper.createObjectNode();
		bulkUpdateObj.set("metrics", objectMapper.readValue(objectMapper.writeValueAsString(metrics), JsonNode.class));
		bulkUpdateObj.set("setElementInfo", objectMapper.readValue(objectMapper.writeValueAsString(setElementInfoES), JsonNode.class));
		bulkUpdateObj.set("stateInfo", objectMapper.readValue(objectMapper.writeValueAsString(bulkStateInfo.getStateInfo()), JsonNode.class));
		return bulkUpdateObj;
	}
	
}
