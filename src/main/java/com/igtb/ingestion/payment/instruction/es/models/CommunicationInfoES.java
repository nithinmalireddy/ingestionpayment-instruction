package com.igtb.ingestion.payment.instruction.es.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class CommunicationInfoES.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CommunicationInfoES {

	/** The contact person. */
	private String contactPerson;
	
	/** The mobile. */
	private String mobile;
	
	/** The phone. */
	private List<String> phone;
	
	/** The fax. */
	private String fax;
	
	/** The email. */
	private List<String> email;
	
	/** The web page. */
	private String webPage;

	@SuppressWarnings("unchecked")
	@JsonProperty("phone")
	private void getPhone(final Object phone) { // NOPMD unused private method
		if (phone != null) {
			if (phone.getClass().equals(ArrayList.class)) {
				this.phone = (List<String>) phone;
			} else {
				this.phone = Arrays.asList(phone.toString());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@JsonProperty("email")
	private void getEmail(final Object email) { // NOPMD unused private method
		if (email != null) {
			if (email.getClass().equals(ArrayList.class)) {
				this.email = (List<String>) email;
			} else {
				this.email = Arrays.asList(email.toString());
			}
		}
	}
}
