
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Attachment.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Attachment {

    /** The file id. */
    @JsonProperty("fileId")
    public String fileId;
    
    /** The file name. */
    @JsonProperty("fileName")
    public String fileName;
    
    /** The document type. */
    @JsonProperty("documentType")
    public String documentType;
    
    /** The format type. */
    @JsonProperty("formatType")
    public String formatType;
    
    /** The document size. */
    @JsonProperty("documentSize")
    public Long documentSize;

    /** The document remark. */
    @JsonProperty("documentRemark")
    public String documentRemark;
}
