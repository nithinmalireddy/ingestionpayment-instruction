package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

	/**
	 * The Class ChargesAccount.
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Getter
	@Setter
	@ToString
	public class ChargesAccountES {

	
	    /** The currency. */
	    @JsonProperty("currency")
	    public String currencyES;
	    
	    /** The number. */
	    @JsonProperty("number")
	    public String numberES;

	}



