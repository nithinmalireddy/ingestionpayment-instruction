package com.igtb.ingestion.payment.instruction.consumer;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.apache.camel.ProducerTemplate;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;
import com.igtb.ingestion.payment.instruction.service.PaymentIngestionServiceImpl;
import com.igtb.ingestion.payment.instruction.util.EventValidationUtil;
import com.igtb.ingestion.payment.instruction.models.Context;

import lombok.AllArgsConstructor;

/**
 * Consumer class which listens to the kafka topic.
 */
@Component
@DependsOn("paymentKafkaDLQProducer")
@AllArgsConstructor
public class PaymentKafkaPshcConsumer implements AcknowledgingMessageListener<String, String> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentKafkaPshcConsumer.class);

	/** concurrency consistency. */
	private final CountDownLatch latch = new CountDownLatch(1);

	/** Kafka producer to send to Dead Letter Queue. */
	private PaymentKafkaDLQProducer paymentKafkaDLQProducer;

	/** The payment ingestion service impl. */
	private PaymentIngestionServiceImpl serviceImpl;

	/** ObjectMapper to convert string to desired type. */
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	private PaymentKafkaProperties kafkaProperties;

	
	/**
	 * Kafka Message receiver. 1. Read Kafka Message 2. Validate the message if
	 * malformed message, write into Kafka DLQ and commit the message If valid,
	 * continue to ES. 3. Send to Elasticsearch if connection not established or ES
	 * client returns isSucceeded false, raise issue and fail the server. Do not
	 * commit the message
	 *
	 * @param data
	 *            the data
	 * @param acknowledgment
	 *            the acknowledgment
	 */
	@Override
	public void onMessage(final ConsumerRecord<String, String> data, final Acknowledgment acknowledgment) {
		Optional.ofNullable(data).ifPresent(record -> {
			try {
				final JsonNode jsonNode = objectMapper.readTree(record.value());
				LOGGER.info("Going to process the kafka message: {}", jsonNode, " on PaymentKafkaPshcConsumer");
				LOGGER.info("Backend request: {}"+jsonNode+" on kafka message on PaymentKafkaPshcConsumer has been received.. Sending message on Limits topic:: {}", kafkaProperties.getKafkaLimitsTopic());
				
				if(jsonNode.has(PaymentIngestionConstant.CONTEXT) && jsonNode.get(PaymentIngestionConstant.CONTEXT) != null) {
					JsonNode contextJsonNode = jsonNode.get(PaymentIngestionConstant.CONTEXT);
					Context context = objectMapper.readValue(contextJsonNode.toString(), Context.class);
					if(context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_REJECTED)) {
						LOGGER.info("Request status is "+context.getEventType().getStatus()+" hence proceeding to limits...");
						paymentKafkaDLQProducer.sendMessageToLimits(kafkaProperties.getKafkaLimitsTopic(), record.value());
					} else {
						LOGGER.info("Request status is "+context.getEventType().getStatus()+" hence can not proceed to limits...");
					}
				}
				
				serviceImpl.processKafkaMessagePshc(jsonNode);
				acknowledgment.acknowledge();
				latch.countDown();
//				LOGGER.info("Backend request on kafka message on PaymentKafkaPshcConsumer has been processed.. Sending message on Limits topic:: {}", kafkaProperties.getKafkaLimitsTopic());
//				paymentKafkaDLQProducer.sendMessageToLimits(kafkaProperties.getKafkaLimitsTopic(), record.value());
				
			} catch (IOException | RequeueException | InterruptedException | DLQException
					| MessageProcessingException e) {
				 LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_TECH_ST_UPDT_001 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
							+ PaymentIngestionErrorCodeConstants.ERR_TECH_ST_UPDT_001_Desc);
				LOGGER.error("Exception at PaymentKafkaPshcConsumer as: {}", e);
				processedDataToDlq(data, e.getMessage(), acknowledgment);
			}
		});
	}

	/**
	 * Processed data to dlq.
	 *
	 * @param data the data
	 * @param msg the msg
	 * @param acknowledgment the acknowledgment
	 */
	private void processedDataToDlq(final ConsumerRecord<String, String> data, final String msg,
			final Acknowledgment acknowledgment) {
		paymentKafkaDLQProducer.sendToDLQ(data,msg);
		acknowledgment.acknowledge();
		latch.countDown();
		LOGGER.info("Invalid Payload. Hence the payload is directed to DLQ. Error Message ::{}", msg);
	}
}
