package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.igtb.ingestion.payment.instruction.models.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class BusinessField.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessField { //NOPMD TooManyFields

    /** The bp id. */
    private String bpId;
    
    /** The file name. */
    private String fileName;
    
    /** The failed txn amt. */
    private Double failedTxnAmt;
    
    /** The channel. */
    private String channel;
    
    /** The type. */
    private String type;
    
    /** The uploaded date. */
    private String uploadedDate;
    
    /** The internal file id. */
    private String internalFileId;
    
    /** The channel processing flag. */
    private Boolean channelProcessingFlag;
    
    /** The successful txn count. */
    private Integer successfulTxnCount;
    
    /** The reject reason. */
    private String rejectReason;
    
    /** The max txn amount. */
    private Double maxTxnAmount;
    
    /** The debit accounts. */
    private DebitAccounts debitAccounts;
    
    /** The file reference number. */
    private String fileReferenceNumber;
    
    /** The file template name. */
    private String fileTemplateName;
    
    /** The failed txn count. */
    private Integer failedTxnCount;
    
    /** The successful txn amt. */
    private Double successfulTxnAmt;
    
    /** The currency. */
    private Currency currency;
    
    /** The service key. */
    private String serviceKey;
    
    /** The min txn amount. */
    private Double minTxnAmount;
    
    /** The entity. */
    private Entity entity;
    
    /** The status. */
    private String status;

}
