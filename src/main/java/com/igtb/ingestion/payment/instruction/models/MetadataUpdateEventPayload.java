package com.igtb.ingestion.payment.instruction.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class MetadataUpdateEventPayload.
 */
@Getter
@Setter
public class MetadataUpdateEventPayload {
	
	/** The channel seq id. */
	private String channelSeqId;
	
	/** The domain seq id. */
	private String domainSeqId;
	
	/** The transaction file relation. */
	private TransactionFileRelation transactionFileRelation;
	
	
	/**
	 * Instantiates a new metadata update event payload.
	 *
	 * @param channelSeqId the channel seq id
	 * @param domainSeqId the domain seq id
	 * @param transactionFileRelation the transaction file relation
	 */
	public MetadataUpdateEventPayload(final String channelSeqId, final String domainSeqId, TransactionFileRelation transactionFileRelation ) { //NOPMD UseVarargs
		super();
		this.channelSeqId = channelSeqId;
		this.domainSeqId = domainSeqId;
		this.transactionFileRelation=transactionFileRelation;
	}
		
}
