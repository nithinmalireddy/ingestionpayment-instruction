package com.igtb.ingestion.payment.instruction.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * ElasticSearch Property file. During boot startup, binding takes place with
 * the respective external properties.
 * 
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "rabbitMq")
@Getter
public class PaymentRedisProperties {
	
	/** maximum wait time in connection retry. */
	@Value("${Redis.Sentinel.FailoverWaitTimeMS}")
	private int maxWaitTime;

	/** Connection retry in milli seconds. */
	@Value("${redisRetryIntervalInMs}")
	private int retryIntervalInMs;
	

}
