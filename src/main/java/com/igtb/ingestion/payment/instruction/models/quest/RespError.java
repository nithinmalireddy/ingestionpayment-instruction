package com.igtb.ingestion.payment.instruction.models.quest;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The RespError.
 *
 * @author gautam.garg
 */

/** Getter */
@Getter

/** Setter */
@Setter

/** (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@ToString
public class RespError implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8519816326372797413L;
	
	/** The status code. */
	private String statusCode;
	
	/** The key. */
	private String key;
	
	/** The inputs. */
	private String inputs;
	
	/** The message. */
	private String message;
	
}
