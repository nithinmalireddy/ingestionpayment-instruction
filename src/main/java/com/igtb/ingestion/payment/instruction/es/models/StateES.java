package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
public class StateES {

	/** The state code. */
	private String code;
	/** The state name. */
	private String name;
}
