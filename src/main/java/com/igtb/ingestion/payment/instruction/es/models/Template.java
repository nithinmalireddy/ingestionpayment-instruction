
package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class Template.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@NoArgsConstructor
public class Template {

    /** The id. */
    private String id;
    
    /** The template name. */
    private String templateName;

    /**
     * Instantiates a new template.
     *
     * @param id the id
     * @param templateName the template name
     */
    public Template(final String id, final String templateName) {
		this.id = id;
		this.templateName = templateName;
	}
}
