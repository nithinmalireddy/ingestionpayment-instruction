package com.igtb.ingestion.payment.instruction.exception;

/**
 * Exception thrown when an invalid id is used for amend/delete/hold/unhold/cancel 
 * and data for that id is not found in elastic search/ 
 * or accounts details for the given creditor/debtor account GUID not found
 * 
 */
public class QuestException extends Exception{

	/** Serial Version */
	private static final long serialVersionUID = -1019374980870273656L;

	
	/**Constructs a new exception with the specified detail message. 
	 * The cause is not initialized, and may subsequently be initialized by a call.
	 * @param message The exception message
	 */
	public QuestException(final String message) {
		super(message);
	}
}
