package com.igtb.ingestion.payment.instruction.producer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;

/**
 * 
 * This class gets called when an invalid payload is identified and this class
 * helps to push the payload to the DLQ topic.
 * 
 */
@Component
public class PaymentKafkaDLQProducer {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentKafkaDLQProducer.class);

	/** The kafka properties. */
	@Autowired
	private PaymentKafkaProperties kafkaProperties;

	/**
	 * Kafka template. used to send data to Kafka
	 */
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	/**
	 * Send to DLQ.
	 *
	 * @param data the data
	 */
	public void sendToDLQ(final ConsumerRecord<String, String> data,String errorMsg) {
		final JsonNode dataForDLQ = createDataForProducer(data,errorMsg);
		final ProducerRecord<String, String> producerRecord = new ProducerRecord<>(kafkaProperties.getKafkaDlqTopic(), dataForDLQ.toString());
		kafkaTemplate.send(producerRecord);
	}
	public void sendToDLQ(final JsonNode data) {
		final ProducerRecord<String, String> producerRecord = new ProducerRecord<>(kafkaProperties.getKafkaDlqTopic(), data.toString());
		kafkaTemplate.send(producerRecord);
	}
	public void sendKafkaMessageForAEHold(final ProducerRecord<String, String> data) {
		kafkaTemplate.send(data);
	}
	
	public <T> void sendMessageToLimits(String kafkaTopic,String limitsRequest) {
        
		ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(kafkaTopic, limitsRequest);
				
	    future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
	    	
	        @Override
	        public void onSuccess(SendResult<String, String> result) {
	        	LOGGER.info("Sent message=[" + limitsRequest + "] with offset=[" + result.getRecordMetadata().offset() + "]");
	        }
	        @Override
	        public void onFailure(Throwable ex) {
	        	LOGGER.info("Unable to send message=[" + limitsRequest + "] due to : " + ex.getMessage());
	        }
	});
}

	/**
	 * Data is sent to DLQ only when the data is not in expected format. DLQ message
	 * should provide the following 1. Topic name 2. offset of the record 3.
	 * partition of the topic 4. message received 5. Consumer app name 6. Consumer
	 * group id
	 */
	private JsonNode createDataForProducer(final ConsumerRecord<String, String> data,String errorMsg) {
		final JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		final ObjectNode recordForProducer = nodeFactory.objectNode();
		recordForProducer.set(PaymentIngestionConstant.OFF_SET, nodeFactory.numberNode(0));
		recordForProducer.set(PaymentIngestionConstant.PARTITION, nodeFactory.numberNode(0));
		recordForProducer.set(PaymentIngestionConstant.CONSUMER_APP_NAME, nodeFactory.textNode(PaymentIngestionConstant.PAYMENT_STATE_INGESTION));
		recordForProducer.set(PaymentIngestionConstant.CONSUMER_GRP_ID, nodeFactory.textNode(kafkaProperties.getGroupId()));
		recordForProducer.set(PaymentIngestionConstant.ERR_TYPE, nodeFactory.textNode(errorMsg));
		recordForProducer.set(PaymentIngestionConstant.RECIEVED_MSG, nodeFactory.textNode(data.value()));
		return recordForProducer;
	}
}
