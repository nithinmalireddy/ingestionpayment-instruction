package com.igtb.ingestion.payment.instruction.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class PaymentIngestionProperties {

	/** The FAE Url. */
	@Value("${FAE.Url}")
	private String faeUrl;

	@Value("${FAE.dummyJWT}") 
	private String dummyJWT;

	@Value("${FAE.newJWTEndPoint}") 
	private String newJWTEndPoint;

	@Value("${FAE.iGTBD-AtomicAPI-SharedKey}") 
	private String iGTBDAtomicAPISharedKey;

	@Value("${FAE.index}")
	private String index;
	
	@Value("${FAE.type}")
	private String type;
	
	@Value("${FAE.es_hit_count}")
	private String esHitCount;
	
	@Value("${FAE.es_hits}")
	private String esHits;
	
	@Value("${FAE.es_source}")
	private String esSource;

	@Value("${DeclineRTP.Url}")
	private String declineRtpUrl;
	
	/** The UnwindCallToken Url. */
	@Value("${UnwindCallToken.authToken}")
	private String unwindCallAuthToken;

	/** The UNWIND Url. */
	@Value("${Unwind.Url}")
	private String UnwindUrl;
	
	/** The Rest Call time out. */
	@Value("${REST_CALL_TIMEOUT}")
	private int restCallTimeout;

	/** The RefData Url. */
	@Value("${RefData.Url}")
	private String RefDataUrl;
	
	@Value("${groupChannelSeqIdFetch_SleepMs}")
	private int groupChannelSeqIdFetch_SleepMs;
	
	}
