package com.igtb.ingestion.payment.instruction.util;

/**
 * The Class BulkIdGenerator is used for generating unique file Id for uploaed file, report and attachment.
 */
public class BulkIdGenerator {

    /** The template. */
    private final String template;
    
    /** The creation time millis. */
    private final long creationTimeMillis;
    
    /** The last time millis. */
    private long lastTimeMillis;
    
    /** The discriminator. */
    private long discriminator;

    /**
     * Instantiates a new file id generator.
     *
     * @param template the template
     */
    public BulkIdGenerator(final String template){
        this.template=template;
        this.creationTimeMillis = System.currentTimeMillis();
        this.lastTimeMillis = creationTimeMillis;
    }

    /**
     * Creates the fileid.
     *
     * @return the string
     */
    public String createId() {
        String id;
        long now ;
        synchronized(this)
        {
        	now=System.currentTimeMillis();
        }
        if (now == lastTimeMillis) {
            ++discriminator;
        } else {
            discriminator = 0;
        }

        id = String.format("%s-%d-%d", template, creationTimeMillis,discriminator);
        lastTimeMillis = now;

        return id;
    }
}
