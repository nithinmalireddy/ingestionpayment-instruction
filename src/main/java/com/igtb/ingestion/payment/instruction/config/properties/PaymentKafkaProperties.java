package com.igtb.ingestion.payment.instruction.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

import lombok.Getter;

/**
 * Kafka Property file. During boot startup, binding takes place with
 * the respective external properties.
 * 
 */
@Configuration
@EnableKafka
@Getter
public class PaymentKafkaProperties {

	/** The bootstrap servers. */
	@Value("${kafka.BootstrapServers}")
	private String bootstrapServers;

	/** The auto offset reset. */
	@Value("${kafka.config.autooffsetreset}")
	private String autoOffsetReset;

	/** The group id. */
	@Value("${kafka.consumer.group-id.config}")
	private String groupId;

	/** The kafka topic. */
	@Value("${kafka.topic}")
	private String kafkaTopic;

	/** The kafka topic. */
	@Value("${kafka.topicpshc}")
	private String kafkaTopicPshc;

	/** The kafka topic. */
	@Value("${kafka.dlq.topic}")
	private String kafkaDlqTopic;

     /** The kafka topic. */
	@Value("${kafka.notificationtopic}")
	private String kafkaNotifctnTopic;
	
	/** The kafka topic for Limits. */
	@Value("${kafka.limitstopic}")
	private String kafkaLimitsTopic;

}