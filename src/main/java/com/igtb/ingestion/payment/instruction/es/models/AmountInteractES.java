package com.igtb.ingestion.payment.instruction.es.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Amount
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-09T14:08:50.733+05:30")
public class AmountInteractES {
  @JsonProperty("amount")
  private Double amount = null;

  @JsonProperty("currency")
  private String currency = null;

  public AmountInteractES amount(Double amount) {
    this.amount = amount;
    return this;
  }

   /**
   * Transaction amount. OrgnlInstdAmt name in cancel money.
   * @return amount
  **/
  @ApiModelProperty(value = "Transaction amount. OrgnlInstdAmt name in cancel money.")
  @JsonProperty("amount")
  public Double getAmount() {
    return amount;
  }

  @JsonProperty("amount")
  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public AmountInteractES currency(String currency) {
    this.currency = currency;
    return this;
  }

   /**
   * Transaction currency. OrgnlInstdAmt ccy field in cancel money
   * @return currency
  **/
  @ApiModelProperty(value = "Transaction currency. OrgnlInstdAmt ccy field in cancel money")
  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }

  @JsonProperty("currency")
  public void setCurrency(String currency) {
    this.currency = currency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AmountInteractES amount = (AmountInteractES) o;
    return Objects.equals(this.amount, amount.amount) &&
        Objects.equals(this.currency, amount.currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, currency);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Amount {\n");
    
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}


