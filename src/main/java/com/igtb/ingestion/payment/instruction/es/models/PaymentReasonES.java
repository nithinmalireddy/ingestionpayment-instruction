package com.igtb.ingestion.payment.instruction.es.models;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Payment Reason DAO class.
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentReasonES {
	
	/** The payment reason code. */
//	@NotNull
	private String code;
	
	/** The payment reason description. */
	private String description;
		
}
