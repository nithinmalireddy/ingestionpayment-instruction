package com.igtb.ingestion.payment.instruction.es.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * AmountES class.
 * 
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AmountES {
	
	/** The Amount value. */
//	@NotNull
	private Double value;

	/** The Amount curreny code. */
//	@NotNull
	private String currencyCode;
	
	
	
}
