package com.igtb.ingestion.payment.instruction.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import com.igtb.api.ingestion.commons.stateinfo.models.NextAction;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.ingestion.payment.instruction.constant.DMTransitionStateConstants;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.OutcomeCategoryEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.RequestEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.ResourceEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStatusEnum;

/**
 * UI Status Uility Class.
 * 
 * UI-Status Logic formed on the basis of this page::
 * 
 * https://igtbworks.atlassian.net/wiki/spaces/API/pages/963936624/State+Ingestion+-+StateInfo+population#StateIngestion-StateInfopopulation-5.1)nextAction.key-descmapping
 *
 */
// @Slf4j
public class UIStatusUtil {

	/**
	 * The Enum UIStatusEnum.
	 */
	public enum UIStatusEnum {

		/** The draft. */
		DRAFT("DRAFT"),

		/** The initiated. */
		INITIATED("INITIATED"),

		/** The authorized. */
		AUTHORIZED("AUTHORIZED"),

		/** The pending approval. */
		// For 'In Workflow' Tab,
		PENDING_APPROVAL("PENDING APPROVAL", "authorize"),

		/** The pending release. */
		PENDING_RELEASE("PENDING RELEASE", "release"),

		/** The pending verification. */
		PENDING_VERIFICATION("PENDING VERIFICATION", "verify"),

		/** The rejected. */
		REJECTED("REJECTED"),

		/** The sent to bank pi pt. */
		// For 'Sent To Bank' Tab,
		SENT_TO_BANK_PI("SENT TO BANK", PaymentIngestionConstant.RELEASE_TO_BANK, new String[] { "PI" },
				new String[] { "unhold" }),

		/** The sent to Receiver pi pt. */
		// For 'Sent To Receiver' Tab,
		SENT_TO_RECEIVER_PI("SENT TO RECEIVER", PaymentIngestionConstant.RELEASE_TO_BANK, new String[] { "PI" },
				new String[] { "unhold" }),

		/** The hold pi. */
		HOLD_PI("ON HOLD", "", new String[] { "PI" }, new String[] { "hold" }),

		/** The cancelled. */
		CANCELLED("CANCELLED", "", null, new String[] { "cancel" }),

		/** The posted. */
		// For 'Posted' Tab,
		POSTED("POSTED", "", null, new String[] { "add", "amend" }),

		/** The trashed. */
		// For miscellaneous tabs.
		TRASHED("TRASHED");

		/** The ui status str. */
		public String uiStatusStr;

		/** The next action. */
		private String nextAction;

		/** The applicable resource types. */
		private String[] applicableResourceTypes;

		/** The applicable request types. */
		private String[] applicableRequestTypes;

		/** The rejected tab state list. */
		private static List<TransitionStateEnum> rejectedTabStateList = new ArrayList<>();

		/** The release to bank next action list. */
		private static List<String> releaseToBankNextActionList;

		static {
			releaseToBankNextActionList = Arrays.asList(
					new String[] { PaymentIngestionConstant.RELEASE_TO_BANK, "bank-to-ack", "bank-to-confirm" });
			rejectedTabStateList.addAll(Arrays.asList(new TransitionStateEnum[] { TransitionStateEnum.val_failure,
					TransitionStateEnum.rejected, TransitionStateEnum.cancelled, // this 'cancelled' represents,////
																					// cancelled by user himself
					TransitionStateEnum.release_rejected, TransitionStateEnum.release_failure,
					TransitionStateEnum.backend_rejected }));
		}

		/**
		 * Instantiates a new UI status enum.
		 *
		 * @param desc
		 *            the desc
		 */
		private UIStatusEnum(final String desc) {
			uiStatusStr = desc;
		}

		/**
		 * Instantiates a new UI status enum.
		 *
		 * @param desc
		 *            the desc
		 * @param nextAction
		 *            the next action
		 */
		private UIStatusEnum(final String desc, final String nextAction) {
			this(desc);
			this.nextAction = nextAction;
		}

		/**
		 * Instantiates a new UI status enum.
		 *
		 * @param desc
		 *            the desc
		 * @param nextAction
		 *            the next action
		 * @param applicableResourceTypes
		 *            the applicable resource types
		 * @param applicableRequestTypes
		 *            the applicable request types
		 */
		private UIStatusEnum(final String desc, final String nextAction, final String[] applicableResourceTypes,
				final String[] applicableRequestTypes) {
			this(desc, nextAction);
			this.applicableResourceTypes = applicableResourceTypes;
			this.applicableRequestTypes = applicableRequestTypes;
		}

		/**
		 * Gets the UI status str.
		 *
		 * @return the UI status str
		 */
		private String getUIStatusStr() {
			return uiStatusStr;
		}

		/**
		 * Gets the UI status.
		 *
		 * @param transitionStatus
		 *            the transition status
		 * @param transitionState
		 *            the transition state
		 * @param nextAction
		 *            the next action
		 * @param resourceType
		 *            the resource type
		 * @param outcomeCategory
		 *            the outcome category
		 * @param requestType
		 *            the request type
		 * @return the UI status
		 */
		/*
		 * This method only cares about the UI status for transitional events and not
		 * active record.
		 * 
		 * Active Record status will sent by IPSH. It comprises of :::: -------
		 * -----------------------------------------------------------------------------
		 * 'POSTED', (for CBX 'create' request(both PI and SI) -->> backend_processed)
		 * 
		 * 'CANCELLLED', (for CBX 'cancel'(both PI and SI) request -->>
		 * backend_processed)
		 * 
		 * 'ON-HOLD', (for CBX 'hold' request(for PI) -->> backend_processed)
		 * 
		 * 'PAUSED', (for CBX 'hold' request(for SI) -->> backend_processed)
		 * 
		 * 'SENT TO BANK'(for CBX 'unhold' request(for PI) -->> backend_processed),
		 * 
		 * 'ACTIVE'(for CBX 'unhold' request(for SI) -->> backend_processed)
		 * -----------------------------------------------------------------------------
		 * 
		 */
		public static String getUIStatus(final TransitionStatusEnum transitionStatus,
				final TransitionStateEnum transitionState, String nextAction, final ResourceEnum resourceType,
				final OutcomeCategoryEnum outcomeCategory, final RequestEnum requestType) {

			final String resTypeAbbr = Optional.ofNullable(resourceType).map(resp -> resp.getAbbrev()).orElse(null);
			// if one of the 'release-to-bank', 'bank-to-ack', 'bank-to-confirm'
			if (releaseToBankNextActionList.contains(nextAction)) {
				nextAction = PaymentIngestionConstant.RELEASE_TO_BANK;
			}

			// UI Status Logic.....begins

			// if transitionState -> 'draft', then DRAFT
			if (TransitionStateEnum.draft.equals(transitionState)) {
				return DRAFT.getUIStatusStr();
			}

			// if transitionState -> 'trashed', then TRASHED
			if (TransitionStateEnum.trashed.equals(transitionState)) {
				return TRASHED.getUIStatusStr();
			}

			// if transitionState -> 'rejected, val_failure, release_rejected,
			// release_failure,cancelled, backend_rejected', then REJECTED
			if (rejectedTabStateList.contains(transitionState)) {
				return REJECTED.getUIStatusStr();
			}

			// check if transition status is IN-PROGRESS
			if (TransitionStatusEnum.IN_PROGRESS.equals(transitionStatus)) {

				// check the outcome category
				if (OutcomeCategoryEnum.CHANNEL_STATE_UPDATE.equals(outcomeCategory)) {

					// since nextaction wont be present till 'val_success', or 'val_timeout',
					// so dont check for 'nextAction'
					if (Arrays.asList(DMTransitionStateConstants.ENTRY_STATES).contains(transitionState)) {
						return INITIATED.getUIStatusStr();
					}
				}

				// else, get w.r.t the next-action
				return resolveEnumOnNextActionAndThenResourceType(nextAction, resTypeAbbr);
			}

			// if transitionState is 'backend_processed'
			if (TransitionStateEnum.backend_processed.equals(transitionState)) {
				return resolveEnumFromRequestTypeAndThenResourceType(requestType.toString(), resTypeAbbr);
			}

			// UI Status Logic.....ends
			return null;
		}

		/**
		 * Resolve enum from request type and then resource type.
		 *
		 * @param requestType
		 *            the request type
		 * @param resourceType
		 *            the resource type
		 * @return the string
		 */
		private static String resolveEnumFromRequestTypeAndThenResourceType(final String requestType,
				final String resourceType) {
			String uiStatusStr = null;
			for (final UIStatusEnum e : UIStatusEnum.values()) {
				// first filter wr.t 'applicable request Types'
				if (Optional.ofNullable(e).map(statusEnum -> statusEnum.applicableRequestTypes)
						.filter(requestTypes -> requestTypes != null).map(requestTypes -> Arrays.asList(requestTypes))
						.filter(appRequestTypesList -> appRequestTypesList.contains(requestType)).isPresent()) {

					if (ArrayUtils.isEmpty(e.applicableResourceTypes)) {
						uiStatusStr = e.getUIStatusStr();
					}

					// then if resourceType is to be considered, then filter w.r.t 'resource types'
					if (Optional.ofNullable(e).map(statusEnum -> statusEnum.applicableResourceTypes)
							.filter(resourceTypes -> resourceTypes != null)
							.map(resourceTypes -> Arrays.asList(resourceTypes))
							.filter(appResTpList -> appResTpList.contains(resourceType)).isPresent()) {
						uiStatusStr = e.getUIStatusStr();
					} else {
						continue;
					}
				}
			}
			return uiStatusStr;
		}

		/**
		 * Resolve enum on next action and then resource type.
		 *
		 * @param nextAction
		 *            the next action
		 * @param resourceType
		 *            the resource type
		 * @return the string
		 */
		private static String resolveEnumOnNextActionAndThenResourceType(final String nextAction,
				final String resourceType) {
			for (final UIStatusEnum e : UIStatusEnum.values()) {
				if (Optional.ofNullable(e).map(statusEnum -> statusEnum.nextAction)
						.filter(nxAction -> StringUtils.isNotBlank(nxAction) && nxAction.equals(nextAction))
						.isPresent()) {

					if (ArrayUtils.isEmpty(e.applicableResourceTypes)) {
						return e.getUIStatusStr();
					}

					// then if resourceType is to be considered, then filter w.r.t 'resource types'
					if (Optional.ofNullable(e).map(statusEnum -> statusEnum.applicableResourceTypes)
							.filter(resourceTypes -> resourceTypes != null)
							.map(resourceTypes -> Arrays.asList(resourceTypes))
							.filter(appResTpList -> appResTpList.contains(resourceType)).isPresent()) {
						return e.getUIStatusStr();
					} else {
						continue;
					}
				}
			}
			return null;
		}
	};

	/**
	 * Common Utility method to get the CBX specific UI Status to display to CBX
	 * user.
	 * 
	 * @param stateInfo
	 *            current request's stateinfo
	 * @param typeName
	 *            typename
	 * @return CBX UI Status
	 */
	public static String getCBXUIStatus(final StateInfo stateInfo, final String typeName) {

		final TransitionInfo transitionInfo = Optional.ofNullable(stateInfo).map(stInfo -> stInfo.getTransitionInfo())
				.orElse(null);
		final NextAction nextAction = Optional.ofNullable(stateInfo).map(stInfo -> stInfo.getNextAction()).orElse(null);

		final TransitionStatusEnum transitionStatus = Optional.ofNullable(transitionInfo)
				.map(transInfo -> transInfo.getStatus())
				.map(transStatus -> TransitionStatusEnum.getEnumByString(transStatus)).orElse(null);

		final OutcomeCategoryEnum outcomeCategory = Optional.ofNullable(transitionInfo)
				.map(transInfo -> transInfo.getOutcomeCategory())
				.map(outCat -> OutcomeCategoryEnum.getEnumByString(outCat)).orElse(null);

		final TransitionStateEnum transitionState = Optional.ofNullable(transitionInfo)
				.map(transInfo -> transInfo.getState())
				.map(transState -> EnumUtils.getEnum(TransitionStateEnum.class, transState)).orElse(null);

		final String nextActionStr = Optional.ofNullable(nextAction).map(nxAc -> nxAc.getKey()).orElse(null);

		final ResourceEnum resourceType = Optional.ofNullable(typeName)
				.map(tpNm -> EnumUtils.getEnum(ResourceEnum.class, tpNm)).orElse(null);

		final RequestEnum requestType = Optional.ofNullable(transitionInfo).map(transInfo -> transInfo.getRequestType())
				.map(rt -> EnumUtils.getEnum(RequestEnum.class, rt)).orElse(null);

		return UIStatusEnum.getUIStatus(transitionStatus, transitionState, nextActionStr, resourceType, outcomeCategory,
				requestType);
	}
}
