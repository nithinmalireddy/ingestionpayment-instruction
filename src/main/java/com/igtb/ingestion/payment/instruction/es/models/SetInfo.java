package com.igtb.ingestion.payment.instruction.es.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class SetInfo.
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SetInfo {

	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	/** The type. */
	private String type;
	
	/** The valid count. */
	private Integer validCount;

}
