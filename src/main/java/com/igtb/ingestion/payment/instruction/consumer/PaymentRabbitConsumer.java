package com.igtb.ingestion.payment.instruction.consumer;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import org.apache.camel.Body;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.api.action.commons.exceptions.ProcessingException;
import com.igtb.ingestion.payment.instruction.config.EventValidationConfig;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;
import com.igtb.ingestion.payment.instruction.service.PaymentIngestionServiceImpl;
import com.igtb.ingestion.payment.instruction.util.EventValidationUtil;

import lombok.AllArgsConstructor;

/**
 * The Class PaymentRabbitConsumer.
 */
@Component
@AllArgsConstructor
public class PaymentRabbitConsumer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentRabbitConsumer.class);

	/** the service implementation class for limits. */
	private final PaymentIngestionServiceImpl serviceImpl;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;
	
	/** The event validation util. */
	private final EventValidationUtil eventValidationUtil;
	
	/** The event validation config. */
	private final EventValidationConfig eventValidationConfig;
	private PaymentKafkaDLQProducer paymentKafkaDLQProducer;

	/**
	 * Rabbit listener.
	 *
	 * @param event the json node
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ProcessingException the processing exception
	 * @throws RequeueException the requeue exception
	 * @throws DLQException the DLQ exception
	 * @throws InterruptedException the interrupted exception
	 * @throws MessageProcessingException the message processing exception
	 */
	public void rabbitListener(final @Body JsonNode event) throws Exception {
		LOGGER.info("Start rabbitListener() with evenId::::: {}", event.get(PaymentIngestionConstant.ID));
		try {
			LOGGER.debug("Validating event for single PI for eventId: {}", event.get(PaymentIngestionConstant.ID));
			if (!event.isNull()) {
				final Map<String, ArrayList<String>> singlePIConfig = eventValidationConfig.getValidation().get(PaymentIngestionConstant.PAYMENT_INSTRUCTION_EVENTS);
				if (eventValidationUtil.isValidEvent(event,singlePIConfig.get(PaymentIngestionConstant.SUPPORTED_REQUESTD_TYPE), singlePIConfig.get(PaymentIngestionConstant.SUPPORTED_SERVICE_KEY))) {
					try {
						serviceImpl.processRabbitMessage(event);
					} catch (DataNotValidException | DataNotFoundException e) {
						LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_084 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
								+ PaymentIngestionErrorCodeConstants.ERR_PAYING_084_Desc);
						LOGGER.error("Error in possing for event id: {} as : {}", event.get(PaymentIngestionConstant.ID), e);
						throw new DLQException(e.getMessage()); 
					}
					LOGGER.info("Request is processed for eventId: {}", event.get(PaymentIngestionConstant.ID));

				} else {
					LOGGER.error("For Single PI, Invalid Service Key or request type or Source Identity, hence ignoring the event id: {}", event.get(PaymentIngestionConstant.ID));
				}
			}
			else {
				LOGGER.info("Event for single PI for :::::::: {}", event);
			}
		} catch (IOException | RequeueException e) {
			LOGGER.error("Exception with RequeueException ",e);
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_085 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_085_Desc);
			LOGGER.error("IOException at rabbitListener recevied for eventId: {} as ===> {}", event.get(PaymentIngestionConstant.ID), e);
			validateRequeueExpirationForIOExceptionThread(event.get(PaymentIngestionConstant.CONTEXT));
			processedDataToDlq(event, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_086 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_086_Desc);
			LOGGER.error("Exception at rabbitListener recevied for eventId: {} as ===> {}", event.get(PaymentIngestionConstant.ID), e);
			LOGGER.error("Exception at rabbitListener recevied for eventId:" , e);
			processedDataToDlq(event, e.getMessage());
		}
	}

	/**
	 * Validate requeue expiration for IO exception thread.
	 *
	 * @param contextNode the context node
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 * @throws DLQException the DLQ exception
	 * @throws RequeueException the requeue exception
	 */
	private void validateRequeueExpirationForIOExceptionThread(final JsonNode contextNode)
			throws  InterruptedException, DLQException, RequeueException {

		/*
		 * This method will check and execute requeue as long as the difference between
		 * event time of the request and the current time is lesser than 24hrs. Upon
		 * crossing 24hrs of requeuing, the request will be pushed to dlq.
		 */

		final String contextEventTime = Optional.ofNullable(contextNode.get(PaymentIngestionConstant.EVENTTIME))
				.map(eventTime -> eventTime.toString()).orElse(null);
		final String channelSeqId = Optional.ofNullable(contextNode.get(PaymentIngestionConstant.CHANNEL_SQ_ID))
				.map(channelId -> channelId.toString()).orElse(null);
		if (contextEventTime == null) {
			LOGGER.error(
					"EventTime is missing in the request for the channelSeqId ---> {}, hence pushing the event to DLQ",
					channelSeqId);
			throw new DLQException("Inbound request eventTime is missing");
		} else {
			// formatting the eventtime to localdatetime type
			final LocalDateTime formattedEventTime = getFormattedDate(contextEventTime);
			final LocalDateTime currentDateTime = LocalDateTime.now();
			final Duration d1 = Duration.between(formattedEventTime, currentDateTime);
			final Duration d2 = Duration.ofHours(24);
			if (d1.compareTo(d2) > 0) {
				LOGGER.error("Requeue Time expired for the channelSeqId ---> {}, hence pushing the event to DLQ",
						channelSeqId);
				throw new DLQException("Requeue Time expired");
			} else {
				Thread.sleep(paymentRabbitProperties.getRetrySleepTime());
				LOGGER.error("Requeuing the event with this channelseqid -->>> " + channelSeqId);
			}
		}
	}

	/**
	 * Gets the formatted date.
	 *
	 * @param eventTime
	 *            the event time
	 * @return the formatted date
	 */
	private LocalDateTime getFormattedDate(String eventTime) {//final
		LOGGER.info("getFormattedDate:: BEFORE = {}",eventTime);
		eventTime = eventTime.substring(1, eventTime.length()-1);
		LOGGER.info("getFormattedDate:: AFTER = {}",eventTime);
		if (eventTime != null) {
			try {
				final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				//final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				return LocalDateTime.parse(eventTime, formatter);
			} catch (DateTimeParseException e) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_087 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_087_Desc);
				LOGGER.error("----->>>>>> Invalid EventTime format <<<<<-----");
				return null;
			}
		}
		return null;
	}
	private void processedDataToDlq(final JsonNode data, final String msg){
		ObjectMapper objectMapper = new ObjectMapper();
		try {
		final JsonNodeFactory nodeFactory = JsonNodeFactory.instance;
		final ObjectNode recordForProducer = nodeFactory.objectNode();
		recordForProducer.set(PaymentIngestionConstant.OFF_SET, nodeFactory.numberNode(0));
		recordForProducer.set(PaymentIngestionConstant.PARTITION, nodeFactory.numberNode(0));
		recordForProducer.set(PaymentIngestionConstant.CONSUMER_APP_NAME, nodeFactory.textNode(PaymentIngestionConstant.PAYMENT_STATE_INGESTION));
		recordForProducer.set(PaymentIngestionConstant.CONSUMER_GRP_ID, nodeFactory.textNode("PaymentsInstructionsKafkaConsumer"));
		recordForProducer.set(PaymentIngestionConstant.ERR_TYPE, nodeFactory.textNode(msg));
		recordForProducer.set(PaymentIngestionConstant.RECIEVED_MSG, nodeFactory.textNode(objectMapper.writeValueAsString(data)));
		paymentKafkaDLQProducer.sendToDLQ(recordForProducer);
		LOGGER.error("Invalid Payload. Hence the payload is directed to DLQ. Error Message ::{}", msg);
		}catch(Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_088 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_088_Desc);
			LOGGER.error("Exception while sending the payload to DLQ. Error Message ::{}", msg);
		}
	}
}
