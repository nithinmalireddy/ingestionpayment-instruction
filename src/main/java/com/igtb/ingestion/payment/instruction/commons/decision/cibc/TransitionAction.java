package com.igtb.ingestion.payment.instruction.commons.decision.cibc;

import com.igtb.ingestion.payment.instruction.commons.decision.cibc.FromStates;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.ToStates;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.TransitionAction;

public enum TransitionAction
{
  INSERT, UPDATE, REPLACE, REQUEUE, SKIP;

  private static TransitionAction[][] ACTION_MAPPING;

  private void define(FromStates fromStates, ToStates toStates)
  {
    ACTION_MAPPING[fromStates.ordinal()][toStates.ordinal()] = this;
  }

  public static TransitionAction of(FromStates existingState, ToStates toBeState)
  {
    return ACTION_MAPPING[existingState.ordinal()][toBeState.ordinal()];
  }

  static
  {
    ACTION_MAPPING = new TransitionAction[FromStates.values().length][
      ToStates.values().length];

    INSERT.define(FromStates.NO_STATE, ToStates.DRAFT);

    INSERT.define(FromStates.NO_STATE, ToStates.VAL_SUCCESS);

    INSERT.define(FromStates.NO_STATE, ToStates.VAL_TIMEOUT);

    INSERT.define(FromStates.NO_STATE, ToStates.VAL_FAILURE);

    REQUEUE.define(FromStates.NO_STATE, ToStates.CANCELLED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.REJECTED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.APPROVED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.RELEASED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.RELEASE_RETRY);

    REQUEUE.define(FromStates.NO_STATE, ToStates.RELEASE_REJECTED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.NO_STATE, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.NO_STATE, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.TRASHED);

    REQUEUE.define(FromStates.NO_STATE, ToStates.INITIATED_WF);

    REQUEUE.define(FromStates.NO_STATE, ToStates.VERIFIED_WF);

    REQUEUE.define(FromStates.NO_STATE, ToStates.APPROVED_WF);

    REQUEUE.define(FromStates.NO_STATE, ToStates.RELEASED_WF);

    REQUEUE.define(FromStates.NO_STATE, ToStates.TRASHED_WF);

    REQUEUE.define(FromStates.NO_STATE, ToStates.REJECTED_WF);

    REPLACE.define(FromStates.DRAFT, ToStates.DRAFT);

    REPLACE.define(FromStates.DRAFT, ToStates.VAL_SUCCESS);

    REPLACE.define(FromStates.DRAFT, ToStates.VAL_TIMEOUT);

    REPLACE.define(FromStates.DRAFT, ToStates.VAL_FAILURE);

    UPDATE.define(FromStates.DRAFT, ToStates.CANCELLED);

    UPDATE.define(FromStates.DRAFT, ToStates.REJECTED);

    UPDATE.define(FromStates.DRAFT, ToStates.TRASHED);

    UPDATE.define(FromStates.DRAFT, ToStates.REJECTED_WF);

    REQUEUE.define(FromStates.DRAFT, ToStates.APPROVED);

    REQUEUE.define(FromStates.DRAFT, ToStates.RELEASED);

    REQUEUE.define(FromStates.DRAFT, ToStates.RELEASE_RETRY);

    REQUEUE.define(FromStates.DRAFT, ToStates.RELEASE_REJECTED);

    REQUEUE.define(FromStates.DRAFT, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.DRAFT, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.DRAFT, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.DRAFT, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.DRAFT, ToStates.INITIATED_WF);

    REQUEUE.define(FromStates.DRAFT, ToStates.VERIFIED_WF);

    REQUEUE.define(FromStates.DRAFT, ToStates.APPROVED_WF);

    REQUEUE.define(FromStates.DRAFT, ToStates.RELEASED_WF);

    REQUEUE.define(FromStates.DRAFT, ToStates.TRASHED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.APPROVED);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.CANCELLED);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.TRASHED);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.INITIATED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.VERIFIED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.APPROVED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.RELEASED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.TRASHED_WF);

    UPDATE.define(FromStates.VAL_SUCCESS, ToStates.REJECTED_WF);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.RELEASED);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.RELEASE_RETRY);
    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.RELEASE_REJECTED);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.VAL_SUCCESS, ToStates.REJECTED);

    SKIP.define(FromStates.VAL_SUCCESS, ToStates.DRAFT);

    SKIP.define(FromStates.VAL_SUCCESS, ToStates.VAL_SUCCESS);

    SKIP.define(FromStates.VAL_SUCCESS, ToStates.VAL_TIMEOUT);

    SKIP.define(FromStates.VAL_SUCCESS, ToStates.VAL_FAILURE);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.APPROVED);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.CANCELLED);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.TRASHED);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.INITIATED_WF);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.VERIFIED_WF);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.APPROVED_WF);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.RELEASED_WF);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.TRASHED_WF);

    UPDATE.define(FromStates.VAL_TIMEOUT, ToStates.REJECTED_WF);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.RELEASED);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.RELEASE_RETRY);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.RELEASE_REJECTED);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.VAL_TIMEOUT, ToStates.REJECTED);

    SKIP.define(FromStates.VAL_TIMEOUT, ToStates.DRAFT);

    SKIP.define(FromStates.VAL_TIMEOUT, ToStates.VAL_SUCCESS);

    SKIP.define(FromStates.VAL_TIMEOUT, ToStates.VAL_TIMEOUT);

    SKIP.define(FromStates.VAL_TIMEOUT, ToStates.VAL_FAILURE);

    UPDATE.define(FromStates.VAL_FAILURE, ToStates.TRASHED);

    UPDATE.define(FromStates.CANCELLED, ToStates.TRASHED);

    UPDATE.define(FromStates.REJECTED, ToStates.TRASHED);

    UPDATE.define(FromStates.REJECTED, ToStates.REJECTED_WF);

    UPDATE.define(FromStates.APPROVED, ToStates.RELEASED);

    UPDATE.define(FromStates.APPROVED, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.APPROVED, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.APPROVED, ToStates.RELEASE_FAILURE);

    UPDATE.define(FromStates.APPROVED, ToStates.INITIATED_WF);

    UPDATE.define(FromStates.APPROVED, ToStates.VERIFIED_WF);

    UPDATE.define(FromStates.APPROVED, ToStates.APPROVED_WF);

    UPDATE.define(FromStates.APPROVED, ToStates.RELEASED_WF);

    UPDATE.define(FromStates.APPROVED, ToStates.TRASHED_WF);

    REQUEUE.define(FromStates.APPROVED, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.APPROVED, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.APPROVED, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.APPROVED, ToStates.TRASHED);

    SKIP.define(FromStates.APPROVED, ToStates.DRAFT);

    SKIP.define(FromStates.APPROVED, ToStates.VAL_SUCCESS);

    SKIP.define(FromStates.APPROVED, ToStates.VAL_TIMEOUT);

    SKIP.define(FromStates.APPROVED, ToStates.VAL_FAILURE);

    SKIP.define(FromStates.APPROVED, ToStates.CANCELLED);

    SKIP.define(FromStates.APPROVED, ToStates.REJECTED);

    SKIP.define(FromStates.APPROVED, ToStates.APPROVED);

    SKIP.define(FromStates.APPROVED, ToStates.REJECTED_WF);

    UPDATE.define(FromStates.RELEASED, ToStates.BACKEND_ACK_SUCCESS);

    UPDATE.define(FromStates.RELEASED, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.RELEASED, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.RELEASED, ToStates.TRASHED);

    SKIP.define(FromStates.RELEASED, ToStates.DRAFT);

    SKIP.define(FromStates.RELEASED, ToStates.VAL_SUCCESS);

    SKIP.define(FromStates.RELEASED, ToStates.VAL_TIMEOUT);

    SKIP.define(FromStates.RELEASED, ToStates.VAL_FAILURE);

    SKIP.define(FromStates.RELEASED, ToStates.CANCELLED);

    SKIP.define(FromStates.RELEASED, ToStates.REJECTED);

    SKIP.define(FromStates.RELEASED, ToStates.APPROVED);

    SKIP.define(FromStates.RELEASED, ToStates.RELEASED);

    SKIP.define(FromStates.RELEASED, ToStates.RELEASE_RETRY);

    SKIP.define(FromStates.RELEASED, ToStates.RELEASE_REJECTED);

    SKIP.define(FromStates.RELEASED, ToStates.RELEASE_FAILURE);

    SKIP.define(FromStates.RELEASED, ToStates.REJECTED_WF);
    SKIP.define(FromStates.RELEASED, ToStates.INITIATED_WF);
    SKIP.define(FromStates.RELEASED, ToStates.VERIFIED_WF);
    SKIP.define(FromStates.RELEASED, ToStates.APPROVED_WF);
    SKIP.define(FromStates.RELEASED, ToStates.RELEASED_WF);
    SKIP.define(FromStates.RELEASED, ToStates.TRASHED_WF);

    UPDATE.define(FromStates.RELEASE_RETRY, ToStates.RELEASED);

    UPDATE.define(FromStates.RELEASE_RETRY, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.RELEASE_RETRY, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.RELEASE_RETRY, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.RELEASE_RETRY, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.RELEASE_RETRY, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.RELEASE_RETRY, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.RELEASE_RETRY, ToStates.TRASHED);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.DRAFT);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.VAL_SUCCESS);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.VAL_TIMEOUT);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.VAL_FAILURE);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.CANCELLED);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.REJECTED);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.APPROVED);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.INITIATED_WF);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.VERIFIED_WF);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.APPROVED_WF);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.RELEASED_WF);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.REJECTED_WF);

    SKIP.define(FromStates.RELEASE_RETRY, ToStates.TRASHED_WF);

    UPDATE.define(FromStates.RELEASE_REJECTED, ToStates.TRASHED);

    UPDATE.define(FromStates.RELEASE_FAILURE, ToStates.TRASHED);

    UPDATE.define(FromStates.BACKEND_ACK_SUCCESS, ToStates.BACKEND_ACK_SUCCESS);

    UPDATE.define(FromStates.BACKEND_ACK_SUCCESS, ToStates.BACKEND_REJECTED);

    UPDATE.define(FromStates.BACKEND_ACK_SUCCESS, ToStates.BACKEND_PROCESSED);

    UPDATE.define(FromStates.BACKEND_REJECTED, ToStates.TRASHED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.TRASHED);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.TRASHED);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.TRASHED);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.TRASHED);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.TRASHED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.APPROVED);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.APPROVED);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.APPROVED);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.APPROVED);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.APPROVED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.RELEASED);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.RELEASED);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.RELEASED);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.RELEASED);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.RELEASED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.RELEASE_RETRY);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.RELEASE_REJECTED);

    UPDATE.define(FromStates.INITIATED_WF, ToStates.RELEASE_FAILURE);

    UPDATE.define(FromStates.VERIFIED_WF, ToStates.RELEASE_FAILURE);

    UPDATE.define(FromStates.APPROVED_WF, ToStates.RELEASE_FAILURE);

    UPDATE.define(FromStates.RELEASED_WF, ToStates.RELEASE_FAILURE);

    UPDATE.define(FromStates.TRASHED_WF, ToStates.RELEASE_FAILURE);

    REQUEUE.define(FromStates.INITIATED_WF, ToStates.CANCELLED);

    REQUEUE.define(FromStates.VERIFIED_WF, ToStates.CANCELLED);

    REQUEUE.define(FromStates.APPROVED_WF, ToStates.CANCELLED);

    REQUEUE.define(FromStates.RELEASED_WF, ToStates.CANCELLED);

    REQUEUE.define(FromStates.TRASHED_WF, ToStates.CANCELLED);

    REQUEUE.define(FromStates.INITIATED_WF, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.VERIFIED_WF, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.APPROVED_WF, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.RELEASED_WF, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.TRASHED_WF, ToStates.BACKEND_ACK_SUCCESS);

    REQUEUE.define(FromStates.INITIATED_WF, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.VERIFIED_WF, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.APPROVED_WF, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.RELEASED_WF, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.TRASHED_WF, ToStates.BACKEND_REJECTED);

    REQUEUE.define(FromStates.INITIATED_WF, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.VERIFIED_WF, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.APPROVED_WF, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.RELEASED_WF, ToStates.BACKEND_PROCESSED);

    REQUEUE.define(FromStates.TRASHED_WF, ToStates.BACKEND_PROCESSED);

    UPDATE.define(FromStates.REJECTED_WF, ToStates.REJECTED);

    UPDATE.define(FromStates.REJECTED_WF, ToStates.TRASHED);
  }
}