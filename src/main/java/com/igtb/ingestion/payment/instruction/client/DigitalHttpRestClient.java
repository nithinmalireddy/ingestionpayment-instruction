package com.igtb.ingestion.payment.instruction.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.exception.QuestException;
import com.igtb.ingestion.payment.instruction.models.quest.QuestResponse;

@Component
public class DigitalHttpRestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(DigitalHttpRestClient.class);
	
	@Retryable(include = RuntimeException.class, maxAttempts = 3, backoff = @Backoff(delay = 1000, multiplier = 2))
	public ResponseEntity<QuestResponse> execute(String gatekeeperURL, String request, HttpHeaders headers, HttpMethod method) {
		LOGGER.info("At IDigitalHttpRestClient");
		
		ResponseEntity<QuestResponse> response = null;
		try {
			LOGGER.debug("Retry calling at IDigitalHttpRestClient");
			headers.add("Content-Type", "application/json");
			HttpEntity<String> entity = new HttpEntity<>(request, headers);
			RestTemplate restTemplate = new RestTemplate();
			LOGGER.debug("URL :::::{} " , gatekeeperURL);
			LOGGER.debug("Entity  ::::{} " , entity);
			response = restTemplate.exchange(gatekeeperURL, method, entity, QuestResponse.class);
			LOGGER.info("response entity response from conductor is {}", response);
		} catch (HttpClientErrorException ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_011 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_011_Desc);
			LOGGER.error("Couldnt trigger url, msg: {} and responseBody: {}",  ex.getMessage(), ex.getResponseBodyAsString());
		}

		return response;

	}
	
	@Recover
	public ResponseEntity<Object> recover(HttpClientErrorException ex) throws QuestException {
		LOGGER.debug("Response coming from conductor is {}", ex.getStatusCode());
		LOGGER.error("Couldnt trigger workflow::Recovering the HttpClientErrorException");
		return new ResponseEntity<Object>(ex.getResponseBodyAsString(), ex.getStatusCode());
	}

	@Recover
	public ResponseEntity<Object> recover(IllegalArgumentException ex) {
		LOGGER.debug("Response coming from conductor is {}",org.springframework.http.HttpStatus.UNAUTHORIZED);
		LOGGER.error("Couldnt trigger workflow :: Recovering the IllegalArgumentException");
		return new ResponseEntity<Object>(org.springframework.http.HttpStatus.UNAUTHORIZED);
	}
	
	@Recover
	public ResponseEntity<Object> recover(HttpServerErrorException ex) {
		LOGGER.debug("Response coming from conductor is {}",org.springframework.http.HttpStatus.UNAUTHORIZED);
		LOGGER.error("Couldnt trigger workflow :: Recovering the HttpServerErrorException");
		return new ResponseEntity<Object>(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
