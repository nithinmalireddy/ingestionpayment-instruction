package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.igtb.ingestion.payment.instruction.models.Entity;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class PaymentWF.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PaymentWF {

	/** The current action. */
	private String currentAction;

	/** The next action. */
	private String nextAction;

	/** The reject by source. */
	private String rejectBySource;

	/** The reject errors. */
	private List<RejectError> rejectErrors;

	/** The user list. */
	private List<UserList> userList;

	/** The entity. */
	private Entity entity;
	

}
