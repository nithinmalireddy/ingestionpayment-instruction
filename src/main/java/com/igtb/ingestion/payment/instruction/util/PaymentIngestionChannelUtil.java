/**
 * Version no		Author						Comments
 * 1.0				K1 base version				K1 base version
 * 1.1				Anil Ravva					Changes related to Remove the status field PaymentCenterStatus 
 */

package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
//import com.igtb.api.ingestion.commons.decision.StateTransitionData;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.StateTransitionData;
import com.igtb.api.ingestion.commons.stateinfo.models.LastAction;
import com.igtb.api.ingestion.commons.stateinfo.models.RejectionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.ResourceEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.EntityES;
import com.igtb.ingestion.payment.instruction.es.models.FAEBackendEvent;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Entity;
import com.igtb.ingestion.payment.instruction.models.EventSource;
import com.igtb.ingestion.payment.instruction.models.EventType;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;
import com.igtb.ingestion.payment.instruction.models.Requester;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;

import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import lombok.AllArgsConstructor;

/**
 * The Class PaymentIngestionChannelUtil.
 */
@Component
@AllArgsConstructor
public class PaymentIngestionChannelUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionChannelUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The payment ingestion jest util. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The payment ingestion enrichment util. */
	private final PaymentIngestionEnrichmentUtil enrichmentUtil;

	/** The payment ingestion state info util. */
	private final PaymentIngestionHelper paymentIngestionHelper;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/** The supp document util. */
	private final SuppDocumentUtil suppDocumentUtil;

	/** The witholding tax info util. */
	private final WitholdingTaxInfoUtil witholdingTaxInfoUtil;

	/** The transition history util. */
	private final TransitionHistoryUtil transitionHistoryUtil;

	/** The payment instruction builder. */
	@Autowired
	private PaymentInstructionBuilder paymentInstructionBuilder;
	
	@Autowired
	private ContactLastActivityUtil contactLastActivityUtil;	

	@Autowired
	private PaymentKafkaProperties paymentKafkaProperties;

	@Autowired
	private PaymentKafkaDLQProducer kafkaProducer;
	
	/**
	 * Perform first insert.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @param requestMessageJson
	 *            the request message json
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @throws Exception
	 */
	public void performFirstInsert(final PaymentInstruction paymentInstruction, final Context context,
			final JsonNode requestMessageJson, final JsonNode existingStateInfoJson) throws Exception {

		// Mapping from Request PI to PI ES Type
		final PaymentsInstructionsES paymentsInstructionsES = new PaymentsInstructionsES();
		JsonNode userInfoJson;
		if (context.getEventType().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.ADD) || (context.getEventType().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.DELETE) && (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)))) {
			userInfoJson = enrichmentUtil.paymentESEnrichment(paymentsInstructionsES, paymentInstruction, context,requestMessageJson, existingStateInfoJson);

			List<Index> suppDocList = null;
			if (paymentInstruction.getAttachments() != null && paymentInstruction.getAttachments().size() > 0) {
				LOGGER.info("Preparing to persist the supporting documents for instructionId: {}",paymentsInstructionsES.getInstructionId());
				suppDocList = suppDocumentUtil.getSuppDocList(paymentInstruction.getAttachments(),paymentsInstructionsES.getStateInfo());
			} else {
				LOGGER.debug("No attachment available for instructionId: {}",paymentsInstructionsES.getInstructionId());
			}

			List<Index> withHoldingTxList = null;
			if (paymentInstruction.getWhtDetails() != null) {
				LOGGER.info("Preparing to persist the withHoldingTx documents for instructionId: {}",paymentsInstructionsES.getInstructionId());
				withHoldingTxList = witholdingTaxInfoUtil.getWHTDocsIndexList(paymentInstruction.getWhtDetails().getTaxDetails(), paymentsInstructionsES.getStateInfo());
			} else {
				LOGGER.debug("No WHTDetails available for instructionId: {}",paymentsInstructionsES.getInstructionId());
			}

			// inserting PI
			JestResult result = paymentIngestionQueryUtil.insertPaymentInstruction(paymentsInstructionsES,paymentsInstructionsES.getInstructionId(), paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());

			// inserting into contactlastactivities
			contactLastActivityUtil.pushContactLastActivityToES(context,paymentsInstructionsES);
			
			// inserting supporting documents
			if (result.isSucceeded() && suppDocList != null) {
				LOGGER.info("Inserting SuppDocuments for instructionId: {}", paymentsInstructionsES.getInstructionId());
				final JestResult suppDocResult = paymentIngestionQueryUtil.insertBulkRecord(paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType(),suppDocList);
				LOGGER.debug("SuppDocuments bulkResult isSucceeded: {}", suppDocResult.isSucceeded());
			}

			// inserting paymentTaxInfo
			if (result.isSucceeded() && withHoldingTxList != null) {
				LOGGER.info("Inserting paymentTaxInfofor instructionId: {}", paymentsInstructionsES.getInstructionId());
				final JestResult paymentTaxInfoResult = paymentIngestionQueryUtil.insertBulkRecord(paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType(),withHoldingTxList);
				LOGGER.debug("paymentTaxInfo bulkResult isSucceeded: {}", paymentTaxInfoResult.isSucceeded());
			}

			// Updating active records status for fulfillrtp
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
				LOGGER.info("Updating active record fulfillrtp with channelSeqId {} and domainSeqId {} ",paymentsInstructionsES.getInstructionId(), paymentsInstructionsES.getTransactionId());
				updateExisitingActiveRecordInES(paymentsInstructionsES.getStateInfo().getDomainSeqId(),paymentsInstructionsES.getStateInfo().getChannelSeqId(),paymentsInstructionsES.getUiDisplayStatusES(),paymentsInstructionsES);
			}
		} else {
			// handling for hold/unhold/cancel request type
			userInfoJson = enrichmentUtil.handleRequestForOtherRequestType(paymentInstruction, context,requestMessageJson, existingStateInfoJson);
		}

		/*
		 * after successful processing, pushing the enriched transactionhistory payload
		 * onto the elasticsearch
		 */
		transitionHistoryUtil.handleTransitionHistory(context, requestMessageJson, userInfoJson);

	}

	/**
	 * Perform state update only.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param paymentWF
	 *            the payment WF
	 * @param context
	 *            the context
	 * @param existingPayInstructionES
	 *            the existing payment instruction ES
	 * @param existingDocId
	 *            the existing doc id
	 * @param requestMessageJson
	 *            the request message json
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public void performStateUpdateOnly(final PaymentInstruction paymentInstruction, final PaymentWF paymentWF,final Context context, final PaymentsInstructionsES existingPayInstructionES, final String existingDocId,final JsonNode requestMessageJson, final JsonNode existingStateInfoJson)throws IOException, DLQException, MessageProcessingException {
		LOGGER.info("Start performStateUpdateOnly for Context:::::::::: {}", context);
		// pass the stateinfo object for enrichment
		// User Enrichment
		final UserInfo userInfo = new UserInfo();
		Optional.ofNullable(context.getRequester()).map(requester -> requester.getDomainId()).ifPresent(userInfo::setDomainName);
		Optional.ofNullable(context.getRequester()).map(requester -> requester.getId()).ifPresent(userInfo::setUserName);
		final String entity;
		if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
			entity = Optional.ofNullable(paymentWF).map(PaymentWF::getEntity).map(Entity::getId).orElse(Optional.ofNullable(existingPayInstructionES).map(PaymentsInstructionsES::getCreditorAccount).map(AccountES::getEntity).map(EntityES::getId).orElse(null));
			LOGGER.info("SEND_RTP Fetching Entity id::::::: {} ", entity);
		} else {
			entity = Optional.ofNullable(paymentWF).map(PaymentWF::getEntity).map(Entity::getId).orElse(Optional.ofNullable(existingPayInstructionES).map(PaymentsInstructionsES::getDebtorAccount).map(AccountES::getEntity).map(EntityES::getId).orElse(null));
			LOGGER.info("Fetching Entity id::::::: {} ", entity);
		}
		
		StateInfo lESstateInfo = existingPayInstructionES.getStateInfo();
		
		String lEsDomainSeqId = lESstateInfo.getDomainSeqId();
		
		paymentIngestionHelper.enrichUserInfo(entity, userInfo);
		String prevCbxUIStatus = existingPayInstructionES.getCbxUIStatus();
		final JsonNode userInfoJson = objectMapper.convertValue(userInfo, JsonNode.class);
		final StateInfo stateInfo = paymentIngestionHelper.getStateInfoFromCommonsModule(requestMessageJson,userInfoJson, existingStateInfoJson, existingPayInstructionES.getPaymentTypesObj().getDescription());
		if(stateInfo!=null) {
			if(stateInfo.getDomainSeqId()==null || stateInfo.getDomainSeqId().equals("")) {
				if(existingPayInstructionES.getStateInfo()!=null && existingPayInstructionES.getStateInfo().getDomainSeqId()!=null) {
					stateInfo.setDomainSeqId(existingPayInstructionES.getStateInfo().getDomainSeqId()); 
				}
			}
			if(stateInfo.getLastAction()!=null) {
				if(stateInfo.getLastAction().getUserInfo()==null) {
					if(existingPayInstructionES.getStateInfo()!=null && existingPayInstructionES.getStateInfo().getLastAction()!=null && existingPayInstructionES.getStateInfo().getLastAction().getUserInfo()!=null) {
						stateInfo.getLastAction().setUserInfo(existingPayInstructionES.getStateInfo().getLastAction().getUserInfo());
					}
				}
			}
			existingPayInstructionES.setStateInfo(stateInfo);
		}
		
		existingPayInstructionES.setApproverUserId(Optional.ofNullable(stateInfo).map(StateInfo::getLastAction).map(LastAction::getUserInfo).map(UserInfo::getUserName).orElse(null));
		existingPayInstructionES.setApproverDomainId(Optional.ofNullable(stateInfo).map(StateInfo::getLastAction).map(LastAction::getUserInfo).map(UserInfo::getDomainName).orElse(null));
		existingPayInstructionES.setCbxUIStatus(UIStatusUtil.getCBXUIStatus(stateInfo, ResourceEnum.paymentInstructions.toString()));
		LOGGER.info("stateInfo:::::::: {}", stateInfo);
		LOGGER.info("prevCbxUIStatus::::::{} ", prevCbxUIStatus);
		LOGGER.info("context::::::{}", context);
		if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
			if (existingPayInstructionES.getCbxUIStatus().equalsIgnoreCase(PaymentIngestionConstant.PENDING_APPROVAL)) {
				existingPayInstructionES.setCbxUIStatus(prevCbxUIStatus);
			} else {
				LOGGER.info("Current status is not PendApprvl");
			}
		}
		LOGGER.info("Before update existingPayInstructionES::::: {} ", existingPayInstructionES);
		
		if(!validateStatusInfoObj(existingPayInstructionES,existingDocId,context,stateInfo,requestMessageJson,userInfoJson)) {
			LOGGER.info("Valid next status generated");
			// updating cumulativeStatus
			String cumulativeStatusStr = Strings.EMPTY;
			
			
			//1.1 STARTS
			String docStatus = stateInfo.getDocStatus() != null ? stateInfo.getDocStatus() : PaymentIngestionConstant.NULL;
			String cbxUIStatus = existingPayInstructionES.getCbxUIStatus() != null ? existingPayInstructionES.getCbxUIStatus() : PaymentIngestionConstant.NULL;
			String faeIndicator = existingPayInstructionES.getFaeIndicator() != null ? existingPayInstructionES.getFaeIndicator() : PaymentIngestionConstant.NULL;
			String bccAction = existingPayInstructionES.getBccAction() != null ? existingPayInstructionES.getBccAction() : PaymentIngestionConstant.NULL;
			String status= context.getEventType().getStatus();
			String outcomeCategory = context.getEventType().getOutcomeCategory();
			String serviceKey = context.getEventType().getServiceKey();
			String desc = existingPayInstructionES.getStateInfo().getTransitionInfo().getAdditionalInfo().getDesc();
			
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put(PaymentIngestionConstant.DOC_STATUS, docStatus);
			paramMap.put(PaymentIngestionConstant.SERVICE_KEY, serviceKey);
			paramMap.put(PaymentIngestionConstant.OUTCOMECATEGORY, outcomeCategory);
			paramMap.put(PaymentIngestionConstant.BCC_ACTION, bccAction);
			paramMap.put(PaymentIngestionConstant.FAE_INDICATOR, faeIndicator);
			paramMap.put(PaymentIngestionConstant.DESC, desc);
			
			String fetchQryString = paymentIngestionQueryUtil.getFetchQryStatus(paramMap);
			if(fetchQryString.equalsIgnoreCase(PaymentIngestionConstant.DEFAULT_STR))
				fetchQryString = PaymentIngestionConstant.FETCH_QUERY_DEFAULT_STR;
			LOGGER.info("fetchQryString:: "+fetchQryString);
			//DOC_STATUS+TRANSITION_DESC+CBX_UI_STATUS+FAE_IND+BCC_ACTION+IRTP
			
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.DOC_STATUS_STR, docStatus);
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.TRANSITION_DESC, desc);
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.CBX_UI_STATUS, cbxUIStatus);
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.FAE_IND, faeIndicator);
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.BCC_ACTION_STR, bccAction);
			fetchQryString = fetchQryString.replace(PaymentIngestionConstant.BLANK_STR, PaymentIngestionConstant.BLANK);
			LOGGER.info("esFetchQryString :: "+fetchQryString);
			
			//cumulativeStatusStr = esCumulativeString;
			/*if(fetchQryString.contains(PaymentIngestionConstant.IRTP_STR)) {
				updateCumulativeStatusForAEHoldIRTP(context.getDomainSeqId(), context.getChannelSeqId());
			}*/
			if(Strings.isNotBlank(fetchQryString)) {
				existingPayInstructionES.setFetchQueryStatus(fetchQryString);
			}
			//1.1 
			
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)
					&& PaymentIngestionConstant.APPROVED.equalsIgnoreCase(context.getEventType().getStatus())
					&& PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(context.getEventType().getOutcomeCategory())
					&& PaymentIngestionConstant.AE_HOLD.equalsIgnoreCase(existingPayInstructionES.getFaeIndicator())
					&& PaymentIngestionConstant.BCC_PENDING.equalsIgnoreCase(existingPayInstructionES.getBccAction())) {
				cumulativeStatusStr = (stateInfo.getDocStatus() != null ? stateInfo.getDocStatus() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getCbxUIStatus() != null ? existingPayInstructionES.getCbxUIStatus() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getFaeIndicator() != null ? existingPayInstructionES.getFaeIndicator() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getBccAction() != null ? existingPayInstructionES.getBccAction() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.IRTP_STR;
				
				// Updating Active Document
				updateCumulativeStatusForAEHoldIRTP(context.getDomainSeqId(), context.getChannelSeqId());
			} else if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)
					&& PaymentIngestionConstant.RELEASED.equalsIgnoreCase(context.getEventType().getStatus())
					&& PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(context.getEventType().getOutcomeCategory())
					&& PaymentIngestionConstant.AE_HOLD.equalsIgnoreCase(existingPayInstructionES.getFaeIndicator())
					&& PaymentIngestionConstant.BCC_PENDING.equalsIgnoreCase(existingPayInstructionES.getBccAction())) {
				cumulativeStatusStr = PaymentIngestionConstant.BLANK;
			} 
			else {
				cumulativeStatusStr = (stateInfo.getDocStatus() != null ? stateInfo.getDocStatus() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getCbxUIStatus() != null ? existingPayInstructionES.getCbxUIStatus() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getFaeIndicator() != null ? existingPayInstructionES.getFaeIndicator() : PaymentIngestionConstant.NULL)
						+ PaymentIngestionConstant.BLANK
						+ (existingPayInstructionES.getBccAction() != null ? existingPayInstructionES.getBccAction() : PaymentIngestionConstant.NULL);
			}
			
			//To be removed the below IF CONDITION 
			if(Strings.isNotBlank(cumulativeStatusStr)) {
				existingPayInstructionES.setCumulativeStatusES(cumulativeStatusStr);
			}
			//1.1 ENDS
			
			existingPayInstructionES.setUiDisplayStatusES(existingPayInstructionES.getCbxUIStatus()!=null?existingPayInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL);
			boolean isUserAutoApprove = paymentInstructionBuilder.isUserAutoApprove(existingPayInstructionES.getStateInfo().getInitiated().getUserInfo().getUserName(),context.getEventType().getServiceKey());
			LOGGER.info("isUserAutoApprove: {} for channelSeqId: {} eventId: {}",isUserAutoApprove,context.getChannelSeqId(),requestMessageJson.get(PaymentIngestionConstant.ID));
			if(isUserAutoApprove) {
				//Setting NULL as value for Auto Approve users as group-action is not called and to avoid GQL from failing when field is null
				existingPayInstructionES.setGroupChannelSeqId(PaymentIngestionConstant.NULL);
			}
			//Enriching Group ChannelSeqId when not fulfillrtp as group action not called for fulfillrtp and User is not Auto Approve
			if ( !context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP) && !(isUserAutoApprove) ) {
				if(null==existingPayInstructionES.getGroupChannelSeqId() || "".equals(existingPayInstructionES.getGroupChannelSeqId()) || PaymentIngestionConstant.NULL.equalsIgnoreCase(existingPayInstructionES.getGroupChannelSeqId()) ) {
					String pymntInstnGrpID = paymentInstructionBuilder.getPymntInstnGrpID(context.getChannelSeqId());
					existingPayInstructionES.setGroupChannelSeqId((null != pymntInstnGrpID)? pymntInstnGrpID:PaymentIngestionConstant.NULL);
				}
			}
			
			String lNewDomainId = existingPayInstructionES.getStateInfo().getDomainSeqId();
			LOGGER.info("Recevied Domain id::::{} ::ES Domain id:: {}", lNewDomainId,lEsDomainSeqId);
			if(lNewDomainId == null || lNewDomainId.equals("null") || lNewDomainId.equals("") ||
					lEsDomainSeqId == null || lEsDomainSeqId.equals("null")|| lEsDomainSeqId.equals("")|| lNewDomainId.equals(lEsDomainSeqId)) {
				String docStatusType = PaymentIngestionConstant.DOC_STATUS_TRANSITION;
				String lTxnStatusDesc = stateInfo.getTransitionInfo().getAdditionalInfo().getDesc();
				//1.1 starts
				LOGGER.info("existingPayInstructionES.getStateInfo().getDocStatus()+lTxnStatusDesc:: "+existingPayInstructionES.getStateInfo().getDocStatus()+lTxnStatusDesc);
				
				paramMap = new HashMap<String, String>();
				paramMap.put(PaymentIngestionConstant.DOC_STATUS, docStatusType);
				paramMap.put(PaymentIngestionConstant.DESC, lTxnStatusDesc);
				fetchQryString = paymentIngestionQueryUtil.getFetchQryStatus(paramMap);
				LOGGER.debug("paramMap:: "+paramMap+"fetchQryString:: "+fetchQryString);
				if(!fetchQryString.equalsIgnoreCase(PaymentIngestionConstant.DEFAULT_STR)) {
					existingPayInstructionES.setFetchQueryStatus(fetchQryString);//1.1
					LOGGER.debug("existingPayInstructionES:: "+existingPayInstructionES);
				}
				
				//To be commented the below if else condition
				if(lNewDomainId == null || lNewDomainId.equals("null") || lNewDomainId.equals("")) {
					existingPayInstructionES.setPaymentCenterStatus(lTxnStatusDesc.toUpperCase()+"-NULL");
				}else {		
					//TODO need correct fix for this
					if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP) && lTxnStatusDesc.equalsIgnoreCase(PaymentIngestionConstant.REJECTED)){
						if(context.getEventSource().getSourceIdentity().equals(PaymentIngestionConstant.EVNT_SOURCE_INDENTITY)) {
							existingPayInstructionES.setPaymentCenterStatus(lTxnStatusDesc.toUpperCase()+"-NOTNULL");						
						}else {
							existingPayInstructionES.setPaymentCenterStatus(lTxnStatusDesc.toUpperCase()+"-NULL");
						}
					}else {
						existingPayInstructionES.setPaymentCenterStatus(lTxnStatusDesc.toUpperCase()+"-NOTNULL");
					}
				}
				//1.1 ends
				LOGGER.debug("PaymentCenterStatus value for transaction record :"+existingPayInstructionES.getPaymentCenterStatus());
				JestResult result = updateStateInfo(existingDocId, existingPayInstructionES);
				
				if(lNewDomainId!=null && !lNewDomainId.equals("")) {				
					//Update StateInfo.lastaction in active Doc
					ObjectNode existingESPayloadForDomainSeqId =(ObjectNode)paymentIngestionQueryUtil.fetchActiveRecord(lNewDomainId);
					LOGGER.info("Existing Active record Info >>>>>>> {}", existingESPayloadForDomainSeqId.toString());
					PaymentsInstructionsES paymentsInstructionsESActiveRec = objectMapper.readValue(existingESPayloadForDomainSeqId.toString(), PaymentsInstructionsES.class);
					paymentsInstructionsESActiveRec.getStateInfo().setLastAction(existingPayInstructionES.getStateInfo().getLastAction());
					if(existingPayInstructionES.getStateInfo().getTransitionInfo().getRejectionInfo()!=null) {
						paymentsInstructionsESActiveRec.getStateInfo().getTransitionInfo().setRejectionInfo(existingPayInstructionES.getStateInfo().getTransitionInfo().getRejectionInfo());						
						LOGGER.info("Rabbit Rejection Info >>>>>>> {}", existingPayInstructionES.getStateInfo().getTransitionInfo().getRejectionInfo());
					}
					if(existingPayInstructionES.getDebtorAccount()!=null) {
						paymentsInstructionsESActiveRec.setDebtorAccount(existingPayInstructionES.getDebtorAccount());						
					}
					JsonNode activeRecord =objectMapper.convertValue(paymentsInstructionsESActiveRec, JsonNode.class);
					LOGGER.info("Before update Active record Info >>>>>>> {}", activeRecord);
					paymentIngestionQueryUtil.updateBuilder(activeRecord,lNewDomainId, paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType());
				}
				// If state updation successfull, then enrich the transition history
				if (result != null && result.isSucceeded()) {
					LOGGER.info("Updated result for {} >>>>>>> {}", existingDocId, result.getSourceAsString());
					LOGGER.info("State Update is successfull for id:::::::: {}", existingDocId);
					/*
					 * after successful processing, pushing the enriched transactionhistory payload
					 * onto the elasticsearch
					 */
					String statusInContext = context.getEventType().getStatus();
					String serviceKeyInContext = context.getEventType().getServiceKey();
					
					if((statusInContext.equalsIgnoreCase(PaymentIngestionConstant.BACKEND_PROCESSED) && serviceKeyInContext.contains(PaymentIngestionConstant.FULFILL_RTP)) 
					||(statusInContext.equalsIgnoreCase(PaymentIngestionConstant.BACKEND_REJECTED) && serviceKeyInContext.contains(PaymentIngestionConstant.SEND_RTP))
							) {
						JsonNode paymentInstActiveData = paymentIngestionQueryUtil
								.fetchExistingRecordFromES(context.getDomainSeqId());
						String activeDocStatus = paymentInstActiveData.get(PaymentIngestionConstant.STATUS).asText();
						LOGGER.info("{} - {} Active Doc Status ......... {}", serviceKeyInContext, statusInContext, activeDocStatus);
						if(activeDocStatus.equalsIgnoreCase(PaymentIngestionConstant.DECLINED)) {
						LOGGER.info("{} - {} - {} ::: Stopping payment history call ", serviceKeyInContext, statusInContext, activeDocStatus);
						}
						else {
							transitionHistoryUtil.handleTransitionHistory(context, requestMessageJson, userInfoJson);
						}
					}
					else {
						transitionHistoryUtil.handleTransitionHistory(context, requestMessageJson, userInfoJson);
					}
				} else {
					LOGGER.info("State Updation has not yet succeeded for the incoming request with eventId => {}, and channelSeqId => {}",requestMessageJson.get(PaymentIngestionConstant.ID), context.getChannelSeqId());
				}
			}else {
				LOGGER.error("Invalid Active txn id receivded:::::::: {}", lNewDomainId);
			}
			LOGGER.info("Exiting the state information updation for id:::::::: {}", existingDocId);
		}else {
			LOGGER.error("Next Action prepared as null:::::::: {}", stateInfo);
		}
	}

	private boolean validateStatusInfoObj(PaymentsInstructionsES existingPayInstructionES, String existingDocId, Context context, StateInfo stateInfo, JsonNode requestMessageJson, JsonNode userInfoJson) throws IOException, MessageProcessingException, DLQException {
		boolean isNextActionNull = false;
		LOGGER.info("**** Will reject txn for channelSeqId::{} NextAction::{} and stateInfo::{}",context.getChannelSeqId(),stateInfo.getNextAction(),stateInfo);
		if(stateInfo != null && stateInfo.getNextAction() == null && PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equals(stateInfo.getTransitionInfo().getOutcomeCategory()) 
				&& !( TransitionStateEnum.release_rejected.toString().equalsIgnoreCase(stateInfo.getTransitionInfo().getState().trim()) 
				|| TransitionStateEnum.trashed.toString().equalsIgnoreCase(stateInfo.getTransitionInfo().getState().trim()) 
				|| TransitionStateEnum.rejected.toString().equalsIgnoreCase(stateInfo.getTransitionInfo().getState().trim()) ) 
				) {
			LOGGER.error("****Update status to Request validation failed(Next action is null) for channelSequenceId::{}",context.getChannelSeqId());
			existingPayInstructionES = buildStateInfoPI(existingPayInstructionES,context);
			
			//1.1
			//To be removed the below statement
			existingPayInstructionES.setCumulativeStatusES((stateInfo.getDocStatus()!= null?stateInfo.getDocStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getCbxUIStatus()!=null?existingPayInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getFaeIndicator()!=null?existingPayInstructionES.getFaeIndicator():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getBccAction()!=null?existingPayInstructionES.getBccAction():PaymentIngestionConstant.NULL));
			existingPayInstructionES.setFetchQueryStatus((stateInfo.getDocStatus()!= null?stateInfo.getDocStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getCbxUIStatus()!=null?existingPayInstructionES.getCbxUIStatus():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getFaeIndicator()!=null?existingPayInstructionES.getFaeIndicator():PaymentIngestionConstant.NULL)+PaymentIngestionConstant.BLANK+(existingPayInstructionES.getBccAction()!=null?existingPayInstructionES.getBccAction():PaymentIngestionConstant.NULL));//1.1
			//1.1
			existingPayInstructionES.setUiDisplayStatusES("REJECTED");
			existingPayInstructionES.setCbxUIStatus("REJECTED");
			JestResult result = updateStateInfo(existingDocId, existingPayInstructionES);
			
			// If state updation successfull, then enrich the transition history
			if (result != null && result.isSucceeded()) {
				LOGGER.info("State Update is successfull for id:::::::: {}", existingDocId);
				/*
				 * after successful processing, pushing the enriched transactionhistory payload
				 * onto the elasticsearch
				 */
				transitionHistoryUtil.handleTransitionHistory(context, requestMessageJson, userInfoJson);
			} else {
				LOGGER.info("State Updation has not yet succeeded for the incoming request with eventId => {}, and channelSeqId => {}",requestMessageJson.get(PaymentIngestionConstant.ID), context.getChannelSeqId());
			}
			
			
			LOGGER.debug("inside validateStatusInfoObj:::");
			double randNum = Math.random();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern(PaymentIngestionConstant.TIMESTAMP_FORMAT2);
			LocalDateTime now = LocalDateTime.now();
			FAEBackendEvent faeRjctbackendEvent = new FAEBackendEvent();
			faeRjctbackendEvent.setId(String.valueOf(randNum).replace(PaymentIngestionConstant.DOT, PaymentIngestionConstant.BLANK));
			faeRjctbackendEvent.setEventVersion(PaymentIngestionConstant.VERSION);
			Context rjctContextPayload = new Context();
			rjctContextPayload.setChannelSeqId(context.getChannelSeqId());
			rjctContextPayload.setDomainSeqId(faeRjctbackendEvent.getId());
			rjctContextPayload.setEventTime(dtf.format(now));
			
			Requester rjctRequester = new Requester();
			rjctRequester.setId(PaymentIngestionConstant.REQUESTER_ID);
			rjctRequester.setDomainId(PaymentIngestionConstant.REQUESTER_DOMAINID);
			EventType rjctEventType = new EventType();
			rjctEventType.setEventCategory(PaymentIngestionConstant.BUSINESS);
			rjctEventType.setServiceKey(context.getEventType().getServiceKey());
			rjctEventType.setOutcomeCategory(PaymentIngestionConstant.BACKEND_STATE_UPDATED);
			rjctEventType.setStatus(PaymentIngestionConstant.BACKEND_REJECTED);
			rjctEventType.setRequestType(PaymentIngestionConstant.SMALL_ADD);
			rjctEventType.setFormat(PaymentIngestionConstant.PLAIN);
			EventSource rjctEventSource = new EventSource();
			rjctEventSource.setSourceIdentity(PaymentIngestionConstant.EVNT_SOURCE_INDENTITY);
			rjctEventSource.setRegion(PaymentIngestionConstant.REGION);
			rjctEventSource.setCountry(PaymentIngestionConstant.COUNTRY_CANADA);
			rjctContextPayload.setEventType(rjctEventType);
			rjctContextPayload.setEventSource(rjctEventSource);
			rjctContextPayload.setRequester(rjctRequester);
			
			ObjectMapper rjctMapper = new ObjectMapper();
			faeRjctbackendEvent.setContext(rjctContextPayload);
			String rjctJson = rjctMapper.writeValueAsString(faeRjctbackendEvent);
			JSONObject rjctFinalPayload = new JSONObject(rjctJson);
			rjctFinalPayload.put(PaymentIngestionConstant.PAYLOAD, new JSONObject());
			rjctFinalPayload.getJSONObject(PaymentIngestionConstant.CONTEXT).put(PaymentIngestionConstant.REQUESTER,new JSONObject());
			ProducerRecord<String, String> rjctRecord=null;
			rjctFinalPayload.put(PaymentIngestionConstant.ID,String.valueOf(randNum).replace(PaymentIngestionConstant.DOT, PaymentIngestionConstant.BLANK));
			rjctFinalPayload.getJSONObject(PaymentIngestionConstant.PAYLOAD).put(PaymentIngestionConstant.STATUS,PaymentIngestionConstant.REJECTED);
			rjctRecord = new ProducerRecord<>(paymentKafkaProperties.getKafkaTopic(), rjctFinalPayload.toString());
			/**backend_rejected event on Kafka*/
			LOGGER.info("backend_rejected event sending on Kafka for nextaction null:::::{}",rjctRecord);
			kafkaProducer.sendKafkaMessageForAEHold(rjctRecord);
			LOGGER.info("backend_rejected event sent on Kafka");
			isNextActionNull = true;

		}
		return isNextActionNull;
	}

	/**
	 * Update state info.
	 *
	 * @param existingDocId
	 *            the existing doc id
	 * @param paymentsInstructionsES
	 *            the payments instructions ES
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private JestResult updateStateInfo(final String existingDocId, final PaymentsInstructionsES paymentsInstructionsES)
			throws IOException {
		return paymentIngestionQueryUtil.updateExistingDocument(paymentsInstructionsES, existingDocId,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType(), null);
	}

	/**
	 * Perform replace document.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param context
	 *            the context
	 * @param requestMessageJson
	 *            the request message json
	 * @param existingStateInfoJson
	 *            the existing state info json
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void performReplaceDocument(final PaymentInstruction paymentInstruction, final Context context,
			final JsonNode requestMessageJson, final JsonNode existingStateInfoJson) throws Exception {

		// Delete the existing record of paymentInstruction, supportingDocument and
		// paymentTaxInfo.
		LOGGER.info("In performReplaceDocument: {}",context.getChannelSeqId());
//		SearchSourceBuilder searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("paymentInstructionId",
//				context.getChannelSeqId());

		// Delete the existing record of supportingDocument.
//		final JestResult jestResultSuppdoc = paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
//				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType());

		// Delete the existing record of paymentTaxInfo.
//		if (jestResultSuppdoc != null && jestResultSuppdoc.isSucceeded()) {
//			searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("parentInstructionId",
//					context.getChannelSeqId());
//
//			final JestResult jestResultPayTaxInfo = paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
//					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType());
//
//			// Delete the existing record of paymentInstruction
//			if (jestResultPayTaxInfo != null && jestResultPayTaxInfo.isSucceeded()) {
				final SearchSourceBuilder searchBuilderPymtType = paymentIngestionQueryUtil
						.constructSelectQuery("stateInfo" + ".channelSeqId", context.getChannelSeqId());
				LOGGER.info("In performReplaceDocument after SearchSourceBuilder query: {}",context.getChannelSeqId());
				paymentIngestionQueryUtil.deleteByQuery(searchBuilderPymtType, paymentElasticProperties.getPymtIndex(),
						paymentElasticProperties.getPymtType());
				LOGGER.info("In performReplaceDocument last delete query: {}",context.getChannelSeqId());
//			}
//		}
		// Do a fresh Insert
		performFirstInsert(paymentInstruction, context, requestMessageJson, existingStateInfoJson);

	}

	/**
	 * Requeue message.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 * @param requeueMsg
	 *            the requeue msg
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void requeueMessage(final Context context, final StateTransitionData stateTransitionData,
			final String requeueMsg) throws RequeueException, InterruptedException, DLQException {

		final String contextEventTime = Optional.ofNullable(context.getEventTime()).orElse(null);
		if (contextEventTime == null) {
			LOGGER.error("EventTime is missing in the request for the channelSeqId ---> {}, hence pushing the event to DLQ",context.getChannelSeqId());
			throw new DLQException("Inbound request eventTime is missing");
		}
		// formatting the eventtime to localdatetime type
		//added by vinay
		LOGGER.info("CONTEXT EVENT TIME::::"+context.getEventTime());
//		if (!isValidFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",context.getEventTime(), Locale.ENGLISH))
//		{
//			DateTimeFormatter requiredFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
//			DateTimeFormatter currFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
//			LocalDateTime currEventTimeParsed =  LocalDateTime.parse(context.getEventTime(), currFormatter);
//			requiredEventTimeStr = currEventTimeParsed.format(requiredFormatter);
//		}
		final LocalDateTime formattedEventTime = getFormattedDate(context.getEventTime());
		final LocalDateTime currentDateTime = LocalDateTime.now();
		final Duration d1 = Duration.between(formattedEventTime, currentDateTime);
		final Duration d2 = Duration.ofMinutes(paymentRabbitProperties.getRmqRequeueExpTime());
		if (d1.compareTo(d2) > 0) {
			LOGGER.error("Requeue Time expired for the channelSeqId ---> {}, hence pushing the event to DLQ",context.getChannelSeqId());
			throw new DLQException("Requeue Time expired");

		} else {
			Thread.sleep(paymentRabbitProperties.getRetrySleepTime());
			LOGGER.info(
					"Requeue reason ---->>>>> From State: {} From OutCome: {} >>>>>>>>><<<<<<<<<< To State: {} To Outcome: {} ",
					stateTransitionData.getFromState(), stateTransitionData.getFromOutcomeCategory(),
					stateTransitionData.getToState(), stateTransitionData.getToOutcomeCategory());
			throw new RequeueException("---->>>> " + requeueMsg);
		}

	}

	/**
	 * Gets the formatted date.
	 *
	 * @param eventTime
	 *            the event time
	 * @return the formatted date
	 */
	private LocalDateTime getFormattedDate(final String eventTime) {
		if (eventTime == null) {
			return null;
		}
		try {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			//final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
			return LocalDateTime.parse(eventTime, formatter);
		} catch (DateTimeParseException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_031 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_031_Desc);
			LOGGER.error("Invalid EventTime format");
			return null;
		}

	}

	/**
	 * Skip message.
	 *
	 * @param context
	 *            the context
	 * @return the jest result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public JestResult skipMessage(final Context context) throws IOException {
		LOGGER.info("Skipping the event with channelseqid {} and outcomecategory {} ", context.getChannelSeqId(),context.getEventType().getOutcomeCategory());
		return null;
	}

	public void updateExisitingActiveRecordInES(String existingDomainSeqId,String existingChannelSeqId,String UiDisplayStatus,PaymentsInstructionsES paymentsInstructionsES) {
		try {
			LOGGER.info("Start updateExisitingActiveRecordInESr id::::::: {}", existingDomainSeqId);
			// Get active record from transition record
			ObjectNode payloadJson = (ObjectNode) paymentIngestionQueryUtil.fetchExistingRecordFromES(existingDomainSeqId);// paymentsInstructionsES1.getTransactionId()
			ObjectNode payloadJsonTransition = (ObjectNode) paymentIngestionQueryUtil.fetchExistingRecordFromES(existingChannelSeqId);// paymentsInstructionsES1.getTransactionId()
			PaymentsInstructionsES paymentsInstructionsESTransition = objectMapper.readValue(payloadJsonTransition.toString(), PaymentsInstructionsES.class);
			
			PaymentsInstructionsES paymentsInstructionsESActive;
			paymentsInstructionsESActive = objectMapper.readValue(payloadJson.toString(), PaymentsInstructionsES.class);
			LOGGER.info("Before Setting Reason Code");
			paymentsInstructionsESActive.setReason(paymentsInstructionsESTransition.getReason());
			LOGGER.info("After Setting Reason Code");
			paymentsInstructionsESActive.setCbxUIStatus(PaymentIngestionConstant.IN_TRANSIT);
			paymentsInstructionsESActive.setUiDisplayStatusES(UiDisplayStatus);
			paymentsInstructionsESActive.getStateInfo().setChannelSeqId(existingChannelSeqId);
            paymentsInstructionsESActive.setReleaseDate(Optional.ofNullable(paymentsInstructionsES).map(PaymentsInstructionsES::getReleaseDate).orElse(null));
			JestResult result = updateStateInfo(existingDomainSeqId, paymentsInstructionsESActive);
			if (result != null && result.isSucceeded()) {
				LOGGER.info("State Update for fulfillrtp active record is successfull for id::::::: {}", existingDomainSeqId);
			} else {
				LOGGER.info("Result of status update for fulfillrtp active record in case of failure :::::: {}",result.getJsonString());
				LOGGER.info("State Updation has not yet succeeded for the incoming request with domainSequenceId:::::: {} ",existingDomainSeqId);
			}
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_032 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_032_Desc);
			LOGGER.error("Exception e::::: {} ", e);
		}
	}
	//vinay added
	public boolean isValidFormat(String format, String value, Locale locale) {
	    LocalDateTime ldt = null;
	    DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);
	    try {
	        ldt = LocalDateTime.parse(value, fomatter);
	        String result = ldt.format(fomatter);
	        return result.equals(value);
	    } catch (DateTimeParseException e) {
	        try {
	        	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_033 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	        			+ PaymentIngestionErrorCodeConstants.ERR_PAYING_033_Desc);
	            LocalDate ld = LocalDate.parse(value, fomatter);
	            String result = ld.format(fomatter);
	            return result.equals(value);
	        } catch (DateTimeParseException exp) {
	            try {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_034 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	            			+ PaymentIngestionErrorCodeConstants.ERR_PAYING_034_Desc);
	                LocalTime lt = LocalTime.parse(value, fomatter);
	                String result = lt.format(fomatter);
	                return result.equals(value);
	            } catch (DateTimeParseException e2) {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_035 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	    					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_035_Desc);
	            }
	        }
	    }
	    return false;
	}
	private PaymentsInstructionsES buildStateInfoPI(PaymentsInstructionsES existingPayInstructionES, final Context context)
			throws IOException {
		// build state info for PI
		final StateInfo stateInfo = new StateInfo();
		LOGGER.debug("Building stateInfo tag");
		final TransitionInfo transitionInfo = new TransitionInfo();
		//if (status.equalsIgnoreCase(PaymentIngestionConstant.REJECTED)) {
			final RejectionInfo rejectionInfo = new RejectionInfo();
			rejectionInfo.setSource("system");
			rejectionInfo.setMessage("Request validation failed(Next action is null)");
			transitionInfo.setRejectionInfo(rejectionInfo);
			stateInfo.setDocStatus("transition");
		//}
		transitionInfo.setState("val_failure");
		transitionInfo.setStatus("completed");
		transitionInfo.setRequestType(context.getEventType().getRequestType());
		transitionInfo.setOutcomeCategory(context.getEventType().getOutcomeCategory());
		transitionInfo.setLastActivityTs(context.getEventTime());
		stateInfo.setTransitionInfo(transitionInfo);
		existingPayInstructionES.setStateInfo(stateInfo);
		return existingPayInstructionES;
	}
	
	public void updateCumulativeStatusForAEHoldIRTP(String existingDomainSeqId,String existingChannelSeqId) {
		try {
			LOGGER.info("Start updateActiveRecordforAEHold id::::::: {}", existingDomainSeqId);
			// Get active record from transition record
			ObjectNode payloadJsonForActive = (ObjectNode) paymentIngestionQueryUtil.fetchExistingRecordFromES(existingDomainSeqId);// paymentsInstructionsES1.getTransactionId()
			PaymentsInstructionsES paymentsInstructionsESActive;
			paymentsInstructionsESActive = objectMapper.readValue(payloadJsonForActive.toString(), PaymentsInstructionsES.class);
			ObjectNode payloadJsonForTransition = (ObjectNode) paymentIngestionQueryUtil.fetchExistingRecordFromES(existingChannelSeqId);// paymentsInstructionsES1.getTransactionId()
			PaymentsInstructionsES paymentsInstructionsESTransition;
			paymentsInstructionsESTransition = objectMapper.readValue(payloadJsonForTransition.toString(), PaymentsInstructionsES.class);
			
			/** Set status */
			paymentsInstructionsESActive.setCbxUIStatus(paymentsInstructionsESTransition.getCbxUIStatus());
			/** Set Debtor Account */
			paymentsInstructionsESActive.setDebtorAccount(paymentsInstructionsESTransition.getDebtorAccount());
			/** Set cumulative status */
			//1.1 starts To be removed the next line
			paymentsInstructionsESActive.setCumulativeStatusES(
					(paymentsInstructionsESActive.getStateInfo().getDocStatus()!= null?
							paymentsInstructionsESActive.getStateInfo().getDocStatus():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getCbxUIStatus()!=null?
							paymentsInstructionsESTransition.getCbxUIStatus():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getFaeIndicator()!=null?
							paymentsInstructionsESTransition.getFaeIndicator():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getBccAction()!=null?
							paymentsInstructionsESTransition.getBccAction():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					PaymentIngestionConstant.IRTP_STR
					);
			
			paymentsInstructionsESActive.setFetchQueryStatus(
					(paymentsInstructionsESActive.getStateInfo().getDocStatus()!= null?
							paymentsInstructionsESActive.getStateInfo().getDocStatus():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getCbxUIStatus()!=null?
							paymentsInstructionsESTransition.getCbxUIStatus():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getFaeIndicator()!=null?
							paymentsInstructionsESTransition.getFaeIndicator():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					(paymentsInstructionsESTransition.getBccAction()!=null?
							paymentsInstructionsESTransition.getBccAction():
							PaymentIngestionConstant.NULL)+
					PaymentIngestionConstant.BLANK+
					PaymentIngestionConstant.IRTP_STR
					);
			
			//1.1
			
			JestResult result = paymentIngestionQueryUtil.updateExistingDocument(paymentsInstructionsESActive, existingDomainSeqId,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType(), null);
			if (result != null && result.isSucceeded()) {
				LOGGER.info("Updated result for {} >>>>>>> {}", existingDomainSeqId, result.getSourceAsString());
				LOGGER.info("State Update for fulfillrtp active record is successful for id::::::: {}", existingDomainSeqId);
			} else {
				LOGGER.info("Result of status update for fulfillrtp active record in case of failure :::::: {}",result.getJsonString());
				LOGGER.info("State Updation has not yet succeeded for the incoming request with domainSequenceId:::::: {} ",existingDomainSeqId);
			}
		} catch (Exception e) {
			LOGGER.error("Exception e::::: {} ", e);
		}
	}
}
