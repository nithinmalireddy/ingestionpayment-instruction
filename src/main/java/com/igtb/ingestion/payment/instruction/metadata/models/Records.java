package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class Records.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Records { //NOPMD TooManyFields

	/** The arrive on date. */
	@JsonProperty("arrive_on_date")
	private RecordData arriveOnDate;
	
	/** The cur. */
	@JsonProperty("cur")
	private RecordData cur;
	
	/** The charge type. */
	@JsonProperty("charge_type")
	private RecordData chargeType;
	
	/** The exchange rate amount. */
	@JsonProperty("exchange_rate_amount")
	private RecordRateAmount exchangeRateAmount;
	
	/** The send on date. */
	@JsonProperty("send_on_date")
	private RecordData sendOnDate;
	
	/** The beneficiary bank name. */
	@JsonProperty("beneficiary_bank_name")
	private RecordData beneficiaryBankName;
	
	/** The total debit amount. */
	@JsonProperty("total_debit_amount")
	private RecordRateAmount totalDebitAmount;
	
	/** The credit account no. */
	@JsonProperty("credit_account_no")
	private RecordData creditAccountNo;
	
	/** The debit amount. */
	@JsonProperty("debit_amount")
	private RecordRateAmount debitAmount;
	
	/** The payment rail. */
	@JsonProperty("payment_rail")
	private RecordData paymentRail;
	
	/** The debit account curr. */
	@JsonProperty("debit_account_curr")
	private RecordData debitAccountCurr;
	
	/** The any ID. */
	@JsonProperty("anyID")
	private RecordData anyID;
	
	/** The debit account name. */
	@JsonProperty("debit_account_name")
	private RecordData debitAccountName;
	
	/** The total charges. */
	@JsonProperty("total_charges")
	private RecordRateAmount totalCharges;
	
	/** The payment reason. */
	@JsonProperty("payment_reason")
	private RecordData paymentReason;
	
	/** The ben name. */
	@JsonProperty("ben_name")
	private RecordData benName;
	
	/** The purpose code. */
	@JsonProperty("purpose_code")
	private RecordData purposeCode;
	
	/** The id. */
	@JsonProperty("id")
	private RecordData id;
	
	/** The credit account name. */
	@JsonProperty("credit_account_name")
	private RecordData creditAccountName;
	
	/** The st. */
	@JsonProperty("st")
	private RecordData st;
	
	/** The exchange rate. */
	@JsonProperty("exchange_rate")
	private RecordRateAmount exchangeRate;
	
	/** The err. */
	@JsonProperty("err")
	private RecordData err;
	
	/** The pymnt amnt. */
	@JsonProperty("pymnt_amnt")
	private RecordRateAmount pymntAmnt;
	
	/** The debit account no. */
	@JsonProperty("debit_account_no")
	private RecordData debitAccountNo;
	
	/** The vd. */
	@JsonProperty("vd")
	private RecordData vd;
	
	/** The internal txn id. */
	@JsonProperty("internal_txn_id")
	private RecordData internalTxnId;
	
	/** The debit account entity. */
	@JsonProperty("debit_account_entity")
	private RecordData debitAccountEntity;
	
	/** The exchange rate type. */
	@JsonProperty("exchange_rate_type")
	private RecordData exchangeRateType;
	
	/** The beneficiary bank city. */
	@JsonProperty("beneficiary_bank_city")
	private RecordData beneficiaryBankCity;
	
	/** The beneficiary bank country. */
	@JsonProperty("beneficiary_bank_country")
	private RecordData beneficiaryBankCountry;
	
	/** The debit account cntry. */
	@JsonProperty("debit_account_cntry")
	private RecordData debitAccountCntry;
	
	/** The beneficiary bank branch. */
	@JsonProperty("beneficiary_bank_branch")
	private RecordData beneficiaryBankBranch;
	
	/** The file id. */
	@JsonProperty("fileId")
	private RecordData fileId;
	
	/** The credit description. */
	@JsonProperty("credit_description")
	private RecordData creditDescription;
	
	/** The business product id. */
	@JsonProperty("business_product_id")
	private RecordData businessProductId;
	
	/** The customer transaction ref no. */
	@JsonProperty("customer_transaction_ref_no")
	private RecordData customerTransactionRefNo;
	
	/** The beneficiary bank address 1. */
	@JsonProperty("beneficiary_bank_address1")
	private RecordData beneficiaryBankAddress1;
	
	/** The beneficiary bank address 2. */
	@JsonProperty("beneficiary_bank_address2")
	private RecordData beneficiaryBankAddress2;
	
	/** The beneficiary bank address 3. */
	@JsonProperty("beneficiary_bank_address3")
	private RecordData beneficiaryBankAddress3;
	
	/** The beneficiary clearing id. */
	@JsonProperty("beneficiary_clearing_id")
	private RecordData beneficiaryClearingId;
	
	/** The beneficiary clearing id type. */
	@JsonProperty("beneficiary_clearing_id_type")
	private RecordData beneficiaryClearingIdType;
	
	/** The bank entity id. */
	@JsonProperty("bank_entity_id")
	private RecordData bankEntityId;

	/** The bank id. */
	@JsonProperty("bank_id")
	private RecordData bankId;
	
	/** The beneficiary bank type. */
	@JsonProperty("beneficiary_bank_type")
	private RecordData beneficiaryBankType;
	
	/** The beneficiary ishostbank. */
	@JsonProperty("beneficiary_ishostbank")
	private RecordData beneficiaryIshostbank;
	
	/** The dr bank id. */
	@JsonProperty("dr_bank_id")
	private RecordData drBankId;
	
	/** The dr branchcode. */
	@JsonProperty("dr_branch_code")
	private RecordData drBranchcode;
	
	/** The dr bank name. */
	@JsonProperty("dr_bank_name")
	private RecordData drBankName;
	
	/** The dr bank code. */
	@JsonProperty("dr_bank_code")
	private RecordData drBankCode;
	
	/** The dr bank org key. */
	@JsonProperty("dr_bank_org_key")
	private RecordData drBankOrgKey;
	
	/** The dr branch name. */
	@JsonProperty("dr_branch_name")
	private RecordData drBranchName;
	
	/** The payment type code. */
	@JsonProperty("paymentTypeCode")
	private RecordData paymentTypeCode;
	
}
