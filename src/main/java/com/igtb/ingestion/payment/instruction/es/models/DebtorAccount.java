
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class DebtorAccount.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class DebtorAccount {

    /** The id. */
    private String id;
    
    /** The name. */
    private String name;
    
    /** The account no. */
    private String accountNo;
    
    /** The tags. */
    private List<String> tags;
    
    /** The bank. */
    private BankES bank;
    
    /** The status. */
    private String status;
    
    /** The entity. */
    private EntityES entity;
    
    /** The country. */
    private CountryES country;

}
