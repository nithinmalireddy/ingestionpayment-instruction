package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
/**
 * The Class DebitAmountDetails.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)


@Getter
@Setter
public class DebitAmountDetails {
	/** The debitAmount. */
    @JsonProperty("debitAmount")
    public Double debitAmount;
    
    /** The debitCcy. */
    @JsonProperty("debitCcy")
    public String debitCcy;
}
