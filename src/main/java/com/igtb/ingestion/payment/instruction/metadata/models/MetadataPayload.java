package com.igtb.ingestion.payment.instruction.metadata.models;

import java.util.Map;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;


/**
 * The Class MetadataPayload.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetadataPayload {

	/** The file id. */
	@NotEmpty
	public String fileId;
	
	/** The metadata file id. */
	@NotEmpty
	public String metadataFileId;

	/** The file uri. */
	@NotEmpty
	public String fileUri;
	
	/** The status. */
	@NotEmpty
	public String status;
	
	/** The error message. */
	private String errorMessage;
	
	/**
	 * Map metadata properties.
	 *
	 * @param metadata the metadata
	 */
	@JsonProperty("metadata")
    private void mapMetadataProperties(final Map<String, Object> metadata) { //NOPMD unusedPrivateMethod
		this.metadataFileId = (String)metadata.get("fileId");
    	this.fileUri = (String)metadata.get("fileUri");
    	this.status = (String)metadata.get("status");
    	this.errorMessage = (String)metadata.get("errorMessage");
    }

}
