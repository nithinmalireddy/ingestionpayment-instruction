package com.igtb.ingestion.payment.instruction.es.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class SetElementInfo.
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SetElementInfo {

	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	/** The type. */
	private String type;
	
	/** The valid count. */
	private Integer validCount;
	
	/** The control element. */
	private Boolean controlElement;
}
