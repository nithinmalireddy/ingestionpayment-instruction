package com.igtb.ingestion.payment.instruction.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BuildStatusConfiguration {
	private static final Logger LOGGER = LoggerFactory.getLogger(BuildStatusConfiguration.class);
			/*
			key = status+serviceKey+docStatus+outcomeCategory+bccaction+faeindicator+desc
			Retry In Progress
			approvedpaymnt/sendmoneytransitionbackend-state-updatedNULLNULLNULL
			
			backend_updated-faepaymnt/sendmoneytransitionbackend-state-updatedNULLNULLNULL= {DOC_STATUS_STR}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR}
			
			value = Statuc String which will be used to save into the "fetchQueryStatus" field in ES, And existing cumulativeStatus and PaymentCenterStatus will be removed

			status=eventType.status,serviceKey=eventType.serviceKey,docStatus=From ES,outcomeCategory=eventType.outcomeCategory
			bccaction=From ES bccaction,faeindicator=From ES faeindicator,desc=From ES stateInfo.transitionInfo.additionalInfo.desc
			cumulativeStatus<This is a configured string>

			
			approvedpaymnt/fulfillrtpNULLchannel-state-updatedPENDINGAEHoldNULL=DOC_STATUS_STR+TRANSITION_DESC+CBX_UI_STATUS+FAE_IND+BCC_ACTION_STR+IRTP
			releasedpaymnt/fulfillrtpNULLchannel-state-updatedPENDINGAEHoldNULL=BLANK_STR
			NULLNULLactiveNULLNULLNULLcancelled=DOC_STATUS_STR+TRANSITION_DESC
			NULLNULLactiveNULLNULLNULLexpired=DOC_STATUS_STR+TRANSITION_DESC
			NULLNULLactiveNULLNULLNULLdeclined=DOC_STATUS_STR+TRANSITION_DESC
			NULLNULLtransitionNULLNULLNULLrejected=DOC_STATUS_STR+TRANSITION_DESC
			NULLNULLNULLNULLNULLNULLNULL=DOC_STATUS_STR+TRANSITION_DESC+CBX_UI_STATUS+FAE_IND+BCC_ACTION_STR
			*/
	
	/*NULLNULLactiveNULLNULLNULLcancelled= {DOC_STATUS_STR}{TRANSITION_DESC},NULLNULLactiveNULLNULLNULLexpired= {DOC_STATUS_STR}{TRANSITION_DESC},NULLNULLactiveNULLNULLNULLdeclined= {DOC_STATUS_STR}{TRANSITION_DESC},NULLNULLtransitionNULLNULLNULLrejected= {DOC_STATUS_STR}{TRANSITION_DESC},*/
	public final static String CONFIG_KEY_VALUES = "approvedpaymnt/fulfillrtpNULLchannel-state-updatedPENDINGAEHoldNULL= {DOC_STATUS_STR}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR}{IRTP},releasedpaymnt/fulfillrtpNULLchannel-state-updatedPENDINGAEHoldNULL= {BLANK_STR},NULLNULLNULLNULLNULLNULLNULL= {DOC_STATUS_STR}{TRANSITION_DESC}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR},"
			+ "NULLNULLactiveNULLNULLNULLcancelled=activecancelled,NULLNULLactiveNULLNULLNULLexpired=activeexpired,NULLNULLactiveNULLNULLNULLdeclined=activedeclined,NULLNULLtransitionNULLNULLNULLRejected=transitionRejected"+
			"backend_updated-faepaymnt/sendmoneytransitionbackend-state-updatedNULLNULLNULL= {DOC_STATUS_STR}{CBX_UI_STATUS}{FAE_IND}{BCC_ACTION_STR}";
	
	
	public Map<String, String> getConfigMap(){
		String[] keyValueArray = CONFIG_KEY_VALUES.split(",");
		Map<String, String> configMap = new HashMap<String, String>();
		String[] keyValuePair = null;
		for (int i = 0; i < keyValueArray.length; i++) {
			keyValuePair = keyValueArray[i].split("=");
			configMap.put(keyValuePair[0], keyValuePair[1]);
		}
		LOGGER.info("configMap:: ", configMap);
		return configMap;
	}
}
