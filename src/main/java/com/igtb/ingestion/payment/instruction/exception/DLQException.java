package com.igtb.ingestion.payment.instruction.exception;

/**
 * The Class DLQException.
 */
public class DLQException extends Exception{

    /**  Serial Version. */
    private static final long serialVersionUID = 2582722819300428882L;


    /**
     * Instantiates a new DLQ exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public DLQException(final String message, final Throwable cause) {
        super(message, cause);
    }


    /**
     * Instantiates a new DLQ exception.
     *
     * @param message the message
     */
    public DLQException(final String message) {
        super(message);
    }
}
