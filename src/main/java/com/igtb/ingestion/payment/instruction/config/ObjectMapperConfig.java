package com.igtb.ingestion.payment.instruction.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class ObjectMapperConfig.
 */
@Configuration
@Slf4j
public class ObjectMapperConfig {
	
	/**
	 * Object mapper.
	 *
	 * @return the object mapper
	 */
	@Bean
	@Primary
	public ObjectMapper objectMapper() {

		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS);
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.setSerializationInclusion(Include.USE_DEFAULTS);
		objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
		objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
		log.info("OBJECT MAPPER DEFAULT");
		return objectMapper;

	}
	
	
	/**
	 * Object mapper for alerts.
	 *
	 * @return the object mapper
	 */
	@Bean("objectMapperAlerts")
	public ObjectMapper objectMapperAlerts() {

		final ObjectMapper objectMapper = new ObjectMapper();
//		objectMapper.disable(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS);
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
//		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
//		objectMapper.setSerializationInclusion(Include.USE_DEFAULTS);
//		objectMapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
//		objectMapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setSerializationInclusion(Include.NON_EMPTY);
		log.info("OBJECT MAPPER ALERTS");
		return objectMapper;

	}	
	
}
