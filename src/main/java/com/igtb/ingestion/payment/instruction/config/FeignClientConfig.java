package com.igtb.ingestion.payment.instruction.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.util.BulkProcessingChannelUtil;

import feign.RetryableException;
import feign.Retryer;

@Configuration
public class FeignClientConfig {
    @Bean
    public Retryer retryer() {
        return new Custom();
    }

}

class Custom implements Retryer {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkProcessingChannelUtil.class);
    private final int maxAttempts;
    private final long backoff;
    int attempt;

    public Custom() {
        this(2000, 3);
    }

    public Custom(long backoff, int maxAttempts) {
        this.backoff = backoff;
        this.maxAttempts = maxAttempts;
        this.attempt = 1;
    }

    public void continueOrPropagate(RetryableException e) {
        if (attempt++ >= maxAttempts) {
            throw e;
        }

        try {
            Thread.sleep(backoff);
        } catch (InterruptedException ignored) {

        	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_012 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_012_Desc);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Retryer clone() {
        return new Custom(backoff, maxAttempts);
    }
}