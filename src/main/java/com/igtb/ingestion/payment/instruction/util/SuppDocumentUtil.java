package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.ingestion.payment.instruction.es.models.SupportingDocumentES;
import com.igtb.ingestion.payment.instruction.models.Attachment;

import io.searchbox.core.Index;

/**
 * Supporting Document Utiltiy.
 * 
 */
@Component
public class SuppDocumentUtil {

	/** Supporting documents */
	private List<SupportingDocumentES> suppDocList;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SuppDocumentUtil.class);

	/**
	 * A utility method to persist all the attached supporting documents.
	 *
	 * @param attachments
	 *            the attachments
	 * @param stateInfo
	 *            parent instructionsid
	 * @return the supp doc list
	 * @throws IOException
	 *             exception
	 */
	public List<Index> getSuppDocList(final List<Attachment> attachments, final StateInfo stateInfo)
			throws IOException {
		LOGGER.info(">.............Started SuppDocumentUtil :: persistSupportingDocumentsinES.............<");
		populateSuppDocsList(attachments, stateInfo);
		if (suppDocList != null && suppDocList.size() > 0) {
			LOGGER.info("Returning from SuppDocumentUtil :: persistSupportingDocumentsinES");
			return getSuppDocsIndexList();
		} else {
			LOGGER.warn("Returning from SuppDocumentUtil without populating list");
			return null;
		}
	}

	private void populateSuppDocsList(final List<Attachment> attachments, final StateInfo stateInfo) {
		LOGGER.info("Started SuppDocumentUtil :: populateSupportingDocList");
		suppDocList = new ArrayList<>();
		if (attachments == null) {
			LOGGER.debug("No attachments found");
			return;
		}
		for (final Attachment attachment : attachments) {
			final SupportingDocumentES suppDoc = new SupportingDocumentES();
			suppDoc.setDocumentId(attachment.getFileId());
			suppDoc.setDocumentType(attachment.getDocumentType());
			suppDoc.setDocumentName(attachment.getFileName());
			suppDoc.setPaymentInstructionId(stateInfo.getChannelSeqId());
			suppDoc.setPaymentDocStatus(stateInfo.getDocStatus());
			suppDoc.setPaymentTransactionId(stateInfo.getDomainSeqId());
			suppDoc.setUploadDateTime(stateInfo.getLastAction().getTs());
			suppDoc.setDocumentFormatType(attachment.getFormatType());
			suppDoc.setDocumentRemark(attachment.getDocumentRemark());
			suppDoc.setSize(attachment.getDocumentSize());
			suppDocList.add(suppDoc);
		}
	}

	/**
	 * Helper method to bulk index the supporting documents attached.
	 * 
	 * @return
	 * 
	 * @throws IOException
	 *             IO Exception
	 */
	private List<Index> getSuppDocsIndexList() {
		LOGGER.info("Started SuppDocumentUtil :: addSupportingDocumentsinES");
		final List<Index> listSuppDocIndex = new ArrayList<>();
		if (suppDocList != null) {
			LOGGER.info("List of supporting documents to be inserted are -> {}", suppDocList);
			for (final SupportingDocumentES suppDOC : suppDocList) {
				if (Optional.ofNullable(suppDOC).map(SupportingDocumentES::getDocumentId).isPresent()) {
					listSuppDocIndex.add(new Index.Builder(suppDOC)
							.id(suppDOC.getDocumentId() + "-" + suppDOC.getPaymentInstructionId()).build());
				}
			}

			return listSuppDocIndex;
		}
		return null;
	}
}
