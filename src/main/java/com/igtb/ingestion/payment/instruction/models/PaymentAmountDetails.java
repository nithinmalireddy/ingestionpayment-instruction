package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
/**
 * The Class PaymentAmountDetails.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class PaymentAmountDetails {
	
	/** The paymentAmount. */
    @JsonProperty("paymentAmount")
    public Double paymentAmount;
    
    /** The paymentCcy. */
    @JsonProperty("paymentCcy")
    public String paymentCcy;

}
