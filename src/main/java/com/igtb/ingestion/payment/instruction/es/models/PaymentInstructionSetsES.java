
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class PaymentInstructionSetsES.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ToString
public class PaymentInstructionSetsES { // NOPMD TooManyFields

    /** The record set id. */
    private String recordSetId;
    
    /** The type. */
    private String type;
    
    /** The corporate entity. */
    private EntityES corporateEntity;
    
    /** The set name. */
    private String setName;
    
    /** The upload file name. */
    private String uploadFileName;
    
    /** The template. */
    private Template template;
    
    /** The payment reason. */
    private PaymentReasonES paymentReason;
    
    /** The debtor account. */
    private DebtorAccount debtorAccount;
    
    /** The requested execution date. */
    private String requestedExecutionDate;
    
    /** The txn currency. */
    private TxnCurrency txnCurrency;
    
    /** The instructed amount base ccy. */
    private AmountES instructedAmountBaseCcy;
    
    /** The instructed amount base ccy. */
    private AmountES instructedAmount;
    
    /** The metrics. */
    private Metrics metrics;
    
    /** The partial approval. */
    private Boolean partialApproval;
    
    /** The has sets. */
    private Boolean hasSets;
    
    /** The created. */
    private String created;
    
    /** The last modified. */
    private String lastModified;

    /** The additional properties. */
    private Map<String, Object> additionalInfo = new HashMap();
    
    /** The parent set. */
    private ParentSet parentSet;
    
    /** The file size. */
    private Long fileSizeInBytes;
    
    /** The file processing flag. */
    private String fileProcessingFlag;
    
	/** The state info. */
	private StateInfo stateInfo;
	
	/** The set element info. */
	private SetElementInfoES setElementInfo;
	
	/** The status. */
	private String status;
	
	/** The payment types obj. */
	private PaymentTypeES paymentTypesObj;
}
