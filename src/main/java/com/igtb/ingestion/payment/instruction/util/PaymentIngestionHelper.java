package com.igtb.ingestion.payment.instruction.util;

import static com.igtb.api.ingestion.commons.file.stateInfo.FileStateInfoUtil.fileStateInfoEnrichment;
import static com.igtb.api.ingestion.commons.stateinfo.StateInfoUtil.stateInfoEnrichment;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.igtb.api.ingestion.commons.file.models.FileStateInfo;
import com.igtb.api.ingestion.commons.file.stateInfo.FileStateInfoUtil;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.DMTransitionStateConstants;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;
import com.igtb.ingestion.payment.instruction.es.models.EntityES;
import com.igtb.ingestion.payment.instruction.es.models.UserES;
import com.igtb.ingestion.payment.instruction.models.Context;

import io.searchbox.client.JestResult;
import io.searchbox.strings.StringUtils;
import lombok.AllArgsConstructor;

/**
 * The Class PaymentIngestionHelper.
 */
@Component

/**
 * Instantiates a new payment ingestion helper.
 *
 * @param producerTemplate
 *            the producer template
 * @param paymentRabbitProperties
 *            the payment rabbit properties
 * @param paymentElasticProperties
 *            the payment elastic properties
 * @param objectMapper
 *            the object mapper
 * @param paymentIngestionQueryUtil
 *            the payment ingestion query util
 * @param beTriggeredNonWFAlertsEnrichUtil
 *            the be triggered non WF alerts enrich util
 */
@AllArgsConstructor
public class PaymentIngestionHelper {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionHelper.class);

	/** The producer template. */
	private final ProducerTemplate producerTemplate;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/** The payment ingestion jest util. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The BE Triggered NonWf Alerts Enrichment Util *. */
	private final BETriggeredNonWFAlertsEnrichUtil beTriggeredNonWFAlertsEnrichUtil;

	/**
	 * Gets the state info from commons module.
	 *
	 * @param domainRequestJson
	 *            the domain request json
	 * @param userInfoJson
	 *            the user info json
	 * @param existingStateInfo
	 *            the existing state info
	 * @param moduleName
	 *            the module name
	 * @return the state info from commons module
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public StateInfo getStateInfoFromCommonsModule(final JsonNode domainRequestJson, final JsonNode userInfoJson,
			final JsonNode existingStateInfo, final String moduleName) throws IOException {

		// rest call to the framework to be written
		final StateInfo stateInfo = stateInfoEnrichment(domainRequestJson, userInfoJson, existingStateInfo, moduleName);

		return stateInfo;
	}

	/**
	 * Gets the file state info from commons module.
	 *
	 * @param domainRequestJson
	 *            the domain request json
	 * @param userInfoJson
	 *            the user info json
	 * @param existingStateInfo
	 *            the existing state info
	 * @param moduleName
	 *            the module name
	 * @return the file state info from commons module
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public FileStateInfo getFileStateInfoFromCommonsModule(final JsonNode domainRequestJson,
			final JsonNode userInfoJson, final JsonNode existingStateInfo, final String moduleName) throws IOException {

		// rest call to the framework to be written
		final FileStateInfo stateInfo = fileStateInfoEnrichment(domainRequestJson, userInfoJson, existingStateInfo,
				moduleName);

		return stateInfo;
	}

	/**
	 * Gets the file state info and mark rejected as no valid record.
	 *
	 * @param existingStateInfo
	 *            the existing state info
	 * @return the file state info and mark rejected as no valid record
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public FileStateInfo getFileStateInfoAndMarkRejectedAsNoValidRecord(final FileStateInfo existingStateInfo)
			throws IOException {

		// rest call to the framework to be written
		FileStateInfoUtil.markRejectedAsNoValidRecord(existingStateInfo);
		LOGGER.debug("Updated fileStateInfo for NONE_VALID: {}", existingStateInfo);
		return existingStateInfo;
	}

	/**
	 * Enrich user info.
	 *
	 * @param entity
	 *            the entity
	 * @param userInfo
	 *            the user info
	 * @return the entity ES
	 */
	public EntityES enrichUserInfo(final String entity, final UserInfo userInfo) {
		try {
			LOGGER.debug("User enrichment started with entity: {}, userName: {}, domainName: {}", entity,userInfo.getUserName(), userInfo.getDomainName());

			if (StringUtils.isBlank(userInfo.getUserName()) || StringUtils.isBlank(userInfo.getDomainName())) {
				LOGGER.warn("Not enriching user as userName: {} or domainName: {} not available",userInfo.getUserName(), userInfo.getDomainName());
				return null;
			}

			final String[] fetchFields = { "first_name", "last_name", "imageId", "domainInfo", "communicationInfo" };
			final JestResult jestResult = paymentIngestionQueryUtil.enrichUserInfoUsingTermNestedQuery(userInfo.getUserName(), userInfo.getDomainName(), "user_name.keyword", "domainInfo.name.keyword",fetchFields, paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getUserType());
			EntityES entityEs = new EntityES();
			if (jestResult != null) {
				LOGGER.debug("User query response: {}", jestResult.getSourceAsString());
				final JsonArray hitsArray = Optional.ofNullable(jestResult.getJsonObject()).map(jsonObject -> jsonObject.get(PaymentIngestionConstant.HITS)).map(outerHits -> outerHits.getAsJsonObject()).map(innerHitJsonObject -> innerHitJsonObject.get(PaymentIngestionConstant.HITS)).map(innerHits -> innerHits.getAsJsonArray()).orElse(null);
				if (hitsArray != null) {
					hitsArray.forEach(hit -> {
						final JsonObject userJson = hit.getAsJsonObject();
						if (userJson != null && userJson.get(PaymentIngestionConstant.SOURCE) != null) {
							try {
								final UserES userES = objectMapper.readValue(hit.getAsJsonObject().get(PaymentIngestionConstant.SOURCE).toString(),UserES.class);

								// 3. imageId
								userInfo.setImageId(userES.getImageId());

								// 4. role
								userES.getDomainInfo().stream().filter(Objects::nonNull).filter(domain -> userInfo.getDomainName().equalsIgnoreCase(domain.getName())).forEach(domainInfo -> {domainInfo.getCorporateDesignations().stream().filter(corporate -> entity.equalsIgnoreCase(corporate.getCorporate().getId()) || entity.equalsIgnoreCase(corporate.getCorporate().getKey())).forEach(corpDesg -> {
														final String role = Optional.ofNullable(corpDesg.getDesignation()).map(desg -> desg.getName()).orElse(null);

														userInfo.setRole(role);
														EntityES primaryEntity = Optional.ofNullable(domainInfo.getPrimaryEntity()).orElse(new EntityES());
														entityEs.setId(primaryEntity.getId());
														entityEs.setName(primaryEntity.getName());
													});
										});

								// 5. name
								userInfo.setName(userES.getFirstName() + " " + userES.getLastName());

								// 6. email
								Optional.ofNullable(userES.getCommunicationInfo()).map(commInfo -> commInfo.getEmail()).ifPresent(userInfo::setEmail);

								// 7. phone
								Optional.ofNullable(userES.getCommunicationInfo()).map(commInfo -> commInfo.getPhone()).ifPresent(userInfo::setPhone);

							} catch (IOException e) {
								LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_037 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
										+ PaymentIngestionErrorCodeConstants.ERR_PAYING_037_Desc);
								LOGGER.error("Enrichment failed for userinfo: {}, with cause :", userInfo.getUserName(),e);
							}
						}

					});
				} else {
					LOGGER.error("Enrichment failed as hitsArray is null, jestResult : {}", jestResult.getJsonString());
				}
				return entityEs;
			}
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_038 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_038_Desc);
			LOGGER.error("StateInformation userInfo Role enrichment failed");
		}

		return null;
	}

	/**
	 * Publish backend events to rabbit.
	 *
	 * @param requestJsonNode
	 *            the request json node
	 * @param context
	 *            the context
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void publishBackendEventsToRabbit(final JsonNode requestJsonNode, final Context context) throws IOException {
		LOGGER.info("Start  publishBackendEventsToRabbit() for status ::::::::::: {}", context.getEventType().getStatus());
		// Convert the streaming event payload to alerts payload as per
		/*
		 * https://igtbworks.atlassian.net/wiki/spaces/IPSH/pages/1086228888/Workflow+
		 * and+Non-Workflow+Alerts+For+Payments#WorkflowandNon-WorkflowAlertsForPayments
		 * -TableMappingofStreamingEventsv/sAlertsEvent and
		 * https://igtbworks.atlassian.net/wiki/spaces/IPSH/pages/1086228888/Workflow+
		 * and+Non-Workflow+Alerts+For+Payments#WorkflowandNon-WorkflowAlertsForPayments
		 * -ContentrequiredforNotificationDispatcher/andAlertEngine.
		 */
		// isbackenddata event (backend_created, backend_updated, backend_deleted)
		final boolean isBackendDataEvent = Arrays.asList(DMTransitionStateConstants.BE_DATA_EVENTS).contains(TransitionStateEnum.getTransitionStateEnumType(Optional.ofNullable(context).map(ctx -> ctx.getEventType()).map(evtTp -> evtTp.getStatus()).orElse(null)));

		// if backend data event from CBX initiated txns
		if (isBackendDataEvent && !StringUtils.isBlank(context.getChannelSeqId())) {
			LOGGER.debug("Alerts events will not be published for the active events emitted for CBX initiated txns with channelSeqId => {},"+ "and domainSeqId=>{}",context.getChannelSeqId(), context.getDomainSeqId());
			return;
		}
		// else continue...
		ObjectNode requestObjectNode = beTriggeredNonWFAlertsEnrichUtil.convertedAlertsEvtFromStreamingEvt(requestJsonNode, context, isBackendDataEvent);

		// Construct kafka to rmq publish url
		final String camelEndpoint = buildPublishUrl();
		try {
			// Generate routing Key by fetching information from context part of the request
			final String routingKey = generateRoutingKey(context);

			final Map<String, Object> headers = new HashMap<String, Object>();
			headers.put(PaymentIngestionConstant.RMQ_ROUTING_KEY, routingKey);
			headers.put(PaymentIngestionConstant.RMQ_DELIVERY_MODE, Integer.valueOf(2)); // 2=persistent
			headers.put(PaymentIngestionConstant.RMQ_TIMPSTAMP, new Date());

			// Publish events asynchronously (send and forget)
			producerTemplate.sendBodyAndHeaders(camelEndpoint, requestObjectNode.toString(), headers);
			LOGGER.info("Backend Event Published Successfully:::::: {}", requestObjectNode);
			LOGGER.info("Event published to rabbitmq with routing key::::: {}", routingKey);
		} catch (Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_039 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_039_Desc);
			LOGGER.error("Exception while publishing events to rabbitmq : {} {}", ex.getMessage(), ex);
		}
	}

	/**
	 * Builds the publish url.
	 *
	 * @return the string
	 */
	private String buildPublishUrl() {

		return paymentRabbitProperties.getName() + "://" + paymentRabbitProperties.getHost() + ":"
				+ paymentRabbitProperties.getPort() + "/" + paymentRabbitProperties.getExchange() + "?autoDelete=false"
				+ "&exchangeType=topic" + "&skipQueueDeclare=true" + "&username=" + paymentRabbitProperties.getUser()
				+ "&password=" + paymentRabbitProperties.getPassword() + "&vhost=" + paymentRabbitProperties.getVhost()
				+ "&prefetchCount=" + paymentRabbitProperties.getPrefatchCount();
	}

	/**
	 * Generate routing key.
	 *
	 * @param context
	 *            the context
	 * @return the string
	 */
	private String generateRoutingKey(final Context context) {
		final String country = Optional.ofNullable(context.getEventSource())
				.map(eventSource -> eventSource.getCountry()).orElse(null);
		final String region = Optional.ofNullable(context.getEventSource()).map(eventSource -> eventSource.getRegion())
				.orElse(null);
		final String serviceKey = Optional.ofNullable(context.getEventType())
				.map(eventType -> eventType.getServiceKey()).orElse(null);

		// Routing Key --> <category>.<region>.<country>.<serviceKey>.<format>
		final StringBuilder routingKeyBuilder = new StringBuilder(20);

		// 1. <category>
		routingKeyBuilder.append("business.");

		if (!StringUtils.isBlank(region) && !"null".equalsIgnoreCase(region) && !"*".equalsIgnoreCase(region)) {
			// 2. <region>
			routingKeyBuilder.append(region);
		}
		routingKeyBuilder.append('.');

		if (!StringUtils.isBlank(country) && !"null".equalsIgnoreCase(country) && !"*".equalsIgnoreCase(region)) {
			// 3. <country>
			routingKeyBuilder.append(country);
		}
		routingKeyBuilder.append('.');

		if (!StringUtils.isBlank(serviceKey) && !"null".equalsIgnoreCase(serviceKey) && !"*".equalsIgnoreCase(region)) {
			// 4. <serviceKey>
			routingKeyBuilder.append(serviceKey);
		}
		// 5. <format>
		routingKeyBuilder.append(".plain");

		return Optional.ofNullable(routingKeyBuilder).map(routingKey -> routingKey.toString()).orElse(null);
	}

}
