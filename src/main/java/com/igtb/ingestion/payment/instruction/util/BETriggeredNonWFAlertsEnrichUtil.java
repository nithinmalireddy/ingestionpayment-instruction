package com.igtb.ingestion.payment.instruction.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.api.ingestion.commons.stateinfo.models.Initiated;
import com.igtb.api.ingestion.commons.stateinfo.models.LastAction;
import com.igtb.api.ingestion.commons.stateinfo.models.NextAction;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.UserInfo;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.RequestEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;
import com.igtb.ingestion.payment.instruction.es.models.AccountES;
import com.igtb.ingestion.payment.instruction.es.models.BankES;
import com.igtb.ingestion.payment.instruction.es.models.CreditorES;
import com.igtb.ingestion.payment.instruction.es.models.DebtorES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.RemainingInfoES;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Requester;
import com.igtb.ingestion.payment.instruction.util.UIStatusUtil.UIStatusEnum;

import io.searchbox.client.JestResult;
import io.searchbox.strings.StringUtils;

/**
 * Enrichment of backend triggered non workflow alerts utility class.
 * 
 * @author vaishali.gupta
 *
 */
/**
 * @author vaishali.gupta
 *
 */
@Component
//@AllArgsConstructor
public class BETriggeredNonWFAlertsEnrichUtil {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BETriggeredNonWFAlertsEnrichUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;
	
	/** The payment ingestion query util. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;
	
	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/**
	 *  The public constructor to initialize the member variables.
	 *
	 * @param paymentElasticProperties the payment elastic properties
	 * @param paymentIngestionQueryUtil the payment ingestion query util
	 * @param objectMapper the object mapper
	 */
	public BETriggeredNonWFAlertsEnrichUtil(PaymentElasticProperties paymentElasticProperties,
			PaymentIngestionQueryUtil paymentIngestionQueryUtil, @Qualifier(value="objectMapperAlerts") ObjectMapper objectMapper) {
		super();
		this.paymentElasticProperties = paymentElasticProperties;
		this.paymentIngestionQueryUtil = paymentIngestionQueryUtil;
		this.objectMapper = objectMapper;
	}


	/**
	 * Gets the enriched payload to be published for alerts.
	 *
	 * @param requestJsonNode request event node
	 * @param context context
	 * @param isBackendDataEvent the is backend data event
	 * @return the object node
	 */
	public ObjectNode convertedAlertsEvtFromStreamingEvt(final JsonNode requestJsonNode,  final Context context, final boolean isBackendDataEvent) {
		LOGGER.info("Converting streaming backend events to alerts events for publishing alerts.");
		final ObjectNode requestObjectNode = (ObjectNode) requestJsonNode;
		final JsonNode payloadJsonNode = requestJsonNode.get(PaymentIngestionConstant.PAYLOAD);
		PaymentsInstructionsES paymentsInstructionsES = objectMapper.convertValue(payloadJsonNode, 
				PaymentsInstructionsES.class);
		PaymentsInstructionsES paymentsInstructionsAlertES = new PaymentsInstructionsES();
		final PaymentsInstructionsES paymentInstructionActiveES = objectMapper.convertValue(paymentIngestionQueryUtil.jestGetESBuilder(context.getDomainSeqId(), 
				paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType()), 
				PaymentsInstructionsES.class);
		final PaymentsInstructionsES paymentInstructionTransitionES = objectMapper.convertValue(paymentIngestionQueryUtil.jestGetESBuilder(context.getChannelSeqId(), 
				paymentElasticProperties.getPymtIndex(),paymentElasticProperties.getPymtType()), 
				PaymentsInstructionsES.class);
		boolean isHUCReqEnum = Optional.ofNullable(context).map(ctx -> ctx.getEventType()).map(evtTp -> evtTp.getRequestType())
				.map(reqType -> RequestEnum.getRequestEnumType(reqType))
				.map(reqEnum -> RequestEnum.isHUCRequestType(reqEnum))
				.orElse(false);
		
//		TODO : Commenting due to NullPointer while getDebtorDetails. - Deepa - 26June2020
		//Update the context for Alerts
		updateAlertsContext(context, paymentsInstructionsES, isBackendDataEvent, isHUCReqEnum, paymentInstructionActiveES, paymentInstructionTransitionES);
		
		//Update the payload for Alerts(only for backend data events)
		updateAlertsPayload(paymentsInstructionsES, isBackendDataEvent,isHUCReqEnum, paymentsInstructionsAlertES, paymentInstructionActiveES);
		
		final ObjectNode contextObjectNode = objectMapper.convertValue(context, ObjectNode.class);
		final ObjectNode payloadObjectNode = objectMapper.convertValue(paymentsInstructionsAlertES, 
				ObjectNode.class);
		
		requestObjectNode.put("id", "evt_".concat(UUID.randomUUID().toString()));
		requestObjectNode.set("context", contextObjectNode);
		requestObjectNode.set("payload", payloadObjectNode);
		return requestObjectNode;
	}


	/**
	 * Updates the alerts payload.
	 *
	 * @param paymentsInstructionsES incoming streaming payload
	 * @param isBackendDataEvent flag to identify if the incoming req is backend data event or not
	 * @param isHUCReqEnum the is HUC req enum
	 * @param paymentsInstructionsAlertES resulting alert payload
	 * @param paymentInstructionActiveES the payment instruction active ES
	 */
	private void updateAlertsPayload(PaymentsInstructionsES paymentsInstructionsES, final boolean isBackendDataEvent,
			final boolean isHUCReqEnum, PaymentsInstructionsES paymentsInstructionsAlertES, final PaymentsInstructionsES paymentInstructionActiveES
			) {
		LOGGER.debug("Update alerts payload");
		if(isBackendDataEvent) {
			if(!isHUCReqEnum) // create payload from incoming request
				enrichPIAlerts(paymentsInstructionsES, paymentsInstructionsAlertES);
			else // create payload from active data present in ES
				enrichPIAlerts(paymentInstructionActiveES, paymentsInstructionsAlertES);
		}
		LOGGER.debug("returning with {}", paymentsInstructionsAlertES);
	}


	/**
	 * Enrich the PI Alerts data.
	 * 
	 * @param paymentsInstructionsES PI data from request(for add/amend) or PI data from ES(for h/u/c)
	 * @param paymentsInstructionsAlertES resultant alerts PI
	 */
	private void enrichPIAlerts(PaymentsInstructionsES paymentsInstructionsES,
			PaymentsInstructionsES paymentsInstructionsAlertES) {
		//Update the payload as per Alerts(only for backend data events)
		// creditor
		Optional.ofNullable(paymentsInstructionsES.getCreditor())
			.filter(creditor -> creditor.getId() != null && !creditor.getId().isEmpty()).ifPresent(creditor -> {
				CreditorES creditorES = new CreditorES();
				creditorES.setId(creditor.getId());
				paymentsInstructionsAlertES.setCreditor(creditorES);
			});
		// debtor account
		Optional.ofNullable(paymentsInstructionsES.getDebtorAccount())
			.filter(acc -> acc.getAccountNo() != null && !acc.getAccountNo().isEmpty()).ifPresent(debtAcc -> {
				AccountES accES = new AccountES();
				accES.setAccountNo(debtAcc.getAccountNo());
				accES.setName(debtAcc.getName());
				Optional.ofNullable(debtAcc.getBank()).ifPresent(bank -> {
					BankES bankES = new BankES();
					bankES.setId(bank.getId());
					bankES.setName(bank.getName());
					accES.setBank(bankES);
				});
				paymentsInstructionsAlertES.setDebtorAccount(accES);
			});
		// creditor account
		Optional.ofNullable(paymentsInstructionsES.getCreditorAccount())
			.filter(acc -> acc.getAccountNo() != null && !acc.getAccountNo().isEmpty()).ifPresent(credAcc -> {
				AccountES accES = new AccountES();
				accES.setAccountNo(credAcc.getAccountNo());
				accES.setName(credAcc.getName());
				paymentsInstructionsAlertES.setCreditorAccount(accES);
			});
		// other remaining info
		Optional.ofNullable(paymentsInstructionsES.getOtherRemainingInfo()).filter(
			remInfo -> remInfo.getCreditorAccNumType() != null && !remInfo.getCreditorAccNumType().isEmpty())
			.ifPresent(remInfo -> {
				RemainingInfoES remInfoES = new RemainingInfoES();
				remInfoES.setCreditorAccNumType(remInfo.getCreditorAccNumType());
			});
		paymentsInstructionsAlertES.setUserDateTime(paymentsInstructionsES.getUserDateTime());
		paymentsInstructionsAlertES.setDebtor(paymentsInstructionsES.getDebtor());
		paymentsInstructionsAlertES.setInstructedAmount(paymentsInstructionsES.getInstructedAmount());
		paymentsInstructionsAlertES.setPaymentReason(paymentsInstructionsES.getPaymentReason());
		paymentsInstructionsAlertES.setReason(paymentsInstructionsES.getReason());
	}

	
	/**
	 * Updates the alerts context.
	 * 
	 * @param context incoming streaming context
	 * @param payloadJsonNode incoming payload node
	 * @param opContext context
	 * @param isBackendDataEvent flag to identify if the incoming req is backend data event or not
	 */
	private void updateAlertsContext(final Context context, final PaymentsInstructionsES paymentsInstructionsES,
			final boolean isBackendDataEvent, final boolean isHUCReqEnum, final PaymentsInstructionsES paymentInstructionActiveES,
			final PaymentsInstructionsES paymentInstructionTransitionES) {
		LOGGER.info("Update alerts context");
		final Optional<Context> opContext = Optional.ofNullable(context);
		String contextStatus = opContext.map(ctx -> ctx.getEventType()).map(evtTp -> evtTp.getStatus()).orElse(null);
		// reading from pom resource
		final String formattedSourceIdentity = paymentElasticProperties.getApplicationName() + "-"+ paymentElasticProperties.getVersion();
		
		LOGGER.debug("---> SourceIdentity----->>>> {}", formattedSourceIdentity);
		
		//1. Update the context -> source identity
		opContext.map(ctx -> ctx.getEventSource()).ifPresent(evSrc -> {
			evSrc.setSourceIdentity(formattedSourceIdentity);
		});
		
		//2. Update the context -> event time
		opContext.ifPresent(ctx -> ctx.setEventTime(new DateTime(DateTimeZone.UTC).toString()));
		
		//3. Update the context -> requester -> domainId(for Alerts)
		//Get customerid from incoming payload else fetch from active record else fetch from transition record
		//Mudasir - changed Debtor to DebtorDetails
		try {
			Function<PaymentsInstructionsES, Optional<String>> getDebtor = paymentsInstructionES -> 
						Optional.ofNullable(paymentsInstructionsES).map(p -> Optional.ofNullable(p.getDebtorDetails()).map(DebtorES ::getId).orElse(null));
			final String customerId = getDebtor.apply(paymentsInstructionsES).orElse(getDebtor.apply(paymentInstructionActiveES)
								.orElse(getDebtor.apply(paymentInstructionTransitionES).orElse(null)));
			final String domainId = fetchDomainIdFromES(customerId, context.getChannelSeqId(), isBackendDataEvent);
			Requester requester = new Requester();
			requester.setDomainId(domainId);
			opContext.ifPresent(ctx -> {ctx.setRequester(requester);});
		}catch(Exception e) {
			LOGGER.error("Debtor detauils not present. Hence, not setting domain id, requestro detail. Proceeding with rest of actions for channelSeQ:{}",context.getChannelSeqId());
		}
		
		//VERY IMP: 
		// To remove domainSeqId from backend_ack_success (otherwise INPROGRESS -> BACKEND_ACK_SUCCESS request in action module
		// will be updated with domainSeqId
		// and the new hold/unhold/and cancel request will always have 409 conflict)
		if(TransitionStateEnum.backend_ack_success.toString().equals(contextStatus)) {
			opContext.ifPresent(ctx ->{ctx.setDomainSeqId(null);});
		}
		
		//4. Update the context -> status(for Alerts)(for only backend data events of backend initiated request)
		// 1. backend_created, backend_updated events and
		// 2. with empty or null channelSeqId's
		
		//Note: Not to consume the backend data events coming for CBX initiated requests
		if(isBackendDataEvent && StringUtils.isBlank(context.getChannelSeqId())) {
			LOGGER.info("Updating the context->status for alerts...");
			String payloadStatus = paymentsInstructionsES.getCbxUIStatus();
			
			boolean hucRejectFlag = Optional.ofNullable(paymentsInstructionsES.getHucRejectFlag())
					.map(rejFlg -> rejFlg.booleanValue()).orElse(false);
			
			//to convert as below:
			/**
			 * <a> 
			 * https://igtbworks.atlassian.net/wiki/spaces/IPSH/pages/1086228888/Workflow+and+Non-Workflow+Alerts+For+Payments#WorkflowandNon-WorkflowAlertsForPayments-TableMappingofStreamingEventsv/sAlertsEvent
			 * </a>
			 * 
			 * eg..
			 * backend_created -> INITIATED => backend_created_init
			 * backend_updated -> INITIATED => backend_created_init
			 */
			String mappedAlertsCtxStatus= PaymentIngestionConstant.BACKEND_CREATED.concat(
					getAlertsCtxStatusSuffix(isHUCReqEnum, payloadStatus, contextStatus, hucRejectFlag));
			LOGGER.info("The updated alerts context status for domainSeqId =>> {} is =>>> {}", context.getDomainSeqId(),
					mappedAlertsCtxStatus);
			opContext.map(ctx -> ctx.getEventType()).ifPresent(evtTp -> {
				evtTp.setStatus(mappedAlertsCtxStatus);
			});
		}
		LOGGER.debug("returning with {}", context);
	}


	/**
	 * 
	 * Gets the corresponding alerts on the basis of status of the icnoming request.
	 * 
	 * @param isHUCReqEnum is HUC request type
	 * @param payloadStatus payload status
	 * @param contextStatus context status
	 * @param hucRejectFlag huc reject flag
	 * @return {@link String} context status suffix for alerts (_init, _auth, _rej, _posted)
	 */
	private String getAlertsCtxStatusSuffix(boolean isHUCReqEnum, String payloadStatus, String contextStatus, boolean hucRejectFlag) {
		LOGGER.info("called with isHUCReqEnum=>{}, payloadStatus=>{}, contextStatus=>{}, hucRejectFlag=>{}", isHUCReqEnum, payloadStatus,
				contextStatus, hucRejectFlag);
		String _init = "_init";
		String _auth = "_auth";
		String _rej = "_rej";
		String _posted = "_posted";
		String backend_created = PaymentIngestionConstant.BACKEND_CREATED;
		String backend_updated = PaymentIngestionConstant.BACKEND_UPDATED;
		
		if(UIStatusEnum.INITIATED.uiStatusStr.equals(payloadStatus)) {
			return _init;
		}
		
		if(!isHUCReqEnum && backend_created.equals(contextStatus)) {
			if(UIStatusEnum.AUTHORIZED.uiStatusStr.equals(payloadStatus)) 
				return _auth;
			else if(UIStatusEnum.SENT_TO_BANK_PI.uiStatusStr.equals(payloadStatus)) 
				return _auth;
			else if(UIStatusEnum.SENT_TO_RECEIVER_PI.uiStatusStr.equals(payloadStatus)) 
				return _auth; 
			else if(UIStatusEnum.REJECTED.uiStatusStr.equals(payloadStatus))
				return _rej;
			else if(UIStatusEnum.POSTED.uiStatusStr.equals(payloadStatus)) 
				return _posted;
			else {
				LOGGER.error("The incoming payload status ==>>> [{}] request is not allowed for publishing alerts", 
						payloadStatus);
				return "";
			}
		}else if(isHUCReqEnum && backend_updated.equals(contextStatus)){
			if(payloadStatus != null && !hucRejectFlag) return _auth;
			else return _rej;
		}
		return "";
	}
	
	
	/**
	 * Fetch domainId for a customer from ES.
	 * 
	 * @param customerId customer id
	 * @param channelSeqId channel SeqId
	 * @param isBackendDataEvent flag to identify if the incoming req is backend data event or not
	 * @return {@link String} domain id 
	 */
	public String fetchDomainIdFromES(final String customerId, final String channelSeqId, final boolean isBackendDataEvent) {
		LOGGER.info("called with customerId => {}, channelSeqId => {} and isBackendDataEvent => {}", customerId, channelSeqId, isBackendDataEvent);
		String domainId = null;
		//if not backend data event meaning ack/nack/processed
		if(!isBackendDataEvent) {
			final Map<String, String> matchStrings = new HashMap<>();
			matchStrings.put("stateInfo.channelSeqId", channelSeqId);
			
			final String[] fetchStrings = {"stateInfo"};
			
			final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings, fetchStrings, 
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			
			final Optional<StateInfo> stateInfo = Optional.ofNullable(jestResult.getSourceAsObject(PaymentsInstructionsES.class))
					.map(PaymentsInstructionsES::getStateInfo);
			
			HashSet<String> domainNameSet = new HashSet<>();
			
			Consumer<Optional<UserInfo>> domainNames = userInfo -> domainNameSet.add(userInfo.map(UserInfo::getDomainName).orElse(null)); 
			domainNames.accept(stateInfo.map(StateInfo::getInitiated).map(Initiated::getUserInfo));
			domainNames.accept(stateInfo.map(StateInfo::getLastAction).map(LastAction::getUserInfo));
			domainNames.accept(stateInfo.map(StateInfo::getNextAction).map(NextAction::getUserInfo).map(userList -> userList.get(0)));

			if(domainNameSet.size() > 0) {
				Iterator<String> iter = domainNameSet.iterator();
				domainId = iter.next();
			}else {
				LOGGER.warn("No domain name found for the CBX request with channelSeqId => {}",channelSeqId);
			}
			
		// else if backend records(IPSH initiated requests), fetch using the customer id
		}else {
			final String searchBoolQuery = buildGetDomainByCustomerIdQuery(customerId);
			final JsonNode hitsNode = paymentIngestionQueryUtil.jestSearchESBuilder(searchBoolQuery, 
					paymentElasticProperties.getOrgIndex(), paymentElasticProperties.getDomainsType());
			JsonNode sourceNode = Optional.ofNullable(hitsNode).filter(hits -> hits.size() > 0).map(t -> t.get(0))
			.map(t -> t.get(PaymentIngestionConstant.SOURCE)).orElse(null);
			
			//TODO: Add iterators to ensure to iterate over all hitsnodes
			domainId = Optional.ofNullable(sourceNode).map(domainNode -> domainNode.get("name"))
					.map(JsonNode::asText).orElse(null);
			
		}
		LOGGER.info("returning with domainId =>>> {}", domainId);
		return domainId;
	}
	
	/**
	 * Builds the get domain by customer ids query.
	 *
	 * @param customerId the customer id
	 * @return the string query
	 */
	public static String buildGetDomainByCustomerIdQuery(final String customerId) {
		final String format = "{\"bool\":{\"should\":[{\"terms\":{\"domainEntities.id\":[\"%s\"]}}]}}";
		return String.format(format, customerId);
	}
}
