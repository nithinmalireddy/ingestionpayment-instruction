
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "docStatus",
    "domainSeqId",
    "channelSeqId",
    "type"
})
public class StateDocInfoES {

    @JsonProperty("docStatus")
    private String docStatus;
    
    @JsonProperty("domainSeqId")
    private String domainSeqId;
    
    @JsonProperty("channelSeqId")
    private String channelSeqId;
        
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("docStatus")
    public String getDocStatus() {
        return docStatus;
    }

    @JsonProperty("docStatus")
    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    @JsonProperty("domainSeqId")
    public String getDomainSeqId() {
        return domainSeqId;
    }

    @JsonProperty("domainSeqId")
    public void setDomainSeqId(String domainSeqId) {
        this.domainSeqId = domainSeqId;
    }
    
    @JsonProperty("channelSeqId")
    public String getChannelSeqId() {
		return channelSeqId;
	}
    
    @JsonProperty("channelSeqId")
	public void setChannelSeqId(String channelSeqId) {
		this.channelSeqId = channelSeqId;
	}

	@JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
