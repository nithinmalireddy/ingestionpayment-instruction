package com.igtb.ingestion.payment.instruction.util;

import static com.igtb.api.ingestion.commons.transhistory.TransactionHistoryUtil.getTransactionHistoryInfo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.api.ingestion.commons.transhistory.models.TransactionHistoryInfo;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.EventSource;
import com.igtb.ingestion.payment.instruction.models.EventType;

import lombok.AllArgsConstructor;

/**
 * The Class TransitionHistoryUtil.
 */
@Component
@AllArgsConstructor
public class TransitionHistoryUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TransitionHistoryUtil.class);

	/** The valid TH approval status. */
	static HashMap<String, Boolean> validTHApprovalStatus = new HashMap<>();

	/** The valid TH status. */
	static HashMap<String, Boolean> validTHStatus = new HashMap<>();

	/** The payment ingestion enrichment util. */
	private PaymentIngestionEnrichmentUtil enrichmentUtil;
	
	ObjectMapper objMapper=new ObjectMapper();

	private PaymentIngestionQueryUtil paymentIngestionQueryUtil;
	/**
	 * Handle transition history.
	 *
	 * @param context
	 *            the context
	 * @param requestMessageJson
	 *            the request message json
	 * @param userInfoJson
	 *            the user info json
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	/*
	 * after successful processing, pushing the enriched transactionhistory payload
	 * onto the elasticsearch
	 */
	public void handleTransitionHistory(final Context context, final JsonNode requestMessageJson,
			final JsonNode userInfoJson) throws IOException, MessageProcessingException, DLQException {
		LOGGER.info("Context received in handleTransitionHistory context:{},requestMessageJson:{}",context,requestMessageJson);
		if (filterTransactionHistoryEvents(context, requestMessageJson.get(PaymentIngestionConstant.PAYLOAD))) {
			//Skip BACKEND_REJECTED history for cancelled event
			if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_MONEY)) {
				JsonNode paymentInstActiveRecordData = paymentIngestionQueryUtil
						.fetchActiveRecord(context.getDomainSeqId());
				if(paymentInstActiveRecordData!=null) {
					if(paymentInstActiveRecordData.get(PaymentIngestionConstant.STATUS).asText().equalsIgnoreCase(PaymentIngestionConstant.CANCELLED)) {
						if(context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_REJECTED)) {
							return;	
						}
					}
				}
			}
			
			LOGGER.info("Enriching PI Transaction History information requestMessageJson:{}",requestMessageJson);
			final TransactionHistoryInfo transactionHistoryInfo = getTransInfo(requestMessageJson, userInfoJson,"PaymentInstruction");
			
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {

				if (context.getEventType().getStatus()!=null && context.getEventType().getStatus().toLowerCase()
						.equalsIgnoreCase(PaymentIngestionConstant.APPROVED)) {
					// in approved case domainseqid not present in context
					JsonNode paymentInstTransitionData = paymentIngestionQueryUtil
							.fetchExistingRecordFromES(context.getChannelSeqId());
					LOGGER.info("paymentInstDomainIdData ::::{}", paymentInstTransitionData);
//					****Get clearingSystemReference from active doc ut now we get from transition doc thats why comment this code
//					JsonNode paymentInstData = paymentIngestionQueryUtil.fetchActiveRecord(domainSeqId);
//					String domainSeqId = paymentInstData.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.DOMAIN_SEQ_ID).asText();
//					LOGGER.info("paymentInstData ::::{}", paymentInstData);
					transactionHistoryInfo
							.setChannelSeqId(paymentInstTransitionData.get(PaymentIngestionConstant.ORGN_GRP_INFO_STS_ID).get(PaymentIngestionConstant.CLER_SYS_REF).asText());
				} else {
					LOGGER.info("In If  context.getDomainSeqId()::::{}", context.getChannelSeqId());
					JsonNode paymentInstTransitionData = paymentIngestionQueryUtil
							.fetchExistingRecordFromES(context.getChannelSeqId());
					LOGGER.info("paymentInstDomainIdData ::::{}", paymentInstTransitionData);
//					****Get clearingSystemReference from active doc but now we get from transition doc thats why comment below code
//					JsonNode paymentInstData = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
//					LOGGER.info("paymentInstData ::::{}", paymentInstData);
					transactionHistoryInfo.setChannelSeqId(paymentInstTransitionData.get(PaymentIngestionConstant.ORGN_GRP_INFO_STS_ID).get(PaymentIngestionConstant.CLER_SYS_REF).asText());
				}

				if (context.getEventType().getStatus().equals("val_success")) {
					if (context.getEventType().getRequestType().equals("add")) {
						transactionHistoryInfo.setDesc("Accepted:by");
					}
				}
				
				if (context.getEventType().getStatus()!=null && context.getEventType().getStatus().toLowerCase()
						.equalsIgnoreCase(PaymentIngestionConstant.INITIATED)) {
					LOGGER.info("Inside INITIATED FRTP ::::{}", context);
					if (context.getEventType().getRequestType().equals("add")) {
						transactionHistoryInfo.setDesc("Accepted Pending Approval:from");
					} else if (context.getEventType().getRequestType().equals("delete")) {
						transactionHistoryInfo.setDesc("Declined Approval Pending:from");
					}
				}
			}
			
			LOGGER.info("After Enriching PI Transaction History information transactionHistoryInfo:{}",objMapper.writeValueAsString(transactionHistoryInfo));
			enrichmentUtil.pushTransitionHistoryToES(transactionHistoryInfo);
		}
	}

	/**
	 * Gets the trans info.
	 *
	 * @param requestJson
	 *            the request json
	 * @param userInfoJson
	 *            the user info json
	 * @param moduleName
	 *            the module name
	 * @return the trans info
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private TransactionHistoryInfo getTransInfo(final JsonNode requestJson, final JsonNode userInfoJson,
			final String moduleName) throws IOException {
		LOGGER.info("start getTransInfo():::::userInfoJson{}",userInfoJson);
		// rest call to the framework to be written
		final TransactionHistoryInfo transInfo = getTransactionHistoryInfo(requestJson, userInfoJson, moduleName);

		return transInfo;
	}

	/**
	 * Filter transaction history events.
	 *
	 * @param context
	 *            the context
	 * @param payload
	 *            the payload
	 * @return true, if successful
	 */
	private boolean filterTransactionHistoryEvents(final Context context, final JsonNode payload) {
		LOGGER.info("Start filterTransactionHistoryEvents() with context::::::::{}",context);
		boolean isTHIngestionAllowed = false;
		// transaction event matrix
		Optional<EventType> optionEvtTp = Optional.ofNullable(context.getEventType());
		Optional<EventSource> optionEvtSrc = Optional.ofNullable(context.getEventSource());
		String outcomeCategory = optionEvtTp.map(eventType -> eventType.getOutcomeCategory()).orElse(null);
		String status = optionEvtTp.map(eventType -> eventType.getStatus()).orElse(null);
		if (PaymentIngestionConstant.BACKEND_STATE_UPDATED.equalsIgnoreCase(outcomeCategory) || PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)) {
			isTHIngestionAllowed = isValidTHChannelStatus(status);
		} else if (PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equalsIgnoreCase(outcomeCategory)) {
			// check the sourceIdentity and apply filters on it
			isTHIngestionAllowed = filterTHWorkflowEvents(context, status, payload);
		}
		if (!isTHIngestionAllowed) {
			LOGGER.info(">>>>>>>>>>Transition History not allowed for events :: with outcomecategory -> [{}] with status -> [{}] with sourceIdentity -> [{}]",outcomeCategory, status, optionEvtSrc.map(evtSrc -> evtSrc.getSourceIdentity()));
		}
		return isTHIngestionAllowed;
	}

	/**
	 * Filter TH workflow events.
	 *
	 * @param context
	 *            the context
	 * @param status
	 *            the status
	 * @param payload
	 *            the payload
	 * @return the boolean
	 */
	private boolean filterTHWorkflowEvents(final Context context, final String status, final JsonNode payload) {
		LOGGER.info("Start filterTHWorkflowEvents() with status::::::::{}",status);
		if (isValidateSourceComponent(context, ".*approval.*")) {
			if ("initiated".equalsIgnoreCase(status)) {
				// check if nextAction is not equal to none
				if (!"none".equalsIgnoreCase(Optional.ofNullable(payload.get(PaymentIngestionConstant.NEXTACTION)).map(nextAction -> nextAction.textValue()).orElse(null))) {
					return true;
				}

			}
			// else part
			return isValidTHApprovalStatus(status);

		} else if (isValidateSourceComponent(context, ".*limit.*||.*entitlement.*")) {

			if ("rejected".equalsIgnoreCase(status)) {
				// check if currentAction=initiate
				if (!"initiate".equalsIgnoreCase(Optional.ofNullable(payload.get(PaymentIngestionConstant.CURRENTACTION)).map(nextAction -> nextAction.textValue()).orElse(null))) {
					return true;
				}
			}
		}
		return false;

	}

	/**
	 * Checks if is valid TH approval status.
	 *
	 * @param status
	 *            the status
	 * @return true, if is valid TH approval status
	 */
	private boolean isValidTHApprovalStatus(final String status) {
		LOGGER.info("Start isValidTHApprovalStatus() with status::::::::{}",status);
		validTHApprovalStatus.put("verified", true);
		validTHApprovalStatus.put("approved", true);
		validTHApprovalStatus.put("released", true);
		validTHApprovalStatus.put("rejected", true);

		return Optional.ofNullable(validTHApprovalStatus.get(status)).orElse(false);
	}

	/**
	 * Checks if is valid TH status.
	 *
	 * @param status
	 *            the status
	 * @return true, if is valid TH status
	 */
	private boolean isValidTHChannelStatus(final String status) {
		LOGGER.info("Start isValidTHChannelStatus() with status::::::::{}",status);
		validTHStatus.put("draft", true);
		validTHStatus.put("val_success", true);
		validTHStatus.put("val_timeout", true);
		validTHStatus.put("released", true);
		validTHStatus.put("release_rejected", true);
		validTHStatus.put("release_failure", true);
		validTHStatus.put("backend_rejected", true);
		validTHStatus.put("backend_processed", true);

		return Optional.ofNullable(validTHStatus.get(status)).orElse(false);
	}

	/**
	 * Checks if is validate source component.
	 *
	 * @param context
	 *            the context
	 * @param regexString
	 *            the regex string
	 * @return the boolean
	 */
	private Boolean isValidateSourceComponent(final Context context, final String regexString) {
		LOGGER.info("Start isValidateSourceComponent() with :::::::: context{}",context);
		String regexP = regexString;
		String lCaseSourceIdentity = Optional.ofNullable(context.getEventSource()).map(eventSource -> eventSource.getSourceIdentity()).map(sourceIdentity -> sourceIdentity.toLowerCase()).orElse(null);
		return lCaseSourceIdentity.matches(regexP);

	}
}
