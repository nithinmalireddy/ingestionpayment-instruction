package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Context.
 */
@Getter
@Setter
public class Context {
  
  /** The event type. */
  @JsonProperty("eventType")
  private EventType eventType;

  /** The channel seq id. */
  @JsonProperty("channelSeqId")
  private String channelSeqId;

  /** The domain seq id. */
  @JsonProperty("domainSeqId")
  private String domainSeqId;

  /** The event source. */
  @JsonProperty("eventSource")
  private EventSource eventSource;

  /** The event time. */
  @JsonProperty("eventTime")
  private String eventTime;

  /** The requester. */
  @JsonProperty("requester")
  private Requester requester;
  

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final Context context = (Context) obj;
    return Objects.equals(this.eventType, context.eventType) &&
        Objects.equals(this.channelSeqId, context.channelSeqId) &&
        Objects.equals(this.domainSeqId, context.domainSeqId) &&
        Objects.equals(this.eventSource, context.eventSource) &&
        Objects.equals(this.eventTime, context.eventTime) &&
        Objects.equals(this.requester, context.requester);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(eventType, channelSeqId, domainSeqId, eventSource, eventTime, requester);
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(120);
    sb.append("class Context { eventType: ").append(toIndentedString(eventType))
      .append("    channelSeqId: ").append(toIndentedString(channelSeqId))
      .append("    domainSeqId: ").append(toIndentedString(domainSeqId))
      .append("    eventSource: ").append(toIndentedString(eventSource))
      .append("    eventTime: ").append(toIndentedString(eventTime))
      .append("    requester: ").append(toIndentedString(requester))
      .append('}');
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param obj the obj
   * @return the string
   */
  private String toIndentedString(final Object obj) {
    if (obj == null) {
      return "null";
    }
    return obj.toString().replace("\n", "\n    ");
  }

}

