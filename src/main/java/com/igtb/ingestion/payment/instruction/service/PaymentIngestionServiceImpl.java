/**
 * Version no		Author						Comments
 * 1.0				K1 base version				K1 base version
 * 1.1				Anil Ravva					Changes related to Remove the status field PaymentCenterStatus 
 */

package com.igtb.ingestion.payment.instruction.service; //NOPMD excessiveImports

import static com.igtb.ingestion.payment.instruction.commons.decision.cibc.StateTransitionDecisionModule.getActionForTransitionCIBC;

import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.util.Strings;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Maps;
//import com.igtb.api.ingestion.commons.decision.FromStates;
//import com.igtb.api.ingestion.commons.decision.StateTransitionData;
//import com.igtb.api.ingestion.commons.decision.TransitionAction;
import com.igtb.api.ingestion.commons.stateinfo.models.RejectionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.FromStates;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.StateTransitionData;
import com.igtb.ingestion.payment.instruction.commons.decision.cibc.TransitionAction;
import com.igtb.ingestion.payment.instruction.config.PshcReferenceDataInquiryProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentIngestionProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.AmountES;
import com.igtb.ingestion.payment.instruction.es.models.FAEBackendEvent;
import com.igtb.ingestion.payment.instruction.es.models.IncomingRTPES;
import com.igtb.ingestion.payment.instruction.es.models.InvoiceDetailsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentRailES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentReasonES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentTypeES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsDlqNotificationES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsNotificationES;
import com.igtb.ingestion.payment.instruction.es.models.SupplementaryDataES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.EventSource;
import com.igtb.ingestion.payment.instruction.models.EventType;
import com.igtb.ingestion.payment.instruction.models.PaymentInstruction;
import com.igtb.ingestion.payment.instruction.models.Requester;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;
import com.igtb.ingestion.payment.instruction.util.CommonServiceUitility;
import com.igtb.ingestion.payment.instruction.util.JoseClientUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionBackendUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionChannelUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionEnrichmentUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionHelper;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionQueryUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionRedisUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentInstructionBuilder;
import com.igtb.ingestion.payment.instruction.util.UIStatusUtil.UIStatusEnum;
import com.jayway.jsonpath.JsonPath;

import io.searchbox.client.JestResult;
import io.searchbox.strings.StringUtils;
import lombok.AllArgsConstructor;

/**
 * Event level dispatching takes place in this class
 * PaymentIngestionServiceImpl.
 * 
 */
@Component
@AllArgsConstructor
public class PaymentIngestionServiceImpl {

//	@Autowired
//	private RestTemplate restTemplate;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentIngestionServiceImpl.class);

	/** The payment ingestion backend util. */
	private PaymentIngestionBackendUtil backendUtil;

	/** The payment ingestion enrichment util. */
	private PaymentIngestionEnrichmentUtil enrichmentUtil;

	/** The payment ingestion channel util. */
	private PaymentIngestionChannelUtil ingestionChannelUtil;

	/** The payment ingestion redis util. */
	private PaymentIngestionRedisUtil paymentIngestionRedisUtil;
	
	/** The Payment elastic properties. */
	private PaymentElasticProperties paymentElasticProperties;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	/** The payment ingestion helper. */
	private PaymentIngestionHelper paymentIngestionHelper;

	/** The payment ingestion jest util. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The workflow accepted status. */
	private static Map<String, Boolean> workflowAcceptedStatus = new HashMap<String, Boolean>();
	
	/** The users kafka properties. */
	@Autowired
	private PaymentKafkaProperties paymentKafkaProperties;
	
	/** The payment ingestion properties. */
	private PaymentIngestionProperties paymentIngestionProperties;
	
	@Autowired
	private PshcReferenceDataInquiryProperties pshcReferenceDataInquiryProperties;
	
	@Autowired
	private PaymentKafkaDLQProducer kafkaProducer;
	
	private CommonServiceUitility commonServiceUtility;
	
	/** The jose client util. */
	private JoseClientUtil joseClientUtil;

	/** The payment instruction builder. */
	@Autowired
	private PaymentInstructionBuilder paymentInstructionBuilder;
	/**
	 * Process kafka message.
	 *
	 * @param requestMessageJson
	 *            the request message json
	 * @param eventType
	 *            the event type
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public void processKafkaMessage(final JsonNode requestMessageJson, final String eventType)throws IOException, InterruptedException, RequeueException, DLQException, MessageProcessingException {
		
		LOGGER.info("Backend data event handling start inside processKafkaMessage() at TIME::::::: {}",new Date());
		LOGGER.info("Backend data event handling for the event id: {}",requestMessageJson.get(PaymentIngestionConstant.ID));
		Context context =null;
		boolean isAckProcessed = true;
		boolean isFulFillDuplicateTxn = false;
		final JsonNode contextJson = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		if(null!= contextJson){
			context = objectMapper.readValue(contextJson.toString(), Context.class);
		}
		else {
			throw new NullPointerException(String.valueOf(context));
		}
		if ((context.getEventType().getStatus()).equalsIgnoreCase(PaymentIngestionConstant.BACKEND_CREATED) 
				&& context.getEventType().getRequestType().equalsIgnoreCase(PaymentIngestionConstant.ADD) 
				&& context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_RTP)) {
			Thread.sleep(15000);
		}
		// Extracting Payload Information from Request Message
		ObjectNode payloadJson = (ObjectNode) requestMessageJson.get(PaymentIngestionConstant.PAYLOAD);
		LOGGER.info("payloadJson from kafka with requestType:::::::{} and status:::::::::{} and domainSeqID::::::::{}", payloadJson,context.getEventType().getStatus(),context.getDomainSeqId() );
		LOGGER.info("Message Received on Kafka with domainSeqID: {} and payload : {}",context.getDomainSeqId(),requestMessageJson);
		LOGGER.info("setting transactionId :::::: {}", context.getDomainSeqId());
		payloadJson.put(PaymentIngestionConstant.TRANSACTION_ID, context.getDomainSeqId());
		if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
			//Enriching payload for FullfilRTP
			if (context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_CREATED)) {
				final JsonNode existingActiveDoc = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
				if(existingActiveDoc!=null) {
					isFulFillDuplicateTxn = true;
					LOGGER.error("Duplicate Event Type:"+context.getEventType().getStatus()+ " received for ID:"+context.getDomainSeqId());
				}
				//This change is made after discussed with BA for incomingRTP have two reference number this can be change later on when we know more
			}else if(context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_UPDATED)) {
				if(payloadJson.has(PaymentIngestionConstant.CLER_SYS_REF)) {
					payloadJson.put(PaymentIngestionConstant.CLER_SYS_REF_UMM, payloadJson.get(PaymentIngestionConstant.CLER_SYS_REF).asText());
					payloadJson.remove(PaymentIngestionConstant.CLER_SYS_REF);
				}
			}
			// Skip if modify notification received from INTERAC and transaction is at Checker (7393 and 7399)
			if( !(PaymentIngestionConstant.channel_state_updated_incomingrtpstatus.equals(context.getEventSource().getSourceIdentity())) ) {
				if(PaymentIngestionConstant.BACKEND_UPDATED.equals(context.getEventType().getStatus())
						&& PaymentIngestionConstant.BACKEND_STATE_UPDATED.equals(context.getEventType().getOutcomeCategory())
						&& (context.getChannelSeqId() == null || "".equals(context.getChannelSeqId())) ) {
					//check transition doc if created is at Checker level
					final JsonNode existingTransitionDoc = paymentIngestionQueryUtil.fetchTransitionRecord(context.getDomainSeqId());
					LOGGER.info("[fetchTransitionRecordFromDomain] existingTransitionDoc::{}",existingTransitionDoc);
					if(existingTransitionDoc != null) {
						PaymentsInstructionsES paymentsInstructionsESRec = objectMapper.readValue(existingTransitionDoc.toString(), PaymentsInstructionsES.class);
						//Check if status is ACCEPTED PENDING APPROVAL or DECLINED PENDING APPROVAL
						if( paymentsInstructionsESRec.getCbxUIStatus() != null 
								&& paymentsInstructionsESRec.getCbxUIStatus().contains(PaymentIngestionConstant.PENDING_APPROVAL) ) {
							
							LOGGER.error("Skippping event for Notification with id: {} on domainSeqId: {}"
							,requestMessageJson.get(PaymentIngestionConstant.ID),context.getDomainSeqId());
							return;
						}
					}
				}
			}
			payloadJson = enrichPaymentInstrESForFulfillRTP(payloadJson,context);
			LOGGER.info("isFulFillDuplicateTxn:::"+isFulFillDuplicateTxn);
			LOGGER.info("PayloadJson after enrichPaymentInstrESForFulfillRTP() ::::::: {}", payloadJson);
		 }
		 else {
			LOGGER.info("setting transactionId :::::: {}::::::Channel Seq id::::{}", context.getDomainSeqId(),context.getChannelSeqId());
			String lData = paymentIngestionRedisUtil.pullDataToRedis(context.getChannelSeqId()+PaymentIngestionConstant.BACKEND_ACK_SUCC);
			String lNotificaitonStatus = (payloadJson.has(PaymentIngestionConstant.STATUS)?payloadJson.get(PaymentIngestionConstant.STATUS).asText():null);
			LOGGER.info("lNotificaitonStatus:::"+lNotificaitonStatus);
			ObjectNode existingESPayloadForChannelSeqId =(ObjectNode)paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
			LOGGER.info("lData:"+lData);
			if(lData == null || !lData.equals("Y")) {
				holdCurrentMessageProcessing(requestMessageJson,context);
				isAckProcessed = false;
			}else {
				PaymentsInstructionsES paymentsInstructionsESRec = objectMapper.readValue(existingESPayloadForChannelSeqId.toString(), PaymentsInstructionsES.class);
				String lNewDomainId = paymentsInstructionsESRec.getStateInfo().getDomainSeqId();
				LOGGER.info("lNewDomainId:::"+lNewDomainId);
				//1.1
				Map<String, String> paramMap = new HashMap<String, String>();
				paramMap.put(PaymentIngestionConstant.DOC_STATUS, (lNewDomainId == null || lNewDomainId.equals("null") || lNewDomainId.equals(""))?PaymentIngestionConstant.DOC_STATUS_TRANSITION :PaymentIngestionConstant.DOC_STATUS_ACTIVE);
				paramMap.put(PaymentIngestionConstant.DESC, lNotificaitonStatus);
				String fetchQryString = paymentIngestionQueryUtil.getFetchQryStatus(paramMap);
				if(!fetchQryString.equalsIgnoreCase(PaymentIngestionConstant.DEFAULT_STR)) {
					payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, fetchQryString);
				}
				// To be Commented starts
				if(lNewDomainId == null || lNewDomainId.equals("null") || lNewDomainId.equals("")) {
					payloadJson.put("paymentCenterStatus", lNotificaitonStatus+"-NULL");//
				}else {				
					payloadJson.put("paymentCenterStatus", lNotificaitonStatus+"-NOTNULL");
				}
				// To be Commented ends 
				//1.1 ends
				
			    payloadJson = enrichPayloadForBackendEvents(payloadJson,context,existingESPayloadForChannelSeqId);
			}
			LOGGER.info("PayloadJson after enrichPayloadForBackendEvents ::::::: {}", payloadJson);
		}
		if(!payloadJson.has(PaymentIngestionConstant.NOTIFICATION_LANGUAGE)) {
			payloadJson.put(PaymentIngestionConstant.NOTIFICATION_LANGUAGE, "en");
			LOGGER.info("##Enriched Notification Language for channelSeqId::{} or domainSeqId::{}",context.getChannelSeqId(),context.getDomainSeqId());
		}
		
		if(isAckProcessed && !isFulFillDuplicateTxn) {
		switch (eventType) {
			case PaymentIngestionConstant.FILE:
				dispatchKafkaMessageFileBackendCreated(context, payloadJson);
				break;
			case PaymentIngestionConstant.BULK:
				dispatchKafkaMessageFileBackendCreated(context, payloadJson);
				break;
			case PaymentIngestionConstant.SINGLE:
				dispatchKafkaMessageBasedOnRequestTypes(context, payloadJson);
				break;
			default:
				break;
		}
		/*
		 * After successful processing, sending all backend state events to rabbitmq for
		 * other apis to consume.
		 * not use in cibc
		 */
		//paymentIngestionHelper.publishBackendEventsToRabbit(requestMessageJson, context);
		}
	}
	//Enrich Backend_created & Backend_updated Payload from exsiting ChannelSeqID
	public ObjectNode enrichPayloadForBackendEvents(final ObjectNode payloadJson, final Context context, ObjectNode existingESPayloadForChannelSeqId)
	{
		
		LOGGER.info("start enrichPayloadForBackendEvents() for ServiceKey :::::::{} and payload:::::::{} ",context.getEventType().getServiceKey(),payloadJson);
		//ObjectNode existingESPayloadForChannelSeqId = null;
		// Extracting payload Information from existing
		try {
		//existingESPayloadForChannelSeqId =(ObjectNode)paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
			payloadJson.set(PaymentIngestionConstant.END_TO_END_IDNTFCN, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.END_TO_END_IDNTFCN));
			payloadJson.set(PaymentIngestionConstant.INSTRCTED_AMNT, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.INSTRCTED_AMNT));
			payloadJson.set(PaymentIngestionConstant.EQUIVLNT_AMNT, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.EQUIVLNT_AMNT));
			payloadJson.set(PaymentIngestionConstant.CDTR, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.CDTR));
			payloadJson.set(PaymentIngestionConstant.CREDITOR_ACCOUNT, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.CREDITOR_ACCOUNT));
			payloadJson.set(PaymentIngestionConstant.DEBTOR_ACCOUNT, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.DEBTOR_ACCOUNT));
			payloadJson.set(PaymentIngestionConstant.DBTR_DETAILS, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.DBTR_DETAILS));
			payloadJson.set(PaymentIngestionConstant.SUPPLEMENTRY_DATA, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.SUPPLEMENTRY_DATA));
			payloadJson.set(PaymentIngestionConstant.INSTRUCTION_ID, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.INSTRUCTION_ID));
			payloadJson.set(PaymentIngestionConstant.PYMNT_RAIL_OBJ, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.PYMNT_RAIL_OBJ));
			payloadJson.set(PaymentIngestionConstant.PYMNT_REASON, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.PYMNT_REASON));
			payloadJson.set(PaymentIngestionConstant.PYMNT_TYP_OBJ, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.PYMNT_TYP_OBJ));
			payloadJson.set(PaymentIngestionConstant.EXPIRY_DATE, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.EXPIRY_DATE));
			payloadJson.set(PaymentIngestionConstant.REQUESTED_EXECUTION_DATE, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.REQUESTED_EXECUTION_DATE));
			payloadJson.set(PaymentIngestionConstant.OPTION, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.OPTION));
			payloadJson.set(PaymentIngestionConstant.INITIATING_PRTY, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.INITIATING_PRTY));
			payloadJson.set(PaymentIngestionConstant.PYMNT_METHOD, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.PYMNT_METHOD));
			payloadJson.put(PaymentIngestionConstant.PARENT_CREATE_REQ_CHANNEL_SEQID, context.getChannelSeqId());
			payloadJson.set(PaymentIngestionConstant.OTHR_REMNG_INFO, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.OTHR_REMNG_INFO));
			payloadJson.set(PaymentIngestionConstant.CATEGORY_PURPOSE, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.CATEGORY_PURPOSE));
			payloadJson.put(PaymentIngestionConstant.CUST_ID, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.CUST_ID).asText());
			payloadJson.put(PaymentIngestionConstant.CUST_NAME, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.CUST_NAME).asText());
			payloadJson.put(PaymentIngestionConstant.PU_ID, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.PU_ID).asText());
			payloadJson.put(PaymentIngestionConstant.REMITTANCEINFORMATION_1, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.REMITTANCEINFORMATION_1).asText());
			payloadJson.put(PaymentIngestionConstant.CREATION_DATE_TIME, context.getEventTime());
			payloadJson.put(PaymentIngestionConstant.FAE_INDICATOR, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.FAE_INDICATOR).asText());
			payloadJson.put(PaymentIngestionConstant.BCC_ACTION, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.BCC_ACTION).asText());
			payloadJson.set(PaymentIngestionConstant.RELEASE_DATE, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.RELEASE_DATE));
			payloadJson.putObject(PaymentIngestionConstant.STATE_INFO).set(PaymentIngestionConstant.LAST_ACTION, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.LAST_ACTION));
			Date currentDate = new Date();
	        DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT2);
	        payloadJson.put(PaymentIngestionConstant.USER_DATE_TIME, df.format(currentDate));
//			if(payloadJson.has(PaymentIngestionConstant.STATUS) && payloadJson.get(PaymentIngestionConstant.STATUS).asText().equalsIgnoreCase(PaymentIngestionConstant.CREJECTED))
//			{
//				payloadJson.put(PaymentIngestionConstant.STATUS, PaymentIngestionConstant.BACKEND_CREJECTED);
//			}
			payloadJson.put(PaymentIngestionConstant.NOTIFICATION_LANGUAGE, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.NOTIFICATION_LANGUAGE).asText());
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_065 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_065_Desc);
			LOGGER.error("Exception in enrichPayloadForBackendEvents() :::::{}",e);
		} 
		return payloadJson;
	}
	//Enriching the payload for FullfilRTP
    public ObjectNode enrichPaymentInstrESForFulfillRTP(final ObjectNode payloadJson, final Context context){
    	LOGGER.info("start enrichPaymentInstrESForFulfillRTP() for ServiceKey :::::::{} and payload:::::::{} ",context.getEventType().getServiceKey(),payloadJson);
    	ObjectNode existingESPayloadForChannelSeqId = null;
    	try {
    		//CIBCCBX-5773 modification notification comes, before Acceptance of fulfillRTP
    		if (context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_CREATED) 
    				|| (context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_UPDATED) && null == context.getChannelSeqId()) ) {
    			IncomingRTPES incomingRTPES = new IncomingRTPES();
    			incomingRTPES.setRtPFulfillmentIndicator(PaymentIngestionConstant.YES);
    			incomingRTPES.setOriginatorFIRTPRefNum(context.getDomainSeqId());
			/*final JsonNode existingActiveDoc = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
			if (existingActiveDoc != null) {
				payloadJson.put("isduplicateTxn", PaymentIngestionConstant.Y);
			}*/
    			payloadJson.put(PaymentIngestionConstant.FILE_AUTH_PRTY, PaymentIngestionConstant.Y);
    			//mapping incomingRTP tag
    			payloadJson.set(PaymentIngestionConstant.INCMNG_RTP, objectMapper.convertValue(incomingRTPES, JsonNode.class));
    			AmountES amountEs=new AmountES(); 
    			if(payloadJson.has(PaymentIngestionConstant.PYMNT_METHOD)) {
    				if(payloadJson.get(PaymentIngestionConstant.PYMNT_METHOD).asText().equals(PaymentIngestionConstant.TRF)) {
    					amountEs.setCurrencyCode(payloadJson.get(PaymentIngestionConstant.INSTRCTED_AMNT).get(PaymentIngestionConstant.CURR_CODE).asText());
    					amountEs.setValue(payloadJson.get(PaymentIngestionConstant.INSTRCTED_AMNT).get(PaymentIngestionConstant.VALUE).asDouble());
    					payloadJson.set(PaymentIngestionConstant.INSTR_AMNT_BASE_CCY, objectMapper.convertValue(amountEs, JsonNode.class));
    				}
    			}
    			SupplementaryDataES supplementaryDataES = new SupplementaryDataES();
    			InvoiceDetailsES invoiceDetailsES = new InvoiceDetailsES();
    			invoiceDetailsES.setInvoiceNo(payloadJson.get(PaymentIngestionConstant.INVOICE_NO).asText());
    			invoiceDetailsES.setInvoiceDate(payloadJson.get(PaymentIngestionConstant.INVOICE_DATE).asText());
    			supplementaryDataES.setInvoiceDetails(invoiceDetailsES);
    			//Mapping supplementryData Tag
    			payloadJson.set(PaymentIngestionConstant.SUPPLEMENTRY_DATA, objectMapper.convertValue(supplementaryDataES, JsonNode.class));
    			//enriching paymentRailObj
    			PaymentRailES paymentRailES = new PaymentRailES();
    			paymentRailES.setCode(PaymentIngestionConstant.PAYMNT);
    			paymentRailES.setDescription(PaymentIngestionConstant.FULFILL_RTP);
    			payloadJson.set(PaymentIngestionConstant.PYMNT_RAIL_OBJ, objectMapper.convertValue(paymentRailES, JsonNode.class));
//    			//enriching paymentTypesObj
    			PaymentTypeES paymentTypeES = new PaymentTypeES();
    			paymentTypeES.setCode(PaymentIngestionConstant.FULFILL_RTP);
    			paymentTypeES.setDescription(PaymentIngestionConstant.FULFILL_RTP);
    			payloadJson.set(PaymentIngestionConstant.PYMNT_TYP_OBJ, objectMapper.convertValue(paymentTypeES, JsonNode.class));
//    			//enriching paymentReason
    			PaymentReasonES paymentReasonES = new PaymentReasonES();
    			paymentReasonES.setCode(PaymentIngestionConstant.FULFILL_RTP);
    			if(payloadJson.has(PaymentIngestionConstant.PYMNT_REASON)) {
    				if(payloadJson.get(PaymentIngestionConstant.PYMNT_REASON).has(PaymentIngestionConstant.DESCRIPTION)) {
    					paymentReasonES.setDescription(payloadJson.get(PaymentIngestionConstant.PYMNT_REASON).get(PaymentIngestionConstant.CODE).asText()); // JiraID:5737			    					
    				}
    			}else {
    				paymentReasonES.setDescription(PaymentIngestionConstant.DIRECT_CREDIT);
    			}
    			payloadJson.set(PaymentIngestionConstant.PYMNT_REASON, objectMapper.convertValue(paymentReasonES, JsonNode.class));
    			payloadJson.put(PaymentIngestionConstant.CATEGORY_PURPOSE, PaymentIngestionConstant.FULFILL_RTP);
    			payloadJson.put(PaymentIngestionConstant.CUST_ID, payloadJson.get(PaymentIngestionConstant.CUST_ID).asText());
    			payloadJson.put(PaymentIngestionConstant.CUST_NAME, payloadJson.get(PaymentIngestionConstant.CUST_NAME).asText());
    			payloadJson.put(PaymentIngestionConstant.PU_ID, payloadJson.get(PaymentIngestionConstant.PU_ID).asText());
    			payloadJson.put(PaymentIngestionConstant.LOCAL_INST_CODE, PaymentIngestionConstant.BTR);
    			payloadJson.set(PaymentIngestionConstant.EQUIVLNT_AMNT, payloadJson.get(PaymentIngestionConstant.INSTRCTED_AMNT));
    			Date currentDate = new Date();
    	        DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.TIMESTAMP_FORMAT2);
    	        payloadJson.put(PaymentIngestionConstant.USER_DATE_TIME, df.format(currentDate));
    			LOGGER.info("User Date Time::::{}",payloadJson.get(PaymentIngestionConstant.USER_DATE_TIME));
    		}
			if((null != context.getChannelSeqId() && !("null".equalsIgnoreCase(context.getChannelSeqId()))) && (context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_CREATED)||context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_UPDATED)))
			{
				existingESPayloadForChannelSeqId =(ObjectNode)paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
				payloadJson.put(PaymentIngestionConstant.FAE_INDICATOR, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.FAE_INDICATOR).asText());
				payloadJson.put(PaymentIngestionConstant.BCC_ACTION, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.BCC_ACTION).asText());
				payloadJson.set(PaymentIngestionConstant.DEBTOR_ACCOUNT, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.DEBTOR_ACCOUNT));
				payloadJson.set(PaymentIngestionConstant.DBTR_DETAILS, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.DBTR_DETAILS));
				payloadJson.set(PaymentIngestionConstant.FILE_CTRL_SUM, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.FILE_CTRL_SUM));
				payloadJson.set(PaymentIngestionConstant.BATCH_CTRL_SUM, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.BATCH_CTRL_SUM));
				payloadJson.put(PaymentIngestionConstant.CREATION_DATE_TIME, context.getEventTime());
				payloadJson.put(PaymentIngestionConstant.FAE_INDICATOR, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.FAE_INDICATOR).asText());
				payloadJson.put(PaymentIngestionConstant.BCC_ACTION, existingESPayloadForChannelSeqId.get(PaymentIngestionConstant.BCC_ACTION).asText());
    		}
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_066 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_066_Desc);
			LOGGER.error("Exception in enrichPaymentInstrESForFulfillRTP() :::::{}",e);
		}
    	
		LOGGER.info("end enrichPaymentInstrESForFulfillRTP() for ServiceKey :::::::{} and payload:::::::{} ",context.getEventType().getServiceKey(),payloadJson);
		return payloadJson;
	}
	
	

	public void processKafkaMessageFAE(final JsonNode requestMessageJson) throws IOException,InterruptedException, RequeueException, DLQException, MessageProcessingException {
		LOGGER.info("Backend data event handling for the FAE event id:::: {}",requestMessageJson.get(PaymentIngestionConstant.ID));
		// Extracting Context Information from Request Message
		final JsonNode contextJson = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		// Extracting Payload Information from Request Message
		ObjectNode payloadJson = (ObjectNode) requestMessageJson.get(PaymentIngestionConstant.PAYLOAD);
		LOGGER.info("PAYLOADJSON FROM KAFKA-FAE:::::::{}", payloadJson);
		// Converting JsonNode to Context model
		Context context = objectMapper.readValue(contextJson.toString(), Context.class);
		JsonNode existingRecordJson = paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
		LOGGER.info("Existing Record from ES in AEHold Case:::::::{}", existingRecordJson);
		PaymentsInstructionsES existingRecord = objectMapper.readValue(existingRecordJson.toString(), PaymentsInstructionsES.class);
		String currentDocStatus = existingRecord.getCbxUIStatus();
		String faeIndicator = existingRecord.getFaeIndicator();
		String cumulativeStatus = existingRecord.getCumulativeStatusES();
		String currentDocState = existingRecord.getStateInfo().getDocStatus();
		String domainSeqId = existingRecord.getStateInfo().getDomainSeqId();
		LOGGER.info("cumulativeStatus FROM ES at present:::::::{}", cumulativeStatus);
		if(payloadJson.has(PaymentIngestionConstant.BCC_ACTION)) {
			String bccAction = payloadJson.get(PaymentIngestionConstant.BCC_ACTION).asText();
			LOGGER.info("bccAction FROM FAE Admin:::::::{}", bccAction);
			// Business Exceptions as per Exception-Catalogue v7.9.xlsx
			if(PaymentIngestionConstant.AE_HOLD.equals(bccAction)) {
				LOGGER.error("Response AEHold received from FAE Admin {} {} for {}"
						,PaymentIngestionErrorCodeConstants.ERR_FAE_BCC_HOLD,PaymentIngestionErrorCodeConstants.ERR_FAE_BCC_HOLD_DESC,context.getChannelSeqId());
			}
			cumulativeStatus = currentDocState+currentDocStatus+faeIndicator+bccAction;
			LOGGER.info("cumulativeStatus after update:::::::{}", cumulativeStatus);
			payloadJson.put("cumulativeStatus", cumulativeStatus);
			payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, currentDocState+currentDocStatus+faeIndicator+bccAction);//1.1
		}
		LOGGER.info("payloadJson after update:::::::{}", payloadJson);
		LOGGER.info("Existing Record status ES in AEHold Case:::::::{}", currentDocStatus);
		JestResult result = backendUtil.updatePaymentInstructionChannelSeqId(payloadJson, context);
		// FAE CALL AFTER CHECKER ACTION
		LOGGER.info("FAE UPDATED :::::::{}", result.isSucceeded());
		if (result.isSucceeded()) {
			if (payloadJson.get(PaymentIngestionConstant.BCC_ACTION) != null && (payloadJson.get(PaymentIngestionConstant.BCC_ACTION).asText().equalsIgnoreCase(PaymentIngestionConstant.AE_RELEASE))) {
				LOGGER.info("<<<<< Inside AERelease Case >>>>>");
				if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.SEND_WIRE) && !checkWireCutoffTime())
				{
					processKafkaMessageForAEHold(context);
					LOGGER.error("processed AE-HOLD after wire cuttoff time fail");
				}
				else {
					LOGGER.info("calling FAE workflow for Release case");
					processApiCallForAERelease(context, domainSeqId);
				}
			} else if (payloadJson.get(PaymentIngestionConstant.BCC_ACTION) != null && (String.valueOf(payloadJson.get(PaymentIngestionConstant.BCC_ACTION).asText()).equalsIgnoreCase(PaymentIngestionConstant.AE_HOLD))) {
				LOGGER.info("<<<<< Inside AEHold Case >>>>>");
				if(currentDocStatus.equalsIgnoreCase(PaymentIngestionConstant.SENT_TO_BANK)) {
					LOGGER.info("calling FAE workflow for Hold case");
					if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
						LOGGER.info("calling Decline end point for FRTP");
						processDeclinerequestForIRTP(requestMessageJson, domainSeqId);
						sendRequestToLogicalEndAState(context.getEventType().getServiceKey(), context.getChannelSeqId(), "trash", "Trashed transition doc upon recieving AEHold from BCC Checker");
					}else {
						processApiCallForAERelease(context, domainSeqId);
					}
				}
				else if(currentDocStatus.equalsIgnoreCase(PaymentIngestionConstant.PENDING_APPROVAL)) {
					LOGGER.error("Reject action received from BCC User before Checker action {} {} for {}"
							,PaymentIngestionErrorCodeConstants.ERR_PSHC_BCC_REJ1,PaymentIngestionErrorCodeConstants.ERR_PSHC_BCC_REJ1_DESC,context.getChannelSeqId());
					LOGGER.info("Calling Action API end point for rejection in case of:::::::{}", payloadJson.get(PaymentIngestionConstant.BCC_ACTION).asText());
					sendRequestToLogicalEndAState(context.getEventType().getServiceKey(), context.getChannelSeqId(), "reject", "Rejected transaction upon recieving AEHold from BCC Checker");	
				}
				else if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)) {
					processDeclinerequestForIRTP(requestMessageJson, domainSeqId);
					sendRequestToLogicalEndAState(context.getEventType().getServiceKey(), context.getChannelSeqId(), "trash", "Trashed transition doc upon recieving AEHold from BCC Checker");
				}
				else {
					LOGGER.error("Reject action received from BCC User after Checker action {} {} for {}"
							,PaymentIngestionErrorCodeConstants.ERR_PSHC_BCC_REJ2,PaymentIngestionErrorCodeConstants.ERR_PSHC_BCC_REJ2_DESC,context.getChannelSeqId());
					processKafkaMessageForAEHold(context);
				}
				LOGGER.error("processed AE-HOLD after BCC-ACTION for {}",context.getChannelSeqId());
			}
		}
		else
		{
			LOGGER.error("FAE UPDATED :::::::{}", result.isSucceeded());
		}
	}
	

	public void processDeclinerequestForIRTP(JsonNode requestMessageJson, String domainSeqId) throws JsonParseException, JsonMappingException, IOException, InterruptedException, RequeueException, DLQException, MessageProcessingException {
		LOGGER.info("Inside processDeclinerequestForIRTP method");
		LOGGER.info("Generate Reject Payload for DeclineIRTP");
		final JsonNode contextJson = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		ObjectNode payloadJson = (ObjectNode) requestMessageJson.get(PaymentIngestionConstant.PAYLOAD);
		Context context = objectMapper.readValue(contextJson.toString(), Context.class);
		
		final String declineUrl = paymentIngestionProperties.getDeclineRtpUrl();
		LOGGER.info("declineUrl:::",declineUrl);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		Map<String, String> params = new HashMap<>();
		headers.set(PaymentIngestionConstant.AUTHORIZATION, paymentIngestionProperties.getUnwindCallAuthToken());
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		headers.set(PaymentIngestionConstant.CHANNEL_SQ_ID,context.getChannelSeqId());
		headers.set(PaymentIngestionConstant.DOMAIN_SEQ_ID,domainSeqId);
		params.put("reasonCode", "a5997117c4eb");
		//params.put("reasonDesc", "a5997117c4eb");
		/** Build the request*/
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
		/**use postForEntity method for HTTP call */
		((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setConnectTimeout(paymentIngestionProperties.getRestCallTimeout());
		LOGGER.info("entity is::::::{}", entity.toString());
		ResponseEntity<String> response = restTemplate.postForEntity(declineUrl, entity, String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			String fileStatus = JsonPath.read(response.getBody(),
					"fi_to_fi_payment_status_report.transaction_information_and_status[0].transaction_status");

			LOGGER.info("[invokeIILDeclineRTPInterface] Response from IIL: {} and fileStatus: {}", response.getBody(), fileStatus);
			if (Strings.isNotBlank(fileStatus) && !fileStatus.equalsIgnoreCase("ACTC")) {
				processRejectMessageForDecline(context);
			}
			LOGGER.info("FAE OK STATUS ::::: {}", response.getStatusCode());
			sendRequestToLogicalEndAState(context.getEventType().getServiceKey(),context.getChannelSeqId(), "trash","Trashed Transition record based on receiving notification");
		} else {
			LOGGER.info("FAE ERROR STATUS :::::: {}", response.getBody());
			processRejectMessageForDecline(context);
		}
		
		LOGGER.info("response is::::::{}", response.toString());
	}
	
	public void processRejectMessageForDecline(Context context) {
		try {	
			Context context_Reject = new Context();
			context_Reject.setChannelSeqId(context.getChannelSeqId());
			context_Reject.setDomainSeqId(context.getDomainSeqId());
			context_Reject.setEventTime(context.getEventTime());
			context_Reject.setRequester(context.getRequester());
			context_Reject.setEventSource(context.getEventSource());
			EventType eventType=new EventType();
			eventType.setEventCategory(context.getEventType().getEventCategory());
			eventType.setRequestType(context.getEventType().getRequestType());
			eventType.setFormat(context.getEventType().getFormat());
			eventType.setServiceKey(context.getEventType().getServiceKey());
			eventType.setOutcomeCategory(context.getEventType().getOutcomeCategory());
			eventType.setStatus(PaymentIngestionConstant.BACKEND_UPDATED);
			context_Reject.setEventType(eventType);
			ObjectNode payload_Reject=objectMapper.createObjectNode();
			payload_Reject.put("isoStatusCd", "RJCT");
			payload_Reject.put(PaymentIngestionConstant.STATUS, PaymentIngestionConstant.REJECTED);
			
			ObjectNode reject_payload = objectMapper.createObjectNode();
			reject_payload.set(PaymentIngestionConstant.PAYLOAD, payload_Reject);
			reject_payload.put(PaymentIngestionConstant.EVENTVERSION, "1.0");
			reject_payload.set(PaymentIngestionConstant.CONTEXT, objectMapper.convertValue(context_Reject, ObjectNode.class));
			reject_payload.put(PaymentIngestionConstant.ID, "evt_"+ String.valueOf(Math.random()).replace(".",""));
			LOGGER.info("Reject Payload for DeclineIRTP:::{}",reject_payload.toString());
			processKafkaMessage(objectMapper.readTree(reject_payload.toString()),PaymentIngestionConstant.SINGLE);
		}catch(Exception ex) {
			LOGGER.error("Error in processRejectMessageForDecline to process kafka message");
		}
	}
	
	public void processUnwindCallOnFaeHold(final Context context) throws IOException {
		LOGGER.info("Processing Unwind call for rejected payment for ChannelSeqId ::::::{}", context.getChannelSeqId());
		LOGGER.info("UnWind URL::::::{}", paymentIngestionProperties.getUnwindUrl());
		final String unwindUrl = paymentIngestionProperties.getUnwindUrl();
		String lCurrentStatus = null;
		if(context.getChannelSeqId()!=null) {
			lCurrentStatus = paymentIngestionQueryUtil.getStatusUsingDomainSeqId(context.getChannelSeqId());
		}
		LOGGER.info("lCurrentStatus:::::::::{} ",lCurrentStatus);	
		if(!PaymentIngestionConstant.LOGICAL_END_STATGES.contains(lCurrentStatus)) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		Map<String, String> params = new HashMap<>();
		headers.set(PaymentIngestionConstant.AUTHORIZATION, paymentIngestionProperties.getUnwindCallAuthToken());
		headers.setContentType(MediaType.APPLICATION_JSON);
		params.put(PaymentIngestionConstant.CHANNEL_SQ_ID, context.getChannelSeqId());
		/** Build the request*/
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(params, headers);
		/**use postForEntity method for HTTP call */
		((SimpleClientHttpRequestFactory)restTemplate.getRequestFactory()).setConnectTimeout(paymentIngestionProperties.getRestCallTimeout());
		LOGGER.info("entity is::::::{}", entity.toString());
		ResponseEntity<Boolean> response = restTemplate.postForEntity(unwindUrl, entity, Boolean.class);
		LOGGER.info("response is::::::{}", response.toString());
		switch(response.getStatusCode())
		{
		   case OK :
			   LOGGER.info("Unwind call status::::{}, & success:::: {}", response.getStatusCode(), response.getBody());
		      break;
		   case BAD_REQUEST :
			   LOGGER.error("Unwind call status::::{}", response.getStatusCode());
		      break;
		   case REQUEST_TIMEOUT :
			   LOGGER.error("Unwind call status::::{}", response.getStatusCode());
			   break; 
		   default : 
			   LOGGER.error("Unwind call status::::{}, & success:::: {}", response.getStatusCode(), response.getBody());
		}
		LOGGER.info("Unwind call status::::{}, & success:::: {}", response.getStatusCode(), response.getBody());
		}else {
			LOGGER.error("Payment is in Logical end stage:"+ lCurrentStatus+" .Can not make Unwind call for id :"+context.getChannelSeqId());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void processApiCallForAERelease(final Context context, String domainSeqId) {
		RestTemplate restTemplate = new RestTemplate();
		/** create headers */
		HttpHeaders headers = new HttpHeaders();
		/** set custom header */
		headers.set(PaymentIngestionConstant.CHANNEL_SQ_ID, context.getChannelSeqId());
		headers.set(PaymentIngestionConstant.SERVICE_KEY, context.getEventType().getServiceKey());
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(PaymentIngestionConstant.AUTHORIZATION,
				"Bearer "+processBackendEventFAE(context.getChannelSeqId(), context.getEventType().getServiceKey()));
		String workflow_url_fae = paymentIngestionProperties.getFaeUrl();
		workflow_url_fae = workflow_url_fae.replace("<serviceKey>", context.getEventType().getServiceKey());
		LOGGER.info("workflow_url_fae ::::: {}", workflow_url_fae);
		/** build the request */
		JSONObject body = new JSONObject();
		JSONObject payload = new JSONObject();
		payload.put("channelSeqId", context.getChannelSeqId());
		payload.put("domainSeqId", domainSeqId);
		body.put("payload", payload);
		HttpEntity<String> entity = new HttpEntity<String>(body.toString(), headers);
		/** use postForEntity method for HTTP call */
		LOGGER.info("Entity:::::{}", entity);
		ResponseEntity<String> response = restTemplate.postForEntity(workflow_url_fae, entity,
				String.class);
		LOGGER.info("CONDUCTOR RESPONSE ::::: {}", response);
		if (response.getStatusCode() == HttpStatus.OK) {
			LOGGER.info("FAE RELEASE STATUS ::::: {}", response.getStatusCode());
		} else {
			LOGGER.info("FAE ERROR STATUS :::::: {}", response.getBody());
		}
	}
	
	private String processBackendEventFAE(String seqChannelId, String serviceKey) {
		ResponseEntity<String> response = null;
		String jwtToken = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String userInfo = commonServiceUtility.getUserInfo(seqChannelId).toString();
			LOGGER.error("userInfo={}", userInfo);
			HttpEntity<String> requestHeader = new HttpEntity<>(userInfo,
					commonServiceUtility.setHeaderForNewJwt(paymentIngestionProperties.getDummyJWT()));
			String restJWTEndPoint = paymentIngestionProperties.getNewJWTEndPoint();
			LOGGER.info("requestHeader={} dummyJWT={} ", requestHeader, paymentIngestionProperties.getDummyJWT());
			response = restTemplate.exchange(restJWTEndPoint, HttpMethod.POST, requestHeader, String.class);
			JSONObject responseJson = new JSONObject(response.getBody());
			LOGGER.info("responseJson={}", responseJson);
			jwtToken = responseJson.getString("jwtToken");
			LOGGER.info("newJWTToken={}", jwtToken);
		} catch (Exception e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_067 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_067_Desc);
			LOGGER.error("Excption found={}", e.getMessage());

		}
		return jwtToken;
	}

	private void processKafkaMessageForAEHold(Context context) throws JsonProcessingException {
		double randNum1 = Math.random();
		double randNum2 = Math.random();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(PaymentIngestionConstant.TIMESTAMP_FORMAT2);
		LocalDateTime now = LocalDateTime.now();
		/************************************* Backend_ack_success*************************************
		 *Backend_ack_success FAE payload*/
		// TODO: Commenting for BCCReject  
//		FAEBackendEvent faeAckbackendEvent = new FAEBackendEvent();
//		faeAckbackendEvent.setId(String.valueOf(randNum1).replace(PaymentIngestionConstant.DOT, PaymentIngestionConstant.BLANK));
//		faeAckbackendEvent.setEventVersion(PaymentIngestionConstant.VERSION);
//		/**Backend_ack_success context*/
//		Context ackContextPayload = new Context();
//		ackContextPayload.setChannelSeqId(context.getChannelSeqId());
//		//ackContextPayload.setDomainSeqId(faeAckbackendEvent.getId()); //Fix for 2259
//		ackContextPayload.setEventTime(dtf.format(now));
//		/** Backend_ack_Success requester*/
//		Requester ackRequester = new Requester();
//		ackRequester.setId(PaymentIngestionConstant.REQUESTER_ID);
//		ackRequester.setDomainId(PaymentIngestionConstant.REQUESTER_DOMAINID);
//		/**Backend_ack_success eventType */
//		EventType ackEventType = new EventType();
//		ackEventType.setEventCategory(PaymentIngestionConstant.BUSINESS);
//		ackEventType.setServiceKey(context.getEventType().getServiceKey());
//		ackEventType.setOutcomeCategory(PaymentIngestionConstant.BACKEND_STATE_UPDATED);
//		ackEventType.setStatus(PaymentIngestionConstant.BACKEND_ACK_SUCC);
//		ackEventType.setRequestType(PaymentIngestionConstant.SMALL_ADD);
//		ackEventType.setFormat(PaymentIngestionConstant.PLAIN);
//		/**Backend_ack_success eventSource */
//		EventSource ackEventSource = new EventSource();
//		ackEventSource.setSourceIdentity(PaymentIngestionConstant.EVNT_SOURCE_INDENTITY);
//		ackEventSource.setRegion(PaymentIngestionConstant.REGION);
//		ackEventSource.setCountry(PaymentIngestionConstant.COUNTRY_CANADA);
//		ackContextPayload.setRequester(ackRequester);
//		ackContextPayload.setEventType(ackEventType);
//		ackContextPayload.setEventSource(ackEventSource);
//
//		ObjectMapper ackMapper = new ObjectMapper();
//		faeAckbackendEvent.setContext(ackContextPayload);
//		String ackJson = ackMapper.writeValueAsString(faeAckbackendEvent);
//		JSONObject ackFinalPayload = new JSONObject(ackJson);
//		ackFinalPayload.put(PaymentIngestionConstant.PAYLOAD, new JSONObject());
//		ackFinalPayload.getJSONObject(PaymentIngestionConstant.CONTEXT).put(PaymentIngestionConstant.REQUESTER,new JSONObject());
//
//		ProducerRecord<String, String> ackRecord = new ProducerRecord<>(paymentKafkaProperties.getKafkaTopic(),ackFinalPayload.toString());
//		/**backend_ack_success event on Kafka */
//		LOGGER.info("backend_ack_success event sending on Kafka for FAE:::::{}",ackRecord);
//		kafkaProducer.sendKafkaMessageForAEHold(ackRecord);
//		LOGGER.info("backend_ack_success event sent on Kafka for FAE");
		/************************************************************************ Backend_rejected*************************************
		 *Backend_rejected FAE payload*/
		FAEBackendEvent faeRjctbackendEvent = new FAEBackendEvent();
		faeRjctbackendEvent.setId(String.valueOf(randNum2).replace(PaymentIngestionConstant.DOT, PaymentIngestionConstant.BLANK));
		faeRjctbackendEvent.setEventVersion(PaymentIngestionConstant.VERSION);
		Context rjctContextPayload = new Context();
		rjctContextPayload.setChannelSeqId(context.getChannelSeqId());
		//rjctContextPayload.setDomainSeqId(faeRjctbackendEvent.getId()); //Fix for 2259
		rjctContextPayload.setEventTime(dtf.format(now));
		
		Requester rjctRequester = new Requester();
		rjctRequester.setId(PaymentIngestionConstant.REQUESTER_ID);
		rjctRequester.setDomainId(PaymentIngestionConstant.REQUESTER_DOMAINID);
		EventType rjctEventType = new EventType();
		rjctEventType.setEventCategory(PaymentIngestionConstant.BUSINESS);
		rjctEventType.setServiceKey(context.getEventType().getServiceKey());
		rjctEventType.setOutcomeCategory(PaymentIngestionConstant.BACKEND_STATE_UPDATED);
		rjctEventType.setStatus(PaymentIngestionConstant.BACKEND_REJECTED);
		rjctEventType.setRequestType(PaymentIngestionConstant.SMALL_ADD);
		rjctEventType.setFormat(PaymentIngestionConstant.PLAIN);
		EventSource rjctEventSource = new EventSource();
		rjctEventSource.setSourceIdentity(PaymentIngestionConstant.EVNT_SOURCE_INDENTITY);
		rjctEventSource.setRegion(PaymentIngestionConstant.REGION);
		rjctEventSource.setCountry(PaymentIngestionConstant.COUNTRY_CANADA);
		rjctContextPayload.setEventType(rjctEventType);
		rjctContextPayload.setEventSource(rjctEventSource);
		rjctContextPayload.setRequester(rjctRequester);
		
		ObjectMapper rjctMapper = new ObjectMapper();
		faeRjctbackendEvent.setContext(rjctContextPayload);
		faeRjctbackendEvent.setAdditionalProperty(PaymentIngestionConstant.BCC_ACTION, PaymentIngestionConstant.AE_HOLD);
		
		String rjctJson = rjctMapper.writeValueAsString(faeRjctbackendEvent);
		JSONObject rjctFinalPayload = new JSONObject(rjctJson);
		rjctFinalPayload.put(PaymentIngestionConstant.PAYLOAD, new JSONObject());
		rjctFinalPayload.getJSONObject(PaymentIngestionConstant.CONTEXT).put(PaymentIngestionConstant.REQUESTER,new JSONObject());
		ProducerRecord<String, String> rjctRecord=null;
		rjctFinalPayload.put(PaymentIngestionConstant.ID,String.valueOf(randNum2).replace(PaymentIngestionConstant.DOT, PaymentIngestionConstant.BLANK));
		rjctFinalPayload.getJSONObject(PaymentIngestionConstant.PAYLOAD).put(PaymentIngestionConstant.STATUS,PaymentIngestionConstant.REJECTED);
		rjctRecord = new ProducerRecord<>(paymentKafkaProperties.getKafkaTopic(), rjctFinalPayload.toString());
		/**backend_rejected event on Kafka*/
		LOGGER.info("backend_rejected event sending on Kafka for FAE:::::{}",rjctRecord);
		kafkaProducer.sendKafkaMessageForAEHold(rjctRecord);
		LOGGER.info("backend_rejected event sent on Kafka for FAE");
	}

	private boolean checkWireCutoffTime() throws JsonProcessingException {
		final String cutTimeurl=paymentIngestionProperties.getRefDataUrl();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		Date currentDate = new Date();
        DateFormat df = new SimpleDateFormat(PaymentIngestionConstant.WIRE_CUTOFF_DTM_24FORMATE);
		ObjectNode objectnode = JsonNodeFactory.instance.objectNode();
		ObjectNode RefDataValidationRequest = JsonNodeFactory.instance.objectNode();
		ObjectNode validateKeyJSON = JsonNodeFactory.instance.objectNode();
		validateKeyJSON.put(PaymentIngestionConstant.SERVICE_ID, PaymentIngestionConstant.SERVICE_ID_VAL);
		validateKeyJSON.put(PaymentIngestionConstant.BSDN_DTM, df.format(currentDate));
		validateKeyJSON.put(PaymentIngestionConstant.CUT_OFF_TYPE, PaymentIngestionConstant.CUT_OFF_VAL);
		validateKeyJSON.put(PaymentIngestionConstant.PROD_TYPE, PaymentIngestionConstant.PROD_TYPE_VAL);
		RefDataValidationRequest.set(PaymentIngestionConstant.VALIDATE_KEY, validateKeyJSON);
		objectnode.set(PaymentIngestionConstant.REF_DATA_REQ_TAG, RefDataValidationRequest);
		headers.setContentType(MediaType.APPLICATION_JSON);
		/**Auth value is null bcoz only need to send Auth key in header*/
		headers.set(PaymentIngestionConstant.AUTHORIZATION, null);
		/**build the request*/
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(objectnode,headers);
		/**use postForEntity method for HTTP call*/
		LOGGER.info("RequestHeader::::::{},RequestBody::::::{}",entity.getHeaders(),entity.getBody());
		ResponseEntity<ObjectNode> response = restTemplate.postForEntity(cutTimeurl, entity, ObjectNode.class);
		LOGGER.info("Response::::::{} HttpStatusCode::::{}",response.getBody().get(PaymentIngestionConstant.REF_DATA_RESP_TAG),response.getStatusCodeValue());
		if (response.getStatusCodeValue() == PaymentIngestionConstant.ACCEPTED_CODE || response.getStatusCodeValue() == 200) {
			LOGGER.info("isWireCutoffTime Valid ::::::{}", response.getBody().get(PaymentIngestionConstant.REF_DATA_RESP_TAG).get(PaymentIngestionConstant.VALIDATE_KEY).get(PaymentIngestionConstant.VALID_STATUS).asBoolean());
			return response.getBody().get(PaymentIngestionConstant.REF_DATA_RESP_TAG).get(PaymentIngestionConstant.VALIDATE_KEY).get(PaymentIngestionConstant.VALID_STATUS).asBoolean();
		}
		else {
			LOGGER.error("ERROR isWireCutoffTime Valid ::::::{}", response.getBody().get(PaymentIngestionConstant.VALID_STATUS).asBoolean());
			return response.getBody().get(PaymentIngestionConstant.REF_DATA_RESP_TAG).get(PaymentIngestionConstant.VALIDATE_KEY).get(PaymentIngestionConstant.VALID_STATUS).asBoolean();
		}
		
		
	}

	
	
	/**
	 * Process rabbit message.
	 *
	 * @param requestMessageJson
	 *            the request message json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void processRabbitMessage(final JsonNode requestMessageJson) throws Exception {
		/** Extracting Context Information from Request Message*/
		final JsonNode contextJson = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		
		/** Converting JsonNode to Context model*/
		 Context context = objectMapper.readValue(contextJson.toString(), Context.class);
		/**
		 * separating the events based on the outcome category[channel-state-updated,
		 * channel-workflow-update, backend-state-updated] for processing since each
		 * channel/routes has its own implementations
		 */
		dispatchRMQMsgBasedOnOutcomeCategory(requestMessageJson, context);
		/**
		 * After successful processing, sending all backend state events to rabbitmq for
		 * other apis to consume.
		 */
//		Commenting to solve Requeue
//		if (PaymentIngestionConstant.BACKEND_STATE_UPDATED.equalsIgnoreCase(Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getOutcomeCategory()).orElse(null))) {
//			paymentIngestionHelper.publishBackendEventsToRabbit(requestMessageJson, context);
//		}

	}

	/**
	 * Dispatch kafka message based on request types.
	 *
	 * @param context
	 *            the context
	 * @param payloadJson
	 *            the payment instruction
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */

	private void dispatchKafkaMessageBasedOnRequestTypes(final Context context, final ObjectNode payloadJson)throws IOException, DLQException {
		LOGGER.info("Start dispatchKafkaMessageBasedOnRequestTypes() at TIME ::::{} with PAYLOAD:::::::{}", new Date(),payloadJson);
		final String status = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getStatus()).orElse(null);
		final Function<JsonNode, String> getUIStatus = node -> Optional.ofNullable(node).map(json -> json.get(PaymentIngestionConstant.STATUS)).map(JsonNode::asText).orElse(null);
		String currentPaylodStatus = getUIStatus.apply(payloadJson);
		final JsonNode existingActiveDoc = paymentIngestionQueryUtil.fetchActiveRecord(context.getDomainSeqId());
		final String activeRecordStatus = getUIStatus.apply(existingActiveDoc);
		PaymentsInstructionsES existingActiveRecES = null;
		if(existingActiveDoc!=null) {
			existingActiveRecES = objectMapper.readValue(existingActiveDoc.toString(), PaymentsInstructionsES.class);			
		}
		LOGGER.info("paymentType::::::: {}  ", context.getEventType().getServiceKey());
		LOGGER.info("currentPaylodStatus::::::: {}  and exsitingActiveRecordStatus:::: {} ", currentPaylodStatus,activeRecordStatus);

		//mapping stateInfo
		ObjectNode stateInfo = null;
		LOGGER.info("stateInfo before enrichment ::::::::{}", stateInfo);
		stateInfo = JsonNodeFactory.instance.objectNode();
		stateInfo.put(PaymentIngestionConstant.DOC_STATUS, PaymentIngestionConstant.DOC_STATUS_ACTIVE);
		stateInfo.put(PaymentIngestionConstant.DOMAIN_SEQ_ID, context.getDomainSeqId());
		if(payloadJson.has(PaymentIngestionConstant.STATE_INFO)) {
			if(payloadJson.get(PaymentIngestionConstant.STATE_INFO).has(PaymentIngestionConstant.LAST_ACTION)) {
				stateInfo.set("lastAction",payloadJson.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.LAST_ACTION));
			}
		}
		if (!context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)){
			stateInfo.put(PaymentIngestionConstant.CHANNEL_SQ_ID, context.getChannelSeqId());
		}
		//mapping transitionInfo
		ObjectNode transitionInfo = JsonNodeFactory.instance.objectNode();
		LOGGER.info("transitionInfo before enrichment ::::::::{}", transitionInfo);
		if (payloadJson.has(PaymentIngestionConstant.STATUS) && payloadJson.get(PaymentIngestionConstant.STATUS).asText().equalsIgnoreCase(PaymentIngestionConstant.COMPLETED)) {
			transitionInfo.put(PaymentIngestionConstant.STATUS, PaymentIngestionConstant.COMPLETED.toLowerCase());
		} else {
			transitionInfo.put(PaymentIngestionConstant.STATUS, PaymentIngestionConstant.IN_PROGRESS);
		}
		if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)){
			if(payloadJson.has(PaymentIngestionConstant.STATUS) && payloadJson.get(PaymentIngestionConstant.STATUS).asText().equalsIgnoreCase(PaymentIngestionConstant.REJECTED)) {
				RejectionInfo rejInfo=existingActiveRecES != null ? existingActiveRecES.getStateInfo().getTransitionInfo().getRejectionInfo() : null;
				LOGGER.info("kafka Rejection Info  >>>>>>> {}", rejInfo);
				if(rejInfo==null) {
						ObjectNode rejectionInfo = JsonNodeFactory.instance.objectNode();
						rejectionInfo.put(PaymentIngestionConstant.REJECTIONINFO_SOURCE, PaymentIngestionConstant.SYSTEM);
						rejectionInfo.put(PaymentIngestionConstant.MESSAGE, PaymentIngestionConstant.BACKEND_REJECTION);					
						transitionInfo.set(PaymentIngestionConstant.REJECTION_INFO, rejectionInfo);
						LOGGER.info("kafka Rejection Info >>>>>>> {}", rejectionInfo.asText());
				}
				stateInfo.put(PaymentIngestionConstant.CHANNEL_SQ_ID, "");
			}
		}
		stateInfo.set(PaymentIngestionConstant.TRANSITION_INFO, transitionInfo);
		//mapping initiated json
		if (PaymentIngestionConstant.BACKEND_CREATED.equalsIgnoreCase(status) || PaymentIngestionConstant.BACKEND_UPDATED.equalsIgnoreCase(status)) {
			JsonNode initiatedJson = paymentIngestionQueryUtil.getInitiatorOfACreateRequestForATxn(payloadJson);
			if(payloadJson.has(PaymentIngestionConstant.CREATION_DATE_TIME)) {
				initiatedJson=objectMapper.createObjectNode().set(PaymentIngestionConstant.TS, payloadJson.get(PaymentIngestionConstant.CREATION_DATE_TIME));
				stateInfo.set(PaymentIngestionConstant.INITIATED, initiatedJson);
			}
		}
		payloadJson.set(PaymentIngestionConstant.STATE_INFO, stateInfo);
		// 
		LOGGER.info("stateInfo after enrichment:::: {}", stateInfo);
		if(payloadJson.has(PaymentIngestionConstant.STATUS)) {
			String cumilativeStatus =(PaymentIngestionConstant.DOC_STATUS_ACTIVE+payloadJson.get(PaymentIngestionConstant.STATUS).asText()+(payloadJson.has(PaymentIngestionConstant.FAE_INDICATOR)?payloadJson.get(PaymentIngestionConstant.FAE_INDICATOR).asText():PaymentIngestionConstant.NULL)+(payloadJson.has(PaymentIngestionConstant.BCC_ACTION)?payloadJson.get(PaymentIngestionConstant.BCC_ACTION).asText():PaymentIngestionConstant.NULL));
			LOGGER.info("cumilativeStatus:::: {}", cumilativeStatus);
			payloadJson.put(PaymentIngestionConstant.CUMULATIVE_STATUS, cumilativeStatus);
			payloadJson.put(PaymentIngestionConstant.FETCH_QUERY_STATUS, cumilativeStatus);
			payloadJson.put(PaymentIngestionConstant.UIDISPLAY_STATUS, payloadJson.get(PaymentIngestionConstant.STATUS).asText());
			LOGGER.info("payloadJson:::: {}", payloadJson);
		}
		
//		if (!isEventOnlyForPublishingAlerts(activePayloadNewRequestStatus, activePayloadPrevRequestStatus)) {
			if (PaymentIngestionConstant.BACKEND_CREATED.equalsIgnoreCase(status)&& PaymentIngestionConstant.ADD.equalsIgnoreCase(context.getEventType().getRequestType())) {
				LOGGER.info("PayloadJson when backendCreated with domainSeqId::::::{} and servicesKey ::::::::{} and status:::::{}",context.getDomainSeqId(),context.getEventType().getServiceKey(),context.getEventType().getStatus());
				backendUtil.addPiSupDocWhtDoc(payloadJson, context);
			} else if (PaymentIngestionConstant.BACKEND_UPDATED.equalsIgnoreCase(status)) {
				LOGGER.info("PayloadJson when backendUpdated with domainSeqId::::::{} and servicesKey ::::::::{} and status:::::{}",context.getDomainSeqId(),context.getEventType().getServiceKey(),context.getEventType().getStatus());
				if(context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP) && payloadJson.has(PaymentIngestionConstant.INVOICE_DATE) && payloadJson.has(PaymentIngestionConstant.INVOICE_NO)) {
					// invoice details
					LOGGER.info("Updating Invoice Details::::::");
					InvoiceDetailsES invoiceDetailsES = new InvoiceDetailsES();
					invoiceDetailsES.setInvoiceDate(payloadJson.get(PaymentIngestionConstant.INVOICE_DATE).asText());
					invoiceDetailsES.setInvoiceNo(payloadJson.get(PaymentIngestionConstant.INVOICE_NO).asText());
					ObjectMapper mapper = new ObjectMapper();
					
					LOGGER.info("Updating Invoice Details::::::{}", invoiceDetailsES);
					if(payloadJson.has(PaymentIngestionConstant.SUPPLEMENTRY_DATA)){
						JsonNode supp_data = payloadJson.get(PaymentIngestionConstant.SUPPLEMENTRY_DATA);
						LOGGER.info("has SupplementaryDate::::::{}", supp_data);
						((ObjectNode)supp_data).set(PaymentIngestionConstant.INVOICE_DETAILS, mapper.readTree(mapper.writeValueAsString(invoiceDetailsES)));
						((ObjectNode)payloadJson).set(PaymentIngestionConstant.SUPPLEMENTRY_DATA, mapper.readTree(mapper.writeValueAsString(supp_data)));
					} else {
						SupplementaryDataES supplementaryDataES = new SupplementaryDataES();
						supplementaryDataES.setInvoiceDetails(invoiceDetailsES);
						String supplementaryDataESJson = mapper.writeValueAsString(supplementaryDataES);
						LOGGER.info("does not have SupplementaryDate so created new::::::{}", supplementaryDataESJson);
						((ObjectNode)payloadJson).set(PaymentIngestionConstant.SUPPLEMENTRY_DATA, mapper.readTree(supplementaryDataESJson));
					}
					LOGGER.info("Updated payload::::::{}", payloadJson);
				}
				JestResult jestResult =backendUtil.updatePaymentInstruction(payloadJson, context);
				LOGGER.info("After backend update called:"+activeRecordStatus);
			if (context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP)
					&& jestResult != null && activeRecordStatus.equals(PaymentIngestionConstant.IN_TRANSIT)
					&& !(context.getEventSource().getSourceIdentity()
							.equalsIgnoreCase(PaymentIngestionConstant.channel_state_updated_incomingrtpstatus))) {
					LOGGER.info("Inside trash condition");
					JsonNode transitionRecord = paymentIngestionQueryUtil.fetchTransitionRecord(context.getDomainSeqId());
					LOGGER.info("<<<<< Fetched Transition record >>>>> :::"+transitionRecord);
					String lChannelSeqId = transitionRecord.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.CHANNEL_SQ_ID).asText();
					LOGGER.info("<<<<< Fetched channelSeqId from ES >>>>> :::"+lChannelSeqId);
					sendRequestToLogicalEndAState(context.getEventType().getServiceKey(),lChannelSeqId, "trash","Trashed Transition record based on receiving notification");
				}
				
			} else {
				LOGGER.info("Unknown Event has arrived, hence considering this request as invalid");
			}
//		} else {
//			LOGGER.info("Incoming Event with domainSeqId => [{}] received with prev payload -> status => [{}] and new payload ->status => [{}] is restricted and is only meant for publishing alerts "+ "and hence not be processed further.",context.getDomainSeqId(), activePayloadPrevRequestStatus, activePayloadNewRequestStatus);
//		}

	}

	/**
	 * Checks if the incoming backend event is to be persisted in ES or it is only
	 * sent for publishing to Alerts Engine.
	 * 
	 * The incoming requests not to be further processed include
	 * 
	 * INITIATED, AUTHORIZED ->> Not required and REJECTED events (before SENT TO
	 * BANK) ->> since ipsh authorization succeeds after request is sent to bank so
	 * ingestion will process and ingested only after successfully authorized not
	 * rejection before authorization because those requests might have inconsistent
	 * data which might lead to inquiry failure
	 * 
	 * @param newStatus
	 *            new incoming status
	 * @param prevStatus
	 *            prev incoming status
	 * @return {@link Boolean} flag to decide if to be persisted or only sent for
	 *         alerts
	 */
	private boolean isEventOnlyForPublishingAlerts(String newStatus, String prevStatus) {
		LOGGER.info("Start isEventOnlyForPublishingAlerts() with newStatus:::::::{} & prevStatus:::::{}",newStatus,prevStatus);
		return Arrays.asList(UIStatusEnum.INITIATED.toString(), UIStatusEnum.AUTHORIZED.toString()).contains(newStatus)
				|| (UIStatusEnum.REJECTED.toString().equals(newStatus) && StringUtils.isBlank(prevStatus));
	}

	/**
	 * Dispatch kafka message file backend created.
	 *
	 * @param context
	 *            the context
	 * @param payloadJson
	 *            the payload json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	private void dispatchKafkaMessageFileBackendCreated(final Context context, final ObjectNode payloadJson)
			throws IOException, MessageProcessingException {

		LOGGER.info("hanlding dispatchKafkaMessageFileBackendCreated");

		final String status = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getStatus())
				.orElse(null);

		if (PaymentIngestionConstant.BACKEND_CREATED.equalsIgnoreCase(status)) {
			backendUtil.buildPIAndInsert(payloadJson);
		} else if (PaymentIngestionConstant.BACKEND_UPDATED.equalsIgnoreCase(status)) {
			final ObjectNode stateInfo = JsonNodeFactory.instance.objectNode();
			stateInfo.put("docStatus", PaymentIngestionConstant.DOC_STATUS_ACTIVE);
			stateInfo.put(PaymentIngestionConstant.DOMAIN_SEQ_ID, context.getDomainSeqId());
			payloadJson.set(PaymentIngestionConstant.STATE_INFO, stateInfo);
			backendUtil.updatePaymentInstruction(payloadJson, context);
		} else {
			LOGGER.info("Unknow Event has arrived, hence considering this request as invalid");
		}
	}

	/**
	 * Dispatch RMQ msg based on outcome category.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void dispatchRMQMsgBasedOnOutcomeCategory(final JsonNode message, final Context context) throws Exception {
		LOGGER.info("Start dispatchRMQMsgBasedOnOutcomeCategory() for status:::::::{} and servicesKey::::::{} ", context.getEventType().getStatus(),context.getEventType().getServiceKey());
		final String outcomeCategory = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getOutcomeCategory()).orElse(null);
		if(context.getChannelSeqId()!=null) {
			LOGGER.info("Message received on Rabbit for channelSeq Id: {} and payload : {}", context.getChannelSeqId(),message);
		}else {
			LOGGER.info("Message receivedon on Rabbit payload : {}", message);
		}
		/*
		 * 1. copying request json to paymentIngestionHelper object for stateinfo
		 * Enrichment
		 */
		final JsonNode payload;
		if (message.get(PaymentIngestionConstant.PAYLOAD) == null || message.get(PaymentIngestionConstant.PAYLOAD).isNull()) {
			payload = objectMapper.createObjectNode();
			((ObjectNode) message).set(PaymentIngestionConstant.PAYLOAD, payload);
		} else {
			payload = message.get(PaymentIngestionConstant.PAYLOAD);
		}

		final String status = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getStatus()).orElse(null);
		LOGGER.info("status :::: {} ", status);
		final String serviceKey = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getServiceKey()).orElse(null);
		LOGGER.info("servicesKey :::: {} ", serviceKey);
		
		if (PaymentIngestionConstant.BACKEND_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)|| PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(outcomeCategory) && payload != null) {
			// Status Validation
			if (null != status && isValidStatus(status.toUpperCase(Locale.ENGLISH))) {
				if(null != serviceKey && "paymnt/fulfillrtp".equalsIgnoreCase(serviceKey) && PaymentIngestionConstant.REJECTED.equalsIgnoreCase(status)) {
					sendRequestToLogicalEndAState(serviceKey, context.getChannelSeqId(), PaymentIngestionConstant.TRASH_ACTION, "Request rejected by system. Check workflow for further details.");
				}
				else {
				LOGGER.info("Converting JsonPayload to PaymentInstruction model");
				final PaymentInstruction paymentInstruction = objectMapper.readValue(payload.toString(),PaymentInstruction.class);
				performCommonActions(message, context, paymentInstruction, null);
				}
			} else {
				LOGGER.error("Unknown status, hence Skipping the event id: {}",message.get(PaymentIngestionConstant.ID));
				throw new DLQException("Unknown status, hence Skipping the event id: {}  "+message.get(PaymentIngestionConstant.ID));
			}

		}

		else if (PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equalsIgnoreCase(outcomeCategory) && payload != null) {
			// Status Validation -> appending wf to the status value for all workflow events
			if (isValidStatus((status +PaymentIngestionConstant.WORKFLOW).toUpperCase(Locale.ENGLISH))) {

				/*
				 * 2. copying workflow payload to PaymentIngestionStateInfoHelper object for
				 * stateinfo Enrichment
				 */
				final PaymentWF paymentwf = objectMapper.readValue(payload.toString(), PaymentWF.class);

				// SourceIdentity Check for workflow rejected event
				if (PaymentIngestionConstant.REJECTED.equalsIgnoreCase(status)) {
					validateAndProcessWorkflowRejectedEvent(message, context, paymentwf);

				}
				// initiated, verified, approved, released, trashed
				else if (isWorkflowAcceptedStatus(status)) {
					final Boolean isValidSource = isValidateSourceComponent(context, ".*approval.*");
					if (isValidSource) {
						performCommonActions(message, context, null, paymentwf);
					} else {
						LOGGER.info(
								"EventMatrix Check -->>> This status {} from approval workflow centre is restricted or not a valid source, hence skipping this event id {}",
								status, message.get(PaymentIngestionConstant.ID));
					}
				}

			} else {
				LOGGER.info("Unknown Workflow event, hence Skipping the event id: {}",
						message.get(PaymentIngestionConstant.ID));
			}
		}

		else {
			LOGGER.info("Unknown OutCome Category, hence Skipping the event id: {}",message.get(PaymentIngestionConstant.ID));
		}

	}

	/**
	 * Validate bean.
	 *
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param channelSeqId
	 *            the channel seq id
	 * @return the list
	 * @throws DLQException
	 *             the DLQ exception
	 */
	private void validateBean(final PaymentInstruction paymentInstruction, final String channelSeqId)
			throws DLQException {
		final List<String> errorMessages = new ArrayList<>();
		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		validator.validate(paymentInstruction).stream().forEach(violation -> {
			final StringBuilder messageSet = new StringBuilder(110);
			messageSet.append("\n >>>> Property Path:").append(violation.getPropertyPath()).append(" <<<< \n >>>> Value that violated the constraint : ").append(violation.getInvalidValue()).append(" <<<< \n >>>> Constraint : ").append(violation.getMessage()).append(" <<<<");
			errorMessages.add(messageSet.toString());
		});
		if (!errorMessages.isEmpty()) {
			LOGGER.error("Validation Failed for the payment instruction with this channelSeqId --> {} and the Error Messages {}",channelSeqId, errorMessages);
			throw new DLQException("Bean Validation failed, hence sending to DLQ {}");
		}
	}

	/**
	 * Perform common actions.
	 *
	 * @param context
	 *            the context
	 * @param paymentInstruction
	 *            the payment instruction
	 * @param paymentWF
	 *            the payment WF
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void performCommonActions(final JsonNode message, final Context context,final PaymentInstruction paymentInstruction, final PaymentWF paymentWF) throws Exception {

		final String contextStatus = Optional.ofNullable(context).map(ctxt -> ctxt.getEventType()).map(eventType -> eventType.getStatus()).orElse(null);
		LOGGER.info("Start performCommonActions() for event: {} and context->status: {} channelSeqId: {}",message.get(PaymentIngestionConstant.ID), contextStatus, context.getChannelSeqId());
		/*
		 * Validation Begins triggering validation Custom Exception to be thrown and
		 * will further to be sent to dlq. This check is valid for all non draft event.
		 */
		if (paymentInstruction != null && !PaymentIngestionConstant.DRAFT_STATE.equalsIgnoreCase(contextStatus)) {
			validateBean(paymentInstruction, context.getChannelSeqId());
		}

		// acquireRedisLock
		if (paymentIngestionRedisUtil.acquirePurgeBatchLock(context.getChannelSeqId())) {

			LOGGER.info("fetching stateinformation of existing payment instruction payload for channelSeqId::::: {}",context.getChannelSeqId());

			/* Fetching existing PaymentInstruction data from elasticsearch if available */
			final Map<String, Object> existingInfoMap = enrichmentUtil.fetchExistingDocumentInfoFromES(context,message);

			final PaymentsInstructionsES existingPIES = (PaymentsInstructionsES) existingInfoMap.get(PaymentIngestionConstant.PAYMNT_INSTR_OBJ);

			final StateInfo stateInfo = Optional.ofNullable(existingPIES).map(PaymentsInstructionsES::getStateInfo).orElse(new StateInfo());

			final String domainSeqId = Optional.ofNullable(context).map(Context::getDomainSeqId ).orElse(null);
			
			final String bccAction = Optional.ofNullable(existingPIES).map(PaymentsInstructionsES::getBccAction).orElse(null);
			
			LOGGER.info(">>>> BccAction for ChannelSeqID {} : {}",context.getChannelSeqId(), bccAction);
			
			//Enriching Group ChannelSeqId when not fulfillrtp as group action not called for fulfillrtp and User is not Auto Approve
			if ( !context.getEventType().getServiceKey().contains(PaymentIngestionConstant.FULFILL_RTP) && context.getEventType().getStatus().equalsIgnoreCase(PaymentIngestionConstant.DRAFT_STATUS) ) {
				LOGGER.info(">>>> INSIDE GROUP CHANNELSEQID {} : {}",context.getChannelSeqId(), context.getEventType().getStatus());

				if(null==paymentInstruction.getGroupChannelSeqId() || "".equals(paymentInstruction.getGroupChannelSeqId()) || PaymentIngestionConstant.NULL.equalsIgnoreCase(paymentInstruction.getGroupChannelSeqId()) ) {
					LOGGER.info(">>>> INSIDE GROUP CHANNELSEQID IS NULL {} : {}",context.getChannelSeqId(), paymentInstruction.getGroupChannelSeqId());

					String pymntInstnGrpID = Strings.EMPTY;
					int count = 1;
					while( count <= 10 ) {
						try {
							Thread.sleep(paymentIngestionProperties.getGroupChannelSeqIdFetch_SleepMs());
							count = count + 1;
						} catch (InterruptedException e) {
							LOGGER.error("exception in thread sleep in getChannelSeqIdReleased : ",e);
						}
//						log.info("execute while loop and count is {}: ", count);					
//						redisMapOP = redisRepository.getHgetAll(Constant.ACTION_REQ_STATEINFO+channelSeqId);
//						jsonObj = new org.json.JSONObject(redisMapOP.get("metaData").toString());
						pymntInstnGrpID = paymentInstructionBuilder.getPymntInstnGrpID(context.getChannelSeqId());
						LOGGER.info("Count : {} GroupChannelSeqId : {} ", count, pymntInstnGrpID);
						
						if(Strings.isNotBlank(pymntInstnGrpID))
							break;
					}
					
					paymentInstruction.setGroupChannelSeqId((null != pymntInstnGrpID)? pymntInstnGrpID:PaymentIngestionConstant.NULL);
					LOGGER.info(">>>> SETTING GROUP CHANNELSEQID {} : {}",context.getChannelSeqId(), pymntInstnGrpID);
				}
			}

			/*
			 * call to fetch all past channelseqids from elasticsearch for the given
			 * domainseqid.
			 */

			if (!StringUtils.isBlank(domainSeqId)) {
				final List<String> pastChannelSeqIdList = enrichmentUtil.fetchPastChannelSeqIds(domainSeqId);
				final List<String> filteredChannelSeqIdList = pastChannelSeqIdList.stream().filter(channelSeqId -> !channelSeqId.equalsIgnoreCase(context.getChannelSeqId())).collect(Collectors.toList());
				if (filteredChannelSeqIdList != null) {
					stateInfo.setPastChannelRequestIds(filteredChannelSeqIdList);
					LOGGER.info("fetched list of past channeld seq Ids {} for the domainseqid {}",filteredChannelSeqIdList, domainSeqId);
				}
				else {
					LOGGER.error("fetched list of past channeld seq Ids {} for the domainseqid {}",filteredChannelSeqIdList, domainSeqId);
				}
			}

			// 2. setting paymentIngestionStateInfoHelper stateinfo
			final JsonNode stateInfoJson = objectMapper.convertValue(stateInfo, JsonNode.class);
			final StateTransitionData stateTransitionData = new StateTransitionData();
			// Enriching stateTransitionData info from existing record in ES.
			setFromStateTransitionData(stateInfo, stateTransitionData);

			/*
			 * Enriching stateTransitionData info from request payload. passing the
			 * stateTransitionData itself[has "From *" informations] to get enriched with
			 * "To *" informations and return
			 */
			setToStateTransitionData(context, stateTransitionData);

			/*
			 * Action finder call to figure out the action [possible actions are insert,
			 * replace, update, requeue, skip]
			 */
			//final TransitionAction transitionAction = getActionForTransition(stateTransitionData); //K1
			final TransitionAction transitionAction = getActionForTransitionCIBC(stateTransitionData);
			String action=null;
			final String toState = Optional.ofNullable(transitionAction).map(transAction->stateTransitionData.getToState().toString()).orElse(PaymentIngestionConstant.SKIP);
			LOGGER.info("toState::::::{} and OutcomeCategory is::::::{} ",toState,context.getEventType().getOutcomeCategory());
			LOGGER.info(">>>> bccAction ::::::{} ",bccAction);
			if(toState.equalsIgnoreCase("rejected") && context.getEventType().getOutcomeCategory().equalsIgnoreCase("channel-state-updated"))
			{
				action=PaymentIngestionConstant.UPDATE;
			} else if (toState.equalsIgnoreCase(PaymentIngestionConstant.BACKEND_REJECTED) && PaymentIngestionConstant.AE_HOLD.equalsIgnoreCase(bccAction)
					&& context.getEventType().getOutcomeCategory().equalsIgnoreCase(PaymentIngestionConstant.BACKEND_STATE_UPDATED)) {
				LOGGER.info("Inside IfElse for B/E Rejected for BccReject");
				action = PaymentIngestionConstant.UPDATE;
			}
			else {
				 action = Optional.ofNullable(transitionAction).map(transAction -> transAction.toString()).orElse(PaymentIngestionConstant.SKIP);
			}
			LOGGER.info("Action receive from common ingestion as: {} for ChannelSeqId: {} and eventId: {}", action,context.getChannelSeqId(), message.get(PaymentIngestionConstant.ID));
			final String requeueMsg = "Requeuing this event as per instruction level common matrix for channelSeqId: "+ context.getChannelSeqId();
			//changes done after code review 
			switch(action)
			{
			   case PaymentIngestionConstant.INSERT :
				   ingestionChannelUtil.performFirstInsert(paymentInstruction, context, message, stateInfoJson);
			      break;
			   case PaymentIngestionConstant.UPDATE :
				   ingestionChannelUtil.performStateUpdateOnly(paymentInstruction, paymentWF, context, existingPIES,(String) existingInfoMap.get(PaymentIngestionConstant.DOC_ID), message, stateInfoJson);
			      break; 
			   case PaymentIngestionConstant.REPLACE :
				   ingestionChannelUtil.performReplaceDocument(paymentInstruction, context, message, stateInfoJson);
				   break;
			   case PaymentIngestionConstant.REQUEUE :
				   paymentIngestionRedisUtil.releasePurgeBatchLock(context.getChannelSeqId());
				   ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);
				   break; 
			   case PaymentIngestionConstant.SKIP :
				   ingestionChannelUtil.skipMessage(context);
				   break; 
			   default : 
				   paymentIngestionRedisUtil.releasePurgeBatchLock(context.getChannelSeqId());
				   ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);
			}
			paymentIngestionRedisUtil.releasePurgeBatchLock(context.getChannelSeqId());
			LOGGER.info("Redis lock RELEASED for channelSeqId: {} and context: {} event: {}",context.getChannelSeqId(), context, message.get(PaymentIngestionConstant.ID));
		} else {
			final String requeueMsg = "Requeuing the event due to unavailability of redis lock, for channelSeqId: "+ context.getChannelSeqId()+" and event Id: "+message.get(PaymentIngestionConstant.ID);
			LOGGER.info("{} requeueMsg {} ",message.get(PaymentIngestionConstant.ID), requeueMsg);
			final StateTransitionData stateTransitionData = new StateTransitionData();

			/*
			 * setting only "***********State To***********" Information as this request
			 * hasn't reached a stage to fetch the existing information to derive to an
			 * action that needs to be taken. Enriching stateTransitionData info from
			 * request payload.
			 */
			LOGGER.info("Setting only **************To State************ Information at this stage");
			setToStateTransitionData(context, stateTransitionData);
			ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);

		}

	}

	/**
	 * Checks if is valid status.
	 *
	 * @param status
	 *            the status
	 * @return the boolean
	 */
	private Boolean isValidStatus(final String status) {
		final Map<String, FromStates> fromStateMap = Maps.newHashMapWithExpectedSize(FromStates.values().length);
		for (final FromStates fromState : FromStates.values()) {
			fromStateMap.put(fromState.name(), fromState);
		}
		final FromStates returnedStatus = Optional.ofNullable(fromStateMap.get(status)).orElse(null);
		return returnedStatus == null ? false : true;
	}

	/**
	 * Validate and process workflow rejected event.
	 *
	 * @param context
	 *            the context
	 * @param paymentwf
	 *            the paymentwf
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void validateAndProcessWorkflowRejectedEvent(final JsonNode message, final Context context,
			final PaymentWF paymentwf) throws Exception {
		final Boolean isValidEntitlementOrLimitEvent = isValidateSourceComponent(context, ".*limit.*||.*entitlement.*");
		if (isValidEntitlementOrLimitEvent) {
			final Boolean isCurrentAction = PaymentIngestionConstant.INITIATE
					.equalsIgnoreCase(paymentwf.getCurrentAction()) ? true : false;
			if (isCurrentAction) {
				performCommonActions(message, context, null, paymentwf);
			} else {
				LOGGER.info(
						"Sources being *limit* or *quest* and the current action isn't initiate hence ignoring the request.");
			}
		}
		// Approval Event
		else if (isValidateSourceComponent(context, ".*approval.*")) {
			performCommonActions(message, context, null, paymentwf);
		}
	}

	/**
	 * Sets the from state transition data.
	 *
	 * @param stateInfo
	 *            the state info
	 * @param stateTransitionData
	 *            the state transition data
	 */
	private void setFromStateTransitionData(final StateInfo stateInfo, final StateTransitionData stateTransitionData) {

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getLastActivityTs)
				.ifPresent(stateTransitionData::setFromEventTime);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getState)
				.ifPresent(stateTransitionData::setFromState);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getOutcomeCategory)
				.ifPresent(stateTransitionData::setFromOutcomeCategory);
		LOGGER.info("Inside setFromStateTransitionData :::: StateInfo {} and StateTransitionData {}",stateInfo, stateTransitionData);
	}

	/**
	 * Sets the to state transition data.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 * @param lastUpdateEventTime
	 *            the last update event time
	 */
	private void setToStateTransitionData(final Context context, final StateTransitionData stateTransitionData) {
        //vinay for 1078 - 1092
		LOGGER.info("Inside setToStateTransitionData :::: Context {} and StateTransitionData {}",context, stateTransitionData);
		String requiredEventTimeStr=null;
		if (!isValidFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",context.getEventTime(), Locale.ENGLISH))
		{
			DateTimeFormatter requiredFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			DateTimeFormatter currFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
			LocalDateTime currEventTimeParsed =  LocalDateTime.parse(context.getEventTime(), currFormatter);
			requiredEventTimeStr = currEventTimeParsed.format(requiredFormatter);
			stateTransitionData.setToEventTime(requiredEventTimeStr);
			stateTransitionData.setToState(context.getEventType().getStatus());
			stateTransitionData.setToOutcomeCategory(context.getEventType().getOutcomeCategory());
			LOGGER.info("IF setToStateTransitionData::: {}", stateTransitionData);
		}
		else {
		stateTransitionData.setToEventTime(context.getEventTime());

		stateTransitionData.setToState(context.getEventType().getStatus());

		stateTransitionData.setToOutcomeCategory(context.getEventType().getOutcomeCategory());
			LOGGER.info("Else setToStateTransitionData::: {}", stateTransitionData);
		}
	}

	/**
	 * Gets the redis key for purge batch lock.
	 *
	 * @param payloadType
	 *            the payload type
	 * @return the redis key for purge batch lock
	 */
	public static String getRedisKeyToSetLock(final String payloadType) {
		return PaymentIngestionConstant.REDIS_LOCK_KEY_PREFIX + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ PaymentIngestionConstant.REDIS_DOMAIN_KEY + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ "channel-state" + PaymentIngestionConstant.REDIS_KEY_SEPARATOR + payloadType;
	}

	/**
	 * Checks if is workflow accepted status.
	 *
	 * @param status
	 *            the status
	 * @return true, if is workflow accepted status
	 */
	private boolean isWorkflowAcceptedStatus(final String status) {
		workflowAcceptedStatus.put(PaymentIngestionConstant.INITIATED, true);
		workflowAcceptedStatus.put(PaymentIngestionConstant.VARIFIED, true);
		workflowAcceptedStatus.put(PaymentIngestionConstant.APPROVED, true);
		workflowAcceptedStatus.put(PaymentIngestionConstant.RELEASED, true);
		workflowAcceptedStatus.put(PaymentIngestionConstant.TRASHED, true);
		return Optional.ofNullable(workflowAcceptedStatus.get(status)).orElse(false);
	}

	/**
	 * Checks if is validate source component.
	 *
	 * @param context
	 *            the context
	 * @param regexString
	 *            the regex string
	 * @return the boolean
	 */
	private Boolean isValidateSourceComponent(final Context context, final String regexString) {
		final String regexP = regexString;
		final String lCaseSourceIdentity = Optional.ofNullable(context.getEventSource())
				.map(eventSource -> eventSource.getSourceIdentity())
				.map(sourceIdentity -> sourceIdentity.toLowerCase(Locale.ENGLISH)).orElse(PaymentIngestionConstant.BLANK);
		return lCaseSourceIdentity.matches(regexP);

	}
	
	public void processKafkaMessagePshc(final JsonNode requestMessageJson) throws IOException, InterruptedException, RequeueException, DLQException, MessageProcessingException {
		
		JestResult jestResult = null;
		List<String> errorList = new ArrayList<>();
		try{
			Map<String,String> esTypeMap = pshcReferenceDataInquiryProperties.getFetchKeyEsTypeMap();
			String elasticIndexName = pshcReferenceDataInquiryProperties.getEsIndexName();
			LOGGER.info("Map of elastic types configured in property {} and Elastic Index Name {} ", esTypeMap, elasticIndexName);
			JsonNode payload = requestMessageJson.has(PaymentIngestionConstant.REF_DATA_INQRY_RESP) && requestMessageJson.get(PaymentIngestionConstant.REF_DATA_INQRY_RESP) != null ?
					requestMessageJson.get(PaymentIngestionConstant.REF_DATA_INQRY_RESP) : JsonNodeFactory.instance.objectNode();

			Iterator<Entry<String, JsonNode>> payLoadIterator = payload.fields();
			
			while(payLoadIterator.hasNext()){
				Entry<String, JsonNode> name = payLoadIterator.next();
				String nameOfESType = esTypeMap.get(name.getKey());
				JsonNode jsonNodeData = name.getValue();
				LOGGER.info("Fetch Name of Es Type {}  , Fetch Key Json Payload {} ", nameOfESType, jsonNodeData);
				paymentIngestionQueryUtil.deleteESDocumentByIndexType(elasticIndexName, nameOfESType);
				jestResult = paymentIngestionQueryUtil.indexBuilder(jsonNodeData, "", elasticIndexName, nameOfESType);
				LOGGER.info("jestResult : {} ", jestResult);
				
				if(jestResult != null && jestResult.isSucceeded()){
					LOGGER.info("Success Response after data inserted into elastic schema {} ", jestResult.getResponseCode());
				}else if(jestResult != null && !jestResult.isSucceeded()){
					LOGGER.info("Data failed to insert into elastic schema with response code {} and response message {} ", jestResult.getResponseCode(), jestResult.getErrorMessage());
					errorList.add(jestResult.getErrorMessage());
				}else{
					LOGGER.error("Service has failed to insert data into Elastic schema for request message {}", requestMessageJson);
				}
			}
			LOGGER.info("Error List {} ", errorList);
		}catch(Exception ex){
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_068 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_068_Desc);
			LOGGER.error("Exception thrown while inserting data into elastic schema for error {} ", ex);
		}
	}
	//added by vinay push notifiction to the ES
    public void pushKafkaNotificationPayloadOnES(final PaymentsNotificationES paymentsNotificationES) throws IOException, InterruptedException, RequeueException, DLQException, MessageProcessingException {
		
		JestResult jestResult = null;
		List<String> errorList = new ArrayList<>();
		try{
				LOGGER.info("Map of elastic types configured in property {} and Elastic Index Name {} ",paymentElasticProperties.getPaynotificnType(),paymentElasticProperties.getPymtIndex());
				jestResult = paymentIngestionQueryUtil.indexBuilder(objectMapper.convertValue(paymentsNotificationES, ObjectNode.class),paymentsNotificationES.getNotificationIdES(), paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaynotificnType());
				LOGGER.info("jestResult : {} ", jestResult);
				if(jestResult != null && jestResult.isSucceeded()){
					LOGGER.info("Success Response after data inserted into elastic schema {} ", jestResult.getResponseCode());
				}else if(jestResult != null && !jestResult.isSucceeded()){
					LOGGER.info("Data failed to insert into elastic schema with response code {} and response message {} ", jestResult.getResponseCode(), jestResult.getErrorMessage());
					errorList.add(jestResult.getErrorMessage());
				}else{
//					LOGGER.error("Service has failed to insert data into Elastic schema for request message {}", requestMessageJson);
				}
			
			LOGGER.info("Error List {} ", errorList);
		}catch(Exception ex){
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_069 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_069_Desc);
			LOGGER.error("Exception thrown while inserting data into elastic schema for error {} ", ex);
		}
	}
    public void pushKafkaDlqPayloadOnES(Context context, JsonNode jsonNode, ObjectNode pMsgNode) {
		LOGGER.debug("Inside pushKafkaDlqPayloadOnES::::");
		JestResult jestResult = null;
		List<String> errorList = new ArrayList<>();
    	try {
    		final String eventStatusType = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.STATUS)).map(status -> status.textValue()).orElse(null);
			final String requestType = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.REQUEST_TYPE)).map(reqType -> reqType.textValue()).orElse(null);
			JsonNode requestId = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.CHANNEL_SQ_ID)).orElse(null);
			if(requestId == null) {
				LOGGER.debug("Inside requestId::::"+requestId);
				requestId = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.DOMAIN_SEQ_ID)).orElse(null);
			}
			if(requestId == null) {
				LOGGER.debug("Inside requestId::::"+requestId);
				requestId = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.ID)).orElse(null);
			}
			LOGGER.debug("Outside requestId::::"+requestId.textValue());
			final String contextEventTime = Optional.ofNullable(pMsgNode.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENTTIME)).map(eventTime -> eventTime.textValue()).orElse(null);
			final String errorType = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.ERR_TYPE).textValue()).orElse(null);
			String receivedMsg = Optional.ofNullable(jsonNode.get(PaymentIngestionConstant.RECIEVED_MSG).textValue()).orElse(null);
			Object json = objectMapper.readValue(pMsgNode.toString(), Object.class);
			receivedMsg =  objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			final PaymentsDlqNotificationES paymentsDlqNotificationES = new PaymentsDlqNotificationES();
			paymentsDlqNotificationES.setEventTypeES(eventStatusType);
			paymentsDlqNotificationES.setEventES(requestType);
			paymentsDlqNotificationES.setRequestIdES(requestId.textValue());
			paymentsDlqNotificationES.setMessageES(receivedMsg);
			paymentsDlqNotificationES.setExceptionTypeES(errorType);
			paymentsDlqNotificationES.setCreationTimeES(contextEventTime);
			jestResult = paymentIngestionQueryUtil.indexBuilder(objectMapper.convertValue(paymentsDlqNotificationES, ObjectNode.class),paymentsDlqNotificationES.getRequestIdES(), paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaydlqnotificnType());  
			LOGGER.info("jestResult : {} ", jestResult);
			if(jestResult != null && jestResult.isSucceeded()){
				LOGGER.info("Success Response after data inserted into elastic schema {} ", jestResult.getResponseCode());
			}else if(jestResult != null && !jestResult.isSucceeded()){
				LOGGER.info("Data failed to insert into elastic schema with response code {} and response message {} ", jestResult.getResponseCode(), jestResult.getErrorMessage());
				errorList.add(jestResult.getErrorMessage());
			}else{
			}
			LOGGER.info("Error List {} ", errorList);
    	}catch(Exception ex) {
    		LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_070 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_070_Desc);
    		LOGGER.error("Exception in pushKafkaDlqPayloadOnES::::::",ex);
    	}
	}
    public boolean holdCurrentMessageProcessing(JsonNode requestMessageJson, Context context) {
		String receivedMsg = null;
		JestResult jestResult = null;
		List<String> errorList = new ArrayList<>();
		boolean isHoldSuccess = false;
		try {
			final String eventStatusType = Optional.ofNullable(requestMessageJson.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.STATUS)).map(status -> status.textValue()).orElse(null);
			paymentIngestionRedisUtil.pushBackendCreateDataToRedis(context.getChannelSeqId()+eventStatusType,objectMapper.writeValueAsString(requestMessageJson));
			final String requestType = Optional.ofNullable(requestMessageJson.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENT_TYPE)).map(eventType -> eventType.get(PaymentIngestionConstant.REQUEST_TYPE)).map(reqType -> reqType.textValue()).orElse(null);
			JsonNode requestId = Optional.ofNullable(requestMessageJson.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.CHANNEL_SQ_ID)).orElse(null);
			final String contextEventTime = Optional.ofNullable(requestMessageJson.get(PaymentIngestionConstant.CONTEXT)).map(ctxt -> ctxt.get(PaymentIngestionConstant.EVENTTIME)).map(eventTime -> eventTime.textValue()).orElse(null);
			Object json = objectMapper.readValue(requestMessageJson.toString(), Object.class);
			receivedMsg =  objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			final PaymentsDlqNotificationES paymentsDlqNotificationES = new PaymentsDlqNotificationES();
			paymentsDlqNotificationES.setEventTypeES(eventStatusType);
			paymentsDlqNotificationES.setEventES(requestType);
			paymentsDlqNotificationES.setRequestIdES(requestId.textValue());
			paymentsDlqNotificationES.setMessageES(receivedMsg);
			paymentsDlqNotificationES.setExceptionTypeES("NOTIFICATION_HOLD");
			paymentsDlqNotificationES.setCreationTimeES(contextEventTime);
			jestResult = paymentIngestionQueryUtil.indexBuilder(objectMapper.convertValue(paymentsDlqNotificationES, ObjectNode.class),paymentsDlqNotificationES.getRequestIdES(), paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaydlqnotificnType());  
			LOGGER.info("jestResult : {} ", jestResult);
			if(jestResult != null && jestResult.isSucceeded()){
				LOGGER.info("Success Response after data inserted into elastic schema {} ", jestResult.getResponseCode());
			}else if(jestResult != null && !jestResult.isSucceeded()){
				LOGGER.info("Data failed to insert into elastic schema with response code {} and response message {} ", jestResult.getResponseCode(), jestResult.getErrorMessage());
				errorList.add(jestResult.getErrorMessage());
			}else{
			}
			LOGGER.info("Error List {} ", errorList);
			isHoldSuccess = true; 
		}catch(Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_071 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_071_Desc);
			LOGGER.error("Exception came for request id :",context.getDomainSeqId());
			LOGGER.error("Exception while executing holdCurrentMessageProcessing:::",ex);
		}
		return isHoldSuccess;
	}
    public boolean isValidFormat(String format, String value, Locale locale) {
	    LocalDateTime ldt = null;
	    DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, locale);
	    try {
	        ldt = LocalDateTime.parse(value, fomatter);
	        String result = ldt.format(fomatter);
	        return result.equals(value);
	    } catch (DateTimeParseException e) {
	        try {
	        	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_072 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_072_Desc);
	            LocalDate ld = LocalDate.parse(value, fomatter);
	            String result = ld.format(fomatter);
	            return result.equals(value);
	        } catch (DateTimeParseException exp) {
	            try {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_073 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	    					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_073_Desc);
	                LocalTime lt = LocalTime.parse(value, fomatter);
	                String result = lt.format(fomatter);
	                return result.equals(value);
	            } catch (DateTimeParseException e2) {
	            	LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_074 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
	    					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_074_Desc);
	            }
	        }
	    }
	    return false;
	}
	
	
	
  private ResponseEntity<String> sendRequestToLogicalEndAState(String serviceKey, String channelSeqId, String action, String remarks) {
		ResponseEntity<String> apiResponse = null;
		JSONObject context = new JSONObject();
		context.put("action", action);//trash OR reject
		context.put("channelSeqId", channelSeqId);
		context.put("remarks", remarks);
		
		JSONObject requestPayload = new JSONObject();
		requestPayload.put("context", context);
		
		String token = null;
		LOGGER.info("TOKEN :: {}",token );
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		
		try {
			token = joseClientUtil.generateJWT(context.toString());
			LOGGER.info("TOKEN :: {}",token );
			// can set the content Type
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
			
			// Custom Headers
			headers.add(joseClientUtil.getRequestId(), String.valueOf(System.currentTimeMillis()));
			headers.add(joseClientUtil.getSharedKey(), "I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq");
			headers.add(joseClientUtil.getAuthorization(), "Bearer "+token);
			
			
			LOGGER.debug("[sendRequestToLogicalEndAState] Headers: {}", headers);
			
			switch (serviceKey) {
				case "paymnt/sendmoney":
				serviceKey = "action-payments/igtb-payments/v1/SendMoney";
				break;
				case "paymnt/sendwires":
				serviceKey = "cibc-action-wire/wire-payments/v1/SendWire";
				break;
				case "paymnt/fulfillrtp":
				serviceKey = "action-payments/igtb-payments/v1/fulfillrtp";
				break;
				case "paymnt/sendrtp":
				serviceKey = "send-rtp/igtb-payments-rtp/v1/request-to-pay";
				break;
				
				default:
				break;
			}
			
			HttpEntity<String> entity = new HttpEntity<String>(requestPayload.toString(), headers);
			LOGGER.info("[sendRequestToLogicalEndAState] Http Request prepared {}", entity);
			String actionURL =  null;
			if(action.equalsIgnoreCase("trash")) {
			 LOGGER.info("<<<<<<<< INSIDE TRASH BLOCK >>>>>>>");
			 actionURL=joseClientUtil.getTrashUrl().replace("<serviceKey>", serviceKey).replace("<channelSeqId>", channelSeqId);
			} else {
			 LOGGER.info("<<<<<<<< INSIDE REJECT BLOCK >>>>>>>");
			 actionURL=joseClientUtil.getRejectUrl().replace("<serviceKey>", serviceKey).replace("<channelSeqId>", channelSeqId);	
			}
			try {
				apiResponse = restTemplate.exchange(new URI(actionURL),HttpMethod.PUT,entity,String.class);				
			}catch(HttpClientErrorException ex) {
				LOGGER.error("Exception in Trash getResponseBodyAsString>>>{}",ex.getResponseBodyAsString());
				LOGGER.error("Exception in Trash getMessage>>>{}",ex.getMessage());
			}
			LOGGER.info("apiResponse:::"+apiResponse);
			if (apiResponse != null && HttpStatus.OK.equals(apiResponse.getStatusCode())) {
				LOGGER.info("[sendRequestToLogicalEndAState] Response: {}",apiResponse);
				LOGGER.info("Request {} - {} trashed/rejected successfully.", serviceKey, channelSeqId);
			} else if (apiResponse!=null && HttpStatus.NOT_FOUND.equals(apiResponse.getStatusCode())) {
				LOGGER.info("[sendRequestToLogicalEndAState] FAILURE Response: {}",apiResponse.getBody());
			}
			
			else {
				LOGGER.error("Error while trashing/rejecting request {} - {} :: Response={}", serviceKey, channelSeqId,apiResponse);
			}
		}catch(Exception ex) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_075 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_075_Desc);
			LOGGER.error("Excepion raised while trash the Transition record :"+channelSeqId);
			LOGGER.error("Excepion message :",ex);
		}
		return apiResponse;
   }	
  
 public void updateTransitionRecord(Context pContext) throws JsonParseException, JsonMappingException, IOException {
	 LOGGER.info("inside updateTransitionRecord:::");
	 Context context = objectMapper.readValue(pContext.toString(), Context.class);
	 JsonNode existingRecordJson = paymentIngestionQueryUtil.fetchExistingRecordFromES(context.getChannelSeqId());
	 LOGGER.info("Existing Record from ES :::::::{}", existingRecordJson);
	 PaymentsInstructionsES existingRecord = objectMapper.readValue(existingRecordJson.toString(), PaymentsInstructionsES.class);
	 String currentStatus = existingRecord.getCbxUIStatus();
	 LOGGER.info("currentStatus::"+currentStatus);
	 if(currentStatus!=null && currentStatus.equals("Retry in progress")) {
		 existingRecord.setCbxUIStatus(PaymentIngestionConstant.SENT_TO_BANK);
		 existingRecord.setUiDisplayStatusES(PaymentIngestionConstant.SENT_TO_BANK);
		 existingRecord.setCumulativeStatusES("transition"+PaymentIngestionConstant.SENT_TO_BANK+existingRecord.getFaeIndicator()+existingRecord.getBccAction());
	 }
	 JestResult result = paymentIngestionQueryUtil.updateExistingDocument(existingRecord, context.getChannelSeqId(),
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType(), null);
	 if (result != null && result.isSucceeded()) {
			LOGGER.info("Updated result for {} >>>>>>> {}", context.getChannelSeqId(), result.getSourceAsString());
			LOGGER.info("State Update for fulfillrtp active record is successful for id::::::: {}", context.getChannelSeqId());
		} else {
			LOGGER.info("Result of status update for fulfillrtp active record in case of failure :::::: {}",result.getJsonString());
			LOGGER.info("State Updation has not yet succeeded for the incoming request with domainSequenceId:::::: {} ",context.getChannelSeqId());
		}
	 
 }
}
