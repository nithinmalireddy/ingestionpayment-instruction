package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
/**
 * The Class FxRateES.
 */
public class FxRateES {

	/** The fx rate. */
	private Double fxRate;
	
	/** The fx rate type. */
	private String fxRateType;
	
	/** The fx contract id. */
	private String fxContractId;
	
	/** The fx amount. */
	private Double fxAmount;
	
	/** The fx currency. */
	private String fxCurrency;
	
	/** The fx eq amount. */
	private Double fxEqAmount;
	
	/** The fx eq base currency. */
	private String fxEqBaseCurrency;
}
