
package com.igtb.ingestion.payment.instruction.models;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.igtb.ingestion.payment.instruction.es.models.BenefIdentifiers;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class PaymentInstruction.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@ToString
@Setter
public class PaymentInstruction { // NOPMD TooManyFields

	// New Fields added for ParentPayload -Starts

	@JsonProperty("msgId")
	private String msgId = null;

	@JsonProperty("fileNbOfTxs")
	private Integer fileNbOfTxs = null;

	@JsonProperty("fileCtrlSum")
	private Double fileCtrlSum = null;
	
	@JsonProperty("notificationChannel")
	private NotificationChannel notificationChannel;

	@JsonProperty("fileAuthstnPrtry")
	private String fileAuthstnPrtry = null;

	@JsonProperty("pmtInfId")
	private String pmtInfId = null;

	@JsonProperty("batchNbOfTxs")
	private String batchNbOfTxs = null;

	@JsonProperty("batchCtrlSum")
	private Double batchCtrlSum = null;

	@JsonProperty("instructedAgentId")
	private String instructedAgentId = null;

	@JsonProperty("instructingAgentId")
	private String instructingAgentId = null;

	@JsonProperty("serviceLvlCode")
	private String serviceLvlCode = null;

	@JsonProperty("localInstrumentCode")
	private String localInstrumentCode = null;

	@JsonProperty("supplementaryData")
	private SupplementaryData supplementaryData = null;

	@JsonProperty("cancellationDetails")
	private CancellationDetails cancellationDetails = null;

	@JsonProperty("incomingRTP")
	private IncomingRTP incomingRTP = null;

	@JsonProperty("originalGroupInformationAndStatus")
	private OriginalGroupInformationAndStatus originalGroupInformationAndStatus = null;

	@JsonProperty("interacTransactionAmountLimit")
	private Amount interacTransactionAmountLimit = null;

	@JsonProperty("expiryDate")
	private String expiryDate = null;

	@JsonProperty("paymentCondition")
	private PaymentCondition paymentCondition = null;

	@JsonProperty("initiatingParty")
	private InitiatingParty initiatingParty = null;

	@JsonProperty("faeIndicator")
	private String faeIndicator = null;

	/* New BCC Action field added to capture BCC action for FAE */
	@JsonProperty("bccAction")
	private String bccAction = null;

	// New Fields added for ParentPayload - Ends

	/** The payment method. */
	@JsonProperty("paymentMethod")
	private String paymentMethod;

	/** The IntermediaryAgent. */
	@JsonProperty("intermediaryAgent")
	private IntermediaryAgent intermediaryAgent;

	/** The category purpose. */
	@JsonProperty("categoryPurpose")
	private CategoryPurpose categoryPurpose;

	/** The charge account. */
//	@JsonProperty("chargeAccount")
//	private ChargeAccount chargeAccount;
	/** The charge account. */
	@JsonProperty("chargesAccount")
	private ChargesAccount chargesAccount;

	/** The maker user id. */
	@JsonProperty("makerUserId")
	private String makerUserId;

	/** The user date time. */
	@JsonProperty("userDateTime")
	private String userDateTime;

	/** The user date time. */
	@JsonProperty("bankRemark")
	private String bankRemark;

	/** The requested execution date. */
	@JsonProperty("requestedExecutionDate")
	private String requestedExecutionDate;

	/** The business product code. */
	@JsonProperty("businessProductCode")
	private String businessProductCode;

	/** The business paymentReason. */
	@JsonProperty("paymentReason")
	private PaymentReason paymentReason;
	
	/** The reasonCode. */
	@JsonProperty("reasonCode")
	private String reasonCode;

	/** The processing date. */
	@JsonProperty("processingDate")
	private String processingDate;


	@JsonProperty("releaseDate")
	private String releaseDate;

	/** The charge amount. */
	@JsonProperty("chargeAmount")
	private Double chargeAmount;

	/** The end to end identification. */
	@JsonProperty("endToEndIdentification")
	private String endToEndIdentification;

	/** The instruction identification. */
	@JsonProperty("instructionIdentification")
	private String instructionIdentification;

	/** The txn initiation type. */
	@JsonProperty("txnInitiationType")
	private String txnInitiationType;

	/** The debtor. */
	@JsonProperty("debtor")
	private Debtor debtor;

	/** The debtor account. */
	@JsonProperty("debtorAccount")
	private Account debtorAccount;

	/** The debtor agent. */
	@JsonProperty("debtorAgent")
	private Agent debtorAgent;

	/** The charge bearer. */
	@JsonProperty("chargeBearer")
	private ChargeBearer chargeBearer;

	/** The intermediary agent 1. */
	@JsonProperty("intermediaryAgent1")
	private String intermediaryAgent1;

	/** The payment product. */
	@JsonProperty("paymentProduct")
	private PaymentProduct paymentProduct;

	/** The creditor. */
	@JsonProperty("creditor")
	private Creditor creditor;

	/** The creditor agent. */
	@JsonProperty("creditorAgent")
	private Agent creditorAgent;

	/** The creditor agent. */
	@JsonProperty("benefIdentifiers")
	private BenefIdentifiers benefIdentifiers;

	/** The creditor account. */
	@JsonProperty("creditorAccount")
	private Account creditorAccount;

	@JsonProperty("limitUtilizeDateTime")
	private String limitUtilizeDateTime = null;

	@JsonProperty("limitUtilizeFlag")
	private String limitUtilizeFlag = null;

	/** The instructed amount. */
	@JsonProperty("instructedAmount")
	private InstructedAmount instructedAmount;
	
	/** The cadEquivalentAmount amount. */
	@JsonProperty("CADEquivalentAmount")
	private Double cadEquivalentAmount;

	/** The fx details. */
	@JsonProperty("fxDetails")
	private List<FxDetail> fxDetails;

	/** The equivalent amount. */
	@JsonProperty("equivalentAmount")
	private EquivalentAmount equivalentAmount;

	/** The wht details. */
	@JsonProperty("whtDetails")
	private WhtDetails whtDetails;

	/** The remittance information. */
	@JsonProperty("remittanceInformation")
	private RemittanceInformation remittanceInformation;

	/** The debit time. */
	@JsonProperty("debitTime")
	private String debitTime;

	/** The date type indicator. */
	@JsonProperty("dateTypeIndicator")
	private String dateTypeIndicator;

	/** The debit amount flag. */
	@JsonProperty("debitAmountFlag")
	private Boolean debitAmountFlag;

	/** The attachments. */
	@JsonProperty("attachments")
	private List<Attachment> attachments;

	/** The sendadviceflag(Y or N). */
	@JsonProperty("sendAdviceFlag")
	private String sendAdviceFlag;

	/** The remark. */
	@JsonProperty("remark")
	private String remark;

	/** The txn level addinl info. */
	@JsonProperty("txnLevelAddinlInfo")
	private Map<String, String> txnLevelAddinlInfo;

	/** The multi set addnl info. */
	@JsonProperty("multiSetAddnlInfo")
	private List<Map<String, String>> multiSetAddnlInfo;

	@JsonProperty("paymentTypesObj")
	private PaymentTypesObj paymentTypesObj = null;

	@JsonProperty("debitAmountDetails")
	private DebitAmountDetails debitAmountDetails = null;

	@JsonProperty("paymentAmountDetails")
	private PaymentAmountDetails paymentAmountDetails = null;

	@JsonProperty("notificationLanguage")
	private String notificationLanguage = null;

	@JsonProperty("invoiceDate")
	private String invoiceDate;

	@JsonProperty("invoiceNo")
	private String invoiceNo;

	/** The account creditorAccPaymentType */
	@JsonProperty("creditorAccPaymentType")
	public String creditorAccPaymentType;

	/** The account creditorAccAliasRefNo */
	@JsonProperty("creditorAccAliasRefNo")
	public String creditorAccAliasRefNo;




	@JsonProperty("auditId")
	private String auditId;


	@JsonProperty("clearingSystemReference")
	private String clearingSystemReference = null;


	@JsonProperty("typeOfAmount")
	private String typeOfAmount;

	@JsonProperty("isCpFlagOn")
	private Boolean isCPFlagOn;

	@JsonProperty("custId")
	private String custId;
	@JsonProperty("custName")
	private String custName;
	@JsonProperty("puId")
	private String puId;
	@JsonProperty("recommendCost")
	private RecommendationCost recommendationCost = null;

	@JsonProperty("cumulativeStatus")
	private String cumulativeStatus;
	@JsonProperty("txnWorkItemId")
	private String txnWorkItemId;
	
	@JsonProperty("groupChannelSeqId")
	private String groupChannelSeqId;
	
	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 * unique id needed by send money as CC2 requires it. Assignment ID field in
	 * case of cancel money.
	 * 
	 * @return msgId
	 **/

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public Integer getFileNbOfTxs() {
		return fileNbOfTxs;
	}

	public void setFileNbOfTxs(Integer fileNbOfTxs) {
		this.fileNbOfTxs = fileNbOfTxs;
	}

	public Double getFileCtrlSum() {
		return fileCtrlSum;
	}

	public void setFileCtrlSum(Double fileCtrlSum) {
		this.fileCtrlSum = fileCtrlSum;
	}

	public String getFileAuthstnPrtry() {
		return fileAuthstnPrtry;
	}

	public void setFileAuthstnPrtry(String fileAuthstnPrtry) {
		this.fileAuthstnPrtry = fileAuthstnPrtry;
	}

	public String getPmtInfId() {
		return pmtInfId;
	}

	public void setPmtInfId(String pmtInfId) {
		this.pmtInfId = pmtInfId;
	}

	public String getBatchNbOfTxs() {
		return batchNbOfTxs;
	}

	public void setBatchNbOfTxs(String batchNbOfTxs) {
		this.batchNbOfTxs = batchNbOfTxs;
	}

	public Double getBatchCtrlSum() {
		return batchCtrlSum;
	}

	public void setBatchCtrlSum(Double batchCtrlSum) {
		this.batchCtrlSum = batchCtrlSum;
	}

	public String getInstructedAgentId() {
		return instructedAgentId;
	}

	public void setInstructedAgentId(String instructedAgentId) {
		this.instructedAgentId = instructedAgentId;
	}

	public String getInstructingAgentId() {
		return instructingAgentId;
	}

	public void setInstructingAgentId(String instructingAgentId) {
		this.instructingAgentId = instructingAgentId;
	}

	public SupplementaryData getSupplementaryData() {
		return supplementaryData;
	}

	public void setSupplementaryData(SupplementaryData supplementaryData) {
		this.supplementaryData = supplementaryData;
	}

	public CancellationDetails getCancellationDetails() {
		return cancellationDetails;
	}

	public void setCancellationDetails(CancellationDetails cancellationDetails) {
		this.cancellationDetails = cancellationDetails;
	}

	public IncomingRTP getIncomingRTP() {
		return incomingRTP;
	}

	public void setIncomingRTP(IncomingRTP incomingRTP) {
		this.incomingRTP = incomingRTP;
	}

	public OriginalGroupInformationAndStatus getOriginalGroupInformationAndStatus() {
		return originalGroupInformationAndStatus;
	}

	public void setOriginalGroupInformationAndStatus(
			OriginalGroupInformationAndStatus originalGroupInformationAndStatus) {
		this.originalGroupInformationAndStatus = originalGroupInformationAndStatus;
	}

	public Amount getInteracTransactionAmountLimit() {
		return interacTransactionAmountLimit;
	}

	public void setInteracTransactionAmountLimit(Amount interacTransactionAmountLimit) {
		this.interacTransactionAmountLimit = interacTransactionAmountLimit;
	}

	/**
	 * Expiry date for RTP
	 * 
	 * @return expiryDate
	 **/
	@ApiModelProperty(value = "Expiry date for RTP")
	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * Get paymentCondition
	 * 
	 * @return paymentCondition
	 **/
	@ApiModelProperty(value = "")
	public PaymentCondition getPaymentCondition() {
		return paymentCondition;
	}

	public void setPaymentCondition(PaymentCondition paymentCondition) {
		this.paymentCondition = paymentCondition;
	}

	public InitiatingParty getInitiatingParty() {
		return initiatingParty;
	}

	public void setInitiatingParty(InitiatingParty initiatingParty) {
		this.initiatingParty = initiatingParty;
	}

	public String getFaeIndicator() {
		return faeIndicator;
	}

	public void setFaeIndicator(String faeIndicator) {
		this.faeIndicator = faeIndicator;
	}
}
