package com.igtb.ingestion.payment.instruction.util;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.igtb.api.ingestion.commons.bulk.decisionmatrix.BulkTransitionData;
import com.igtb.api.ingestion.commons.bulk.models.BulkStateInfo;
import com.igtb.api.ingestion.commons.bulk.stateInfo.FileStatusUtil;
import com.igtb.api.ingestion.commons.stateinfo.models.AdditionalInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.RejectionInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.util.CommonsIngestionConstants;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentRabbitProperties;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsInstructionsES;
import com.igtb.ingestion.payment.instruction.es.models.SetElementInfo;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.BackendPayload;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.Entity;
import com.igtb.ingestion.payment.instruction.models.TxnData;
import com.igtb.ingestion.payment.instruction.models.bulk.Bulk;
import com.igtb.ingestion.payment.instruction.models.bulk.BulkActionApiPayload;
import com.igtb.ingestion.payment.instruction.models.bulk.BulkEventPayload;
import com.igtb.ingestion.payment.instruction.models.bulk.TxnSlip;

import io.searchbox.action.BulkableAction;
import io.searchbox.client.JestResult;
import lombok.AllArgsConstructor;

/**
 * The Class BulkProcessingChannelUtil.
 */
@Component
@AllArgsConstructor
@SuppressWarnings("rawtypes")
public class BulkProcessingChannelUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkProcessingChannelUtil.class);

	/** The payment elastic properties. */
	private final PaymentElasticProperties paymentElasticProperties;

	/** The payment ingestion mapper. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** The payment rabbit properties. */
	private final PaymentRabbitProperties paymentRabbitProperties;

	/** The bulk processing helper. */
	private final BulkProcessingHelper bulkProcessingHelper;

	/** The object mapper. */
	private final ObjectMapper objectMapper;

	/**
	 * Gets the existing P iset.
	 *
	 * @param matchString
	 *            the match string
	 * @return the existing P iset
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	@SuppressWarnings("unlikely-arg-type")
	public PaymentInstructionSetsES getExistingPIset(final Map<String, String> matchString)
			throws MessageProcessingException {

		LOGGER.debug("searching PISet with matchString: {}", matchString);
		try {

			final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(
					matchString, null, paymentElasticProperties.getPymtIndex(),
					paymentElasticProperties.getPaymentInstructionSets());

			PaymentInstructionSetsES paymentInstructionSetsES = null;
			if (StringUtils.isNotBlank(jestResult.getSourceAsString())) {
				paymentInstructionSetsES = objectMapper.readValue(jestResult.getSourceAsString(),
						new TypeReference<PaymentInstructionSetsES>() {
						});
			}
			return paymentInstructionSetsES;
		} catch (IOException e) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_003 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_003_Desc);
			LOGGER.error("No Existing Record found for the requested fileId/channelseqid: {}, exception is: {}",
					matchString.get(0), e);
			throw new MessageProcessingException("Exception while fetching records");
		}

	}

	/**
	 * Requeue message.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 * @param requeueMsg
	 *            the requeue msg
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws DLQException
	 *             the DLQ exception
	 */
	public void requeueMessage(final Context context, final BulkTransitionData stateTransitionData,
			final String requeueMsg) throws RequeueException, InterruptedException, DLQException {
		try {

			final String contextEventTime = Optional.ofNullable(context.getEventTime()).orElse(null);
			if (contextEventTime == null) {
				LOGGER.error(
						"EventTime is missing in the request for the channelSeqId ---> {}, hence pushing the event to DLQ",
						context.getChannelSeqId());
				throw new DLQException("Inbound request eventTime is missing");
			}
			// formatting the eventtime to localdatetime type
			final LocalDateTime formattedEventTime = getFormattedDate(context.getEventTime());
			final LocalDateTime currentDateTime = LocalDateTime.now();
			final Duration d1 = Duration.between(formattedEventTime, currentDateTime);
			final Duration d2 = Duration.ofMinutes(paymentRabbitProperties.getRmqRequeueExpTime());
			if (d1.compareTo(d2) > 0) {
				LOGGER.error("Requeue Time expired for the channelSeqId ---> {}, hence pushing the event to DLQ",
						context.getChannelSeqId());
				throw new DLQException("Requeue Time expired");

			} else {
				Thread.sleep(paymentRabbitProperties.getRetrySleepTime());
				LOGGER.info(
						"Requeue reason ---->>>>> From State: {} From OutCome: {} >>>>>>>>><<<<<<<<<< To State: {} To Outcome: {} ",
						stateTransitionData.getFromState(), stateTransitionData.getFromOutcomeCategory(),
						stateTransitionData.getToState(), stateTransitionData.getToOutcomeCategory());
				throw new RequeueException("---->>>> " + requeueMsg);
			}
		} catch (DateTimeParseException dtpe) {
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_002 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_002_Desc);
			dtpe.printStackTrace();
		}
	}

	/**
	 * Gets the formatted date.
	 *
	 * @param eventTime
	 *            the event time
	 * @return the formatted date
	 */
	private LocalDateTime getFormattedDate(final String eventTime) {
		if (eventTime == null) {
			return null;
		}
		try {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			return LocalDateTime.parse(eventTime, formatter);
		} catch (DateTimeParseException e) {
			
			LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_002 + " "
					+ PaymentIngestionErrorCodeConstants.ERR_PAYING_002_Desc);
			LOGGER.error("----->>>>>> Invalid EventTime format <<<<<-----");
			return null;
		}

	}

	/**
	 * File update bulk refrsh instr insert.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @param bulkEventPayload
	 *            the bulk event payload
	 * @param existingPiSet
	 *            the existing pi set
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 *             the message processing exception
	 */
	public void fileUpdateBulkRefrshInstrInsert(final JsonNode message, final Context context,
			final BulkEventPayload bulkEventPayload, final PaymentInstructionSetsES existingPiSet)
			throws IOException, MessageProcessingException {

		LOGGER.debug("fileUpdateBulkRefrshInstrInsert handling started for eventId: {}", message.get("id"));

		// bulk delete list
		final List<BulkableAction> bulkDeleteList = new ArrayList<>();

		// bulk update list
		final List<BulkableAction> bulkUpdateList = new ArrayList<>();

		// bulk insert list
		final List<BulkableAction> bulkInsertList = new ArrayList<>();

		// Instr[PI] update list
		final List<BulkableAction> instrUpdateList = new ArrayList<>();

		// isPartialProcessing at file level
		Boolean isPartialProcessing = false;

		final List<BulkStateInfo> enrichedBulkStateInfo = bulkProcessingHelper.buildBulkStateInfo(message, context,
				existingPiSet.getCorporateEntity().getId(), null);

		for (int i = 0; i < bulkEventPayload.getBulks().size(); i++) {
			final Bulk bulkRequest = bulkEventPayload.getBulks().get(i);
			isPartialProcessing = isPartialProcessing || bulkRequest.getPartialProcessing();
			switch (bulkRequest.getBulkAction().toUpperCase(Locale.ENGLISH)) {
			case PaymentIngestionConstant.BULK_ACTION_DELETE:
				handleBulkDeleteAction(bulkRequest, bulkDeleteList);
				break;
			case PaymentIngestionConstant.BULK_ACTION_UNCHANGED:
				handleBulkUnchangedAction(bulkRequest, enrichedBulkStateInfo.get(i), bulkUpdateList);
				break;
			case PaymentIngestionConstant.BULK_ACTION_ADD:
				handleBulkAddAction(context, bulkRequest, enrichedBulkStateInfo.get(i), existingPiSet, bulkInsertList,
						instrUpdateList);
				break;
			case PaymentIngestionConstant.BULK_ACTION_UPDATE:
				handleBulkUpdateAction(context, bulkRequest, enrichedBulkStateInfo.get(i), existingPiSet,
						bulkUpdateList, instrUpdateList);
				break;
			default:
				LOGGER.warn("No matching action found for bulk");
				break;
			}
		}

		if (!bulkDeleteList.isEmpty()) {
			final JestResult jestResult = paymentIngestionQueryUtil.bulkActionExecutor(bulkDeleteList,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
			LOGGER.debug("DELETE bulkAction completed for eventId: {}, isSucceeded: {}", message.get("id"),
					jestResult.isSucceeded());
		}

		if (!bulkUpdateList.isEmpty()) {
			final JestResult jestResult = paymentIngestionQueryUtil.bulkActionExecutor(bulkUpdateList,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
			LOGGER.debug("UNCHANGED bulkAction completed for eventId: {}, isSucceeded: {}", message.get("id"),
					jestResult.isSucceeded());
		}

		if (!bulkInsertList.isEmpty()) {
			final JestResult jestResult = paymentIngestionQueryUtil.bulkActionExecutor(bulkInsertList,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
			LOGGER.debug("ADD bulkAction completed for eventId: {}, isSucceeded: {}", message.get("id"),
					jestResult.isSucceeded());
		}

		if (!instrUpdateList.isEmpty()) {
			final JestResult jestResult = paymentIngestionQueryUtil.bulkActionExecutor(instrUpdateList,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			LOGGER.debug("ADD bulkAction at instruction level completed for eventId: {}, isSucceeded: {}",
					message.get("id"), jestResult.isSucceeded());
		}

		// updating PISet File
		handlePiSetFileUpdate(message, context, existingPiSet, isPartialProcessing);

		LOGGER.info("fileUpdateBulkRefrshInstrInsert handling done for eventId: {}", message.get("id"));
	}

	/**
	 * Handle bulk update action.
	 *
	 * @param context
	 *            the context
	 * @param bulkRequest
	 *            the bulk request
	 * @param bulkStateInfo
	 *            the bulk state info
	 * @param existingPiSet
	 *            the existing pi set
	 * @param bulkUpdateList
	 *            the bulk update list
	 * @param instrUpdateList
	 *            the instr update list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws MessageProcessingException
	 */
	private void handleBulkUpdateAction(final Context context, final Bulk bulkRequest,
			final BulkStateInfo bulkStateInfo, final PaymentInstructionSetsES existingPiSet,
			final List<BulkableAction> bulkUpdateList, final List<BulkableAction> instrUpdateList)
			throws IOException, MessageProcessingException {

		// building PISet bulk
		final ObjectNode bulkUpdateObj = bulkProcessingHelper.buildBulkUpdateObj(bulkRequest, bulkStateInfo,
				existingPiSet.getMetrics(), existingPiSet.getSetElementInfo());

		bulkUpdateList.add(paymentIngestionQueryUtil.getIndexBuilder(existingPiSet.getRecordSetId(), bulkUpdateObj,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets()));

		// get list of instr linked with bulkId

		final Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("setElementInfo.id", bulkRequest.getBulkId());
		final String[] fetchStrings = { PaymentIngestionConstant.SETELEMENT_INFO, PaymentIngestionConstant.STATE_INFO,
				"transactionId" };
		final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(matchStrings,
				fetchStrings, paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
		if (!jestResult.isSucceeded()) {
			throw new MessageProcessingException(
					"Unable to fetch instructions linked with bulkId: " + bulkRequest.getBulkId());
		}

		final List<PaymentsInstructionsES> paymentsInstructionsESs = jestResult
				.getSourceAsObjectList(PaymentsInstructionsES.class);

		final List<String> rejectedTxns = Optional.ofNullable(bulkRequest)
				.map(obj -> obj.getTxnSlip().stream()
						.filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase(PaymentIngestionConstant.REJECTED))
						.map(txn -> txn.getInternalTxnId()).collect(Collectors.toList()))
				.orElse(new ArrayList<>());
		final List<String> skippedTxns = Optional.ofNullable(bulkRequest)
				.map(obj -> obj.getTxnSlip().stream().filter(txnSlip -> txnSlip.getAction().equalsIgnoreCase("skipped"))
						.map(txn -> txn.getInternalTxnId()).collect(Collectors.toList()))
				.orElse(new ArrayList<>());
		final Integer validCount = existingPiSet.getMetrics().getTotal().getCount() - rejectedTxns.size()
				- skippedTxns.size();
		final SetElementInfo setElementInfoControl = new SetElementInfo(existingPiSet.getRecordSetId(),
				existingPiSet.getSetName(), existingPiSet.getType(), validCount, true);
		String instrRecordId = null;
		for (PaymentsInstructionsES existingPI : paymentsInstructionsESs) {
			if (existingPI.getSetElementInfo().getControlElement()) {
				if (!(rejectedTxns.contains(existingPI.getTransactionId())
						|| skippedTxns.contains(existingPI.getTransactionId()))) {
					instrRecordId = existingPI.getTransactionId();
					break;
				}
			} else if (!(rejectedTxns.contains(existingPI.getTransactionId())
					&& skippedTxns.contains(existingPI.getTransactionId()))) {
				instrRecordId = existingPI.getTransactionId();
			}
			if (StringUtils.isNotBlank(instrRecordId)) {
				final ObjectNode instrUpdateObj = objectMapper.createObjectNode();
				instrUpdateObj.set(PaymentIngestionConstant.SETELEMENT_INFO,
						objectMapper.readValue(objectMapper.writeValueAsString(setElementInfoControl), JsonNode.class));
				instrUpdateList.add(paymentIngestionQueryUtil
						.getUpdateBuilder(bulkRequest.getFileId() + "." + instrRecordId, instrUpdateObj));
				LOGGER.debug("setElementInfo modified with controlElement for transactionId: {}", instrRecordId);
			}
		}

		// modify PI [Instr]
		final SetElementInfo setElementInfo = new SetElementInfo(existingPiSet.getRecordSetId(),
				existingPiSet.getSetName(), existingPiSet.getType(), 0, false);

		for (final TxnSlip txn : bulkRequest.getTxnSlip()) {
			if (txn.getAction().equalsIgnoreCase(PaymentIngestionConstant.REJECTED)) {
				final ObjectNode transInfoObj = objectMapper.createObjectNode();
				transInfoObj.put(PaymentIngestionConstant.STATE, PaymentIngestionConstant.REJECTED);
				transInfoObj.put("docStatus", "uncorrelated");
				transInfoObj.put(PaymentIngestionConstant.STATE, PaymentIngestionConstant.REJECTED);
				transInfoObj.put(PaymentIngestionConstant.LAST_ACTIVITYTS, context.getEventTime());

				final ObjectNode rejectInfoObj = objectMapper.createObjectNode();
				rejectInfoObj.put("source", "Rejected by user");
				rejectInfoObj.put("message", txn.getRejectRemark());
				transInfoObj.set("rejectionInfo", rejectInfoObj);

				final ObjectNode stateInfoObj = objectMapper.createObjectNode();
				stateInfoObj.set(PaymentIngestionConstant.TRANSITION_INFO, transInfoObj);

				final ObjectNode instrUpdateObj = objectMapper.createObjectNode();
				instrUpdateObj.set(PaymentIngestionConstant.SETELEMENT_INFO,
						objectMapper.readValue(objectMapper.writeValueAsString(setElementInfo), JsonNode.class));
				instrUpdateObj.set(PaymentIngestionConstant.STATE_INFO, stateInfoObj);
				final String recordId = bulkRequest.getFileId() + "." + txn.getInternalTxnId();
				instrUpdateList.add(paymentIngestionQueryUtil.getUpdateBuilder(recordId, instrUpdateObj));
				LOGGER.debug("PI recordSetId: {} added into instrUpdateList.", recordId);
			}
		}

	}

	/**
	 * Handle bulk add action.
	 *
	 * @param bulkRequest
	 *            the bulk request
	 * @param bulkStateInfo
	 *            the bulk state info
	 * @param existingPiSet
	 *            the existing pi set
	 * @param bulkInsertList
	 *            the bulk insert list
	 * @param instrUpdateList
	 *            the instr update list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void handleBulkAddAction(final Context context, final Bulk bulkRequest, final BulkStateInfo bulkStateInfo,
			final PaymentInstructionSetsES existingPiSet, final List<BulkableAction> bulkInsertList,
			final List<BulkableAction> instrUpdateList) throws IOException {

		// building PISet bulk
		final PaymentInstructionSetsES piSetBulk = bulkProcessingHelper.buildPISetBulk(bulkRequest, bulkStateInfo,
				existingPiSet);
		bulkInsertList.add(paymentIngestionQueryUtil.getIndexBuilder(piSetBulk.getRecordSetId(), piSetBulk,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets()));

		// modify PI [Instr]
		final SetElementInfo setElementInfo = new SetElementInfo(piSetBulk.getRecordSetId(), piSetBulk.getSetName(),
				piSetBulk.getType(), bulkRequest.getTxnData().size(), false);
		Boolean isControlSet = false;
		for (final TxnData txn : bulkRequest.getTxnData()) {
			if (!isControlSet) {
				setElementInfo.setControlElement(true);
				isControlSet = true;
			}

			final ObjectNode transInfoObj = objectMapper.createObjectNode();
			if (bulkRequest.getPartialProcessing()) {
				transInfoObj.put(PaymentIngestionConstant.APPROVAL_SCOPE, "bulk");
			} else {
				transInfoObj.put(PaymentIngestionConstant.APPROVAL_SCOPE, "file");
			}
			transInfoObj.put(PaymentIngestionConstant.LAST_ACTIVITYTS, context.getEventTime());
			final ObjectNode stateInfoObj = objectMapper.createObjectNode();
			stateInfoObj.set(PaymentIngestionConstant.TRANSITION_INFO, transInfoObj);

			final ObjectNode instrUpdateObj = objectMapper.createObjectNode();
			instrUpdateObj.set(PaymentIngestionConstant.SETELEMENT_INFO,
					objectMapper.readValue(objectMapper.writeValueAsString(setElementInfo), JsonNode.class));
			instrUpdateObj.set(PaymentIngestionConstant.STATE_INFO, stateInfoObj);
			final String recordId = bulkRequest.getFileId() + "." + txn.getInternalTxnId();
			instrUpdateList.add(paymentIngestionQueryUtil.getUpdateBuilder(recordId, instrUpdateObj));
			LOGGER.debug("PI recordSetId: {} added into instrUpdateList.", recordId);
		}
	}

	/**
	 * Handle bulk unchanged action.
	 *
	 * @param bulkRequest
	 *            the bulk request
	 * @param bulkStateInfo
	 *            the bulk state info
	 * @param bulkUpdateList
	 *            the bulk update list
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void handleBulkUnchangedAction(final Bulk bulkRequest, final BulkStateInfo bulkStateInfo,
			final List<BulkableAction> bulkUpdateList) throws IOException {
		final StateInfo stateInfo = bulkStateInfo.getStateInfo();
		final ObjectNode updateBulkObj = objectMapper.createObjectNode();
		updateBulkObj.set(PaymentIngestionConstant.STATE_INFO,
				objectMapper.readValue(objectMapper.writeValueAsString(stateInfo), JsonNode.class));
		updateBulkObj.put(PaymentIngestionConstant.HAS_SETS, bulkStateInfo.isHasSets());
		updateBulkObj.put(PaymentIngestionConstant.PARTIAL_APPROVAL, bulkStateInfo.isPartialApproval());
		bulkUpdateList.add(paymentIngestionQueryUtil.getUpdateBuilder(bulkRequest.getBulkId(), updateBulkObj));
		LOGGER.debug("bulkId: {} added into updateBulkList with updated stateInfo", bulkRequest.getBulkId());
	}

	/**
	 * Handle bulk delete action.
	 *
	 * @param bulkRequest
	 *            the bulk request
	 * @param bulkDeleteList
	 *            the bulk delete list
	 */
	private void handleBulkDeleteAction(final Bulk bulkRequest, final List<BulkableAction> bulkDeleteList) {
		bulkDeleteList.add(paymentIngestionQueryUtil.getDeleteBuilder(bulkRequest.getBulkId(),
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets()));
		LOGGER.debug("bulkId: {} added into deleteBulkList", bulkRequest.getBulkId());
	}

	/**
	 * Handle pi set file update.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @param existingPiSet
	 *            the existing pi set
	 * @param isPartialProcessing
	 *            the bulk request
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void handlePiSetFileUpdate(final JsonNode message, final Context context,
			final PaymentInstructionSetsES existingPiSet, final Boolean isPartialProcessing)
			throws MessageProcessingException, IOException {

		LOGGER.debug("handling file additionalInfo update for eventId: {}", message.get("id"));
		final String fileId;
		if (existingPiSet.getType().equals("bulk")) {
			fileId = existingPiSet.getParentSet().getId();
		} else {
			fileId = existingPiSet.getRecordSetId();
		}
		try {
			synchronized (fileId) {
				LOGGER.debug("synchronized lock acquire on fileId: {} for eventId: {}", fileId, message.get("id"));

				// fetch existing PISet File
				final Map<String, String> srarchKeyValue = new HashMap<>();
				srarchKeyValue.put("_id", fileId);
				final PaymentInstructionSetsES existingPiSetFile = getExistingPIset(srarchKeyValue);

				final LocalDateTime formattedEventTime = getFormattedDate(context.getEventTime());
				final LocalDateTime lastActivityTs = getFormattedDate(
						existingPiSetFile.getStateInfo().getTransitionInfo().getLastActivityTs());
				if (formattedEventTime != null && lastActivityTs != null
						&& formattedEventTime.compareTo(lastActivityTs) > 0) {

					// get all additionalInfo of bulk related to file
					final List<AdditionalInfo> bulkAddlInfoList = getAdditionalInfoList(
							existingPiSetFile.getRecordSetId());

					// get file level additinalInfo from ingestion-common
					final AdditionalInfo fileAddlInfo = FileStatusUtil
							.deriveFileStatusBasedOnBulkStatuses(bulkAddlInfoList);

					// build object for updating PISetFile
					ObjectNode updatePiSetFileObj = null;
					final ObjectNode transInfoObj = objectMapper.createObjectNode();
					transInfoObj.put(PaymentIngestionConstant.APPROVAL_SCOPE, "bulk");

					if (null != fileAddlInfo) {
						updatePiSetFileObj = objectMapper.createObjectNode();
						transInfoObj.put(PaymentIngestionConstant.TRANSITION_INFO,
								objectMapper.writeValueAsString(fileAddlInfo));
					} else {
						LOGGER.warn(
								"Not updating additionalInfo for PISetFile as no additional found for provided list: {}",
								bulkAddlInfoList);
					}

					if (context.getEventType().getStatus().equals(CommonsIngestionConstants.FILE_BULKS_CREATED)) {
						if (null == updatePiSetFileObj) {
							updatePiSetFileObj = objectMapper.createObjectNode();
						}
						updatePiSetFileObj.put(PaymentIngestionConstant.HAS_SETS, true);
						updatePiSetFileObj.put(PaymentIngestionConstant.PARTIAL_APPROVAL, isPartialProcessing);
						transInfoObj.put(PaymentIngestionConstant.STATE, CommonsIngestionConstants.FILE_BULKS_CREATED);
					}

					if (null != updatePiSetFileObj) {
						transInfoObj.put(PaymentIngestionConstant.LAST_ACTIVITYTS, context.getEventTime());
						final ObjectNode stateInfoObj = objectMapper.createObjectNode();
						stateInfoObj.set(PaymentIngestionConstant.TRANSITION_INFO, transInfoObj);
						updatePiSetFileObj.set(PaymentIngestionConstant.STATE_INFO, stateInfoObj);
						final JestResult jestResult = paymentIngestionQueryUtil.updateExistingDocument(
								updatePiSetFileObj, fileId, paymentElasticProperties.getPymtIndex(),
								paymentElasticProperties.getPaymentInstructionSets(), null);
						LOGGER.debug("PISet File isUpdated: {} for eventId: {}", jestResult.isSucceeded(),
								message.get("id"));
					}
				} else {
					LOGGER.info(
							"Not updating additionlInfo for PISetfile as eventTime: {} and lastActivityTs: {} is greater than eventTime for eventId: {}",
							formattedEventTime, lastActivityTs, message.get("id"));
				}
			}
		} catch (NullPointerException exp) {
			LOGGER.warn("NullPointerException while synchronized based on fileId");
			throw new MessageProcessingException("NullPointerException while synchronized based on fileId");
		}
	}

	/**
	 * Gets the additional info list.
	 *
	 * @param recordSetId
	 *            the record set id
	 * @return the additional info list
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	private List<AdditionalInfo> getAdditionalInfoList(final String recordSetId) {
		final Map<String, String> srarchKeyValue = new HashMap<>();
		srarchKeyValue.put("parentSet.id", recordSetId);

		final String[] fetchStrings = { "stateInfo.transitionInfo.additionalInfo" };

		final JestResult jestResult = paymentIngestionQueryUtil.fetchSelectiveDataByPassingMatchingString(
				srarchKeyValue, fetchStrings, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentInstructionSets());

		List<AdditionalInfo> transAdditionalInfos = new ArrayList<>();
		if (jestResult.isSucceeded()) {
			final List<PaymentInstructionSetsES> response = jestResult
					.getSourceAsObjectList(PaymentInstructionSetsES.class);
			transAdditionalInfos = response.stream().map(res -> res.getStateInfo())
					.map(stateInfo -> stateInfo.getTransitionInfo()).map(transInfo -> transInfo.getAdditionalInfo())
					.collect(Collectors.toList());
		} else {
			LOGGER.error("Error while fetching transitionInfo.additionalInfo as : {}", jestResult.getErrorMessage());
		}

		return transAdditionalInfos;
	}

	@SuppressWarnings("unchecked")
	public void fileBulkUpdate(final JsonNode message, final Context context,
			final BulkActionApiPayload bulkActionApiPayload, final PaymentWF paymentWF,
			final BackendPayload backendPayload, final PaymentInstructionSetsES existingPiSetES)
			throws IOException, MessageProcessingException {

		// build stateInfo
		final String entity = Optional.ofNullable(paymentWF).map(PaymentWF::getEntity).map(Entity::getId)
				.orElse(Optional.ofNullable(existingPiSetES).map(piSet -> piSet.getCorporateEntity())
						.map(corpEntity -> corpEntity.getId()).orElse(""));

		final BulkStateInfo existingBulkStateInfo = new BulkStateInfo();
		existingBulkStateInfo.setHasSets(existingPiSetES.getHasSets());
		existingBulkStateInfo.setPartialApproval(existingPiSetES.getPartialApproval());
		existingBulkStateInfo.setStateInfo(existingPiSetES.getStateInfo());

		final List<BulkStateInfo> enrichedBulkStateInfos = bulkProcessingHelper.buildBulkStateInfo(message, context,
				entity, new ArrayList(Arrays.asList(existingBulkStateInfo)));
		final BulkStateInfo enrichedBulkStateInfo = Optional.ofNullable(enrichedBulkStateInfos)
				.map(stInfoList -> stInfoList.get(0)).orElse(new BulkStateInfo());

		final ObjectNode updatePiSetBulkObj = objectMapper.createObjectNode();
		updatePiSetBulkObj.set(PaymentIngestionConstant.STATE_INFO, objectMapper
				.readValue(objectMapper.writeValueAsString(enrichedBulkStateInfo.getStateInfo()), JsonNode.class));
		updatePiSetBulkObj.put(PaymentIngestionConstant.HAS_SETS, enrichedBulkStateInfo.isHasSets());
		updatePiSetBulkObj.put(PaymentIngestionConstant.PARTIAL_APPROVAL, enrichedBulkStateInfo.isPartialApproval());

		final JestResult jestResult = paymentIngestionQueryUtil.updateExistingDocument(updatePiSetBulkObj,
				existingPiSetES.getRecordSetId(), paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentInstructionSets(), null);
		LOGGER.debug("PISet Bulk isUpdated: {} for eventId: {}", jestResult.isSucceeded(), message.get("id"));

		// updating PISet File
		handlePiSetFileUpdate(message, context, existingPiSetES, existingPiSetES.getPartialApproval());

		LOGGER.info("fileBulkUpdate handling done for event id: {}", message.get("id"));
	}

	public void fileBulkInstrUpdate(final JsonNode message, final Context context,
			final BulkEventPayload bulkEventPayload, final PaymentWF paymentWF, final BackendPayload backendPayload,
			final PaymentInstructionSetsES existingPiSetES) throws IOException, MessageProcessingException {

		if (context.getEventType().getOutcomeCategory().equals(PaymentIngestionConstant.CHANNEL_STATE_UPDATED)
				|| context.getEventType().getOutcomeCategory()
						.equals(PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE)) {

			if (context.getEventType().getStatus().equalsIgnoreCase(CommonsIngestionConstants.BULK_DEF_NOCHANGE)) {
				final Bulk bulkRequest = bulkEventPayload.getBulks().get(0);
				bulkRequest.setBulkAction(PaymentIngestionConstant.BULK_ACTION_UPDATE);
				bulkEventPayload.getBulks().remove(0);
				bulkEventPayload.getBulks().add(bulkRequest);
				fileUpdateBulkRefrshInstrInsert(message, context, bulkEventPayload, existingPiSetES);
				LOGGER.debug("BULK_DEF_NOCHANGE handled for eventId: {}", message.get("id"));
			} else if (context.getEventType().getStatus().equalsIgnoreCase(CommonsIngestionConstants.TRASHED)) {
				handleTrashEvent(message, context, existingPiSetES);
			}
		} else if (context.getEventType().getOutcomeCategory().equals(PaymentIngestionConstant.BACKEND_STATE_UPDATED)) {
			// Bankend state event handling
			LOGGER.debug("Handling backend status: {} event for eventId: {}", context.getEventType().getStatus(),
					message.get("id"));

			// PI update list
			final List<BulkableAction> pIUpdateList = new ArrayList<>();
			for (final TxnData txnData : backendPayload.getTxnData()) {
				final String status = txnData.getStatus().replace("processed", "posted");
				pIUpdateList.add(paymentIngestionQueryUtil.getUpdateBuilder(
						backendPayload.getFileId() + "." + txnData.getInternalTxnId(),
						buildStateInfoPI(status, txnData.getRejectReason(), context)));
			}

			// build stateInfo
			final String entity = Optional.ofNullable(paymentWF).map(PaymentWF::getEntity).map(Entity::getId)
					.orElse(Optional.ofNullable(existingPiSetES).map(piSet -> piSet.getCorporateEntity())
							.map(corpEntity -> corpEntity.getId()).orElse(""));

			final BulkStateInfo existingBulkStateInfo = new BulkStateInfo();
			existingBulkStateInfo.setHasSets(existingPiSetES.getHasSets());
			existingBulkStateInfo.setPartialApproval(existingPiSetES.getPartialApproval());
			existingBulkStateInfo.setStateInfo(existingPiSetES.getStateInfo());

			@SuppressWarnings("unchecked")
			final List<BulkStateInfo> enrichedBulkStateInfos = bulkProcessingHelper.buildBulkStateInfo(message, context,
					entity, new ArrayList(Arrays.asList(existingBulkStateInfo)));
			final BulkStateInfo enrichedBulkStateInfo = Optional.ofNullable(enrichedBulkStateInfos)
					.map(stInfoList -> stInfoList.get(0)).orElse(new BulkStateInfo());

			final ObjectNode updatePiSetBulkObj = objectMapper.createObjectNode();
			updatePiSetBulkObj.set(PaymentIngestionConstant.STATE_INFO, objectMapper
					.readValue(objectMapper.writeValueAsString(enrichedBulkStateInfo.getStateInfo()), JsonNode.class));
			updatePiSetBulkObj.put(PaymentIngestionConstant.HAS_SETS, enrichedBulkStateInfo.isHasSets());
			updatePiSetBulkObj.put(PaymentIngestionConstant.PARTIAL_APPROVAL,
					enrichedBulkStateInfo.isPartialApproval());

			final JestResult jestResult = paymentIngestionQueryUtil.updateExistingDocument(updatePiSetBulkObj,
					existingPiSetES.getRecordSetId(), paymentElasticProperties.getPymtIndex(),
					paymentElasticProperties.getPaymentInstructionSets(), null);
			LOGGER.debug("PISet Bulk isUpdated: {} for eventId: {}", jestResult.isSucceeded(), message.get("id"));

			if (jestResult.isSucceeded() && !pIUpdateList.isEmpty()) {
				final JestResult result = paymentIngestionQueryUtil.bulkActionExecutor(pIUpdateList,
						paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
				LOGGER.debug("Update PI stateInfo is completed for eventId: {}, isSucceeded: {}", message.get("id"),
						result.isSucceeded());
			}

			// updating PISet File
			handlePiSetFileUpdate(message, context, existingPiSetES, existingPiSetES.getPartialApproval());

			LOGGER.debug("Completed handling backend status: {} event for eventId: {}",
					context.getEventType().getStatus(), message.get("id"));
		}

	}

	private ObjectNode buildStateInfoPI(final String status, final String rejectReason, final Context context)
			throws IOException {
		// build state info for PI
		final StateInfo stateInfo = new StateInfo();

		final TransitionInfo transitionInfo = new TransitionInfo();
		if (status.equalsIgnoreCase(PaymentIngestionConstant.REJECTED)) {
			final RejectionInfo rejectionInfo = new RejectionInfo();
			rejectionInfo.setSource("system");
			rejectionInfo.setMessage(rejectReason);
			transitionInfo.setRejectionInfo(rejectionInfo);
			stateInfo.setDocStatus("uncorrelated");
		} else {
			stateInfo.setDocStatus("correlated");
		}
		transitionInfo.setState(status);
		transitionInfo.setStatus("completed");
		transitionInfo.setRequestType(context.getEventType().getRequestType());
		transitionInfo.setOutcomeCategory(context.getEventType().getOutcomeCategory());
		transitionInfo.setLastActivityTs(context.getEventTime());
		stateInfo.setTransitionInfo(transitionInfo);

		return (ObjectNode) objectMapper.createObjectNode().set(PaymentIngestionConstant.STATE_INFO,
				objectMapper.readValue(objectMapper.writeValueAsString(stateInfo), JsonNode.class));
	}

	private void handleTrashEvent(JsonNode message, Context context, PaymentInstructionSetsES existingPiSetES)
			throws IOException, MessageProcessingException {
		LOGGER.debug("trashEvent handling started for eventId: {}", message.get("id"));

		final List<BulkStateInfo> enrichedBulkStateInfo = bulkProcessingHelper.buildBulkStateInfo(message, context,
				existingPiSetES.getCorporateEntity().getId(), null);

		final ObjectNode bulkUpdateObj = objectMapper.createObjectNode();
		bulkUpdateObj.set(PaymentIngestionConstant.STATE_INFO, objectMapper.readValue(
				objectMapper.writeValueAsString(enrichedBulkStateInfo.get(0).getStateInfo()), JsonNode.class));
		// bulk update list
		final List<BulkableAction> bulkUpdateList = new ArrayList<>();
		bulkUpdateList.add(paymentIngestionQueryUtil.getUpdateBuilder(existingPiSetES.getRecordSetId(), bulkUpdateObj));

		// updating instruction level
		String[] conditionValues = new String[] { existingPiSetES.getRecordSetId() };
		final JsonObject reqJsonForTrashInstr = paymentIngestionQueryUtil
				.getUpdateQueryScriptForTrash("setElementInfo.id", conditionValues);
		paymentIngestionQueryUtil.partialUpdateRecord(paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPymtType(), reqJsonForTrashInstr);

		// updating bulk PISet
		final JestResult jestResult = paymentIngestionQueryUtil.bulkActionExecutor(bulkUpdateList,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
		LOGGER.debug("Updated bulk PI Set for eventId: {}, isSucceeded: {}", message.get("id"),
				jestResult.isSucceeded());

		// updating PISet File
		handlePiSetFileUpdate(message, context, existingPiSetES, existingPiSetES.getPartialApproval());

		LOGGER.debug("trashEvent handling completed for eventId: {}", message.get("id"));
	}

}
