package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Withholding Tax DAO Class.
 * 
 */
@ToString
@Getter
@Setter
public class WithholdingTaxES {
	
	/** The WHT Parent TransactionId. */
	private String parentTransactionId;
	
	/** The WHT Parent InstructionId. */
	private String parentInstructionId;
	
	/** The WHT Doc Parent Instructions Doc Status. */
	private String parentDocStatus;
	
	/** The WHT IncomeType Code.*/
	private String whtIncomeTypeCode;
	
	/** The WHT Income Type Description. */
	private String incomeTypeDescription;
	
	/** The WHT Deduction Date. */
	private String whtDeductionDate;
	
	/** The WHT Amount to be Taxed. */
	private AmountES amounttobeTaxed;
	
	/** The WHT Tax Rate. */
	private Float taxRate;
	
	/** The WHT Tax Amount. */
	private AmountES taxAmount;
		
}
