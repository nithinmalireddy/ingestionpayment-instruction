package com.igtb.ingestion.payment.instruction.consumer;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.service.PaymentIngestionServiceImpl;

import lombok.AllArgsConstructor;

/**
 * Consumer class which listens to the kafka topic.
 */
@Component
//@DependsOn("paymentKafkaDLQProducer")
@AllArgsConstructor
public class PaymentDlqKafkaConsumer implements AcknowledgingMessageListener<String, String> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentDlqKafkaConsumer.class);
	

	/** concurrency consistency. */
	private final CountDownLatch latch = new CountDownLatch(1);

	/** The payment ingestion service impl. */
	private PaymentIngestionServiceImpl serviceImpl;

	/** ObjectMapper to convert string to desired type. */
	private ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Kafka Message receiver. 1. Read Kafka Message 2. Validate the message if
	 * malformed message, write into Kafka DLQ and commit the message If valid,
	 * continue to ES. 3. Send to Elasticsearch if connection not established or ES
	 * client returns isSucceeded false, raise issue and fail the server. Do not
	 * commit the message
	 *
	 * @param data
	 *            the data
	 * @param acknowledgment
	 *            the acknowledgment
	 */
	@Override
	public void onMessage(final ConsumerRecord<String, String> data, final Acknowledgment acknowledgment) {
		Optional.ofNullable(data).ifPresent(record -> {
			try {
				final JsonNode jsonNode = objectMapper.readTree(record.value());
				LOGGER.info("Backend event: {}", jsonNode);
								
				if(LOGGER.isDebugEnabled())LOGGER.debug("Error Type::::: {}", jsonNode.get("errorType"));
				
				TextNode lMsgNode  = (TextNode)jsonNode.get(PaymentIngestionConstant.RECIEVED_MSG);
				ObjectNode lObjNode = (ObjectNode) new ObjectMapper().readTree(lMsgNode.asText());
				if(LOGGER.isDebugEnabled())LOGGER.debug("lMsg::::: {}", lObjNode);
				final JsonNode contextJson = lObjNode.get(PaymentIngestionConstant.CONTEXT);
				//LOGGER.info("Msg :::::{}",contextJson);
				Context context;
				if (contextJson == null) {
					throw new NullPointerException();
				} else {
					context = objectMapper.readValue(contextJson.toString(), Context.class);
				}
				
					//Start CIBC transition event for transition data(updating record on channelSeqID)
					
				LOGGER.info("Going to process the kafka message for :::::: {} :::::::::::{}",context.getEventType().getServiceKey(),context.getEventType().getStatus());
				serviceImpl.pushKafkaDlqPayloadOnES(context,jsonNode,lObjNode);
				acknowledgment.acknowledge();
				latch.countDown();
				LOGGER.info("Backend request on kafka message for :::::: {} :::::::::::{}",context.getEventType().getServiceKey(),context.getEventType().getStatus());
					
					
			} catch (Exception e) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_025 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_025_Desc);
				LOGGER.error("Exception at PaymentKafkaConsumer as:::: {}", e);
				acknowledgment.acknowledge();
				//Need to trigger notification for failure
				//processedDataToDlq(data, e.getMessage(), acknowledgment);
			}
		});
	}
}
