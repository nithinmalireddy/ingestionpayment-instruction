package com.igtb.ingestion.payment.instruction.consumer;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionErrorCodeConstants;
import com.igtb.ingestion.payment.instruction.es.models.PaymentsNotificationES;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.PaymentsNotification;
import com.igtb.ingestion.payment.instruction.producer.PaymentKafkaDLQProducer;
import com.igtb.ingestion.payment.instruction.service.PaymentIngestionServiceImpl;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionQueryUtil;

import lombok.AllArgsConstructor;

/**
 * Consumer class which listens to the kafka topic.
 */
@Component
@DependsOn("paymentKafkaDLQProducer")
@AllArgsConstructor
public class PaymentKafkaNotificationConsumer implements AcknowledgingMessageListener<String, String> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentKafkaNotificationConsumer.class);

	/** concurrency consistency. */
	private final CountDownLatch latch = new CountDownLatch(1);

	/** Kafka producer to send to Dead Letter Queue. */
	private PaymentKafkaDLQProducer paymentKafkaDLQProducer;

	/** The payment ingestion service impl. */
	private PaymentIngestionServiceImpl serviceImpl;
	
	/** The payment ingestion query util. */
	private final PaymentIngestionQueryUtil paymentIngestionQueryUtil;

	/** ObjectMapper to convert string to desired type. */
	private ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Kafka Message receiver. 1. Read Kafka Message 2. Validate the message if
	 * malformed message, write into Kafka DLQ and commit the message If valid,
	 * continue to ES. 3. Send to Elasticsearch if connection not established or ES
	 * client returns isSucceeded false, raise issue and fail the server. Do not
	 * commit the message
	 *
	 * @param data
	 *            the data
	 * @param acknowledgment
	 *            the acknowledgment
	 */
	@Override
	public void onMessage(final ConsumerRecord<String, String> data, final Acknowledgment acknowledgment) {
		Optional.ofNullable(data).ifPresent(record -> {
			try {
					JsonNode jsonNode = objectMapper.readTree(record.value());
					if(jsonNode.get(PaymentIngestionConstant.NOTF_TYPE).asText().equals(PaymentIngestionConstant.NOTF_RECEIVE_RTP)) {
						Thread.sleep(15000);
					}
					LOGGER.info("Notification Data read from kafka ::::: {}", jsonNode);
					LOGGER.info("Going to process the kafka message for Notification");
					final PaymentsNotificationES paymentsNotificationES = enrichPaymentsNotificationES(jsonNode);
					serviceImpl.pushKafkaNotificationPayloadOnES(paymentsNotificationES);						
					acknowledgment.acknowledge();
					latch.countDown();
					LOGGER.info("Backend request on kafka message on PaymentKafkaNotificationConsumer has been processed");
			} catch (IOException | RequeueException | InterruptedException | DLQException | MessageProcessingException e) {
				LOGGER.error(PaymentIngestionErrorCodeConstants.ERR_PAYING_083 + PaymentIngestionErrorCodeConstants.ERR_SPLITER
						+ PaymentIngestionErrorCodeConstants.ERR_PAYING_083_Desc);
				LOGGER.error("Exception at PaymentKafkaConsumer as: {}", e);
				processedDataToDlq(data, e.getMessage(), acknowledgment);
			}
		});
	}
	

	/**
	 * Processed data to dlq.
	 *
	 * @param data the data
	 * @param msg the msg
	 * @param acknowledgment the acknowledgment
	 */
	private void processedDataToDlq(final ConsumerRecord<String, String> data, final String msg,final Acknowledgment acknowledgment) {
		paymentKafkaDLQProducer.sendToDLQ(data,msg);
		acknowledgment.acknowledge();
		latch.countDown();
		LOGGER.info("Invalid Payload. Hence the payload is directed to DLQ. Error Message ::{}", msg);
	}
	
	
	
	//Enrich payload
	private PaymentsNotificationES enrichPaymentsNotificationES(final JsonNode notificationJson)throws DLQException, JsonParseException, JsonMappingException, IOException {
		String accountType = "NULL";
		JsonNode existingESPayloadForRefNo = null;
			// Extracting payload Information from existing
		existingESPayloadForRefNo = paymentIngestionQueryUtil.fetchExistingRecordFromESForNotification(notificationJson.get(PaymentIngestionConstant.INTERAC_ID).asText());
			
			final PaymentsNotificationES paymentsNotificationES = new PaymentsNotificationES();
			LOGGER.info("Converting kafka Notification Payload to PaymentsNotification model");
			
			final PaymentsNotification paymentsNotification = objectMapper.readValue(notificationJson.toString(),PaymentsNotification.class);
		paymentsNotificationES.setEventES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getEvent).orElse(null));
			paymentsNotificationES.setNotificationIdES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getNotificationId).orElse(null));
			paymentsNotificationES.setNtfcnCategoryES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getNtfcnCategory).orElse(null));
			paymentsNotificationES.setDeclinereason(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getDeclinereason).orElse(null));
			
			//Creditor Account
		paymentsNotificationES.setNotfTypeES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getNotfType).orElse(null));
		paymentsNotificationES.setNotfNameES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getNotfName).orElse(null));
		paymentsNotificationES.setNameES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getName).orElse(null));
		paymentsNotificationES.setNameTypeES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getNameType).orElse(null));
		paymentsNotificationES.setAccountAliasHandleES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getAccountAliasHandle).orElse(null));
		paymentsNotificationES.setAccountAliasReferenceES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getAccountAliasReference).orElse(null));
			
		JsonNode jsonNode = paymentIngestionQueryUtil.fetchAccountTypeFromNotificationEventConfig(notificationJson.get(PaymentIngestionConstant.EVENT).asText(),notificationJson.get(PaymentIngestionConstant.NOTF_TYPE).asText());
		if (jsonNode != null && !jsonNode.isNull()) 
		{
			accountType = jsonNode.get(PaymentIngestionConstant.ACCOUNT).asText();
		}
		switch (accountType) {
		case PaymentIngestionConstant.DR:
			paymentsNotificationES.setAccoutNumberES(existingESPayloadForRefNo !=null ? existingESPayloadForRefNo.get(PaymentIngestionConstant.DEBTOR_ACCOUNT).get(PaymentIngestionConstant.ID).asText(): PaymentIngestionConstant.NULL);
			paymentsNotificationES.setPaymntTypeAccIdES(existingESPayloadForRefNo != null ? (existingESPayloadForRefNo.get(PaymentIngestionConstant.PYMNT_TYP_OBJ).get(PaymentIngestionConstant.CODE).asText()+PaymentIngestionConstant.HASH+paymentsNotificationES.getAccoutNumberES()): (paymentsNotificationES.getNotfTypeES().toLowerCase().replace(PaymentIngestionConstant.UNDER_SCORE, PaymentIngestionConstant.BLANK).trim()+PaymentIngestionConstant.HASH+PaymentIngestionConstant.NULL));
			break;
		case PaymentIngestionConstant.CR:
			paymentsNotificationES.setAccoutNumberES(existingESPayloadForRefNo !=null ? existingESPayloadForRefNo.get(PaymentIngestionConstant.CREDITOR_ACCOUNT).get(PaymentIngestionConstant.ID).asText(): PaymentIngestionConstant.NULL);
			paymentsNotificationES.setPaymntTypeAccIdES(existingESPayloadForRefNo != null ? (existingESPayloadForRefNo.get(PaymentIngestionConstant.PYMNT_TYP_OBJ).get(PaymentIngestionConstant.CODE).asText()+PaymentIngestionConstant.HASH+paymentsNotificationES.getAccoutNumberES()): (paymentsNotificationES.getNotfTypeES().toLowerCase().replace(PaymentIngestionConstant.UNDER_SCORE, PaymentIngestionConstant.BLANK).trim()+PaymentIngestionConstant.HASH+PaymentIngestionConstant.NULL));
			break;
		default:
			paymentsNotificationES.setAccoutNumberES(PaymentIngestionConstant.NULL);
			paymentsNotificationES.setPaymntTypeAccIdES(existingESPayloadForRefNo != null ? (existingESPayloadForRefNo.get(PaymentIngestionConstant.PYMNT_TYP_OBJ).get(PaymentIngestionConstant.CODE).asText()+PaymentIngestionConstant.HASH+PaymentIngestionConstant.NULL): (paymentsNotificationES.getNotfTypeES().toLowerCase().replace(PaymentIngestionConstant.UNDER_SCORE, PaymentIngestionConstant.BLANK).trim()+PaymentIngestionConstant.HASH+PaymentIngestionConstant.NULL));
//			paymentsNotificationES.setPaymntTypeAccIdES((paymentsNotificationES.getNotfTypeES().toLowerCase().replace(PaymentIngestionConstant.UNDER_SCORE, PaymentIngestionConstant.BLANK).trim()+PaymentIngestionConstant.HASH+PaymentIngestionConstant.NULL));
			break;
		}
			
			//Amount
		paymentsNotificationES.setInstAmtcurrencyES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getInstAmtcurrency).orElse(null));
		paymentsNotificationES.setInstAmtamountES(Double.valueOf(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getInstAmtamount).orElse(null)));
			// If currency is blank or null then ingest currency as "CAD" 
        	// Amount is declare as double so its automatically converted to 0.0 if its blank
		if(paymentsNotification.getInstAmtcurrency()!=null) {
			if (paymentsNotification.getInstAmtcurrency().equals("") || paymentsNotification.getInstAmtcurrency().equalsIgnoreCase("null")) {
				paymentsNotificationES.setInstAmtcurrencyES(PaymentIngestionConstant.CAD_CURRENCY);
			}
		}else {
			paymentsNotificationES.setInstAmtcurrencyES(PaymentIngestionConstant.CAD_CURRENCY);
		}
		paymentsNotificationES.setParticipantUserIdES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getParticipantUserId).orElse(null));
			paymentsNotificationES.setInteracIdES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getInteracId).orElse(null));
			paymentsNotificationES.setMessageES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getMessage).orElse(null));
			paymentsNotificationES.setPriorityES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getPriority).orElse(null));
			paymentsNotificationES.setCreationDateES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getCreationDate).orElse(null));
		paymentsNotificationES.setDocStatusES(existingESPayloadForRefNo != null ? existingESPayloadForRefNo.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.DOC_STATUS).asText(): PaymentIngestionConstant.NULL);
		paymentsNotificationES.setDomainSeqIdES(existingESPayloadForRefNo != null ? existingESPayloadForRefNo.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.DOMAIN_SEQ_ID).asText(): PaymentIngestionConstant.NULL);
		paymentsNotificationES.setChannelSeqIdES(existingESPayloadForRefNo != null ? existingESPayloadForRefNo.get(PaymentIngestionConstant.STATE_INFO).get(PaymentIngestionConstant.CHANNEL_SQ_ID).asText(): PaymentIngestionConstant.NULL);
		paymentsNotificationES.setParticipantIdES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getParticipantId).orElse(null));
		paymentsNotificationES.setReferenceNumberES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getReferenceNumber).orElse(null));
		LOGGER.info("Notification status:::{}",paymentsNotification.getStatus());
		paymentsNotificationES.setNtfcnStatusES(Optional.ofNullable(paymentsNotification).map(PaymentsNotification::getStatus).orElse(null));

		LOGGER.info("After enrichment Payload to PaymentsNotificationES model:::::::{}",paymentsNotificationES.toString());
			return paymentsNotificationES;
		}
	
	
	
	
}
