package com.igtb.ingestion.payment.instruction.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents crucial information about what type of event has happened in system.
 */
@Getter
@Setter
public class EventType {
  
  /** The event category. */
  @JsonProperty("eventCategory")
  private String eventCategory;

  /** The service key. */
  @JsonProperty("serviceKey")
  private String serviceKey;

  /** The outcome category. */
  @JsonProperty("outcomeCategory")
  private String outcomeCategory;

  /** The status. */
  @JsonProperty("status")
  private String status;

  /** The request type. */
  @JsonProperty("requestType")
  private String requestType;

  /** The format. */
  @JsonProperty("format")
  private String format;
  
  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final EventType eventType = (EventType) obj;
    return Objects.equals(this.eventCategory, eventType.eventCategory) &&
        Objects.equals(this.serviceKey, eventType.serviceKey) &&
        Objects.equals(this.outcomeCategory, eventType.outcomeCategory) &&
        Objects.equals(this.status, eventType.status) &&
        Objects.equals(this.requestType, eventType.requestType) &&
        Objects.equals(this.format, eventType.format);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(eventCategory, serviceKey, outcomeCategory, status, requestType, format);
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(130);
    sb.append("class EventType { eventCategory: ").append(toIndentedString(eventCategory))
      .append("    serviceKey: ").append(toIndentedString(serviceKey))
      .append("    outcomeCategory: ").append(toIndentedString(outcomeCategory))
      .append("    status: ").append(toIndentedString(status))
      .append("    requestType: ").append(toIndentedString(requestType))
      .append("    format: ").append(toIndentedString(format))
      .append('}');
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param obj the obj
   * @return the string
   */
  private String toIndentedString(final Object obj) {
    if (obj == null) {
      return "null";
    }
    return obj.toString().replace("\n", "\n    ");
  }

}

