package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class RecordData.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecordData {

	/** The id. */
	private String id;
	
	/** The value. */
	private String value;
	
	/** The data. */
	private String data;
}
