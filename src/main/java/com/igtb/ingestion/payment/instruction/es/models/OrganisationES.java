package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class OrganisationES.
 */
@Getter
@Setter
//@JsonInclude(content = Include.NON_EMPTY, value = Include.NON_EMPTY)
public class OrganisationES {

	/** The id. */
	private String id;
	
	/** The key. */
	private String key;
	
	/** The name. */
	private String name;
}
