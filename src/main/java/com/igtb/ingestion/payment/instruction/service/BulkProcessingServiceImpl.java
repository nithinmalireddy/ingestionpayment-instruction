package com.igtb.ingestion.payment.instruction.service; // NOPMD moreImport

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.igtb.api.ingestion.commons.bulk.decisionmatrix.BulkAction;
import com.igtb.api.ingestion.commons.bulk.decisionmatrix.BulkTransitionData;
import com.igtb.api.ingestion.commons.bulk.decisionmatrix.BulkTransitionDecisionModule;
import com.igtb.api.ingestion.commons.bulk.eventmatrix.BulkEventMatrix;
import com.igtb.api.ingestion.commons.stateinfo.models.StateInfo;
import com.igtb.api.ingestion.commons.stateinfo.models.TransitionInfo;
import com.igtb.api.ingestion.commons.util.CommonsIngestionConstants;
import com.igtb.ingestion.payment.instruction.constant.PaymentIngestionConstant;
import com.igtb.ingestion.payment.instruction.es.models.PaymentInstructionSetsES;
import com.igtb.ingestion.payment.instruction.es.models.PaymentWF;
import com.igtb.ingestion.payment.instruction.exception.DLQException;
import com.igtb.ingestion.payment.instruction.exception.DataNotFoundException;
import com.igtb.ingestion.payment.instruction.exception.DataNotValidException;
import com.igtb.ingestion.payment.instruction.exception.MessageProcessingException;
import com.igtb.ingestion.payment.instruction.exception.RequeueException;
import com.igtb.ingestion.payment.instruction.models.BackendPayload;
import com.igtb.ingestion.payment.instruction.models.Context;
import com.igtb.ingestion.payment.instruction.models.bulk.BulkActionApiPayload;
import com.igtb.ingestion.payment.instruction.models.bulk.BulkEventPayload;
import com.igtb.ingestion.payment.instruction.util.BulkProcessingChannelUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionHelper;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionRedisUtil;

import lombok.AllArgsConstructor;

/**
 * The Class FileProcessingServiceImpl.
 */
@Component
@AllArgsConstructor
public class BulkProcessingServiceImpl {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkProcessingServiceImpl.class);

	/** The ingestion channel util. */
	private BulkProcessingChannelUtil ingestionChannelUtil;

	/** The payment ingestion redis util. */
	private PaymentIngestionRedisUtil paymentIngestionRedisUtil;

	/** The payment ingestion helper. */
	private PaymentIngestionHelper paymentIngestionHelper;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	/**
	 * Process rabbit message.
	 *
	 * @param requestMessageJson
	 *            the request message json
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	public void procesasBulkRabbitMessage(final JsonNode requestMessageJson)
			throws IOException, RequeueException, DLQException, InterruptedException, MessageProcessingException,
			DataNotValidException, DataNotFoundException {
		// Extracting Context Information from Request Message
		final JsonNode CONTEXTJSON = requestMessageJson.get(PaymentIngestionConstant.CONTEXT);
		Context context;
		// Converting JsonNode to Context model
		if (CONTEXTJSON == null) {
			context = new Context();
		} else {
			context = objectMapper.readValue(CONTEXTJSON.toString(), Context.class);
		}

		/*
		 * separating the events based on the outcome category[channel-state-updated,
		 * channel-workflow-update, backend-state-updated] for processing since each
		 * channel/routes has its own implementations
		 */
		dispatchRMQMsgBasedOnOutcomeCategory(requestMessageJson, context);

		/*
		 * After successful processing, sending all backend state events to rabbitmq for
		 * other apis to consume.
		 */
		if ("backend-state-updated".equalsIgnoreCase(Optional.ofNullable(context.getEventType())
				.map(eventType -> eventType.getOutcomeCategory()).orElse(null))) {
			paymentIngestionHelper.publishBackendEventsToRabbit(requestMessageJson, context);
		}

	}

	/**
	 * Dispatch RMQ msg based on outcome category.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void dispatchRMQMsgBasedOnOutcomeCategory(final JsonNode message, final Context context)
			throws IOException, RequeueException, DLQException, InterruptedException, MessageProcessingException,
			DataNotValidException, DataNotFoundException {

		final String outcomeCategory = Optional.ofNullable(context.getEventType())
				.map(eventType -> eventType.getOutcomeCategory()).orElse(null);

		final JsonNode PAYLOAD;
		if (message.get(PaymentIngestionConstant.PAYLOAD) == null
				|| message.get(PaymentIngestionConstant.PAYLOAD).isNull()) {
			PAYLOAD = objectMapper.createObjectNode();
			((ObjectNode) message).set("payload", PAYLOAD);
		} else {
			PAYLOAD = message.get(PaymentIngestionConstant.PAYLOAD);
		}

		final String status = Optional.ofNullable(context.getEventType()).map(eventType -> eventType.getStatus())
				.orElse(null);

		// validate event status
		if (BulkEventMatrix.isIngestionAllowed(message)) {
			if (PAYLOAD != null) {
				if (Arrays.asList(CommonsIngestionConstants.FILE_BULKS_CREATED,
						CommonsIngestionConstants.BULK_DEF_NOCHANGE, CommonsIngestionConstants.BULK_STATE_UPDATE)
						.contains(status)) {
					final BulkEventPayload bulkEventPayload = objectMapper.readValue(PAYLOAD.toString(),
							BulkEventPayload.class);
					// validate payload
					if (bulkEventPayload != null) {
						validateBean(bulkEventPayload,
								Optional.ofNullable(bulkEventPayload).map(payload -> payload.getBulks())
										.map(bulks -> bulks.get(0)).map(bulk -> bulk.getFileId()).orElse(""));
					}
					performCommonActions(message, context, bulkEventPayload, null, null, null);

				} else if (PaymentIngestionConstant.CHANNEL_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)) {
					final BulkActionApiPayload bulkActionApiPayload = objectMapper.readValue(PAYLOAD.toString(),
							BulkActionApiPayload.class);
					// validate payload
					if (bulkActionApiPayload != null) {
						validateBean(bulkActionApiPayload, bulkActionApiPayload.getFileId());
					}
					performCommonActions(message, context, null, bulkActionApiPayload, null, null);

				} else if (PaymentIngestionConstant.CHANNEL_WORKFLOW_UPDATE.equalsIgnoreCase(outcomeCategory)) {
					final PaymentWF paymentwf = objectMapper.readValue(PAYLOAD.toString(), PaymentWF.class);
					performCommonActions(message, context, null, null, paymentwf, null);

				} else if (PaymentIngestionConstant.BACKEND_STATE_UPDATED.equalsIgnoreCase(outcomeCategory)) {
					final BackendPayload backendPayload = objectMapper.readValue(PAYLOAD.toString(),
							BackendPayload.class);
					performCommonActions(message, context, null, null, null, backendPayload);
				}
			}
		} else {
			LOGGER.info("isIngestionAllowed false, hence Skipping the event id: {}", message.get("id"));
		}

	}

	/**
	 * Perform common actions.
	 *
	 * @param message
	 *            the message
	 * @param context
	 *            the context
	 * @param bulkEventPayload
	 *            the file available payload
	 * @param metadataPayload
	 *            the metadata payload
	 * @param fileTransactionPayload
	 *            the file transaction payload
	 * @param paymentWF
	 *            the payment WF
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws JsonProcessingException
	 *             the json processing exception
	 * @throws RequeueException
	 *             the requeue exception
	 * @throws DLQException
	 *             the DLQ exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws MessageProcessingException
	 *             the message processing exception
	 * @throws DataNotValidException
	 *             the data not valid exception
	 * @throws DataNotFoundException
	 *             the data not found exception
	 */
	private void performCommonActions(final JsonNode message, final Context context,
			final BulkEventPayload bulkEventPayload, final BulkActionApiPayload bulkActionApiPayload,
			final PaymentWF paymentWF, final BackendPayload backendPayload)
			throws IOException, RequeueException, DLQException, InterruptedException, MessageProcessingException {

		final Map<String, String> srarchKeyValue = getSearchId(context, bulkEventPayload, bulkActionApiPayload);
		final String lockId = getLockId(context, bulkEventPayload, bulkActionApiPayload);
		// acquireRedisLocks
		if (paymentIngestionRedisUtil.acquirePurgeBatchLock(lockId)) {

			/* Fetching existing PaymentInstruction data from ES if available */
			LOGGER.info("fetching stateinformation of existing payment instruction set for fileId/channelSeqId: {}",
					lockId);
			final PaymentInstructionSetsES existingPiSetES = ingestionChannelUtil.getExistingPIset(srarchKeyValue);

			final StateInfo stateInfo = Optional.ofNullable(existingPiSetES).map(PaymentInstructionSetsES::getStateInfo)
					.orElse(new StateInfo());

			// 2. setting paymentIngestionStateInfoHelper stateinfo
			final BulkTransitionData stateTransitionData = new BulkTransitionData();

			// Enriching stateTransitionData info from existing record in ES.
			setFromStateTransitionData(stateInfo, stateTransitionData);

			/*
			 * Enriching stateTransitionData info from request payload. passing the
			 * stateTransitionData itself[has "From *" informations] to get enriched with
			 * "To *" informations and return
			 */
			setToStateTransitionData(context, stateTransitionData);

			/*
			 * Action finder call to figure out the action [possible actions are insert,
			 * replace, update, requeue, skip]
			 */
			final BulkAction bulkAction = BulkTransitionDecisionModule.getActionForTransition(stateTransitionData);
			LOGGER.debug("BulkAction receive from common igestion as: {} for id: {}", bulkAction, lockId);

			switch (bulkAction) {
			case FILE_UPD_BULK_REFRESH_INSTR_INSERT:
				// File Update, Bulk Refresh, Instr Update.
				ingestionChannelUtil.fileUpdateBulkRefrshInstrInsert(message, context, bulkEventPayload,
						existingPiSetES);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
				break;
			case FILE_BULK_INSTR_UPDATE:
				// File+Bulk+Instr Update
				ingestionChannelUtil.fileBulkInstrUpdate(message, context, bulkEventPayload, paymentWF, backendPayload,
						existingPiSetES);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
				break;
			case FILE_BULK_UPDATE:
				// FILE_BULK_UPDATE
				ingestionChannelUtil.fileBulkUpdate(message, context, bulkActionApiPayload, paymentWF, backendPayload,
						existingPiSetES);
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
				break;
			case REQUEUE:
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
				final String requeueMsg = "Requeuing this event as per bulk level common matrix for fileId/channelSeqId: "
						+ lockId;
				ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);
				break;
			default: // Skip Action
				LOGGER.info("<<< Skipping the event with fileId/bulkId/channelSeqId: {} and outcomecategory {} >>>",
						lockId, context.getEventType().getOutcomeCategory());
				// releasing the set lock from redis after processing
				releaseRedisLock(lockId);
				break;
			}

		} else {

			final String requeueMsg = "Requeuing the event due to unavailability of redis lock, for fileId/channelSeqId: "
					+ lockId;
			LOGGER.info("-------->>>>>>>>> {} <<<<<<<<<<--------", requeueMsg);
			final BulkTransitionData stateTransitionData = new BulkTransitionData();

			/*
			 * setting only "***********State To***********" Information as this request
			 * hasn't reached a stage to fetch the existing information to derive to an
			 * action that needs to be taken. Enriching stateTransitionData info from
			 * request payload.
			 */
			LOGGER.info("-------->>>>>>>>>Setting only **To State** Information at this stage<<<<<<<<<<--------");
			setToStateTransitionData(context, stateTransitionData);
			ingestionChannelUtil.requeueMessage(context, stateTransitionData, requeueMsg);

		}

	}

	/**
	 * Release redis lock.
	 *
	 * @param id
	 *            the id
	 * @throws RequeueException
	 *             the requeue exception
	 */
	private void releaseRedisLock(final String id) throws RequeueException {
		if (paymentIngestionRedisUtil.releasePurgeBatchLock(id)) {
			LOGGER.info("-------->>>>>>>>Released the obtained Redis Lock<<<<<<<<--------");
		} else {
			LOGGER.info("-------->>>>>>>>Couldn't release the redis lock<<<<<<<<--------");
		}
	}

	/**
	 * Gets the search id.
	 *
	 * @param context
	 *            the context
	 * @param bulkEventPayload
	 *            the bulk event payload
	 * @param bulkActionApiPayload
	 *            the bulk action api payload
	 * @return the search id
	 */
	private Map<String, String> getSearchId(final Context context, final BulkEventPayload bulkEventPayload,
			final BulkActionApiPayload bulkActionApiPayload) {
		final Map<String, String> srarchKeyValue = new HashMap<>();
		final String status = context.getEventType().getStatus().toLowerCase(Locale.ENGLISH);
		if (CommonsIngestionConstants.FILE_BULKS_CREATED.equals(status)) {
			srarchKeyValue.put("_id", bulkEventPayload.getBulks().get(0).getFileId());
		} else if (Arrays.asList(CommonsIngestionConstants.VAL_SUCCESS, CommonsIngestionConstants.VAL_FAILURE,
				CommonsIngestionConstants.VAL_TIMEOUT).contains(status)) {
			srarchKeyValue.put("_id", bulkActionApiPayload.getBulkId());
		} else if (StringUtils.isNotBlank(context.getChannelSeqId())) {
			srarchKeyValue.put("stateInfo.channelSeqId", context.getChannelSeqId());
		}
		return srarchKeyValue;
	}

	/**
	 * Gets the lock id.
	 *
	 * @param context
	 *            the context
	 * @param bulkEventPayload
	 *            the bulk event payload
	 * @param bulkActionApiPayload
	 *            the bulk action api payload
	 * @return the lock id
	 */
	private String getLockId(final Context context, final BulkEventPayload bulkEventPayload,
			final BulkActionApiPayload bulkActionApiPayload) {
		final String status = context.getEventType().getStatus().toLowerCase(Locale.ENGLISH);
		if (CommonsIngestionConstants.FILE_BULKS_CREATED.equals(status)) {
			return bulkEventPayload.getBulks().get(0).getFileId();
		} else if (Arrays.asList(CommonsIngestionConstants.VAL_SUCCESS, CommonsIngestionConstants.VAL_FAILURE,
				CommonsIngestionConstants.VAL_TIMEOUT).contains(status)) {
			return bulkActionApiPayload.getFileId();
		} else {
			return context.getChannelSeqId();
		}
			
	}

	/**
	 * Validate bean.
	 *
	 * @param object
	 *            the object
	 * @param id
	 *            the channel seq id
	 * @return the list
	 * @throws DLQException
	 *             the DLQ exception
	 */
	private void validateBean(final Object object, final String id) throws DLQException {
		final List<String> errorMessages = new ArrayList<>();

		final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		validator.validate(object).stream().forEach(violation -> {
			final StringBuilder messageSet = new StringBuilder(120);
			messageSet.append("\n >>>> Property Path:").append(violation.getPropertyPath())
					.append(" <<<< \n >>>> Value that violated the constraint : ").append(violation.getInvalidValue())
					.append(" <<<< \n >>>> Constraint : ").append(violation.getMessage()).append(" <<<<");
			errorMessages.add(messageSet.toString());
		});

		if (!errorMessages.isEmpty()) {
			LOGGER.info("Validation Failed for fileId/channelSeqId --> {} and the Error Messages {}", id,errorMessages);
			throw new DLQException("Bean Validation failed, hence sending to DLQ {}");
		}
	}

	/**
	 * Sets the from state transition data.
	 *
	 * @param stateInfo
	 *            the state info
	 * @param stateTransitionData
	 *            the state transition data
	 */
	private void setFromStateTransitionData(final StateInfo stateInfo, final BulkTransitionData stateTransitionData) {

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getLastActivityTs)
				.ifPresent(stateTransitionData::setFromEventTime);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getState)
				.ifPresent(stateTransitionData::setFromState);

		Optional.ofNullable(stateInfo).map(StateInfo::getTransitionInfo).map(TransitionInfo::getOutcomeCategory)
				.ifPresent(stateTransitionData::setFromOutcomeCategory);

		LOGGER.info("From State {}, From OutCome Category {}, From EventTime {} ", stateTransitionData.getFromState(),
				stateTransitionData.getFromOutcomeCategory(), stateTransitionData.getFromEventTime());
	}

	/**
	 * Sets the to state transition data.
	 *
	 * @param context
	 *            the context
	 * @param stateTransitionData
	 *            the state transition data
	 */
	private void setToStateTransitionData(final Context context, final BulkTransitionData stateTransitionData) {
		stateTransitionData.setToEventTime(context.getEventTime());

		stateTransitionData.setToState(context.getEventType().getStatus());

		stateTransitionData.setToOutcomeCategory(context.getEventType().getOutcomeCategory());

		LOGGER.info("To State {}, To OutCome Category {}, To EventTime {} ", stateTransitionData.getToState(),
				stateTransitionData.getToOutcomeCategory(), stateTransitionData.getToEventTime());
	}

	/**
	 * Gets the redis key for purge batch lock.
	 *
	 * @param payloadType
	 *            the payload type
	 * @return the redis key for purge batch lock
	 */
	public static String getRedisKeyToSetLock(final String payloadType) {
		return PaymentIngestionConstant.REDIS_LOCK_KEY_PREFIX + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ PaymentIngestionConstant.REDIS_DOMAIN_KEY + PaymentIngestionConstant.REDIS_KEY_SEPARATOR
				+ "channel-state" + PaymentIngestionConstant.REDIS_KEY_SEPARATOR + payloadType;
	}

}
