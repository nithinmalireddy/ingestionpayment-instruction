package com.igtb.ingestion.payment.instruction.models;

import org.json.JSONObject;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class WkflwConductorTriggers.
 */
@Getter
@Setter
@ToString
public class WkflwConductorTriggers {
	
	/** The context. */
	private WkflwContext context;
	
	/** The payload. */
	private JSONObject payload;
	
}
