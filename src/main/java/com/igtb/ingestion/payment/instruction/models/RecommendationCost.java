package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
* The Class RecommendationCost.
*/
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
* Gets the currency.
*
* @return the currency
*/
@Getter

/**
* Sets the currency.
*
* @param currency the new currency
*/
@Setter
public class RecommendationCost {
	
	 /** The value. */
    @JsonProperty("value")
    public Double value;
    
    /** The currency. */
    @JsonProperty("currency")
    public String currency;

}
