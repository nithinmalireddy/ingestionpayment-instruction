package com.igtb.ingestion.payment.instruction.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.AbstractMessageListenerContainer.AckMode;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.config.ContainerProperties;

import com.igtb.ingestion.payment.instruction.config.properties.PaymentKafkaProperties;
import com.igtb.ingestion.payment.instruction.consumer.PaymentDlqKafkaConsumer;
import com.igtb.ingestion.payment.instruction.consumer.PaymentKafkaConsumer;
import com.igtb.ingestion.payment.instruction.consumer.PaymentKafkaNotificationConsumer;
import com.igtb.ingestion.payment.instruction.consumer.PaymentKafkaPshcConsumer;

/**
 * The Class PaymentKafkaConfig.
 */
@Configuration
@EnableKafka
@DependsOn("paymentKafkaConsumer")
public class PaymentKafkaConfig {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentKafkaConfig.class);

	/** The users kafka properties. */
	@Autowired
	private PaymentKafkaProperties paymentKafkaProperties;

	/** The payment kafka consumer. */
	@Autowired
	private PaymentKafkaConsumer paymentKafkaConsumer;
	
	/** The payment kafka PSHC Data consumer. */
	@Autowired
	private PaymentKafkaPshcConsumer paymentKafkaPshcConsumer;


	@Autowired
	private PaymentKafkaNotificationConsumer paymentKafkaNotificationConsumer;
	@Autowired
	private PaymentDlqKafkaConsumer paymentDlqKafkaConsumer;

	/**
	 * Listener container.
	 *
	 * @return the concurrent message listener container
	 */
	@Bean
	public ConcurrentMessageListenerContainer<String, String> listenerContainer() {
		final ContainerProperties containerProps = new ContainerProperties(paymentKafkaProperties.getKafkaTopic());
		containerProps.setAckMode(AckMode.MANUAL);
		containerProps.setSyncCommits(true);
		containerProps.setMessageListener(paymentKafkaConsumer);
		return new ConcurrentMessageListenerContainer<>(consumerFactory(), containerProps);
	}

	@Bean
	public ConcurrentMessageListenerContainer<String, String> pshcListenerContainer() {
		final ContainerProperties containerProps = new ContainerProperties(paymentKafkaProperties.getKafkaTopicPshc());
		containerProps.setAckMode(AckMode.MANUAL);
		containerProps.setSyncCommits(true);
		containerProps.setMessageListener(paymentKafkaPshcConsumer);
		return new ConcurrentMessageListenerContainer<>(consumerFactory(), containerProps);
	}
	@Bean
	public ConcurrentMessageListenerContainer<String, String> notificationListenerContainer() {
		final ContainerProperties containerProps = new ContainerProperties(paymentKafkaProperties.getKafkaNotifctnTopic());
		containerProps.setAckMode(AckMode.MANUAL);
		containerProps.setSyncCommits(true);
		containerProps.setMessageListener(paymentKafkaNotificationConsumer);
		return new ConcurrentMessageListenerContainer<>(consumerFactory(), containerProps);
	}
	@Bean
	public ConcurrentMessageListenerContainer<String, String> dlqListenerContainer() {
		final ContainerProperties containerProps = new ContainerProperties(paymentKafkaProperties.getKafkaDlqTopic());
		containerProps.setAckMode(AckMode.MANUAL);
		containerProps.setSyncCommits(true);
		containerProps.setMessageListener(paymentDlqKafkaConsumer);
		return new ConcurrentMessageListenerContainer<>(consumerFactory(), containerProps);
	}
	
	/**
	 * Consumer Factory.
	 *
	 * @return DefaultKafkaConsumerFactory
	 */
	public DefaultKafkaConsumerFactory<String, String> consumerFactory() {
		return new DefaultKafkaConsumerFactory<>(consumerConfigs());
	}

	/**
	 * Set the consumer configs for consumer factory.
	 *
	 * @return Map of configs
	 */
	private Map<String, Object> consumerConfigs() {
		final Map<String, Object> properties = new HashMap<>();
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, paymentKafkaProperties.getBootstrapServers());
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, paymentKafkaProperties.getGroupId());
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, paymentKafkaProperties.getAutoOffsetReset());
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
		LOGGER.info("Kafka Bootstrap servers {}", paymentKafkaProperties.getBootstrapServers());
		LOGGER.info("Kafka Auto offset {}", paymentKafkaProperties.getAutoOffsetReset());
		LOGGER.info("Kafka Topic {}", paymentKafkaProperties.getKafkaTopic());
		return properties;
	}

	/**
	 * Producer factory.
	 *
	 * @return the producer factory
	 */
	@Bean
	public ProducerFactory<String, String> producerFactory() {
		final Map<String, Object> configProps = new HashMap<>();
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, paymentKafkaProperties.getBootstrapServers());
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	/**
	 * Kafka template. Used to send to Kafka Topic
	 *
	 * @return the kafka template
	 */
	@Bean
	public KafkaTemplate<String, String> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

}
