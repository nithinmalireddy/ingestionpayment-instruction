package com.igtb.ingestion.payment.instruction.models;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * Class which represents relation of attachment with transaction
 * 
 */
@Getter
@Setter
public class TransactionFileRelation {
	/**
	 * FileIds which needs to be linked to filestore
	 */
	private Set<String> link;
	/**
	 * FileIds which needs to be delinked to filestore
	 */
	private Set<String> delink;

}
