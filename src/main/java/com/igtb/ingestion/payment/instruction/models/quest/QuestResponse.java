package com.igtb.ingestion.payment.instruction.models.quest;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * QuestResponse POJO.
 *
 * @author gautam.garg
 */

/** Getter */
@Getter

/** Setter */
@Setter

/** (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@ToString
public class QuestResponse implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1987933093950156245L;
	
	/** The data. */
	private transient Object data;
	
	/** The res code. */
	private String resCode;

	/** The res msg. */
	private String resMsg;
	
	/** The errors. */
	private List<RespError> errors;
	
}
