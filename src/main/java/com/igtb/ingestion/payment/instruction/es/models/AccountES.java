package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * AccountES class.
 * 
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
public class AccountES {

	//to be used to only for schemeName
	/** The account Schemename. */
	@JsonIgnore
	private String schemeName;
		
	/** The accountNumType used only when PI initiated for Adhoc/or AnyID.(ignored otherwise) */
	//[Account Number,IBAN,e-Mail Id,Mobile Number]
	@JsonIgnore
	private String accNumType;
	
	/** The account GUID. */
//	@NotNull
	private String id;
	
	/** The account accountNo. */
//	@NotNull
	private String accountNo;
	
	/** The account name. */
	private String name; 
	
	
	/** The account legalName. */
	private String legalName; 
	
	/** The account status. */
	private String status; 
	
	/** The account alias. */
	private String alias;
	
	/** The account aliasHandleType. */
	private String aliasHandleType;
	/** The account tags. */
	private List<String> tags;
	
	@JsonProperty("accountCreationDate")
    public String accountCreationDate;
	
	/** The account bank details. */
//	@NotNull
//	@Valid
	private BankES bank;
	
	/** The account entity details. */
//	@NotNull
//	@Valid
	private EntityES entity; 
	
    /** The account currency*/
    
    public String currency;
	
	/** The account country. */
	private CountryES country; 
	
	/** The account currency details. */
	private AcctCyyES acctCcy;
	
	/** The short account number. */
	private String shortAccountNo;
	
	private String contactName;
	
	private String isAddedContactBookFlag;
	
	private String schemeNameCode;
	
	
	
	

}
