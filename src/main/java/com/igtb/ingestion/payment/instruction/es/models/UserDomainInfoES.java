package com.igtb.ingestion.payment.instruction.es.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class UserDomainInfoES.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class UserDomainInfoES {

	/** The corporate designations. */
	private List<UserCorpDesignationES> corporateDesignations;

	/** The name. */
	private String name;
	
	/** The primary entity. */
	private EntityES primaryEntity;
	
}
