
package com.igtb.ingestion.payment.instruction.es.models;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "contactId",
    "stateDocInfo",
    "activityInfo",
    "created",
    "lastUpdated"
})
public class ContactLastActivityES {

    @JsonProperty("contactId")
    private String contactId;
    @JsonProperty("stateDocInfo")
    private StateDocInfoES stateDocInfo;
    @JsonProperty("activityInfo")
    private ActivityInfoES activityInfo;
    @JsonProperty("created")
    private String created;
    @JsonProperty("lastUpdated")
    private String lastUpdated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("contactId")
    public String getContactId() {
        return contactId;
    }

    @JsonProperty("contactId")
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    @JsonProperty("stateDocInfo")
    public StateDocInfoES getStateDocInfo() {
        return stateDocInfo;
    }

    @JsonProperty("stateDocInfo")
    public void setStateDocInfo(StateDocInfoES stateDocInfo) {
        this.stateDocInfo = stateDocInfo;
    }

    @JsonProperty("activityInfo")
    public ActivityInfoES getActivityInfo() {
        return activityInfo;
    }

    @JsonProperty("activityInfo")
    public void setActivityInfo(ActivityInfoES activityInfo) {
        this.activityInfo = activityInfo;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("lastUpdated")
    public String getLastUpdated() {
        return lastUpdated;
    }

    @JsonProperty("lastUpdated")
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
