package com.igtb.ingestion.payment.instruction.metadata.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class RecordRateAmount.
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecordRateAmount {

	/** The value. */
	private Double value;
	
	/** The currency code. */
	private String currencyCode;
}
