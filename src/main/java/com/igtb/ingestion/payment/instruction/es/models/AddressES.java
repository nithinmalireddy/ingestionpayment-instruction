package com.igtb.ingestion.payment.instruction.es.models;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class AddressES.
 */
@Getter
@Setter
public class AddressES {
	
	/** The address 1. */
	private String address1;
	
	/** The address 2. */
	private String address2;
	
	/** The address 3. */
	private String address3;
	
	/** The city. */
	private CityES city;
	
	/** The country. */
	private CountryES country;
	

}
