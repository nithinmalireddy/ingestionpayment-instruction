package com.igtb.ingestion.payment.instruction.exception;

/**
 * POJO for  Transform failure custom exception.
 *
 */
public class MandatoryFieldMissingException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8174719950826989687L;
	
	/** message of exception. */
	private final String message;
	
	/** Code of exception. */
	private final String code;
	
	/**
	 * Parameterized Constructors.
	 *
	 * @param message the message
	 */
	public MandatoryFieldMissingException (final String message){
		super(message);
		this.code="";
		this.message = message;
	}

	/**
	 * Parameterized constructor.
	 *
	 * @param code the code
	 * @param message the message
	 */
	public MandatoryFieldMissingException (final String code, final String message){
		super(code + " : " + message);
		this.code = code;
		this.message = message;
	}

	/**
	 * Getter method for message.
	 *
	 * @return the message
	 */
	@Override
	public String getMessage() {
		return message;
	}

	/**
	 * Getter method for cause .
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * override toString() method of object class.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return " MandatoryFieldMissingException [message=" + message + ", code=" + code
				+ "]";
	}

}
