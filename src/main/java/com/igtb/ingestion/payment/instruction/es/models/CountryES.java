package com.igtb.ingestion.payment.instruction.es.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * CountryES class.
 * 
 */
@ToString
//@JsonInclude(content = Include.NON_NULL, value = Include.NON_NULL)
@Getter
@Setter
public class CountryES {
	
	/** The account country code.(Represents iso-char 3 code) */
	@JsonProperty(value = "code")
	private String iso3CharCode;
	
	/** The account country name. */
	private String name;
	
	/** The account country code.(Represents iso-char 2 code) */
	@JsonProperty(value = "iso_2char_code")
	private String iso2CharCode;
		
}
