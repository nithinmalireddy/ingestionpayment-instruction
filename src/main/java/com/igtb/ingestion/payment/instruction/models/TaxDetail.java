
package com.igtb.ingestion.payment.instruction.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class TaxDetail.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

/**
 * Gets the tax amount.
 *
 * @return the tax amount
 */
@Getter

/**
 * Sets the tax amount.
 *
 * @param taxAmount the new tax amount
 */
@Setter
public class TaxDetail {

    /** The wht income type code. */
    @JsonProperty("whtIncomeTypeCode")
    public String whtIncomeTypeCode;
    
    /** The income type description. */
    @JsonProperty("incomeTypeDescription")
    public String incomeTypeDescription;
    
    /** The wht deduction date. */
    @JsonProperty("whtDeductionDate")
    public String whtDeductionDate;
    
    /** The amounttobe taxed. */
    @JsonProperty("amounttobeTaxed")
    public String amounttobeTaxed;
    
    /** The taxable currency. */
    @JsonProperty("taxableCurrency")
    public String taxableCurrency;
    
    /** The tax rate. */
    @JsonProperty("taxRate")
    public String taxRate;
    
    /** The tax amount. */
    @JsonProperty("taxAmount")
    public String taxAmount;

}
