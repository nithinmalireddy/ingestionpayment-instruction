package com.igtb.ingestion.payment.instruction; 

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelExecutionException;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.test.context.TestContextManager;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.igtb.api.action.commons.json.JSONHandler;
import com.igtb.ingestion.payment.instruction.config.properties.PaymentElasticProperties;
import com.igtb.ingestion.payment.instruction.consumer.PaymentKafkaConsumer;
import com.igtb.ingestion.payment.instruction.metadata.models.MetadataFile;
import com.igtb.ingestion.payment.instruction.util.IgtbBlobStorUtil;
import com.igtb.ingestion.payment.instruction.util.PaymentIngestionQueryUtil;

import io.searchbox.client.JestResult;

/**
 * The Class IngestionPaymentInstructionAppTest.
 */
@SpringBootTest(classes = IngestionPaymentInstructionApp.class)
public class IngestionPaymentInstructionAppTest {

	/** The Constant TEST_DOMAIN_SEQ_ID. */
	private static final String TEST_DOMAIN_SEQ_ID = "3811017-test";

	/** The Constant TEST_CHANNEL_SEQ_ID. */
	private static final String TEST_CHANNEL_SEQ_ID = "36570106517-test";

	/** The Constant BACKEND. */
	private static final String BACKEND = "backend";

	/** The Constant TEST_FILEID. */
	private static final String TEST_FILEID = "PAIN001V3-1556218018800-test";

	/** The Constant SEARCH_ID. */
	private static final String SEARCH_ID = "_id";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(IngestionPaymentInstructionAppTest.class);
			
	/** Camel Context. */
	@Autowired
	private CamelContext camelContext;
	
	/** The producer. */
	@Autowired
    private ProducerTemplate producerTemplate;

    /** The Constant MOCK_DUPE_CHECK. */
    public static final String MOCK_PI_SET_ROUTE = "direct:mock-piSet";
    
    /** The Constant MOCK_PI_ROUTE. */
    public static final String MOCK_PI_ROUTE = "direct:mock-pi";
    
	/** The payment ingestion query util. */
	@Autowired
	private PaymentIngestionQueryUtil paymentIngestionQueryUtil;
	
	/** The payment elastic properties. */
	@Autowired
	private PaymentElasticProperties paymentElasticProperties;
	
	/** The web application context. */
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	/** The igtb blob stor util. */
	@MockBean
	private IgtbBlobStorUtil igtbBlobStorUtil;
	
	/** The payment kafka consumer. */
	@Autowired
	private PaymentKafkaConsumer paymentKafkaConsumer;
	
	/** The acknowledgment. */
	//@Autowired
	private final Acknowledgment acknowledgment = new Acknowledgment() {
		
		@Override
		public void acknowledge() {
			// TODO Auto-generated method stub
			
		}
	};
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		new TestContextManager(getClass()).prepareTestInstance(this);
		MockitoAnnotations.initMocks(this);
		MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
	}
	
	/**
	 * Test actual and expected es object after event processed.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void test1RequestTypeFileUploadStateEventsPositive() throws Exception {
		
		camelContext.getRouteDefinition("TRANSITION_EVENTS_FROM_RMQ_PISet")
		 	.autoStartup(true).adviceWith(camelContext, new AdviceWithRouteBuilder() {
			/** Configure camel route */
			@Override
			public void configure() throws Exception {
				replaceFromWith(MOCK_PI_SET_ROUTE);
			}
		});

		/** Path to request event. */
		final String pathToRequestEvent = "test_data/file_upload/events/";
		
		/** Path to expected es record after processing event. */
		final String pathToExpectedEsRecord = "test_data/file_upload/expectedEsRecords/";
		
		//Mocking metadata file
		final MetadataFile metadataFileObj = new ObjectMapper().readValue(IngestionPaymentInstructionAppTest.class.getClassLoader()  
				.getResourceAsStream("test_data/file_upload/metadataFiles/metadataFile.json"),
				MetadataFile.class);
		when(igtbBlobStorUtil.readMetadataFile(any())).thenReturn(metadataFileObj);
		
		//build map 
		final LinkedHashMap<String, String> hmEventAndExpectedResponse = new LinkedHashMap<>(); 
		hmEventAndExpectedResponse.put("file_available.json", "file_availableEsRecord.json");
		hmEventAndExpectedResponse.put("metadata_available.json", "afterMetadata_availableEvent/PiSetFileEsRecord.json");
		hmEventAndExpectedResponse.put("draft.json", "draftEsRecord.json");
		hmEventAndExpectedResponse.put("val_success.json", "val_successEsRecord.json");
		hmEventAndExpectedResponse.put("verifyWF.json", "verifyWfEsRecord.json");
		hmEventAndExpectedResponse.put("approvedWF.json", "approvedWfEsRecord.json");
		hmEventAndExpectedResponse.put("released.json", "releasedEsRecord.json");
		hmEventAndExpectedResponse.put("backend_ack_success.json", "backend_ack_success_fileEsRecord.json");
		hmEventAndExpectedResponse.put("backend_created.json", "backend_created_PI_TxnActiveEsRecord.json");
		hmEventAndExpectedResponse.put("backend_updated_per_txn.json", "backend_updated_PI_TxnActiveEsRecord.json");
		hmEventAndExpectedResponse.put("backend_processed.json", "afterBackend_processedEvent/PISetFileEsRecord.json");
		
		//clear existing record
		clearPiSet();
		
		for (final String eventFileName : hmEventAndExpectedResponse.keySet()) {
			LOGGER.debug("Test: Processing ==> {}", eventFileName);
			
			String inputEventNode = JSONHandler.getObjectMapper()
					.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader()  
					.getResourceAsStream(pathToRequestEvent+eventFileName)).toString();
			inputEventNode = inputEventNode.replace("#currentEventTime", getCurrentISODateTime());
			
			try {
				if(eventFileName.startsWith(BACKEND)) {
					paymentKafkaConsumer.onMessage(new ConsumerRecord<String, String>("mockTopic", 0, 0L, "value", inputEventNode), acknowledgment);
				} else {
					producerTemplate.sendBody(MOCK_PI_SET_ROUTE, ExchangePattern.InOnly, inputEventNode);
				}
			} catch (CamelExecutionException e) {
				Thread.sleep(50000);
				LOGGER.debug("Test: Re-processing ==> {}", eventFileName);
				if(eventFileName.startsWith(BACKEND)) {
					paymentKafkaConsumer.onMessage(new ConsumerRecord<String, String>("mockTopic", 0, 0L, "value", inputEventNode), acknowledgment);
				} else {
					producerTemplate.sendBody(MOCK_PI_SET_ROUTE, ExchangePattern.InOnly, inputEventNode);
				}
			}
			LOGGER.debug("Test: producerTemplate.sendBody done for ==> {}", eventFileName);
			Thread.sleep(20000);
			
			if(Arrays.asList("backend_created.json", "backend_updated_per_txn.json").contains(eventFileName)) {
				verifyActivePI(pathToExpectedEsRecord, hmEventAndExpectedResponse.get(eventFileName), eventFileName);
			} else if("backend_processed.json".equals(eventFileName)) {
				//verify PISet File
				verifyPISetFile(pathToExpectedEsRecord, hmEventAndExpectedResponse, eventFileName);
				
				//verify PI 
				verifyPI(pathToExpectedEsRecord+"afterBackend_processedEvent/PiTxnEsRecord.json", eventFileName);
			} else {
				//verify PISet File
				verifyPISetFile(pathToExpectedEsRecord, hmEventAndExpectedResponse, eventFileName);
			}
			
			if("metadata_available.json".equals(eventFileName)) {
				verifyPiSetBulkAndPI(pathToExpectedEsRecord, eventFileName);
			}
			
			LOGGER.debug("Test: Processing done ==> {}", eventFileName);
		}
		
		//clear record after processing 
		clearPiSet();
	}

	/**
	 * Test 2 request type add for single pi state events positive.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void test2RequestTypeAddForSinglePiStateEventsPositive() throws Exception {
		camelContext.getRouteDefinition("TRANSITION_EVENTS_FROM_RMQ_PI").autoStartup(true).adviceWith(camelContext,
				new AdviceWithRouteBuilder() {
					/** Configure camel route */
					@Override
					public void configure() throws Exception {
						replaceFromWith(MOCK_PI_ROUTE);
					}
				});
			
		/** Path to request event. */
		final String pathToRequestEvent = "test_data/add_PI/events/";

		/** Path to expected es record after processing event. */
		final String pathToExpectedEsRecord = "test_data/add_PI/expectedEsRecords/";

		// build map 
		final LinkedHashMap<String, String> hmEventAndExpectedResponse = new LinkedHashMap<>(); 
		hmEventAndExpectedResponse.put("val_success", "afterVal_successEvent/val_success_es.json");
		hmEventAndExpectedResponse.put("approved", "approved_es.json");
		hmEventAndExpectedResponse.put("released", "released_es.json");
		hmEventAndExpectedResponse.put("backend_ack_success", "backend_ack_transition_es.json");
		hmEventAndExpectedResponse.put("backend_created", "backend_created_active_es.json");
		hmEventAndExpectedResponse.put("backend_updated", "backend_updated_active_es.json");
		hmEventAndExpectedResponse.put("backend_processed", "backend_processed_transition_es.json");
		
		final LinkedHashMap<String, String> hmEventAndExpectedTxnHistoryResponse = new LinkedHashMap<>();
		hmEventAndExpectedTxnHistoryResponse.put("val_success", "transitionHistoryEsRecords/val_successTH.json");
		hmEventAndExpectedTxnHistoryResponse.put("approved", "transitionHistoryEsRecords/approvedTH.json");
		hmEventAndExpectedTxnHistoryResponse.put("released", "transitionHistoryEsRecords/releasedTH.json");
		hmEventAndExpectedTxnHistoryResponse.put("backend_processed", "transitionHistoryEsRecords/backend_processedTH.json");

		// clear existing record
		clearSinglePI();

		for (final String eventFileName : hmEventAndExpectedResponse.keySet()) {
			LOGGER.debug("Test: Processing single PI ==> {}", eventFileName);

			String inputEventNode = JSONHandler.getObjectMapper().readTree(IngestionPaymentInstructionAppTest.class
					.getClassLoader().getResourceAsStream(pathToRequestEvent + eventFileName +".json")).toString(); 
			inputEventNode = inputEventNode.replace("#currentEventTime", getCurrentISODateTime());

			try {
				if(eventFileName.startsWith(BACKEND)) {
					paymentKafkaConsumer.onMessage(new ConsumerRecord<String, String>("mockTopic", 0, 0L, "value", inputEventNode), acknowledgment);
				} else {
					producerTemplate.sendBody(MOCK_PI_ROUTE, ExchangePattern.InOnly, inputEventNode);
				}
			} catch (CamelExecutionException e) {
				Thread.sleep(50000);
				LOGGER.debug("Test: Re-processing single PI ==> {}", eventFileName);
				if(eventFileName.startsWith(BACKEND)) {
					paymentKafkaConsumer.onMessage(new ConsumerRecord<String, String>("mockTopic", 0, 0L, "value", inputEventNode), acknowledgment);
				} else {
					producerTemplate.sendBody(MOCK_PI_ROUTE, ExchangePattern.InOnly, inputEventNode);
				}
			}
			
			LOGGER.debug("Test: producerTemplate.sendBody done for ==> {}", eventFileName);
			final Map<String, String> matchStrings = new HashMap<>();
			
			if(Arrays.asList("backend_created", "backend_updated").contains(eventFileName)) {
				//fetch active record
				matchStrings.put(SEARCH_ID, TEST_DOMAIN_SEQ_ID);
			} else {
				//fetch transition record
				matchStrings.put(SEARCH_ID, TEST_CHANNEL_SEQ_ID);
			}
			final String[] exculdeStrings = { "stateInfo.transitionInfo.lastActivityTs", "stateInfo.initiated.ts", 
					"stateInfo.lastAction.ts" }; 

			Thread.sleep(10000);
			final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(
					matchStrings, exculdeStrings, paymentElasticProperties.getPymtIndex(),
					paymentElasticProperties.getPymtType());

			final String expectedEsRecord = JSONHandler.getObjectMapper()
					.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader().getResourceAsStream(  
							pathToExpectedEsRecord + hmEventAndExpectedResponse.get(eventFileName)))
					.toString();

			JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
			LOGGER.debug("Test: Verified single PI ==> {}", eventFileName);
			
			//verify Transition History
			if(hmEventAndExpectedTxnHistoryResponse.get(eventFileName) != null) {
				verifyTransitionHistory(pathToExpectedEsRecord, hmEventAndExpectedTxnHistoryResponse, eventFileName);
			}			
			//verify supporting doc and WHT doc at val_success 
			if("val_success".equals(eventFileName)) {
				verifySuppDocAndWhtDoc(pathToExpectedEsRecord);
			}

			LOGGER.debug("Test: Processing done single PI ==> {}", eventFileName);
		}

		// clear record after processing
		clearSinglePI();

	}

	/**
	 * Verify active PI.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @param expectedEsFilePath the expected es file path
	 * @param eventFileName the event file name
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void verifyActivePI(final String pathToExpectedEsRecord, final String expectedEsFilePath,
			final String eventFileName) throws JsonProcessingException, IOException {
		LOGGER.debug("Test: Verifying active PI ==> {}", eventFileName);
		final Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put(SEARCH_ID, "intTxn111");
		final String[] exculdeStringsPI = { "created", "lastModified", "stateInfo.transitionInfo.lastActivityTs",   
				"stateInfo.initiated.ts", "stateInfo.lastAction.ts", "userDateTime", "releaseDate",  
				"requestedExecutionDate", "setElementInfo.id", "setElementInfo.name" };  
		
		final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings, exculdeStringsPI, 
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
		
		final String expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader()
				.getResourceAsStream(pathToExpectedEsRecord+expectedEsFilePath)).toString();
		
		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified active PI ==> {}", eventFileName);
		
	}

	/**
	 * Verify PI set file.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @param hmEventAndExpectedResponse the hm event and expected response
	 * @param eventFileName the event file name
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException the json processing exception
	 */
	private void verifyPISetFile(final String pathToExpectedEsRecord,
			final LinkedHashMap<String, String> hmEventAndExpectedResponse, final String eventFileName) 
			throws InterruptedException, IOException, JsonProcessingException {
		LOGGER.debug("Test: Verifying PI Set File ==> {}", eventFileName);
		final Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put(SEARCH_ID, TEST_FILEID);
		final String[] exculdeStrings = { "created", "lastModified", "stateInfo.transitionInfo.lastActivityTs", "stateInfo.initiated.ts",
			      "stateInfo.lastAction.ts" };
		
		final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings, exculdeStrings, 
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
		
		final String expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader()
				.getResourceAsStream(pathToExpectedEsRecord+hmEventAndExpectedResponse.get(eventFileName))).toString();
		
		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified PI Set File ==> {}", eventFileName);
	}


	/**
	 * Verify transition history.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @param hmEventAndExpectedTxnHistoryResponse the hm event and expected txn history response
	 * @param eventFileName the event file name
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException the json processing exception
	 */
	private void verifyTransitionHistory(final String pathToExpectedEsRecord,
			final LinkedHashMap<String, String> hmEventAndExpectedTxnHistoryResponse, final String eventFileName)
			throws IOException, JsonProcessingException {
		Map<String, String> matchStrings;
		JestResult jestResult;
		LOGGER.debug("Test: Verifying Transition History");
		matchStrings = new HashMap<>();
		matchStrings.put("channelSeqId", TEST_CHANNEL_SEQ_ID);
		matchStrings.put("requestInfo.status", eventFileName);
		final String[] exculdeString = { "ts" };

		jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings,
				exculdeString, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPITransitionHistory());

		final String expectedThEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader().getResourceAsStream( 
						pathToExpectedEsRecord + hmEventAndExpectedTxnHistoryResponse.get(eventFileName)))
				.toString();

		JSONAssert.assertEquals(expectedThEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified Transition History");
	}
	
	/**
	 * Verify supp doc and wht doc.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void verifySuppDocAndWhtDoc(final String pathToExpectedEsRecord) throws JsonProcessingException, IOException {
		// verify Supporting document
		LOGGER.debug("Test: Verifying Supporting document");
		Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("paymentInstructionId", TEST_CHANNEL_SEQ_ID);
		final String[] exculdeStrings = { "uploadDateTime" };

		JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings,
				exculdeStrings, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getSupportingDocumentsType());

		String expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader().getResourceAsStream(
						pathToExpectedEsRecord + "afterVal_successEvent/supportingDoc.json"))
				.toString();

		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified Supporting document");
		
		// verify WHT doc
		LOGGER.debug("Test: Verifying WHT document");
		matchStrings = new HashMap<>();
		matchStrings.put("parentInstructionId", TEST_CHANNEL_SEQ_ID);

		jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings,
				null, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPaymentTaxInfoType());

		expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader().getResourceAsStream(
						pathToExpectedEsRecord + "afterVal_successEvent/WhtDoc.json")).toString();

		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified WHT document");
	}

	/**
	 * Gets the current ISO date time.
	 *
	 * @return the current ISO date time
	 */
	private String getCurrentISODateTime() {
		final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'");
		return DF.format(new Date());
	}
	
	/**
	 * Verify pi set bulk and PI.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @param eventFileName the event file name
	 * @throws JsonProcessingException the json processing exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void verifyPiSetBulkAndPI(final String pathToExpectedEsRecord, final String eventFileName) throws JsonProcessingException, IOException {
		//verify PiSet bulk
		LOGGER.debug("Test: Verifying PiSet bulk ==> {}", eventFileName);
		final Map<String, String> matchStrings = new HashMap<>();
		matchStrings.put("parentSet.id", TEST_FILEID);
		final String[] exculdeStrings = { "recordSetId", "setName", "created", "lastModified", "stateInfo.transitionInfo.lastActivityTs", "stateInfo.initiated.ts",
			      "stateInfo.lastAction.ts" };
		
		final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings, exculdeStrings, 
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
		
		final String expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader()
				.getResourceAsStream(pathToExpectedEsRecord+"afterMetadata_availableEvent/PiSetBulkEsRecord.json")).toString();
		
		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified PiSet bulk ==> {}", eventFileName);
		
		verifyPI(pathToExpectedEsRecord+"afterMetadata_availableEvent/PiTxnEsRecord.json", eventFileName);
	}

	/**
	 * Verify PI.
	 *
	 * @param pathToExpectedEsRecord the path to expected es record
	 * @param eventFileName the event file name
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException the json processing exception
	 */
	private void verifyPI(final String pathToExpectedEsRecord, final String eventFileName) throws IOException, JsonProcessingException {
		Map<String, String> matchStrings;
		//verify Payment instruction [transaction level data]
		LOGGER.debug("Test: Verifying Payment instruction ==> {}", eventFileName);
		matchStrings = new HashMap<>();
		matchStrings.put(SEARCH_ID, "PAIN001V3-1556218018800-test.intTxn111");
		final String[] exculdeStringsPI = { "created", "lastModified", "stateInfo.transitionInfo.lastActivityTs",
				"stateInfo.initiated.ts", "stateInfo.lastAction.ts", "userDateTime", "releaseDate",
				"requestedExecutionDate", "setElementInfo.id", "setElementInfo.name" };
		
		final JestResult jestResult = paymentIngestionQueryUtil.exculdeSelectiveDataByPassingMatchingString(matchStrings, exculdeStringsPI, 
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
		
		final String expectedEsRecord = JSONHandler.getObjectMapper()
				.readTree(IngestionPaymentInstructionAppTest.class.getClassLoader()
				.getResourceAsStream(pathToExpectedEsRecord)).toString();
		
		JSONAssert.assertEquals(expectedEsRecord, jestResult.getSourceAsString(), JSONCompareMode.STRICT);
		LOGGER.debug("Test: Verified Payment instruction ==> {}", eventFileName);
	}

	/**
	 * Clear single PI.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void clearSinglePI() throws IOException {
		
		// Delete the existing record of supportingDocument.
		SearchSourceBuilder searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("paymentInstructionId",
				TEST_CHANNEL_SEQ_ID);
		JestResult jestResult = paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getSupportingDocumentsType());
		LOGGER.debug("Test: PI supportingDocument isDeleted ==> {}", jestResult.isSucceeded());

		// Delete the existing record of paymentTaxInfo.
		searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("parentInstructionId", TEST_CHANNEL_SEQ_ID);
		jestResult = paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
				paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentTaxInfoType());
		LOGGER.debug("Test: PI paymentTaxInfo isDeleted ==> {}", jestResult.isSucceeded());

		// Delete the existing record of paymentInstruction
		searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("stateInfo" + ".channelSeqId", TEST_CHANNEL_SEQ_ID);
		jestResult = paymentIngestionQueryUtil.deleteByQuery(searchBuilder, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPymtType());
		LOGGER.debug("Test: PI isDeleted ==> {}", jestResult.isSucceeded());
		
		// Delete the existing record of active paymentInstruction
		searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("stateInfo" + ".domainSeqId", TEST_DOMAIN_SEQ_ID);
		jestResult = paymentIngestionQueryUtil.deleteByQuery(searchBuilder, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPymtType());
		LOGGER.debug("Test: PI isDeleted ==> {}", jestResult.isSucceeded());

		// Delete the Transition History Documents 
		searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("channelSeqId", TEST_CHANNEL_SEQ_ID);
		jestResult = paymentIngestionQueryUtil.deleteByQuery(searchBuilder, paymentElasticProperties.getPymtIndex(),
				paymentElasticProperties.getPITransitionHistory());
		LOGGER.debug("Test: PI Transition history documents isDeleted ==> {}", jestResult.isSucceeded());
	}

	/**
	 * Clear pi set.
	 */
	private void clearPiSet() {
		try {
			//delete PiSet file
			final JestResult jestResult = paymentIngestionQueryUtil.paymentDeleteAction(TEST_FILEID,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
			LOGGER.debug("Test: PiSet file isDeleted ==> {}", jestResult.isSucceeded());
			
			//delete PiSet  bulk
			SearchSourceBuilder searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("parentSet.id",
					TEST_FILEID);
			paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPaymentInstructionSets());
			LOGGER.debug("Test: PiSet bulk isDeleted ==> {}", jestResult.isSucceeded());
			
			//delete PI transaction 
			searchBuilder = paymentIngestionQueryUtil.constructSelectQuery("setInfo.id",
					TEST_FILEID);
			paymentIngestionQueryUtil.deleteByQuery(searchBuilder,
					paymentElasticProperties.getPymtIndex(), paymentElasticProperties.getPymtType());
			LOGGER.debug("Test: PI transaction isDeleted ==> {}", jestResult.isSucceeded());
			
		} catch (IOException e) {
			LOGGER.warn("Test: error while deleting as: ", e);
		}
		
	}
}
