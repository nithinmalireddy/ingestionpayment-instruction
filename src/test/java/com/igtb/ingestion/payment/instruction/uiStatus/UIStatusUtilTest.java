/**
 *    These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of these
 * materials should be reproduced, published, transmitted or distributed in any form or by any means,
 * electronic, mechanical, photocopying, recording or otherwise, or stored in any information storage or
 * retrieval system of any nature nor should the materials be disclosed to third parties or used in any
 * other manner for which this is not authorized, without the prior express written authorization of
 * Intellect Design Arena Ltd.
 *
 * Copyright 2017 Intellect Design Arena Limited. All rights reserved.
 *
 *
 */
package com.igtb.ingestion.payment.instruction.uiStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.OutcomeCategoryEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.RequestEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.ResourceEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStateEnum;
import com.igtb.ingestion.payment.instruction.enums.IngestionEnumTypes.TransitionStatusEnum;
import com.igtb.ingestion.payment.instruction.util.UIStatusUtil.UIStatusEnum;

/**
 * Test UI Status Utility Class.
 * 
 * 
 * @author vaishali.gupta
 * @since 11-Mar-2019/9:31:07 PM
 *
 */
@SpringBootTest
public class UIStatusUtilTest {
	
	
	/** 
	 * Method to test the logic for UI Status.(in all scenarios)
	 * 
	 */
	@Test
	public void testForValidUIStatusFields() throws Exception {
		
		// DRAFT
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.draft, null, 
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("DRAFT"));
		
		//-------------------------------------------------------------------------------------
		// REJECTED
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.val_failure,
				null,ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("REJECTED"));
		assertThat(UIStatusEnum.getUIStatus(null, TransitionStateEnum.rejected, null, null, null,null), is("REJECTED"));
		assertThat(UIStatusEnum.getUIStatus(null, TransitionStateEnum.cancelled, null, null, null,null), is("REJECTED"));
		assertThat(UIStatusEnum.getUIStatus(null, TransitionStateEnum.release_rejected, null, null, null, null), is("REJECTED"));
		assertThat(UIStatusEnum.getUIStatus(null, TransitionStateEnum.release_failure, null, null, null, null), is("REJECTED"));
		assertThat(UIStatusEnum.getUIStatus(null, TransitionStateEnum.backend_rejected, null, null, null, null), is("REJECTED"));
		
		//-------------------------------------------------------------------------------------
		// PENDING APPROVAL(for entry channel
		// states, till here there is no nextAction)
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.val_success,
				null, ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("INITIATED"));
		
		
		// in-progress states with nextAction as 'authorize'----<>> PENDING APPROVAL
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.approved, "authorize",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("PENDING APPROVAL"));
		// in-progress states with nextAction as 'verify' ----<>> PENDING VERIFICATION
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, null, "verify",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.hold), is("PENDING VERIFICATION"));
		
		
		//-------------------------------------------------------------------------------------
		// SENT TO BANK, ACTIVE if nextAction is 'release-to-bank'
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, null, "release-to-bank",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_WORKFLOW_UPDATE, null), is("SENT TO BANK"));
		
		
		// No update, if no nextAction
		assertNull(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.released, "",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add));
		assertNull(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.released, "",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.hold));
		
		
		//no status update for 'released' from workflow if no 'nextAction' is provided
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.released,
				"authorize", ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_WORKFLOW_UPDATE, null), is("PENDING APPROVAL"));
		
		//SENT TO BANK, ACTIVE if nextAction is 'bank-to-confirm', 'bank-to-ack'
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.released, "bank-to-confirm",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("SENT TO BANK"));
		
		//-------------------------------------------------------------------------------------
		// DONE for backend_ack_success(either PI or SI), no status update, retain previous status
		assertNull(UIStatusEnum.getUIStatus(TransitionStatusEnum.IN_PROGRESS, TransitionStateEnum.backend_ack_success, "",
				ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add));
		
		
		//-------------------------------------------------------------------------------------
		// DONE for backend_rejected(either PI or SI), will got in 'REJECTED' tab
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.backend_rejected, "",
						ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add), is("REJECTED"));
		
		
//		//-------------------------------------------------------------------------------------
		// DONE for backend_processed(for add), will got in
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.backend_processed, "",
						ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.add) , is("POSTED"));
		
		// for 'hold' request
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.backend_processed, "",
						ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.hold), is("ON HOLD"));
		
		// for 'unhold' request
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.backend_processed, "",
						ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.unhold), is("SENT TO BANK"));
		
		// for 'delete' request
		assertThat(UIStatusEnum.getUIStatus(TransitionStatusEnum.COMPLETED, TransitionStateEnum.backend_processed, "",
						ResourceEnum.paymentInstructions, OutcomeCategoryEnum.CHANNEL_STATE_UPDATE, RequestEnum.delete) , is("CANCELLED"));

	}
	
}
